#include "Calculator.h"
#include "Calci_Error.h"
#include "DB_Manager.h"
#include "Log_Handle.h"

double accum, s_reg, pnt1 = 1;
double mem;
Grand_Total grand_total;
uint8_t a_flag, e_flag, p_flag, m_flag, mrc_flag, gst_flag, gt_flag, c_flag, mu_flag, s_flag = 0, err_flag = 0;
uint8_t once_equal, once_tax;
uint8_t once_cash = 0;
uint8_t gt_pointor = 0;
double gst_rate[5] = {0.0, 5.0, 12.0, 18.0, 28.0};
PAYMENT_MODE payment_mode_status = PAYMENT_MODE_CASH;
TRANS_TYPE selected_trans_type = TRANS_TYPE_NONE;
static void invalid_cash_value_timeout_callback(void *arg);
uint8_t invalid_cash_value = 0;
const esp_timer_create_args_t invalid_cash_value_timer_args = {.callback = &invalid_cash_value_timeout_callback,
                                                               .name = "cash_value_invalid_timeout"};
esp_timer_handle_t invalid_cash_value_timer;

uint8_t screen_no = 0;

int save_transaction(Trans_Data_Sqlite_Struc *data_str, uint8_t sync_sts)
{
    char *temp_query = (char *)malloc(300 * sizeof(char));
    if (temp_query != NULL)
    {
        if (data_str->trans_amount != 0 && data_str->trans_type != 0 && data_str->payment_mode != 0)
        {
            int rc = 0;
            sprintf(temp_query, "INSERT INTO %s (amount, transactionType, paymentMode, transactionTimeStamp, isSynced, customerName, customerMobile, customerRemarks, cashbookUserId)VALUES(%lf, '%c', '%c', %lld, %d, '%s',  '%s', '%s', %lld)",
                    TABLE_NAME, data_str->trans_amount, data_str->trans_type, data_str->payment_mode, data_str->trans_time, sync_sts, data_str->trans_cust_name, data_str->trans_cust_num, data_str->trans_cust_remark, data_str->cashbook_user_id);

            xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
            rc = db_exec(db, OTHER, temp_query);
            xSemaphoreGive(sqlite_mutex);
            if (rc != SQLITE_OK)
            {
                ESP_LOGE(CALC_TAG, "RC: %d\n", rc);
                remove(DATA_BASE);
                FILE *fp;
                fp = fopen(DATA_BASE, "w");
                fclose(fp);
                if (openDb(DATA_BASE, &db))
                {
                    ESP_LOGE(CALC_TAG, "Opening database failed");
                }
                xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
                rc = db_exec(db, OTHER, CREATE_TABLE);
                xSemaphoreGive(sqlite_mutex);
                if (rc != SQLITE_OK)
                {
                    ESP_LOGE(SYNC, "RC: %d\n", rc);
                }
                free(temp_query);
                return -1;
            }
            free(temp_query);
            return rc;
        }
        else
        {
            free(temp_query);
            return -1;
        }
    }
    else
    {
        ESP_LOGE(CALC_TAG, "Insert query memory allocation failed");
        return -1;
    }
}

int save_transaction(double cash_amount, TRANS_TYPE trans_type, PAYMENT_MODE payment_mode, const char *cust_name, const char *cust_num, const char *cust_remark)
{
    int64_t cashbook_id = 0;
    FILE *fid = fopen(CASHBOOK_ID, "r");
    if (fid == NULL)
    {
        ESP_LOGW(CALC_TAG, "Failed to open Cashbook ID file");
    }
    else
    {
        fread(&cashbook_id, sizeof(int64_t), 1, fid);
        fclose(fid);
    }
    time_t seconds;
    seconds = time(NULL);
    Trans_Data_Sqlite_Struc syncing_data = {};
    syncing_data.cashbook_user_id = cashbook_id;
    syncing_data.payment_mode = (char)payment_mode;
    syncing_data.trans_amount = cash_amount;
    memcpy(syncing_data.trans_cust_name, cust_name, strlen(cust_name) + 1);
    memcpy(syncing_data.trans_cust_num, cust_num, strlen(cust_num) + 1);
    memcpy(syncing_data.trans_cust_remark, cust_remark, strlen(cust_remark) + 1);
    syncing_data.trans_time = seconds;
    syncing_data.trans_type = (char)trans_type;
    int db_save_rsp = save_transaction(&syncing_data, 0);
    if (db_save_rsp == 0)
    {
        print_trans_saved_from_main(true);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        remove_trans_saved_from_main(true);

        total_pending_trans++;
        display_trans_count(true, total_pending_trans);
        syncing_data = {0};
        syncing_data.trans_type = 'I';
        xQueueSend(periodic_sync_data, (void *)&syncing_data, 0);
        if (get_wifi_status())
        {
            if (cashbook_id == 0)
            {
                return error_mode_entry_action(ERROR_LOGIN);
            }
        }
        else
        {
            return error_mode_entry_action(ERROR_WIFI);
        }
    }
    return 1;
}

int get_digit(double input)
{
    int64_t temp = input;
    int x = 0;
    while (temp)
    {
        temp /= 10;
        x++;
    }
    return x;
}
std::string result_to_string(double result, int prec)
{
    std::stringstream stream;
    std::string str = "";

    stream << std::fixed;
    stream << std::setprecision(prec);
    stream << result;
    str = stream.str();
    std::size_t found = str.find('.');
    if (found != std::string::npos)
    {
        char last_digit = str.at(str.length() - 1);
        while (last_digit == '0' || last_digit == '.')
        {
            str.pop_back();
            if (last_digit == '.')
                break;
            last_digit = str.at(str.length() - 1);
        }
    }
    return str;
}
double addition(double op1, double op2)
{
    return op1 + op2;
}
double substraction(double op1, double op2)
{
    return op1 - op2;
}
double multiplication(double op1, double op2)
{
    return op1 * op2;
}
double division(double op1, double op2)
{
    return op1 / op2;
}
double addition_percen(double op1, double op2)
{
    return op1 + ((op1 * op2) / 100);
}
double substraction_percen(double op1, double op2)
{
    return op1 - ((op1 * op2) / 100);
}
double multiplication_percen(double op1, double op2)
{
    return op1 * (op2 / 100);
}
double division_percen(double op1, double op2)
{
    return (100 * op1) / op2;
}
double mu_percen(double op1, double op2)
{
    return (100 * op1) / (100 - op2);
}
void add_to_operand(char key)
{
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        if (screen_no == 0)
            screen_no = 1;
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
        {
            accum = (accum * 10) + x; // add it in result
            if (accum > 999999999999)
                accum = (accum - x) / 10;
            print_operand(true, accum, 0);
        }
        else
        {
            accum = accum + (x * pnt1); // add it in result
            double temp_prec = 1 / pnt1;
            int temp2 = 0;
            while (temp_prec > 1)
            {
                temp_prec = temp_prec / 10;
                temp2++;
            }
            if ((int)accum > (pow(10, (11 - temp2))) - 1)
            {
                accum = accum - (x * pnt1);
                pnt1 *= 10;
                temp2--;
            }
            if (temp2 > 5)
            {
                accum = accum - (x * pnt1);
                pnt1 *= 10;
                temp2--;
            }
            print_operand(true, accum, temp2);
        }
        print_screen_no(true, screen_no);
    }
    else if (a_flag)
    {
        s_flag = 1;
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
        {
            s_reg = (s_reg * 10) + x; // add it in result
            if (s_reg > 999999999999)
                s_reg = (s_reg - x) / 10;
            print_operand(true, s_reg, 0);
        }
        else
        {
            s_reg = s_reg + (x * pnt1); // add it in result
            double temp_prec = 1 / pnt1;
            int temp2 = 0;
            while (temp_prec >= 10)
            {
                temp_prec = temp_prec / 10;
                temp2++;
            }
            if ((int)s_reg > (pow(10, (11 - temp2))) - 1)
            {
                s_reg = s_reg - (x * pnt1);
                pnt1 *= 10;
                temp2--;
            }
            if (temp2 > 5)
            {
                s_reg = s_reg - (x * pnt1);
                pnt1 *= 10;
                temp2--;
            }
            print_operand(true, s_reg, temp2);
        }
        print_screen_no(true, screen_no);
    }
    else if (e_flag)
    {
        screen_no++;
        accum = 0;
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
            accum = (accum * 10) + x; // add it in result
        else
            accum = accum + (x * pnt1); // add it in result
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        remove_equal_perc_sign(true);
        clear_last_calculation(true);
        e_flag = 0;
    }
    else if (p_flag)
    {
        screen_no++;
        accum = 0;
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
            accum = (accum * 10) + x; // add it in result
        else
            accum = accum + (x * pnt1); // add it in result
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        remove_equal_perc_sign(true);
        clear_last_calculation(true);
        p_flag = 0;
    }
    else if (m_flag)
    {
        if (m_flag == '+')
        {
            screen_no++;
            accum = 0;
            double x = (double)key; // type cast the character into double to perforn further operations
            x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
            if (pnt1 == 1)
                accum = (accum * 10) + x; // add it in result
            else
                accum = accum + (x * pnt1); // add it in result
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            remove_mem_sign(true);
            clear_last_calculation(true);
            m_flag = 0;
        }
        else if (m_flag == '-')
        {
            screen_no++;
            accum = 0;
            double x = (double)key; // type cast the character into double to perforn further operations
            x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
            if (pnt1 == 1)
                accum = (accum * 10) + x; // add it in result
            else
                accum = accum + (x * pnt1); // add it in result
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            remove_mem_sign(true);
            clear_last_calculation(true);
            m_flag = 0;
        }
        else if (m_flag == '=')
        {
        }
    }
    else if (gst_flag)
    {
        screen_no++;
        accum = 0;
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
            accum = (accum * 10) + x; // add it in result
        else
            accum = accum + (x * pnt1); // add it in result
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        remove_gst_sign(true);
        clear_last_calculation(true);
        gst_flag = 0;
    }
    else if (c_flag)
    {
    }
    else if (mu_flag)
    {
        s_flag = 1;
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
        {
            s_reg = (s_reg * 10) + x; // add it in result
            if (s_reg > 999999999999)
                s_reg = (s_reg - x) / 10;
            print_operand(true, s_reg, 0);
        }
        else
        {
            s_reg = s_reg + (x * pnt1); // add it in result
            double temp_prec = 1 / pnt1;
            int temp2 = 0;
            while (temp_prec >= 10)
            {
                temp_prec = temp_prec / 10;
                temp2++;
            }
            if ((int)s_reg > (pow(10, (11 - temp2))) - 1)
            {
                s_reg = s_reg - (x * pnt1);
                pnt1 *= 10;
                temp2--;
            }
            if (temp2 > 5)
            {
                s_reg = s_reg - (x * pnt1);
                pnt1 *= 10;
                temp2--;
            }
            print_operand(true, s_reg, temp2);
        }
        print_screen_no(true, screen_no);
    }
    else
    {
        double x = (double)key; // type cast the character into double to perforn further operations
        x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
        if (pnt1 == 1)
            accum = (accum * 10) + x; // add it in result
        else
            accum = accum + (x * pnt1); // add it in result
        print_operand(true, accum, -1);
    }
}
double calculate_inclusive_tax(double amount, uint8_t cost_type, char key)
{
    int x = (int)key; // type cast the character into double to perforn further operations
    x -= 48;
    if (cost_type == 1)
    {
        return amount + (amount * (gst_rate[x] / 100.0));
    }
    else if (cost_type == 2)
    {
        return amount * 100.0 / (gst_rate[x] + 100.0);
    }
    else if (cost_type == 3)
    {
        return amount * gst_rate[x] / 100.0;
    }
    else
    {
        return (100.0 + gst_rate[x]) * amount / gst_rate[x];
    }
}
double calculate_exclusive_tax(double amount, uint8_t cost_type, char key)
{
    int x = (int)key; // type cast the character into double to perforn further operations
    x -= 48;
    if (cost_type == 1)
    {
        return amount * 100.0 / (gst_rate[x] + 100.0);
    }
    else if (cost_type == 2)
    {
        return amount * gst_rate[x] / 100.0;
    }
    else if (cost_type == 3)
    {
        return amount * 100.0 / gst_rate[x];
    }
    else
    {
        return amount * gst_rate[x] / 100.0;
    }
}
void get_num_input(char key)
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (key <= '9' && key >= '0')
    {
        add_to_operand(key);
        if (pnt1 <= 0.1)
            pnt1 = pnt1 / 10;
    }
    else if (key == 'D')
    {
        add_to_operand('0');
        if (pnt1 <= 0.1)
            pnt1 = pnt1 / 10;
        add_to_operand('0');
        if (pnt1 <= 0.1)
            pnt1 = pnt1 / 10;
    }
    else if (key == '.')
    {
        if (pnt1 == 1)
        {
            pnt1 = 0.1;
            if (!a_flag)
                print_operand(true, accum, -2);
            else
                print_operand(true, s_reg, -2);
        }
    }
}
void get_sign_input(char key)
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        a_flag = key;
        print_last_calculation(true, accum, key);
        // store in data
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg, key);
                accum = addition(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                // store in data
                change_last_operation(accum, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, key);
            }
            print_operand(true, accum, -1);
        }
        else if (a_flag == '-')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg, key);
                accum = substraction(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                // store in data
                change_last_operation(accum, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, key);
            }

            print_operand(true, accum, -1);
        }
        else if (a_flag == '*')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg, key);
                accum = multiplication(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                // store in data
                change_last_operation(accum, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, key);
            }

            print_operand(true, accum, -1);
        }
        else if (a_flag == '/')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg, key);
                accum = division(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                // store in data
                change_last_operation(accum, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, key);
            }

            print_operand(true, accum, -1);
        }
        a_flag = key;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (e_flag)
    {
        screen_no++;
        // store in data
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        remove_equal_perc_sign(true);
        print_screen_no(true, screen_no);
        a_flag = key;
        e_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (p_flag)
    {
        screen_no++;
        // store in data
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        remove_equal_perc_sign(true);
        print_screen_no(true, screen_no);
        a_flag = key;
        p_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (m_flag)
    {
        if (m_flag == '+')
        {
            screen_no++;
            print_last_calculation(true, accum, key);
            print_operand(true, accum, -1);
            remove_mem_sign(true);
            print_screen_no(true, screen_no);
            a_flag = key;
            m_flag = 0;
            if (pnt1 != 1)
                pnt1 = 1;
        }
        else if (m_flag == '-')
        {
            screen_no++;
            print_last_calculation(true, accum, key);
            print_operand(true, accum, -1);
            remove_mem_sign(true);
            print_screen_no(true, screen_no);
            a_flag = key;
            m_flag = 0;
            if (pnt1 != 1)
                pnt1 = 1;
        }
        else if (m_flag == '=')
        {
        }
    }
    else if (gst_flag)
    {
        screen_no++;
        // store in data
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        remove_gst_sign(true);
        print_screen_no(true, screen_no);
        a_flag = key;
        gst_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (c_flag)
    {
        return;
    }
    else if (mu_flag)
    {
        screen_no++;
        // store in data
        if (s_flag)
            accum = s_reg;
        s_reg = 0;
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        mu_flag = 0;
        a_flag = key;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else
    {
        return;
    }
}
void get_equal_input(char key)
{
    once_equal = 1;
    once_tax = 0;
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        // store in data
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_equal_sign(true);
        print_screen_no(true, screen_no);
        grand_total.total += accum;
        e_flag = 1;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
            }
        }
        else if (a_flag == '-')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
            }
        }
        else if (a_flag == '*')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            else
            {

                s_reg = accum;
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
            }
        }
        else if (a_flag == '/')
        {
            if (s_flag)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            else
            {
                s_reg = accum;
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_reg = 0;
            }
        }
        grand_total.total += accum;
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_equal_sign(true);
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        a_flag = 0;
        e_flag = 1;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (e_flag)
    {
    }
    else if (p_flag)
    {
        if (s_flag)
        {
            screen_no++;
            print_last_calculation(true, accum, '%', s_reg);
            accum = multiplication_percen(accum, s_reg);
            // store in data
            push_arith_result_operation(accum, key);
            show_operations();
            // store in data
            grand_total.total += accum;
            int integer_digit = get_digit(accum);
            if (integer_digit <= 12)
            {
                int set_prec = 0;
                if (integer_digit <= 7)
                    set_prec = 5;
                else
                    set_prec = (12 - integer_digit);
                std::string output = result_to_string(accum, set_prec);
                print_operand(true, output.c_str());
                print_equal_sign(true);
                print_screen_no(true, screen_no);
            }
            else
            {
                err_flag = 1;
                print_error(true);
            }
            if (pnt1 != 1)
                pnt1 = 1;
            s_flag = 0;
            s_reg = 0;
            p_flag = 0;
            e_flag = 1;
        }
    }
    else if (m_flag)
    {
        if (m_flag == '+')
        {
        }
        else if (m_flag == '-')
        {
        }
        else if (m_flag == '=')
        {
            if (a_flag == '+')
            {
                if (s_reg != 0)
                {
                    print_last_calculation(true, accum, a_flag, s_reg);
                    accum = addition(accum, s_reg);
                    s_reg = 0;
                }
            }
            else if (a_flag == '-')
            {
                if (s_reg != 0)
                {
                    print_last_calculation(true, accum, a_flag, s_reg);
                    accum = substraction(accum, s_reg);
                    s_reg = 0;
                }
            }
            else if (a_flag == '*')
            {
                if (s_reg != 0)
                {
                    print_last_calculation(true, accum, a_flag, s_reg);
                    accum = multiplication(accum, s_reg);
                    s_reg = 0;
                }
            }
            else if (a_flag == '/')
            {
                if (s_reg != 0)
                {
                    print_last_calculation(true, accum, a_flag, s_reg);
                    accum = division(accum, s_reg);
                    s_reg = 0;
                }
            }
            grand_total.total += accum;
            int integer_digit = get_digit(accum);
            if (integer_digit <= 12)
            {
                int set_prec = 0;
                if (integer_digit <= 7)
                    set_prec = 5;
                else
                    set_prec = (12 - integer_digit);
                std::string output = result_to_string(accum, set_prec);
                print_operand(true, output.c_str());
                print_equal_sign(true);
                print_screen_no(true, screen_no);
            }
            else
            {
                err_flag = 1;
                print_error(true);
            }
            m_flag = 0;
            e_flag = 1;
            if (pnt1 != 1)
                pnt1 = 1;
        }
    }
    else if (gst_flag)
    {
    }
    else if (c_flag)
    {
        ESP_LOGI(CALC_TAG, "C flag calci =");
        if (save_transaction(accum, (TRANS_TYPE)c_flag, payment_mode_status, "", "", ""))
        {
            remove_online_trans_instruct(true);
            print_screen_no(true, screen_no);
            print_operand(true, accum, -1);
        }
        payment_mode_status = PAYMENT_MODE_CASH;

        grand_total.total += accum;

        c_flag = 0;
        e_flag = 1;
    }
    else if (mu_flag)
    {
        screen_no++;
        // store in data
        push_arith_result_operation(accum, key);
        show_operations();
        // store in data
        clear_last_calculation(true);
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        mu_flag = 0;
        e_flag = key;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else
    {
    }
}
void get_perc_input(char key)
{
    once_equal = 1;
    once_tax = 0;
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        screen_no++;
        // store in data
        push_arith_operation(accum, key);
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_perc_sign(true);
        print_screen_no(true, screen_no);
        grand_total.total += accum;
        p_flag = 1;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag != 0)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition_percen(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '-')
        {
            if (s_flag != 0)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction_percen(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '*')
        {

            if (s_flag != 0)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication_percen(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '/')
        {
            if (s_flag != 0)
            {
                // store in data
                push_arith_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division_percen(accum, s_reg);
                // store in data
                push_arith_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        grand_total.total += accum;
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_perc_sign(true);
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        a_flag = 0;
        p_flag = 1;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (e_flag)
    {
    }
    else if (p_flag)
    {
    }
    else if (m_flag)
    {
    }
    else if (gst_flag)
    {
    }
    else if (c_flag)
    {
        return;
    }
    else if (mu_flag)
    {
        if (s_flag)
        {
            screen_no++;
            // store in data
            push_arith_operation(s_reg, key);
            show_operations();
            // store in data
            print_last_calculation(true, accum, 'M', s_reg);
            accum = mu_percen(accum, s_reg);
            // store in data
            push_arith_result_operation(accum, key);
            show_operations();
            print_screen_no(true, screen_no);
            // store in data
            grand_total.total += accum;
            int integer_digit = get_digit(accum);
            if (integer_digit <= 12)
            {
                int set_prec = 0;
                if (integer_digit <= 7)
                    set_prec = 5;
                else
                    set_prec = (12 - integer_digit);
                std::string output = result_to_string(accum, set_prec);
                print_operand(true, output.c_str());
                print_perc_sign(true);
                print_screen_no(true, screen_no);
            }
            else
            {
                err_flag = 1;
                print_error(true);
            }
            mu_flag = 0;
            p_flag = 1;
            if (pnt1 != 1)
                pnt1 = 1;
        }
    }
    else
    {
    }
}
void get_input(int8_t gst_type, uint8_t cost_type, char key)
{
    once_tax = 1;
    once_equal = 0;
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        // store in data
        push_tax_operation(accum, gst_type, cost_type, key);
        show_operations();
        // store in data
        if (accum != 0)
        {
            if (gst_type == 1)
            {
                double temp_accum = accum;
                grand_total.net += accum;
                accum = calculate_inclusive_tax(accum, cost_type, key);
                grand_total.gross += accum;
                grand_total.tare += accum - temp_accum;
            }
            else if (gst_type == -1)
            {
                double temp_accum = accum;
                grand_total.gross += accum;
                accum = calculate_exclusive_tax(accum, cost_type, key);
                grand_total.net += accum;
                grand_total.tare += temp_accum - accum;
            }
        }
        // store in data
        push_tax_result_operation(accum, gst_type, cost_type, key);
        show_operations();
        // store in data
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_gst_sign(true, gst_type, key);
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        gst_flag = 1;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag)
            {
                // store in data
                push_tax_operation(s_reg, gst_type, cost_type, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition(accum, s_reg);
                if (gst_type == 1)
                {
                    double temp_accum = accum;
                    grand_total.net += accum;
                    accum = calculate_inclusive_tax(accum, cost_type, key);
                    grand_total.gross += accum;
                    grand_total.tare += accum - temp_accum;
                }
                else if (gst_type == -1)
                {
                    double temp_accum = accum;
                    grand_total.gross += accum;
                    accum = calculate_exclusive_tax(accum, cost_type, key);
                    grand_total.net += accum;
                    grand_total.tare += temp_accum - accum;
                }
                // store in data
                push_tax_result_operation(accum, gst_type, cost_type, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            // else
            // {
            //     // store in data
            //     push_arith_operation(s_reg, 0);
            //     show_operations();
            //     // store in data
            //     if (accum != 0)
            //     {
            //         if (gst_type == 1)
            //             accum = calculate_inclusive_tax(accum, cost_type, key);
            //         else if (gst_type == -1)
            //             accum = calculate_exclusive_tax(accum, cost_type, key);
            //     }
            // }
        }
        else if (a_flag == '-')
        {
            if (s_flag)
            {
                // store in data
                push_tax_operation(s_reg, gst_type, cost_type, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction(accum, s_reg);
                if (gst_type == 1)
                {
                    double temp_accum = accum;
                    grand_total.net += accum;
                    accum = calculate_inclusive_tax(accum, cost_type, key);
                    grand_total.gross += accum;
                    grand_total.tare += accum - temp_accum;
                }
                else if (gst_type == -1)
                {
                    double temp_accum = accum;
                    grand_total.gross += accum;
                    accum = calculate_exclusive_tax(accum, cost_type, key);
                    grand_total.net += accum;
                    grand_total.tare += temp_accum - accum;
                }
                // store in data
                push_tax_result_operation(accum, gst_type, cost_type, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            // else
            // {
            //     // store in data
            //     push_arith_operation(s_reg, 0);
            //     show_operations();
            //     // store in data
            //     if (accum != 0)
            //     {
            //         if (gst_type == 1)
            //             accum = calculate_inclusive_tax(accum, cost_type, key);
            //         else if (gst_type == -1)
            //             accum = calculate_exclusive_tax(accum, cost_type, key);
            //     }
            // }
        }
        else if (a_flag == '*')
        {
            if (s_flag)
            {
                // store in data
                push_tax_operation(s_reg, gst_type, cost_type, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication(accum, s_reg);
                if (gst_type == 1)
                {
                    double temp_accum = accum;
                    grand_total.net += accum;
                    accum = calculate_inclusive_tax(accum, cost_type, key);
                    grand_total.gross += accum;
                    grand_total.tare += accum - temp_accum;
                }
                else if (gst_type == -1)
                {
                    double temp_accum = accum;
                    grand_total.gross += accum;
                    accum = calculate_exclusive_tax(accum, cost_type, key);
                    grand_total.net += accum;
                    grand_total.tare += temp_accum - accum;
                }
                // store in data
                push_tax_result_operation(accum, gst_type, cost_type, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            // else
            // {
            //     // store in data
            //     push_arith_operation(s_reg, 0);
            //     show_operations();
            //     // store in data
            //     if (accum != 0)
            //     {
            //         if (gst_type == 1)
            //             accum = calculate_inclusive_tax(accum, cost_type, key);
            //         else if (gst_type == -1)
            //             accum = calculate_exclusive_tax(accum, cost_type, key);
            //     }
            // }
        }
        else if (a_flag == '/')
        {
            if (s_flag)
            {
                // store in data
                push_tax_operation(s_reg, gst_type, cost_type, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division(accum, s_reg);
                if (gst_type == 1)
                {
                    double temp_accum = accum;
                    grand_total.net += accum;
                    accum = calculate_inclusive_tax(accum, cost_type, key);
                    grand_total.gross += accum;
                    grand_total.tare += accum - temp_accum;
                }
                else if (gst_type == -1)
                {
                    double temp_accum = accum;
                    grand_total.gross += accum;
                    accum = calculate_exclusive_tax(accum, cost_type, key);
                    grand_total.net += accum;
                    grand_total.tare += temp_accum - accum;
                }
                // store in data
                push_tax_result_operation(accum, gst_type, cost_type, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
            // else
            // {
            //     // store in data
            //     push_arith_operation(s_reg, 0);
            //     show_operations();
            //     // store in data
            //     if (accum != 0)
            //     {
            //         if (gst_type == 1)
            //             accum = calculate_inclusive_tax(accum, cost_type, key);
            //         else if (gst_type == -1)
            //             accum = calculate_exclusive_tax(accum, cost_type, key);
            //     }
            // }
        }
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_gst_sign(true, gst_type, key);
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        a_flag = 0;
        gst_flag = 1;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (e_flag)
    {
    }
    else if (p_flag)
    {
    }
    else if (m_flag)
    {
    }
    else if (gst_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            if (gst_type == 1)
                accum = calculate_inclusive_tax(accum, cost_type, key);
            else if (gst_type == -1)
                accum = calculate_exclusive_tax(accum, cost_type, key);
            // store in data
            push_tax_operation(accum, gst_type, cost_type, key);
            show_operations();
            // store in data
        }
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_gst_sign(true, gst_type, key);
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (c_flag)
    {
        return;
    }
    else if (mu_flag)
    {
    }
    else
    {
    }
}
void clear_calculator()
{
    remove_online_trans_instruct(true);
    ESP_LOGI(CALC_TAG, "C flag: %c", c_flag);
    screen_no = 0;
    if (c_flag)
    {
        if (save_transaction(accum, (TRANS_TYPE)c_flag, payment_mode_status, "", "", ""))
        {
            print_normal_mode_ac_scrn(true);
            print_screen_no(true, screen_no);
            display_trans_count(true, total_pending_trans);
        }
    }
    else
    {
        print_normal_mode_ac_scrn(true);
        print_screen_no(true, screen_no);
        display_trans_count(true, total_pending_trans);
    }
    payment_mode_status = PAYMENT_MODE_CASH;
    a_flag = 0;
    e_flag = 0;
    p_flag = 0;
    m_flag = 0;
    gst_flag = 0;
    gt_flag = 0;
    c_flag = 0;
    mrc_flag = 0;
    mu_flag = 0;
    once_equal = 0;
    once_tax = 0;
    once_cash = 0;
    accum = 0;
    s_reg = 0;
    pnt1 = 1;
    grand_total = {0};
    gt_pointor = 0;
    operation.clear();
}
void clear_screen()
{
    if (a_flag)
    {
        s_reg = 0;
        print_operand(true, s_reg, -1);
    }
    else if (c_flag)
    {
        payment_mode_status = PAYMENT_MODE_CASH;
        c_flag = 0;
        remove_online_trans_instruct(true);
        accum = 0;
        print_operand(true, accum, -1);
    }
    else
    {
        accum = 0;
        print_operand(true, accum, -1);
    }
    pnt1 = 1;
}
void correct_operand()
{
    ESP_LOGI(CALC_TAG, "1: pnt1 = %f", pnt1);
    if (a_flag)
    {
        ESP_LOGI(CALC_TAG, "s_reg = %f", s_reg);

        int64_t pnt_to_int = 0;
        double temp_prec = 1 / pnt1;
        while (temp_prec > 1)
        {
            temp_prec = temp_prec / 10;
            pnt_to_int++;
        }

        if (pnt_to_int == 0)
        {
            int64_t temp = (int64_t)s_reg;
            temp /= 10;
            s_reg = (double)temp;
            print_operand(true, s_reg, 0);
        }
        else
        {
            int64_t temp1 = (int64_t)s_reg;
            double temp2 = s_reg - temp1;
            temp2 = temp2 * (pow(10, (log10(1 / pnt1)) - 1));
            int64_t temp3 = (int64_t)(temp2 / 10);
            pnt1 = pnt1 * 10;
            double temp4 = ((double)temp3) / (pow(10, (log10(1 / pnt1)) - 1));
            s_reg = temp4 + (double)temp1;
            print_operand(true, s_reg, (log10(1 / pnt1)) - 1);

            pnt_to_int = 0;
            temp_prec = 0.0;
            temp_prec = 1 / pnt1;
            while (temp_prec > 1)
            {
                temp_prec = temp_prec / 10;
                pnt_to_int++;
            }
            if (pnt_to_int == 1)
            {
                pnt1 = 1;
            }
        }
    }
    else
    {
        ESP_LOGI(CALC_TAG, "accum = %f", accum);
        int64_t pnt_to_int = 0;
        double temp_prec = 1 / pnt1;
        while (temp_prec > 1)
        {
            temp_prec = temp_prec / 10;
            pnt_to_int++;
        }

        if (pnt_to_int == 0)
        {
            int64_t temp = (int64_t)accum;
            temp /= 10;
            accum = (double)temp;
            print_operand(true, accum, 0);
        }
        else
        {
            int64_t temp1 = (int64_t)accum;
            double temp2 = accum - temp1;
            temp2 = temp2 * (pow(10, (log10(1 / pnt1)) - 1));
            int64_t temp3 = (int64_t)(temp2 / 10);
            pnt1 = pnt1 * 10;
            double temp4 = ((double)temp3) / (pow(10, (log10(1 / pnt1)) - 1));
            accum = temp4 + (double)temp1;
            print_operand(true, accum, (log10(1 / pnt1)) - 1);

            pnt_to_int = 0;
            temp_prec = 0.0;
            temp_prec = 1 / pnt1;
            while (temp_prec > 1)
            {
                temp_prec = temp_prec / 10;
                pnt_to_int++;
            }
            if (pnt_to_int == 1)
            {
                pnt1 = 1;
            }
        }
    }
}
void add_in_mem()
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        // store in data
        push_mem_operation(accum, '+');
        show_operations();
        // store in data
        if (accum != 0)
        {
            mem += accum;
        }
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '+');
        m_flag = '+';
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '+');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '-')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '+');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '*')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '+');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '/')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '+');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        mem += accum;
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_mem_sign(true, '+');
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        a_flag = 0;
        m_flag = '+';
    }
    else if (e_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem += accum;
        }
        // store in data
        push_mem_operation(accum, '+');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '+');
        remove_equal_perc_sign(true);
        e_flag = 0;
        m_flag = '+';
    }
    else if (p_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem += accum;
        }
        // store in data
        push_mem_operation(accum, '+');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '+');
        remove_equal_perc_sign(true);
        p_flag = 0;
        m_flag = '+';
    }
    else if (m_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem += accum;
        }
        // store in data
        push_mem_operation(accum, '+');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '+');
    }
    else if (gst_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem += accum;
        }
        // store in data
        push_mem_operation(accum, '+');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '+');
        remove_gst_sign(true);
        gst_flag = 0;
        m_flag = '+';
    }
    else if (c_flag)
    {
        return;
    }
    else if (mu_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem += accum;
        }
        // store in data
        push_mem_operation(accum, '+');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '+');
        clear_last_calculation(true);
        mu_flag = 0;
        m_flag = '+';
    }
    else
    {
    }
}
void sub_in_mem()
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        // store in data
        push_mem_operation(accum, '-');
        show_operations();
        // store in data
        if (accum != 0)
        {
            mem -= accum;
        }
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '-');
        m_flag = '-';
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '-');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '-')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '-');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '*')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '-');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '/')
        {
            if (s_flag)
            {
                // store in data
                push_mem_operation(s_reg, '-');
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division(accum, s_reg);
                s_reg = 0;
                s_flag = 0;
            }
        }
        mem -= accum;
        int integer_digit = get_digit(accum);
        if (integer_digit <= 12)
        {
            int set_prec = 0;
            if (integer_digit <= 7)
                set_prec = 5;
            else
                set_prec = (12 - integer_digit);
            std::string output = result_to_string(accum, set_prec);
            print_operand(true, output.c_str());
            print_mem_sign(true, '-');
            print_screen_no(true, screen_no);
        }
        else
        {
            err_flag = 1;
            print_error(true);
        }
        a_flag = 0;
        m_flag = '-';
    }
    else if (e_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem -= accum;
        }
        // store in data
        push_mem_operation(accum, '-');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '-');
        remove_equal_perc_sign(true);
        e_flag = 0;
        m_flag = '-';
    }
    else if (p_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem -= accum;
        }
        // store in data
        push_mem_operation(accum, '-');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '-');
        remove_equal_perc_sign(true);
        p_flag = 0;
        m_flag = '-';
    }
    else if (m_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem -= accum;
        }
        // store in data
        push_mem_operation(accum, '-');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '-');
    }
    else if (gst_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem -= accum;
        }
        // store in data
        push_mem_operation(accum, '-');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '-');
        remove_gst_sign(true);
        gst_flag = 0;
        m_flag = '-';
    }
    else if (c_flag)
    {
        return;
    }
    else if (mu_flag)
    {
        screen_no++;
        if (accum != 0)
        {
            mem -= accum;
        }
        // store in data
        push_mem_operation(accum, '-');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '-');
        clear_last_calculation(true);
        mu_flag = 0;
        m_flag = '-';
    }
    else
    {
    }
}
void mem_recal()
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (mrc_flag)
    {
        mem = 0;
        return;
    }
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        accum = mem;
        // store in data
        push_mem_operation(accum, '=');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '=');
        mrc_flag = 1;
    }
    else if (a_flag)
    {
        s_reg = mem;
        if (!s_flag)
            screen_no++;

        // store in data
        push_mem_operation(s_reg, '=');
        show_operations();
        // store in data
        print_last_calculation(true, accum, a_flag);
        print_operand(true, s_reg, -1);
        print_mem_sign(true, '=');
        print_screen_no(true, screen_no);
        s_flag = 1;
        mrc_flag = 1;
    }
    else if (e_flag)
    {
        screen_no++;
        accum = mem;
        // store in data
        push_mem_operation(accum, '=');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '=');
        remove_equal_perc_sign(true);
        e_flag = 0;
        mrc_flag = 1;
    }
    else if (p_flag)
    {
        screen_no++;
        accum = mem;
        // store in data
        push_mem_operation(accum, '=');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '=');
        remove_equal_perc_sign(true);
        p_flag = 0;
        mrc_flag = 1;
    }
    else if (m_flag)
    {
        screen_no++;
        accum = mem;
        // store in data
        push_mem_operation(accum, '=');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '=');
        m_flag = 0;
        mrc_flag = 1;
    }
    else if (gst_flag)
    {
        screen_no++;
        accum = mem;
        // store in data
        push_mem_operation(accum, '=');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '=');
        remove_gst_sign(true);
        gst_flag = 0;
        mrc_flag = 1;
    }
    else if (c_flag)
    {
        return;
    }
    else if (mu_flag)
    {
        screen_no++;
        accum = mem;
        // store in data
        push_mem_operation(accum, '=');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_mem_sign(true, '=');
        clear_last_calculation(true);
        mu_flag = 0;
        mrc_flag = 1;
    }
    else
    {
    }
}
void cash_entry(char key)
{
    once_cash = 1;
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        if (accum == 0.0)
        {
            once_cash = 0;
            invalid_cash_value = 1;
            print_invalid_amount_err(true);
            esp_timer_stop(invalid_cash_value_timer);
            ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
            // start timer once for 3 second;
            return;
        }
        screen_no++;
        // store in data
        push_cash_operation(accum, key);
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);

        print_cash_sign(true, key);
        print_online_trans_instruct(true, 1);

        c_flag = key;
        payment_mode_status = PAYMENT_MODE_CASH;
        ESP_LOGI(CALC_TAG, "c flag: %c", c_flag);
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (a_flag)
    {
        screen_no++;
        if (a_flag == '+')
        {
            if (s_flag)
            {
                if (addition(accum, s_reg) == 0.0)
                {
                    once_cash = 0;
                    invalid_cash_value = 1;
                    print_invalid_amount_err(true);
                    // start timer once for 3 second;
                    esp_timer_stop(invalid_cash_value_timer);
                    ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
                    return;
                }
                // store in data
                push_cash_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = addition(accum, s_reg);
                // store in data
                push_cash_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '-')
        {
            if (s_flag)
            {
                if (substraction(accum, s_reg) == 0.0)
                {
                    once_cash = 0;
                    invalid_cash_value = 1;
                    print_invalid_amount_err(true);
                    // start timer once for 3 second;
                    esp_timer_stop(invalid_cash_value_timer);
                    ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
                    return;
                }
                // store in data
                push_cash_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = substraction(accum, s_reg);
                // store in data
                push_cash_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '*')
        {
            if (s_flag)
            {
                if (multiplication(accum, s_reg) == 0.0)
                {
                    once_cash = 0;
                    invalid_cash_value = 1;
                    print_invalid_amount_err(true);
                    // start timer once for 3 second;
                    esp_timer_stop(invalid_cash_value_timer);
                    ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
                    return;
                }
                // store in data
                push_cash_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = multiplication(accum, s_reg);
                // store in data
                push_cash_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        else if (a_flag == '/')
        {
            if (s_flag)
            {
                if (division(accum, s_reg) == 0.0)
                {
                    once_cash = 0;
                    invalid_cash_value = 1;
                    print_invalid_amount_err(true);
                    // start timer once for 3 second;
                    esp_timer_stop(invalid_cash_value_timer);
                    ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
                    return;
                }
                // store in data
                push_cash_operation(s_reg, key);
                show_operations();
                // store in data
                print_last_calculation(true, accum, a_flag, s_reg);
                accum = division(accum, s_reg);
                // store in data
                push_cash_result_operation(accum, key);
                show_operations();
                // store in data
                s_reg = 0;
                s_flag = 0;
            }
        }
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        if (key == '+')
        {
            print_cash_sign(true, '+');
            // save_credit_transaction(accum);
        }
        else if (key == '-')
        {
            print_cash_sign(true, '-');
            // save_debit_transaction(accum);
        }
        print_online_trans_instruct(true, 1);
        a_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
        payment_mode_status = PAYMENT_MODE_CASH;
        c_flag = key;
    }
    else if (e_flag)
    {
        if (accum == 0.0)
        {
            once_cash = 0;
            invalid_cash_value = 1;
            print_invalid_amount_err(true);
            // start timer once for 3 second;
            esp_timer_stop(invalid_cash_value_timer);
            ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
            return;
        }
        screen_no++;
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);

        // store in data
        push_cash_operation(accum, key);
        show_operations();
        // store in data

        if (key == '+')
        {
            print_cash_sign(true, '+');
            // save_credit_transaction(accum);
        }
        else if (key == '-')
        {
            print_cash_sign(true, '-');
            // save_debit_transaction(accum);
        }
        print_online_trans_instruct(true, 1);
        payment_mode_status = PAYMENT_MODE_CASH;
        c_flag = key;
        e_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (p_flag)
    {
        if (accum == 0.0)
        {
            once_cash = 0;
            invalid_cash_value = 1;
            print_invalid_amount_err(true);
            // start timer once for 3 second;
            esp_timer_stop(invalid_cash_value_timer);
            ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
            return;
        }
        screen_no++;
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);

        // store in data
        push_cash_operation(accum, key);
        show_operations();
        // store in data

        if (key == '+')
        {
            print_cash_sign(true, '+');
            // save_credit_transaction(accum);
        }
        else if (key == '-')
        {
            print_cash_sign(true, '-');
            // save_debit_transaction(accum);
        }
        print_online_trans_instruct(true, 1);
        payment_mode_status = PAYMENT_MODE_CASH;
        c_flag = key;
        p_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (m_flag)
    {
    }
    else if (gst_flag)
    {
        if (accum == 0.0)
        {
            once_cash = 0;
            invalid_cash_value = 1;
            print_invalid_amount_err(true);
            // start timer once for 3 second;
            esp_timer_stop(invalid_cash_value_timer);
            ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
            return;
        }
        screen_no++;
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);

        // store in data
        push_cash_operation(accum, key);
        show_operations();
        // store in data

        if (key == '+')
        {
            print_cash_sign(true, '+');
            // save_credit_transaction(accum);
        }
        else if (key == '-')
        {
            print_cash_sign(true, '-');
            // save_debit_transaction(accum);
        }
        payment_mode_status = PAYMENT_MODE_CASH;
        c_flag = key;
        gst_flag = 0;
        print_online_trans_instruct(true, 1);
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (c_flag)
    {
        if (accum == 0.0)
        {
            once_cash = 0;
            invalid_cash_value = 1;
            print_invalid_amount_err(true);
            // start timer once for 3 second;
            esp_timer_stop(invalid_cash_value_timer);
            ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
            return;
        }
        remove_online_trans_instruct(true);
        print_cash_sign(true, key);
        ESP_LOGI(CALC_TAG, "c_flag: %c, Key: %c", c_flag, key);
        if (c_flag == key)
        {
            if (payment_mode_status == PAYMENT_MODE_CASH)
                payment_mode_status = PAYMENT_MODE_ONLINE;
            else if (payment_mode_status == PAYMENT_MODE_ONLINE)
                payment_mode_status = PAYMENT_MODE_LATER;
            else if (payment_mode_status == PAYMENT_MODE_LATER)
                payment_mode_status = PAYMENT_MODE_CASH;
            print_online_trans_instruct(true, payment_mode_status);
        }
        else
        {
            payment_mode_status = PAYMENT_MODE_CASH;
            print_online_trans_instruct(true, payment_mode_status);
        }
        c_flag = key;
    }
    else if (mu_flag)
    {
        if (accum == 0.0)
        {
            once_cash = 0;
            invalid_cash_value = 1;
            print_invalid_amount_err(true);
            // start timer once for 3 second;
            esp_timer_stop(invalid_cash_value_timer);
            ESP_ERROR_CHECK(esp_timer_start_once(invalid_cash_value_timer, 3 * 1000 * 1000));
            return;
        }
        screen_no++;
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);

        // store in data
        push_cash_operation(accum, key);
        show_operations();
        // store in data

        print_cash_sign(true, key);
        payment_mode_status = PAYMENT_MODE_CASH;
        c_flag = key;

        print_online_trans_instruct(true, 1);
        mu_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else
    {
    }
}
void change_mode_to_step_check(char key)
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    show_operations();
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        // store in data
        push_arith_operation(accum, 0);
        // store in data
    }
    else if (a_flag)
    {
        screen_no++;
        if (s_flag)
        {
            // store in data
            push_arith_operation(s_reg, a_flag);
            // store in data
            s_flag = 0;
        }
        else
        {
            // store in data
            push_arith_operation(accum, a_flag);
            // store in data
        }
    }
    // else if (e_flag)
    // {
    //     // store in data
    //     push_arith_result_operation(accum, '=');
    //     show_operations();
    //     // store in data
    // }
    // else if (p_flag)
    // {
    //     // store in data
    //     push_arith_result_operation(accum, '%');
    //     show_operations();
    //     // store in data
    // }
    // else if (m_flag)
    // {
    // }
    // else if (gst_flag)
    // {
    //     // store in data
    //     // push_g_result_operation(accum, '=');
    //     show_operations();
    //     // store in data
    // }
    else if (c_flag)
    {
        //     if (c_flag == '+')
        //         save_credit_transaction(accum, 'C');
        //     else if (c_flag == '-')
        //         save_debit_transaction(accum, 'C');
        //     remove_online_trans_instruct(true);
    }
    else
    {
    }
    show_operations();
}
void calculate_gt(char key)
{
    if (once_tax == 1)
    {
        if (gt_pointor == 3)
            gt_pointor = 1;
        else
            gt_pointor++;
    }
    else if (once_equal == 1)
        gt_pointor = 4;
    if (gt_flag)
    {
        screen_no++;
        switch (gt_pointor)
        {
        case 1:
            accum = grand_total.gross;
            break;
        case 2:
            accum = grand_total.net;
            break;
        case 3:
            accum = grand_total.tare;
            break;
        case 4:
            accum = grand_total.total;
            break;
        }
        // store in data
        push_gt_operation(accum, '+');
        show_operations();
        // store in data
        print_operand(true, accum, -1);
        print_screen_no(true, screen_no);
        print_gt_sign(true, gt_pointor);
    }
    else
    {
        if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                accum = grand_total.gross;
                break;
            case 2:
                accum = grand_total.net;
                break;
            case 3:
                accum = grand_total.tare;
                break;
            case 4:
                accum = grand_total.total;
                break;
            }
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            print_gt_sign(true, gt_pointor);
            gt_flag = 1;
        }
        else if (a_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                s_reg = grand_total.gross;
                break;
            case 2:
                s_reg = grand_total.net;
                break;
            case 3:
                s_reg = grand_total.tare;
                break;
            case 4:
                s_reg = grand_total.total;
                break;
            }
            if (!s_flag)
                screen_no++;

            // store in data
            push_gt_operation(s_reg, gt_pointor);
            show_operations();
            // store in data
            print_last_calculation(true, accum, a_flag);
            print_operand(true, s_reg, -1);
            print_gt_sign(true, gt_pointor);
            print_screen_no(true, screen_no);
            s_flag = 1;
            gt_flag = 1;
        }
        else if (e_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                accum = grand_total.gross;
                break;
            case 2:
                accum = grand_total.net;
                break;
            case 3:
                accum = grand_total.tare;
                break;
            case 4:
                accum = grand_total.total;
                break;
            }
            // store in data
            push_gt_operation(accum, '+');
            show_operations();
            // store in data
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            print_gt_sign(true, gt_pointor);
            remove_equal_perc_sign(true);
            clear_last_calculation(true);
            gt_flag = 1;
        }
        else if (p_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                accum = grand_total.gross;
                break;
            case 2:
                accum = grand_total.net;
                break;
            case 3:
                accum = grand_total.tare;
                break;
            case 4:
                accum = grand_total.total;
                break;
            }
            // store in data
            push_gt_operation(accum, '+');
            show_operations();
            // store in data
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            print_gt_sign(true, gt_pointor);
            remove_equal_perc_sign(true);
            clear_last_calculation(true);
            gt_flag = 1;
        }
        else if (m_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                accum = grand_total.gross;
                break;
            case 2:
                accum = grand_total.net;
                break;
            case 3:
                accum = grand_total.tare;
                break;
            case 4:
                accum = grand_total.total;
                break;
            }
            // store in data
            push_gt_operation(accum, '+');
            show_operations();
            // store in data
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            print_gt_sign(true, gt_pointor);
            remove_mem_sign(true);
            clear_last_calculation(true);
            gt_flag = 1;
        }
        else if (gst_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                accum = grand_total.gross;
                break;
            case 2:
                accum = grand_total.net;
                break;
            case 3:
                accum = grand_total.tare;
                break;
            case 4:
                accum = grand_total.total;
                break;
            }
            // store in data
            push_gt_operation(accum, '+');
            show_operations();
            // store in data
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            print_gt_sign(true, gt_pointor);
            remove_gst_sign(true);
            clear_last_calculation(true);
            gt_flag = 1;
        }
        else if (c_flag)
        {
            return;
        }
        else if (mu_flag)
        {
            screen_no++;
            switch (gt_pointor)
            {
            case 1:
                accum = grand_total.gross;
                break;
            case 2:
                accum = grand_total.net;
                break;
            case 3:
                accum = grand_total.tare;
                break;
            case 4:
                accum = grand_total.total;
                break;
            }
            // store in data
            push_gt_operation(accum, '+');
            show_operations();
            // store in data
            print_operand(true, accum, -1);
            print_screen_no(true, screen_no);
            print_gt_sign(true, gt_pointor);
            remove_equal_perc_sign(true);
            clear_last_calculation(true);
            gt_flag = 1;
            mu_flag = 0;
        }
        else
        {
        }
    }
}
void mark_up(char key)
{
    if (gt_flag)
        remove_gt_sign(true);
    gt_flag = 0;
    gt_pointor = 0;
    if (!a_flag && !e_flag && !p_flag && !m_flag && !gst_flag && !c_flag && !mu_flag)
    {
        screen_no++;
        mu_flag = key;
        print_last_calculation(true, accum, key);

        // store in data
        push_mu_operation(accum, key);
        show_operations();
        // store in data

        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (a_flag)
    {
        screen_no++;
        if (s_flag)
        {
            // store in data
            push_mu_operation(s_reg, key);
            show_operations();
            // store in data
            if (a_flag == '+')
                accum = addition(accum, s_reg);
            else if (a_flag == '-')
                accum = substraction(accum, s_reg);
            else if (a_flag == '*')
                accum = multiplication(accum, s_reg);
            else if (a_flag == '/')
                accum = division(accum, s_reg);
            s_reg = 0;
            s_flag = 0;
            print_last_calculation(true, accum, key);
            print_operand(true, accum, -1);
            // store in data
            push_mu_operation(accum, key);
            show_operations();
            // store in data
        }
        mu_flag = key;
        a_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (e_flag)
    {
        screen_no++;
        // store in data
        push_mu_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        remove_equal_perc_sign(true);
        print_screen_no(true, screen_no);
        mu_flag = key;
        e_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (p_flag)
    {
        screen_no++;
        // store in data
        push_mu_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        remove_equal_perc_sign(true);
        print_screen_no(true, screen_no);
        mu_flag = key;
        p_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (m_flag)
    {
        if (m_flag == '+')
        {
            screen_no++;
            print_last_calculation(true, accum, key);
            print_operand(true, accum, -1);
            remove_mem_sign(true);
            print_screen_no(true, screen_no);
            a_flag = key;
            m_flag = 0;
            if (pnt1 != 1)
                pnt1 = 1;
        }
        else if (m_flag == '-')
        {
            screen_no++;
            print_last_calculation(true, accum, key);
            print_operand(true, accum, -1);
            remove_mem_sign(true);
            print_screen_no(true, screen_no);
            a_flag = key;
            m_flag = 0;
            if (pnt1 != 1)
                pnt1 = 1;
        }
        else if (m_flag == '=')
        {
        }
    }
    else if (gst_flag)
    {
        screen_no++;
        // store in data
        push_mu_operation(accum, key);
        show_operations();
        // store in data
        print_last_calculation(true, accum, key);
        print_operand(true, accum, -1);
        remove_gst_sign(true);
        print_screen_no(true, screen_no);
        mu_flag = key;
        gst_flag = 0;
        if (pnt1 != 1)
            pnt1 = 1;
    }
    else if (c_flag)
    {
        return;
    }
    else
    {
    }
}
static void invalid_cash_value_timeout_callback(void *arg)
{
    invalid_cash_value = 0;
    uint8_t temp = 4;
    xQueueSendFromISR(keypad_queue, &temp, NULL);
}