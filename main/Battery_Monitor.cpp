#include "Battery_Monitor.h"

static const char *BAT_TAG = "Battery Monitor";
/**
 * 3680 mV is 10%
 * 3000 mV is 0%
 * 4200 mV is 100%
 */

static esp_adc_cal_characteristics_t adc1_chars;
static esp_adc_cal_characteristics_t adc2_chars;
static int last_bat_vol = 0;

float get_bat_voltage(void)
{
    int adc_value = 0;
    for (int i = 0; i < 20; i++)
    {
        adc_value += esp_adc_cal_raw_to_voltage(adc1_get_raw(ADC1_CHANNEL_0), &adc1_chars);
        // adc_value += adc1_get_raw(ADC1_CHANNEL_0);
    }
    float avg_adc = adc_value / 20.0;
    if (get_hardware_version() == "001")
    {
        avg_adc = (avg_adc * 5.0) / 3.0;
    }
    else if (get_hardware_version() == "002")
    {
        avg_adc = avg_adc * 2;
    }
    // printf("\nBat Voltage: %f mv\n", avg_adc);
    return avg_adc;
}

float get_chg_detact(void)
{
    int adc_value = 0;
    for (int i = 0; i < 10; i++)
    {
        adc_value += esp_adc_cal_raw_to_voltage(adc1_get_raw(ADC1_CHANNEL_5), &adc2_chars);
        // adc_value += adc1_get_raw(ADC1_CHANNEL_0);
    }
    float avg_adc = adc_value / 10.0;
    avg_adc = (avg_adc * 4096.0) / 3300;
    // printf("\nCharge detact voltage: %f mv\n", avg_adc);
    return avg_adc;
}

static void battery_monitoring_task_example(void *arg)
{
    uint32_t io_num;
    static int charging_sts = 0;
    static uint8_t bat_symbol_sts = 1;
    while (1)
    {
        float bat_vol = get_bat_voltage();
        float bat_det = get_chg_detact();
        if (bat_det <= 300.00)
        {
            if (bat_vol < 4200.00)
            {
                charging_sts++;
                if (charging_sts == 7)
                    charging_sts = 0;
                print_battery_charge(true, charging_sts);
            }
        }
        else if (bat_det >= 450.00)
        {
            if (bat_vol >= 4200.00)
            {
                if (bat_symbol_sts == 1)
                {
                    print_battery_charge(true, 6);
                    bat_symbol_sts = 0;
                }
                else
                {
                    remove_battery_charge_symbol(true);
                    bat_symbol_sts = 1;
                }
            }
            else if (bat_vol >= 3000.00 && bat_vol < 4200.00)
            {
                int temp = bat_vol - 3000.00;
                temp = temp / 200;
                print_battery_charge(true, temp);
                vTaskDelay(5000 / portTICK_RATE_MS);
            }
            else
            {
                print_battery_charge(true, 0);
                if (bat_symbol_sts == 1)
                {
                    print_battery_charge(true, 1);
                    bat_symbol_sts = 0;
                }
                else
                {
                    remove_battery_charge_symbol(true);
                    bat_symbol_sts = 1;
                }
            }
        }
        if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
        {
            ESP_LOGI(BAT_TAG, "Task delete notification received");
            vTaskDelete(NULL);
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
    }
}

int start_battery_monitoring(void)
{
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, (adc_bits_width_t)ADC_WIDTH_BIT_DEFAULT, 0, &adc1_chars);
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, (adc_bits_width_t)ADC_WIDTH_BIT_DEFAULT, 0, &adc2_chars);
    adc1_config_width((adc_bits_width_t)ADC_WIDTH_BIT_DEFAULT);
    adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_11);
    adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_DB_11);

    last_bat_vol = get_bat_voltage();
    float temp = get_chg_detact();
    xTaskCreate(battery_monitoring_task_example, "battery_monitoring_task_example", 2048, NULL, 9, &Battery_Task_Handle);

    if (temp >= 450)
    {
        if (last_bat_vol < 2800.00)
            return 1;
        else if (last_bat_vol < 3000.00)
            return 2;
    }
    return 0;
}