#include "Update_Notify.h"
#include "Display.h"

std::string update_version_name;
std::string update_version_code;
bool update_available_flag = false;

static void update_notify_task(void *arg)
{
    File_Operation read_status;
    Json_Operation json_status;
    while (1)
    {
        if (mode_now == NORMAL_MODE)
        {
            ESP_LOGI(UPD_NTFY, "Checking for update");
            if (get_wifi_status() && read_status.m_exists(USERNAME) && read_status.m_exists(ACCS_TOKEN) && read_status.m_exists(AUTH_TOKEN) && read_status.m_exists(CASHBOOK_ID))
            {
                ESP_LOGI(UPD_NTFY, "WiFi is connected");
                if (read_status.m_exists(UPDATE_STATUS))
                {
                    ESP_LOGI(UPD_NTFY, "Status file is available");
                    std::string last_update_status = read_status.m_read(UPDATE_STATUS);
                    int last_check_year = json_status.get_int(last_update_status.c_str(), "year");
                    int last_check_month = json_status.get_int(last_update_status.c_str(), "month");
                    int last_check_day = json_status.get_int(last_update_status.c_str(), "day");
                    int day_checked = json_status.get_int(last_update_status.c_str(), "checked");
                    time_t t = time(NULL);
                    struct tm tm = *localtime(&t);
                    ESP_LOGI(UPD_NTFY, "now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
                    if ((tm.tm_year + 1900) == last_check_year && (tm.tm_mon + 1) == last_check_month && tm.tm_mday == last_check_day)
                    {
                        ESP_LOGI(UPD_NTFY, "File is of same day");
                        std::string temp_version_code = json_status.get_string(last_update_status.c_str(), "versionCode");
                        ESP_LOGI(UPD_NTFY, "Version code: %s", temp_version_code.c_str());
                        if (temp_version_code != "-1" && temp_version_code != "-2" && temp_version_code != "0" && temp_version_code != "")
                        {
                            if (day_checked)
                            {
                                Upd_Ntfy_Task_Handle = NULL;
                                vTaskDelete(NULL);
                            }
                            else
                            {
                                int letest_ver = std::stoi(json_status.get_string(last_update_status.c_str(), "versionCode"));
                                if (letest_ver > std::stoi(CONFIG_FIRMWARE_VERSION_CODE))
                                {
                                    update_version_code = json_status.get_string(last_update_status.c_str(), "versionCode");
                                    update_version_name = json_status.get_string(last_update_status.c_str(), "versionName");
                                    ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                    update_available_flag = true;
                                    mode_now = UPDATE_NTFY_MODE;
                                    show_update_ntfy_popup(true, update_version_name.c_str());
                                    Upd_Ntfy_Task_Handle = NULL;
                                    vTaskDelete(NULL);
                                }
                            }
                        }
                        else
                        {
                            Upd_Ntfy_Task_Handle = NULL;
                            vTaskDelete(NULL);
                        }
                    }
                    else
                    {
                        ESP_LOGI(UPD_NTFY, "File is not ofsame day");
                        int rsp_code = check_for_update(update_version_code, update_version_name);
                        ESP_LOGI(UPD_NTFY, "Response code: %d", rsp_code);
                        if (rsp_code == 200)
                        {
                            ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                            if (update_version_code != "0")
                            {
                                std::string update_status_json = "";
                                update_status_json = json_status.add_string(update_status_json.c_str(), "versionCode", update_version_code.c_str());
                                update_status_json = json_status.add_string(update_status_json.c_str(), "versionName", update_version_name.c_str());

                                time_t t = time(NULL);
                                struct tm tm = *localtime(&t);
                                ESP_LOGI(UPD_NTFY, "now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
                                update_status_json = json_status.add_int(update_status_json.c_str(), "year", tm.tm_year + 1900);
                                update_status_json = json_status.add_int(update_status_json.c_str(), "month", tm.tm_mon + 1);
                                update_status_json = json_status.add_int(update_status_json.c_str(), "day", tm.tm_mday);

                                read_status.m_write(UPDATE_STATUS, update_status_json.c_str());
                                if (update_status_json != "-1" && update_status_json != "-2" && update_status_json != "0" && update_status_json != "")
                                {
                                    int letest_ver = std::stoi(update_version_code);
                                    if (letest_ver > std::stoi(CONFIG_FIRMWARE_VERSION_CODE))
                                    {
                                        update_available_flag = true;
                                        ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                        mode_now = UPDATE_NTFY_MODE;
                                        show_update_ntfy_popup(true, update_version_name.c_str());
                                        Upd_Ntfy_Task_Handle = NULL;
                                        vTaskDelete(NULL);
                                    }
                                    else
                                    {
                                        Upd_Ntfy_Task_Handle = NULL;
                                        vTaskDelete(NULL);
                                    }
                                }
                                else
                                {
                                    Upd_Ntfy_Task_Handle = NULL;
                                    vTaskDelete(NULL);
                                }
                            }
                            else
                            {
                                Upd_Ntfy_Task_Handle = NULL;
                                vTaskDelete(NULL);
                            }
                        }
                        else if (rsp_code == 401)
                        {
                            int rsp = refresh_token();
                            if (rsp == 200)
                            {
                                rsp_code = check_for_update(update_version_code, update_version_name);
                                ESP_LOGI(UPD_NTFY, "Response code: %d", rsp_code);
                                if (rsp_code == 200)
                                {
                                    ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                    if (update_version_code != "0")
                                    {
                                        std::string update_status_json = "";
                                        update_status_json = json_status.add_string(update_status_json.c_str(), "versionCode", update_version_code.c_str());
                                        update_status_json = json_status.add_string(update_status_json.c_str(), "versionName", update_version_name.c_str());

                                        time_t t = time(NULL);
                                        struct tm tm = *localtime(&t);
                                        ESP_LOGI(UPD_NTFY, "now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
                                        update_status_json = json_status.add_int(update_status_json.c_str(), "year", tm.tm_year + 1900);
                                        update_status_json = json_status.add_int(update_status_json.c_str(), "month", tm.tm_mon + 1);
                                        update_status_json = json_status.add_int(update_status_json.c_str(), "day", tm.tm_mday);

                                        read_status.m_write(UPDATE_STATUS, update_status_json.c_str());
                                        if (update_status_json != "-1" && update_status_json != "-2" && update_status_json != "0" && update_status_json != "")
                                        {
                                            int letest_ver = std::stoi(update_version_code);
                                            if (letest_ver > std::stoi(CONFIG_FIRMWARE_VERSION_CODE))
                                            {
                                                update_available_flag = true;
                                                ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                                mode_now = UPDATE_NTFY_MODE;
                                                show_update_ntfy_popup(true, update_version_name.c_str());
                                                Upd_Ntfy_Task_Handle = NULL;
                                                vTaskDelete(NULL);
                                            }
                                            else
                                            {
                                                Upd_Ntfy_Task_Handle = NULL;
                                                vTaskDelete(NULL);
                                            }
                                        }
                                        else
                                        {
                                            Upd_Ntfy_Task_Handle = NULL;
                                            vTaskDelete(NULL);
                                        }
                                    }
                                }
                                else
                                {
                                    Upd_Ntfy_Task_Handle = NULL;
                                    vTaskDelete(NULL);
                                }
                            }
                            else if (rsp == 401)
                            {
                                log_out_user();
                                Upd_Ntfy_Task_Handle = NULL;
                                vTaskDelete(NULL);
                            }
                            else
                            {
                                Upd_Ntfy_Task_Handle = NULL;
                                vTaskDelete(NULL);
                            }
                        }
                        else
                        {
                            Upd_Ntfy_Task_Handle = NULL;
                            vTaskDelete(NULL);
                        }
                    }
                }
                else
                {
                    ESP_LOGI(UPD_NTFY, "File is not of same day");
                    int rsp_code = check_for_update(update_version_code, update_version_name);
                    ESP_LOGI(UPD_NTFY, "Response code: %d", rsp_code);
                    if (rsp_code == 200)
                    {
                        ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                        if (update_version_code != "0")
                        {

                            std::string update_status_json = "";
                            update_status_json = json_status.add_string(update_status_json.c_str(), "versionCode", update_version_code.c_str());
                            update_status_json = json_status.add_string(update_status_json.c_str(), "versionName", update_version_name.c_str());

                            time_t t = time(NULL);
                            struct tm tm = *localtime(&t);
                            ESP_LOGI(UPD_NTFY, "now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
                            update_status_json = json_status.add_int(update_status_json.c_str(), "year", tm.tm_year + 1900);
                            update_status_json = json_status.add_int(update_status_json.c_str(), "month", tm.tm_mon + 1);
                            update_status_json = json_status.add_int(update_status_json.c_str(), "day", tm.tm_mday);

                            read_status.m_write(UPDATE_STATUS, update_status_json.c_str());
                            if (update_status_json != "-1" && update_status_json != "-2" && update_status_json != "0" && update_status_json != "")
                            {
                                int letest_ver = std::stoi(update_version_code);
                                if (letest_ver > std::stoi(CONFIG_FIRMWARE_VERSION_CODE))
                                {
                                    update_available_flag = true;
                                    ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                    mode_now = UPDATE_NTFY_MODE;
                                    show_update_ntfy_popup(true, update_version_name.c_str());
                                    Upd_Ntfy_Task_Handle = NULL;
                                    vTaskDelete(NULL);
                                }
                                else
                                {
                                    Upd_Ntfy_Task_Handle = NULL;
                                    vTaskDelete(NULL);
                                }
                            }
                            else
                            {
                                Upd_Ntfy_Task_Handle = NULL;
                                vTaskDelete(NULL);
                            }
                        }
                        else
                        {
                            Upd_Ntfy_Task_Handle = NULL;
                            vTaskDelete(NULL);
                        }
                    }
                    else if (rsp_code == 401)
                    {
                        int rsp = refresh_token();
                        if (rsp == 200)
                        {
                            rsp_code = check_for_update(update_version_code, update_version_name);
                            ESP_LOGI(UPD_NTFY, "Response code: %d", rsp_code);
                            if (rsp_code == 200)
                            {
                                ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                if (update_version_code != "0")
                                {
                                    std::string update_status_json = "";
                                    update_status_json = json_status.add_string(update_status_json.c_str(), "versionCode", update_version_code.c_str());
                                    update_status_json = json_status.add_string(update_status_json.c_str(), "versionName", update_version_name.c_str());

                                    time_t t = time(NULL);
                                    struct tm tm = *localtime(&t);
                                    ESP_LOGI(UPD_NTFY, "now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
                                    update_status_json = json_status.add_int(update_status_json.c_str(), "year", tm.tm_year + 1900);
                                    update_status_json = json_status.add_int(update_status_json.c_str(), "month", tm.tm_mon + 1);
                                    update_status_json = json_status.add_int(update_status_json.c_str(), "day", tm.tm_mday);

                                    read_status.m_write(UPDATE_STATUS, update_status_json.c_str());
                                    if (update_status_json != "-1" && update_status_json != "-2" && update_status_json != "0" && update_status_json != "")
                                    {
                                        int letest_ver = std::stoi(update_version_code);
                                        if (letest_ver > std::stoi(CONFIG_FIRMWARE_VERSION_CODE))
                                        {
                                            update_available_flag = true;
                                            ESP_LOGI(UPD_NTFY, "Version Name: %s \t Version Code: %s", update_version_name.c_str(), update_version_code.c_str());
                                            mode_now = UPDATE_NTFY_MODE;
                                            show_update_ntfy_popup(true, update_version_name.c_str());
                                            Upd_Ntfy_Task_Handle = NULL;
                                            vTaskDelete(NULL);
                                        }
                                        else
                                        {
                                            Upd_Ntfy_Task_Handle = NULL;
                                            vTaskDelete(NULL);
                                        }
                                    }
                                    else
                                    {
                                        Upd_Ntfy_Task_Handle = NULL;
                                        vTaskDelete(NULL);
                                    }
                                }
                            }
                            else
                            {
                                Upd_Ntfy_Task_Handle = NULL;
                                vTaskDelete(NULL);
                            }
                        }
                        else if (rsp == 401)
                        {
                            log_out_user();
                            Upd_Ntfy_Task_Handle = NULL;
                            vTaskDelete(NULL);
                        }
                        else
                        {
                            Upd_Ntfy_Task_Handle = NULL;
                            vTaskDelete(NULL);
                        }
                    }
                    else
                    {
                        Upd_Ntfy_Task_Handle = NULL;
                        vTaskDelete(NULL);
                    }
                }
                vTaskDelay(30000 / portTICK_RATE_MS);
            }
            else
            {
                ESP_LOGI(UPD_NTFY, "WiFi is not connected");
                vTaskDelay(30000 / portTICK_RATE_MS);
            }
        }
        if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
        {
            ESP_LOGI(UPD_NTFY, "Task delete notification received");
            vTaskDelete(NULL);
        }
        vTaskDelay(30000 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

void start_update_notifier()
{
    xTaskCreate(update_notify_task, "update notifier", 10240, NULL, 8, &Upd_Ntfy_Task_Handle);
}