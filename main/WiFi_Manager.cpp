#include "WiFi_Manager.h"
#include <esp_log.h>
#include "HTTP_Client.h"
#include "esp_event.h"
#include "esp_wifi.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "nvs_flash.h"
#include <stdio.h>
#include <string.h>
#include "Display.h"

static const char *TAG = "WiFi Manager";

static QueueHandle_t wifi_connect_evt_queue = NULL;

EventGroupHandle_t s_wifi_event_group;

bool get_wifi_status(void)
{
  EventBits_t status_bits = xEventGroupGetBits(s_wifi_event_group);
  if (status_bits & WIFI_CONNECTED_BIT)
  {
    if (!(status_bits & WIFI_FAIL_BIT))
    {
      return true;
    }
  }
  return false;
}

std::string get_wifi_ssid(void)
{
  wifi_ap_record_t ap_info[1] = {0};
  esp_err_t err = esp_wifi_sta_get_ap_info(ap_info);
  if (err == ESP_OK)
  {
    return std::string((char *)ap_info->ssid);
  }
  else
  {
    return "";
  }
}

void wifi_disconnect(void)
{
  esp_wifi_disconnect();
  xEventGroupClearBits(s_wifi_event_group, WIFI_FAIL_BIT);
  xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
}

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
  if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
  {
    ESP_LOGI(TAG, "WiFi started");
    esp_wifi_connect();
  }
  else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
  {
    ESP_LOGE(TAG, "WiFi Disconnected");
    xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
    xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
  }
  else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
  {
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    xEventGroupClearBits(s_wifi_event_group, WIFI_FAIL_BIT);
    uint32_t temp_queue = 1;
    xQueueSendFromISR(wifi_connect_evt_queue, &temp_queue, NULL);
  }
}

static void wifi_manager(void *arg)
{
  uint32_t wifi_connect_recv_queue = 0;
  while (1)
  {
    if (xQueueReceive(wifi_connect_evt_queue, &(wifi_connect_recv_queue), (TickType_t)5000))
    {
      get_base_url();
      send_firmware_version();
    }
    if (get_wifi_status())
    {
      wifi_ap_record_t get_strength;
      esp_wifi_sta_get_ap_info(&get_strength);
      print_signal_strength(true, get_strength.rssi);
    }
    else
    {
      print_signal_strength(true, 0);
      esp_wifi_connect();
    }
    if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
    {
      ESP_LOGI(TAG, "Task delete notification received");
      vTaskDelete(NULL);
    }
  }
  vTaskDelete(NULL);
}

void wifi_init(void)
{
  s_wifi_event_group = xEventGroupCreate();
  wifi_connect_evt_queue = xQueueCreate(2, sizeof(uint32_t));
  ESP_ERROR_CHECK(esp_netif_init());

  ESP_ERROR_CHECK(esp_event_loop_create_default());
  // esp_netif_create_default_wifi_ap();  // new esp idf v4.4 changes for wifi in
  // case of old esp idf version(4.2.2)
  // comment this line
  // case of old esp idf version(4.2.2)
  // comment this line
  esp_netif_create_default_wifi_sta(); // new  esp idf v4.4 changes for wifi in
                                       // case of old esp idf version(4.2.2)
                                       // comment this line
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
  ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_start());
  xTaskCreate(wifi_manager, "wifi_manager_task", 10240, NULL, 10, &WiFi_Task_Handle);
}