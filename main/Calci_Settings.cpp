#include "Calci_Settings.h"
#include "WiFi_Manager.h"
#include "esp_wifi.h"
#include <ctype.h>
#include "esp_log.h"
#include "File_Operation.h"
#include "esp_event.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "Display.h"

static const char *TAG = "Calci Settings";
static int setting_selected_menu = 1;
static char setting_wifi_password_input[26];
static int setting_wifi_password_input_idx = 0;
static int setting_wifi_password_shift_mode = 1;
static char setting_log_mobile_input[11];
static int setting_log_mobile_input_idx = 0;
static char setting_log_otp_input[5];
static int setting_log_otp_input_idx = 0;
static int setting_gst_idx = 0;
static double setting_gst_temp_store = 0;

static char gst_rate_input[6] = "";
static int gst_rate_input_idx = 0;
uint8_t set_gst = 0;
std::string fm_ver = "";

enum Setting_State Selected_Setting_State;
enum Setting_WiFi_State Selected_Setting_WiFi_State;
enum Setting_Log_State Selected_Setting_Log_State;
enum Setting_GST_State Selected_Setting_GST_State;
enum Setting_Info_State Selected_Setting_Info_State;
enum Setting_Update_State Selected_Setting_Update_State;
enum Setting_Help_State Selected_Setting_Help_State;

typedef void (*setting_select_func_t)(void);

uint16_t setting_found_ap_count = 0;
wifi_ap_record_t *setting_found_ap_info;

void setting_ideal_state_helper_print_menu(uint8_t key, int show_menu);

bool is_gst_rate_has_point(void)
{
  for (int i = 0; i < sizeof(gst_rate_input) / sizeof(char); i++)
  {
    if (gst_rate_input[i] == '.')
    {
      return true;
    }
  }
  return false;
}
void add_to_gst_rate(char key)
{
  if (key == '.')
  {
    if (!is_gst_rate_has_point())
    {
      gst_rate_input[gst_rate_input_idx] = key;
      gst_rate_input_idx++;
    }
  }
  else
  {
    if (is_gst_rate_has_point() && gst_rate_input_idx < 6)
    {
      gst_rate_input[gst_rate_input_idx] = key;
      gst_rate_input_idx++;
    }
    else
    {
      if (gst_rate_input_idx < 5)
      {
        gst_rate_input[gst_rate_input_idx] = key;
        gst_rate_input_idx++;
      }
    }
  }
}

void correct_gst_temp_rate(void)
{
  if (gst_rate_input_idx > 0)
  {
    gst_rate_input[gst_rate_input_idx] = 0;
    gst_rate_input_idx--;
  }
}
bool setting_is_logged_in(void)
{
  File_Operation setting_login_file;
  if (setting_login_file.m_exists(AUTH_TOKEN) && setting_login_file.m_exists(ACCS_TOKEN) && setting_login_file.m_exists(CASHBOOK_ID) && setting_login_file.m_exists(USERNAME))
  {
    return true;
  }
  return false;
}
std::string setting_get_ssid(void)
{
  if (get_wifi_status())
  {
    return get_wifi_ssid();
  }
  else
  {
    return "";
  }
}
std::string setting_get_login_detail(void)
{
  File_Operation setting_login_file;
  if (setting_login_file.m_exists(AUTH_TOKEN) && setting_login_file.m_exists(ACCS_TOKEN) && setting_login_file.m_exists(CASHBOOK_ID) && setting_login_file.m_exists(USERNAME))
  {
    std::string temp = setting_login_file.m_read(USERNAME);
    return temp;
  }
  else
  {
    return "";
  }
}
void setting_select_none_state(void)
{
  ESP_LOGI(TAG, "Setting menu none selected");
}
void setting_select_wifi_state(void)
{
  ESP_LOGI(TAG, "Setting WiFi entry action execute");
  display_setting_wifi_scanning(true);
  Selected_Setting_State = SETTING_WIFI_STATE;
  Selected_Setting_WiFi_State = SETTING_WIFI_SELECTION_STATE;
  // suspend wifi manager task so after disconnecting it not retry to connect
  vTaskSuspend(WiFi_Task_Handle);
  vTaskDelay(100 / portTICK_RATE_MS);
  esp_wifi_disconnect();
  esp_err_t err = esp_wifi_scan_start(NULL, true);
  // retrying scanning untill it returns ESP_OK
  while (err != ESP_OK)
  {
    err = esp_wifi_scan_start(NULL, true);
    vTaskDelay(100 / portTICK_RATE_MS);
  }
  err = esp_wifi_scan_get_ap_num(&setting_found_ap_count);
  if (err != ESP_OK)
  {
    print_connection_failed_scrn(true);
    setting_found_ap_count = 0;
    Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
    Selected_Setting_State = SETTING_IDEAL_STATE;
    vTaskResume(WiFi_Task_Handle);
    vTaskDelay(1000 / portTICK_RATE_MS);
    setting_entry_action_handler();
    clear_keypad();
    return;
  }
  setting_found_ap_info = (wifi_ap_record_t *)calloc(setting_found_ap_count, sizeof(wifi_ap_record_t));
  err = esp_wifi_scan_get_ap_records(&setting_found_ap_count, setting_found_ap_info);
  if (err != ESP_OK)
  {
    print_connection_failed_scrn(true);
    setting_found_ap_count = 0;
    free(setting_found_ap_info);
    Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
    Selected_Setting_State = SETTING_IDEAL_STATE;
    vTaskResume(WiFi_Task_Handle);
    vTaskDelay(1000 / portTICK_RATE_MS);
    setting_entry_action_handler();
    clear_keypad();
    return;
  }
  if (setting_found_ap_count == 0)
  {
    print_wifi_not_found_scrn(true);
    setting_found_ap_count = 0;
    free(setting_found_ap_info);
    Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
    Selected_Setting_State = SETTING_IDEAL_STATE;
    vTaskResume(WiFi_Task_Handle);
    vTaskDelay(1000 / portTICK_RATE_MS);
    setting_entry_action_handler();
    clear_keypad();
    return;
  }
  ESP_LOGI(TAG, "Scand done and Network found: %d", setting_found_ap_count);
  if (setting_found_ap_count == 1)
  {
    display_setting_ssid(true, (char *)setting_found_ap_info[0].ssid);
  }
  else if (setting_found_ap_count > 1)
  {
    display_setting_ssid(0, setting_found_ap_count, 'R', (char *)setting_found_ap_info[0].ssid, (char *)setting_found_ap_info[1].ssid);
  }
}
void setting_select_log_state(void)
{
  File_Operation setting_login_file;
  if (setting_login_file.m_exists(AUTH_TOKEN) || setting_login_file.m_exists(ACCS_TOKEN) || setting_login_file.m_exists(USERNAME) || setting_login_file.m_exists(CASHBOOK_ID))
  {
    setting_login_file.m_remove(AUTH_TOKEN);
    setting_login_file.m_remove(ACCS_TOKEN);
    setting_login_file.m_remove(CASHBOOK_ID);
    setting_login_file.m_remove(USERNAME);
    setting_ideal_state_helper_print_menu('R', 2);
  }
  else
  {
    if (get_wifi_status())
    {
      Selected_Setting_State = SETTING_LOG_STATE;
      Selected_Setting_Log_State = SETTING_LOG_MOBILE_STATE;
      display_setting_log_mobile_input(true);
    }
    else
    {
      display_err_wifi_not_connected(true);
      vTaskDelay(1000 / portTICK_RATE_MS);
      setting_ideal_state_helper_print_menu('R', 2);
    }
  }
  clear_keypad();
}
void setting_select_gst_state(void)
{
  Selected_Setting_State = SETTING_GST_STATE;
  Selected_Setting_GST_State = SETTING_GST_MAX_STATE;
  setting_gst_idx = 0;
  print_gst_rate_set_screen(true, setting_gst_idx, 1, std::to_string(gst_rate[0]).c_str(), std::to_string(gst_rate[1]).c_str());
}
void setting_select_info_state(void)
{
  Selected_Setting_State = SETTING_INFO_STATE;
  Selected_Setting_Info_State = SETTING_INFO_FIRMWARE_STATE;
  std::string menu_str_1;
  std::string menu_str_2;
  menu_str_1 = "Firmware :" + get_firmware_version();
  menu_str_2 = "Hardware Ver: " + get_hardware_version();
  print_system_info(true, 1, 1, menu_str_1.c_str(), menu_str_2.c_str());
}
void setting_select_update_state(void)
{
  if (get_wifi_status())
  {
    if (setting_is_logged_in())
    {
      print_checking_update(true);
      std::string update_available = "";
      int rsp = check_for_update(update_available, fm_ver);
      if (rsp == 200)
      {
        if (update_available != "0" && update_available != "")
        {
          menu_pin.menu = 0;
          menu_pin.sub_menu = 0;
          Selected_Setting_State = SETTING_MAX_STATE;
          mode_now = UPDATE_MODE;
          show_update_popup(true, fm_ver.c_str());
        }
        else
        {
          print_no_update_available(true);
          vTaskDelay(3000 / portTICK_RATE_MS);
          setting_ideal_state_helper_print_menu('R', 5);
        }
      }
      else if (rsp == 401)
      {
        rsp = refresh_token();
        if (rsp == 200)
        {
          rsp = check_for_update(update_available, fm_ver);
          if (rsp == 200)
          {
            if (update_available != "0" && update_available != "")
            {
              menu_pin.menu = 0;
              menu_pin.sub_menu = 0;
              Selected_Setting_State = SETTING_MAX_STATE;
              mode_now = UPDATE_MODE;
              show_update_popup(true, fm_ver.c_str());
            }
            else
            {
              print_no_update_available(true);
              vTaskDelay(3000 / portTICK_RATE_MS);
              setting_ideal_state_helper_print_menu('R', 5);
            }
          }
          else
          {
            print_connection_failed_scrn(true);
            vTaskDelay(3000 / portTICK_RATE_MS);
            setting_ideal_state_helper_print_menu('R', 5);
          }
        }
        else if (rsp == 401)
        {
          log_out_user();
          display_err_not_login(true);
          vTaskDelay(1000 / portTICK_RATE_MS);
          setting_ideal_state_helper_print_menu('R', 5);
        }
        else
        {
          print_connection_failed_scrn(true);
          vTaskDelay(3000 / portTICK_RATE_MS);
          setting_ideal_state_helper_print_menu('R', 5);
        }
      }
      else
      {
        print_connection_failed_scrn(true);
        vTaskDelay(3000 / portTICK_RATE_MS);
        setting_ideal_state_helper_print_menu('R', 5);
      }
    }
    else
    {
      display_err_not_login(true);
      vTaskDelay(1000 / portTICK_RATE_MS);
      setting_ideal_state_helper_print_menu('R', 5);
    }
    clear_keypad();
  }
  else
  {
    display_err_wifi_not_connected(true);
    vTaskDelay(1000 / portTICK_RATE_MS);
    setting_ideal_state_helper_print_menu('R', 5);
  }
}
void setting_select_help_state(void)
{
  Selected_Setting_State = SETTING_HELP_STATE;
  Selected_Setting_Help_State = SETTING_HELP_SHOW_STATE;
  print_help_qr_code(true);
}
void setting_ideal_state_helper_print_menu(uint8_t key, int show_menu)
{
  switch (show_menu)
  {
  case 1:
  {
    ESP_LOGI(TAG, "MENU: %d", show_menu);
    ESP_LOGI(TAG, "SSID: %s", setting_get_ssid().c_str());
    ESP_LOGI(TAG, "LOGIN: %s", setting_get_login_detail().c_str());
    display_show_setting_menu(true, show_menu, key, setting_get_ssid().c_str(), setting_get_login_detail().c_str());
    break;
  }
  case 2:
  {
    if (key == 'R')
    {
      ESP_LOGI(TAG, "MENU: %d", show_menu);
      ESP_LOGI(TAG, "SSID: %s", setting_get_ssid().c_str());
      ESP_LOGI(TAG, "LOGIN: %s", setting_get_login_detail().c_str());
      display_show_setting_menu(true, show_menu, key, setting_get_ssid().c_str(), setting_get_login_detail().c_str());
    }
    else if (key == 'L')
    {
      ESP_LOGI(TAG, "MENU: %d", show_menu);
      ESP_LOGI(TAG, "LOGIN: %s", setting_get_login_detail().c_str());
      display_show_setting_menu(true, show_menu, key, setting_get_login_detail().c_str());
    }
    break;
  }
  case 3:
  {
    if (key == 'R')
    {
      ESP_LOGI(TAG, "MENU: %d", show_menu);
      ESP_LOGI(TAG, "LOGIN: %s", setting_get_login_detail().c_str());
      display_show_setting_menu(true, show_menu, key, setting_get_login_detail().c_str());
    }
    else if (key == 'L')
    {
      ESP_LOGI(TAG, "MENU: %d", show_menu);
      display_show_setting_menu(true, show_menu, key);
    }
    break;
  }
  case 4:
  {
    ESP_LOGI(TAG, "MENU: %d", show_menu);
    display_show_setting_menu(true, show_menu, key);
    break;
  }
  case 5:
  {
    ESP_LOGI(TAG, "MENU: %d", show_menu);
    display_show_setting_menu(true, show_menu, key);
    break;
  }
  case 6:
  {
    ESP_LOGI(TAG, "MENU: %d", show_menu);
    display_show_setting_menu(true, show_menu, key);
    break;
  }
  default:
    break;
  }
}
uint8_t setting_wifi_password_shift_mode_ABC_parse_key(uint8_t key, uint8_t key_type, char last_key)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    switch (key)
    {
    case '1':
    {
      if (last_key == 'C')
      {
        return 'A';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '2':
    {

      if (last_key == 'F')
      {
        return 'D';
      }
      else
      {
        return ++last_key;
      }

      break;
    }
    case '3':
    {

      if (last_key == 'I')
      {
        return 'G';
      }
      else
      {
        return ++last_key;
      }

      break;
    }
    case '4':
    {

      if (last_key == 'L')
      {
        return 'J';
      }
      else
      {
        return ++last_key;
      }

      break;
    }
    case '5':
    {

      if (last_key == 'O')
      {
        return 'M';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '6':
    {

      if (last_key == 'R')
      {
        return 'P';
      }
      else
      {
        return ++last_key;
      }

      break;
    }
    case '7':
    {
      if (last_key == 'U')
      {
        return 'S';
      }
      else
      {
        return ++last_key;
      }

      break;
    }
    case '8':
    {

      if (last_key == 'X')
      {
        return 'V';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '9':
    {
      if (last_key == 'Z')
      {
        return 'Y';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '0':
    {
      if (last_key == '#')
        return '!';
      else if (last_key == '!')
        return '@';
      else if (last_key == '@')
        return '#';
      else
        return '!';
      break;
    }
    case 'D':
    {
      if (last_key == '$')
        return '^';
      else if (last_key == '^')
        return '&';
      else if (last_key == '&')
        return '$';
      else
        return '$';
      break;
    }
    case '.':
    {
      if (last_key == '.')
        return ' ';
      else if (last_key == ' ')
        return '?';
      else if (last_key == '?')
        return '.';
      else
        return '.';
      break;
    }
    }
    break;
  }
  case SIG_KEY:
  {
    switch (key)
    {
    case '+':
    {
      if (last_key == '+')
        return '{';
      else if (last_key == '{')
        return '}';
      else if (last_key == '}')
        return '+';
      else
        return '+';
      break;
    }
    case '-':
    {
      if (last_key == '-')
        return '[';
      else if (last_key == '[')
        return ']';
      else if (last_key == ']')
        return '-';
      else
        return '-';
      break;
    }
    case '*':
    {
      if (last_key == '*')
        return '(';
      else if (last_key == '(')
        return ')';
      else if (last_key == ')')
        return '*';
      else
        return '*';
      break;
    }
    case '/':
    {
      if (last_key == '/')
        return '\\';
      else if (last_key == '\\')
        return '"';
      else if (last_key == '"')
        return '/';
      else
        return '/';
      break;
    }
    case '%':
    {
      if (last_key == '%')
        return ':';
      else if (last_key == ':')
        return '|';
      else if (last_key == '|')
        return '%';
      else
        return '%';
      break;
    }
    }
    break;
  }
  case MEM_KEY:
  {
    switch (key)
    {
    case '+':
    {
      if (last_key == '<')
        return ';';
      else if (last_key == ';')
        return '>';
      else if (last_key == '>')
        return '<';
      else
        return '<';
      break;
    }
    case '-':
    {
      if (last_key == '_')
        return '\'';
      else if (last_key == '\'')
        return '~';
      else if (last_key == '~')
        return '_';
      else
        return '_';
      break;
    }
    case '=':
    {
      if (last_key == '=')
        return '^';
      else if (last_key == '^')
        return '`';
      else if (last_key == '`')
        return '=';
      else
        return '=';
      break;
    }
    }
    break;
  }
  }
  return 0;
}
uint8_t setting_wifi_password_shift_mode_abc_parse_key(uint8_t key, uint8_t key_type, char last_key)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    switch (key)
    {
    case '1':
    {
      if (last_key == 'c')
      {
        return 'a';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '2':
    {
      if (last_key == 'f')
      {
        return 'd';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '3':
    {
      if (last_key == 'i')
      {
        return 'g';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '4':
    {
      if (last_key == 'l')
      {
        return 'j';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '5':
    {
      if (last_key == 'o')
      {
        return 'm';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '6':
    {
      if (last_key == 'r')
      {
        return 'p';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '7':
    {
      if (last_key == 'u')
      {
        return 's';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '8':
    {
      if (last_key == 'x')
      {
        return 'v';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '9':
    {
      if (last_key == 'z')
      {
        return 'y';
      }
      else
      {
        return ++last_key;
      }
      break;
    }
    case '0':
    {
      if (last_key == '#')
        return '!';
      else if (last_key == '!')
        return '@';
      else if (last_key == '@')
        return '#';
      else
        return '!';
      break;
    }
    case 'D':
    {
      if (last_key == '$')
        return '^';
      else if (last_key == '^')
        return '&';
      else if (last_key == '&')
        return '$';
      else
        return '$';
      break;
    }
    case '.':
    {
      if (last_key == '.')
        return ' ';
      else if (last_key == ' ')
        return '?';
      else if (last_key == '?')
        return '.';
      else
        return '.';
      break;
    }
    }
    break;
  }
  case SIG_KEY:
  {
    switch (key)
    {
    case '+':
    {
      if (last_key == '+')
        return '{';
      else if (last_key == '{')
        return '}';
      else if (last_key == '}')
        return '+';
      else
        return '+';
      break;
    }
    case '-':
    {
      if (last_key == '-')
        return '[';
      else if (last_key == '[')
        return ']';
      else if (last_key == ']')
        return '-';
      else
        return '-';
      break;
    }
    case '*':
    {
      if (last_key == '*')
        return '(';
      else if (last_key == '(')
        return ')';
      else if (last_key == ')')
        return '*';
      else
        return '*';
      break;
    }
    case '/':
    {
      if (last_key == '/')
        return '\\';
      else if (last_key == '\\')
        return '"';
      else if (last_key == '"')
        return '/';
      else
        return '/';
      break;
    }
    case '%':
    {
      if (last_key == '%')
        return ':';
      else if (last_key == ':')
        return '|';
      else if (last_key == '|')
        return '%';
      else
        return '%';
      break;
    }
    }
    break;
  }
  case MEM_KEY:
  {
    switch (key)
    {
    case '+':
    {
      if (last_key == '<')
        return ';';
      else if (last_key == ';')
        return '>';
      else if (last_key == '>')
        return '<';
      else
        return '<';
      break;
    }
    case '-':
    {
      if (last_key == '_')
        return '\'';
      else if (last_key == '\'')
        return '~';
      else if (last_key == '~')
        return '_';
      else
        return '_';
      break;
    }
    case '=':
    {
      if (last_key == '=')
        return '^';
      else if (last_key == '^')
        return '`';
      else if (last_key == '`')
        return '=';
      else
        return '=';
      break;
    }
    }
    break;
  }
  }
  return 0;
}
uint8_t setting_wifi_password_shift_mode_ABC_add_last_char(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    switch (key)
    {
    case '1':
    {
      return 'A';
      break;
    }
    case '2':
    {
      return 'D';

      break;
    }
    case '3':
    {

      return 'G';

      break;
    }
    case '4':
    {

      return 'J';

      break;
    }
    case '5':
    {

      return 'M';

      break;
    }
    case '6':
    {

      return 'P';

      break;
    }
    case '7':
    {
      return 'S';

      break;
    }
    case '8':
    {

      return 'V';

      break;
    }
    case '9':
    {
      return 'Y';

      break;
    }
    case '0':
    {
      return '!';
      break;
    }
    case 'D':
    {
      return '$';
      break;
    }
    case '.':
    {
      return '.';
      break;
    }
    }
    break;
  }
  case SIG_KEY:
  {
    switch (key)
    {
    case '+':
    {
      return '+';
      break;
    }
    case '-':
    {
      return '-';
      break;
    }
    case '*':
    {
      return '*';
      break;
    }
    case '/':
    {
      return '/';
      break;
    }
    case '%':
    {
      return '%';
      break;
    }
    }
    break;
  }
  case MEM_KEY:
  {
    switch (key)
    {
    case '+':
    {
      return '<';
      break;
    }
    case '-':
    {
      return '_';
      break;
    }
    case '=':
    {
      return '=';
      break;
    }
    }
    break;
  }
  }
  return 0;
}
uint8_t setting_wifi_password_shift_mode_abc_add_last_char(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    switch (key)
    {
    case '1':
    {
      return 'a';
      break;
    }
    case '2':
    {
      return 'd';

      break;
    }
    case '3':
    {

      return 'g';

      break;
    }
    case '4':
    {

      return 'j';

      break;
    }
    case '5':
    {

      return 'm';

      break;
    }
    case '6':
    {

      return 'p';

      break;
    }
    case '7':
    {
      return 's';

      break;
    }
    case '8':
    {

      return 'v';

      break;
    }
    case '9':
    {
      return 'y';

      break;
    }
    case '0':
    {
      return '!';
      break;
    }
    case 'D':
    {
      return '$';
      break;
    }
    case '.':
    {
      return '.';
      break;
    }
    }
    break;
  }
  case SIG_KEY:
  {
    switch (key)
    {
    case '+':
    {
      return '+';
      break;
    }
    case '-':
    {
      return '-';
      break;
    }
    case '*':
    {
      return '*';
      break;
    }
    case '/':
    {
      return '/';
      break;
    }
    case '%':
    {
      return '%';
      break;
    }
    }
    break;
  }
  case MEM_KEY:
  {
    switch (key)
    {
    case '+':
    {
      return '<';
      break;
    }
    case '-':
    {
      return '_';
      break;
    }
    case '=':
    {
      return '=';
      break;
    }
    }
    break;
  }
  }
  return 0;
}
void setting_wifi_password_shift_mode_abc_helper(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type)
{
  if (*setting_wifi_password_last_key_type == key_type && *setting_wifi_password_last_key == key)
  {
    if ((esp_timer_get_time() - *setting_wifi_password_last_key_press_time) < PSW_INPUT_INTERVAL * 1000)
    {
      char last_char = setting_wifi_password_input[setting_wifi_password_input_idx];
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_parse_key(key, key_type, last_char);
    }
    else
    {
      setting_wifi_password_input_idx++;
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_add_last_char(key, key_type);
    }
  }
  else
  {
    if (setting_wifi_password_input_idx == 0 && *setting_wifi_password_last_key_type == 0 && *setting_wifi_password_last_key == 0 && setting_wifi_password_input[0] == 0)
    {
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_add_last_char(key, key_type);
    }
    else
    {
      setting_wifi_password_input_idx++;
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_add_last_char(key, key_type);
    }
  }
  *setting_wifi_password_last_key_type = key_type;
  *setting_wifi_password_last_key = key;
  *setting_wifi_password_last_key_press_time = esp_timer_get_time();
}
void setting_wifi_password_shift_mode_ABC_helper(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type)
{
  if (*setting_wifi_password_last_key_type == key_type && *setting_wifi_password_last_key == key)
  {
    if ((esp_timer_get_time() - *setting_wifi_password_last_key_press_time) < PSW_INPUT_INTERVAL * 1000)
    {
      char last_char = setting_wifi_password_input[setting_wifi_password_input_idx];
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_parse_key(key, key_type, last_char);
    }
    else
    {
      setting_wifi_password_input_idx++;
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_add_last_char(key, key_type);
    }
  }
  else
  {
    if (setting_wifi_password_input_idx == 0 && *setting_wifi_password_last_key_type == 0 && *setting_wifi_password_last_key == 0 && setting_wifi_password_input[0] == 0)
    {
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_add_last_char(key, key_type);
    }
    else
    {
      setting_wifi_password_input_idx++;
      setting_wifi_password_input[setting_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_add_last_char(key, key_type);
    }
  }
  *setting_wifi_password_last_key_type = key_type;
  *setting_wifi_password_last_key = key;
  *setting_wifi_password_last_key_press_time = esp_timer_get_time();
}
void setting_wifi_password_shift_mode_abc(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type, int *selected_wifi)
{
  ESP_LOGI(TAG, "setting_wifi_password_shift_mode_abc calling");
  switch (key_type)
  {
  case NUM_KEY:
  {
    setting_wifi_password_shift_mode_abc_helper(setting_wifi_password_last_key_press_time, setting_wifi_password_last_key_type, setting_wifi_password_last_key, key, key_type);
    char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
    memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
    temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
    print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      ESP_LOGI(TAG, "WiFi: %s", (char *)setting_found_ap_info[*selected_wifi].ssid);
      ESP_LOGI(TAG, "Password: %s", setting_wifi_password_input);
      wifi_config_t setting_wifi_sta_config = {};
      memcpy(setting_wifi_sta_config.sta.ssid, setting_found_ap_info[*selected_wifi].ssid, strlen((char *)setting_found_ap_info[*selected_wifi].ssid));
      memcpy(setting_wifi_sta_config.sta.password, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
      setting_wifi_sta_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
      setting_wifi_sta_config.sta.pmf_cfg.capable = false;
      setting_wifi_sta_config.sta.pmf_cfg.required = false;
      esp_err_t err = esp_wifi_set_config(WIFI_IF_STA, &setting_wifi_sta_config);
      if (err != ESP_OK)
      {
        print_connection_failed_scrn(true);
        *setting_wifi_password_last_key_type = 0;
        *setting_wifi_password_last_key = 0;
        setting_wifi_password_input_idx = 0;
        memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
        Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
        setting_found_ap_count = 0;
        free(setting_found_ap_info);
        vTaskResume(WiFi_Task_Handle);
        Selected_Setting_State = SETTING_IDEAL_STATE;
        vTaskDelay(1000 / portTICK_RATE_MS);
        setting_entry_action_handler();
        clear_keypad();
        return;
      }
      print_connecting_scrn(true);
      err = esp_wifi_connect();
      ESP_LOGI(TAG, "Error: %s", esp_err_to_name(err));
      for (int i = 0; i < 10; i++)
      {
        EventBits_t setting_wifi_bits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10000 / portTICK_PERIOD_MS);
        if (setting_wifi_bits & WIFI_CONNECTED_BIT)
        {
          ESP_LOGI(TAG, "connected to ap SSID");
          break;
        }
        else if (setting_wifi_bits & WIFI_FAIL_BIT)
        {
          ESP_LOGI(TAG, "Failed to connect to SSID");
          esp_wifi_connect();
        }
        else
        {
          ESP_LOGE(TAG, "UNEXPECTED EVENT");
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
      }
      if (get_wifi_status())
      {
        print_connected_scrn(true);
      }
      else
      {
        print_connection_failed_scrn(true);
      }
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      setting_wifi_password_input_idx = 0;
      memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
      Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
      setting_found_ap_count = 0;
      free(setting_found_ap_info);
      vTaskResume(WiFi_Task_Handle);
      Selected_Setting_State = SETTING_IDEAL_STATE;
      vTaskDelay(1000 / portTICK_RATE_MS);
      setting_entry_action_handler();
      clear_keypad();
    }
    else
    {
      setting_wifi_password_shift_mode_abc_helper(setting_wifi_password_last_key_press_time, setting_wifi_password_last_key_type, setting_wifi_password_last_key, key, key_type);
      char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
      memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
      temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
      print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    }
    break;
  }
  case MEM_KEY:
  {
    setting_wifi_password_shift_mode_abc_helper(setting_wifi_password_last_key_press_time, setting_wifi_password_last_key_type, setting_wifi_password_last_key, key, key_type);
    char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
    memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
    temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
    print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    break;
  }
  case CHK_CRT_KEY:
  {
    if (key == 'D')
    {
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      if (setting_wifi_password_input_idx >= 0)
      {
        setting_wifi_password_input[setting_wifi_password_input_idx] = 0;
        setting_wifi_password_input_idx--;
        if (setting_wifi_password_input_idx < 0)
          setting_wifi_password_input_idx = 0;
        char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
        temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
      }
    }
    break;
  }
  case SPC_KEY:
  {
    if (key == 'S')
    {
      setting_wifi_password_shift_mode = 1;
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      print_in_mode(setting_wifi_password_shift_mode);
    }
    break;
  }
  case ERS_KEY:
  {
    *setting_wifi_password_last_key_type = 0;
    *setting_wifi_password_last_key = 0;
    setting_wifi_password_input_idx = 0;
    memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
    Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
    setting_found_ap_count = 0;
    free(setting_found_ap_info);
    vTaskResume(WiFi_Task_Handle);
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_entry_action_handler();
    }
    else if (key == 'C')
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    clear_keypad();
    break;
  }
  default:
  {
    break;
  }
  }
}
void setting_wifi_password_shift_mode_ABC(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type, int *selected_wifi)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    setting_wifi_password_shift_mode_ABC_helper(setting_wifi_password_last_key_press_time, setting_wifi_password_last_key_type, setting_wifi_password_last_key, key, key_type);
    char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
    memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
    temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
    print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      ESP_LOGI(TAG, "WiFi: %s", (char *)setting_found_ap_info[*selected_wifi].ssid);
      ESP_LOGI(TAG, "Password: %s", setting_wifi_password_input);
      wifi_config_t setting_wifi_sta_config = {};
      memcpy(setting_wifi_sta_config.sta.ssid, setting_found_ap_info[*selected_wifi].ssid, strlen((char *)setting_found_ap_info[*selected_wifi].ssid));
      memcpy(setting_wifi_sta_config.sta.password, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
      setting_wifi_sta_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
      setting_wifi_sta_config.sta.pmf_cfg.capable = false;
      setting_wifi_sta_config.sta.pmf_cfg.required = false;
      esp_err_t err = esp_wifi_set_config(WIFI_IF_STA, &setting_wifi_sta_config);
      if (err != ESP_OK)
      {
        print_connection_failed_scrn(true);
        *setting_wifi_password_last_key_type = 0;
        *setting_wifi_password_last_key = 0;
        setting_wifi_password_input_idx = 0;
        memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
        Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
        setting_found_ap_count = 0;
        free(setting_found_ap_info);
        vTaskResume(WiFi_Task_Handle);
        Selected_Setting_State = SETTING_IDEAL_STATE;
        vTaskDelay(1000 / portTICK_RATE_MS);
        setting_entry_action_handler();
        clear_keypad();
        return;
      }
      print_connecting_scrn(true);
      esp_wifi_connect();
      for (int i = 0; i < 10; i++)
      {
        EventBits_t setting_wifi_bits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10000 / portTICK_PERIOD_MS);
        if (setting_wifi_bits & WIFI_CONNECTED_BIT)
        {
          ESP_LOGI(TAG, "connected to ap SSID");
          break;
        }
        else if (setting_wifi_bits & WIFI_FAIL_BIT)
        {
          ESP_LOGI(TAG, "Failed to connect to SSID");
          esp_wifi_connect();
        }
        else
        {
          ESP_LOGE(TAG, "UNEXPECTED EVENT");
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
      }
      if (get_wifi_status())
      {
        print_connected_scrn(true);
      }
      else
      {
        print_connection_failed_scrn(true);
      }
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      setting_wifi_password_input_idx = 0;
      memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
      Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
      setting_found_ap_count = 0;
      free(setting_found_ap_info);
      vTaskResume(WiFi_Task_Handle);
      Selected_Setting_State = SETTING_IDEAL_STATE;
      vTaskDelay(1000 / portTICK_RATE_MS);
      setting_entry_action_handler();
      clear_keypad();
    }
    else
    {
      setting_wifi_password_shift_mode_ABC_helper(setting_wifi_password_last_key_press_time, setting_wifi_password_last_key_type, setting_wifi_password_last_key, key, key_type);
      char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
      memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
      temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
      print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    }
    break;
  }
  case MEM_KEY:
  {
    setting_wifi_password_shift_mode_ABC_helper(setting_wifi_password_last_key_press_time, setting_wifi_password_last_key_type, setting_wifi_password_last_key, key, key_type);
    char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
    memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
    temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
    print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    break;
  }
  case CHK_CRT_KEY:
  {
    if (key == 'D')
    {
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      if (setting_wifi_password_input_idx >= 0)
      {
        setting_wifi_password_input[setting_wifi_password_input_idx] = 0;
        setting_wifi_password_input_idx--;
        if (setting_wifi_password_input_idx < 0)
          setting_wifi_password_input_idx = 0;
        char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
        temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
      }
    }
    break;
  }
  case SPC_KEY:
  {
    if (key == 'S')
    {
      setting_wifi_password_shift_mode = 2;
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      print_in_mode(setting_wifi_password_shift_mode);
    }
    break;
  }
  case ERS_KEY:
  {
    *setting_wifi_password_last_key_type = 0;
    *setting_wifi_password_last_key = 0;
    setting_wifi_password_input_idx = 0;
    memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
    Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
    setting_found_ap_count = 0;
    free(setting_found_ap_info);
    vTaskResume(WiFi_Task_Handle);
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_entry_action_handler();
    }
    else if (key == 'C')
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    clear_keypad();
    break;
  }
  default:
  {
    break;
  }
  }
}
void setting_wifi_password_shift_mode_123(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type, int *selected_wifi)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    if (key != 'D' && key != '.')
    {
      if (setting_wifi_password_input_idx == 0 && *setting_wifi_password_last_key_type == 0 && *setting_wifi_password_last_key == 0 && setting_wifi_password_input[0] == 0)
      {
        setting_wifi_password_input[setting_wifi_password_input_idx] = key;
      }
      else
      {
        setting_wifi_password_input_idx++;
        setting_wifi_password_input[setting_wifi_password_input_idx] = key;
      }
      *setting_wifi_password_last_key_type = key_type;
      *setting_wifi_password_last_key = key;
      *setting_wifi_password_last_key_press_time = esp_timer_get_time();
      char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
      memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
      temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
      print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
    }
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      ESP_LOGI(TAG, "WiFi: %s", (char *)setting_found_ap_info[*selected_wifi].ssid);
      ESP_LOGI(TAG, "Password: %s", setting_wifi_password_input);
      wifi_config_t setting_wifi_sta_config = {};
      memcpy(setting_wifi_sta_config.sta.ssid, setting_found_ap_info[*selected_wifi].ssid, strlen((char *)setting_found_ap_info[*selected_wifi].ssid));
      memcpy(setting_wifi_sta_config.sta.password, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
      setting_wifi_sta_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
      setting_wifi_sta_config.sta.pmf_cfg.capable = false;
      setting_wifi_sta_config.sta.pmf_cfg.required = false;
      esp_err_t err = esp_wifi_set_config(WIFI_IF_STA, &setting_wifi_sta_config);
      if (err != ESP_OK)
      {
        print_connection_failed_scrn(true);
        *setting_wifi_password_last_key_type = 0;
        *setting_wifi_password_last_key = 0;
        setting_wifi_password_input_idx = 0;
        memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
        Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
        setting_found_ap_count = 0;
        free(setting_found_ap_info);
        vTaskResume(WiFi_Task_Handle);
        Selected_Setting_State = SETTING_IDEAL_STATE;
        vTaskDelay(1000 / portTICK_RATE_MS);
        setting_entry_action_handler();
        clear_keypad();
        return;
      }
      print_connecting_scrn(true);
      err = esp_wifi_connect();
      ESP_LOGI(TAG, "Error: %s", esp_err_to_name(err));
      for (int i = 0; i < 10; i++)
      {
        EventBits_t setting_wifi_bits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10000 / portTICK_PERIOD_MS);
        if (setting_wifi_bits & WIFI_CONNECTED_BIT)
        {
          ESP_LOGI(TAG, "connected to ap SSID");
          break;
        }
        else if (setting_wifi_bits & WIFI_FAIL_BIT)
        {
          ESP_LOGI(TAG, "Failed to connect to SSID");
          esp_wifi_connect();
        }
        else
        {
          ESP_LOGE(TAG, "UNEXPECTED EVENT");
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
      }
      if (get_wifi_status())
      {
        print_connected_scrn(true);
      }
      else
      {
        print_connection_failed_scrn(true);
      }
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      setting_wifi_password_input_idx = 0;
      memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
      Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
      setting_found_ap_count = 0;
      free(setting_found_ap_info);
      vTaskResume(WiFi_Task_Handle);
      Selected_Setting_State = SETTING_IDEAL_STATE;
      vTaskDelay(1000 / portTICK_RATE_MS);
      setting_entry_action_handler();
      clear_keypad();
    }
    break;
  }
  case CHK_CRT_KEY:
  {
    if (key == 'D')
    {
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      if (setting_wifi_password_input_idx >= 0)
      {
        setting_wifi_password_input[setting_wifi_password_input_idx] = 0;
        setting_wifi_password_input_idx--;
        if (setting_wifi_password_input_idx < 0)
          setting_wifi_password_input_idx = 0;
        char temp_psw_buf[setting_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setting_wifi_password_input, setting_wifi_password_input_idx + 1);
        temp_psw_buf[setting_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setting_wifi_password_input_idx + 1, setting_wifi_password_shift_mode);
      }
    }
    break;
  }
  case SPC_KEY:
  {
    if (key == 'S')
    {
      setting_wifi_password_shift_mode = 0;
      *setting_wifi_password_last_key_type = 0;
      *setting_wifi_password_last_key = 0;
      print_in_mode(setting_wifi_password_shift_mode);
    }
    break;
  }
  case ERS_KEY:
  {
    *setting_wifi_password_last_key_type = 0;
    *setting_wifi_password_last_key = 0;
    setting_wifi_password_input_idx = 0;
    memset(setting_wifi_password_input, 0, sizeof(setting_wifi_password_input));
    Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
    setting_found_ap_count = 0;
    free(setting_found_ap_info);
    vTaskResume(WiFi_Task_Handle);
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_entry_action_handler();
    }
    else if (key == 'C')
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    clear_keypad();
    break;
  }
  default:
  {
    break;
  }
  }
}
void setting_wifi_selection_state_handler(uint8_t key, uint8_t key_type, int *selected_wifi)
{
  static int wifi_idx = 0;
  switch (key_type)
  {
  case ARW_KEY:
  {
    if (setting_found_ap_count == 1)
    {
      display_setting_ssid(true, (char *)setting_found_ap_info[0].ssid);
    }
    else
    {
      if (key == 'R')
      {
        wifi_idx++;
        if (wifi_idx == setting_found_ap_count)
        {
          wifi_idx = 0;
          ESP_LOGI(TAG, "Selected Network: %d", wifi_idx);
          display_setting_ssid(wifi_idx, setting_found_ap_count, key, (char *)setting_found_ap_info[wifi_idx].ssid, (char *)setting_found_ap_info[wifi_idx + 1].ssid);
        }
        else
        {
          ESP_LOGI(TAG, "Selected Network: %d", wifi_idx);
          display_setting_ssid(wifi_idx, setting_found_ap_count, key, (char *)setting_found_ap_info[wifi_idx - 1].ssid, (char *)setting_found_ap_info[wifi_idx].ssid);
        }
      }
      else if (key == 'L')
      {
        wifi_idx--;
        if (wifi_idx == -1)
        {
          wifi_idx = setting_found_ap_count - 1;
          ESP_LOGI(TAG, "Selected Network: %d", wifi_idx);
          display_setting_ssid(wifi_idx, setting_found_ap_count, key, (char *)setting_found_ap_info[wifi_idx - 1].ssid, (char *)setting_found_ap_info[wifi_idx].ssid);
        }
        else
        {
          ESP_LOGI(TAG, "Selected Network: %d", wifi_idx);
          display_setting_ssid(wifi_idx, setting_found_ap_count, key, (char *)setting_found_ap_info[wifi_idx].ssid, (char *)setting_found_ap_info[wifi_idx + 1].ssid);
        }
      }
    }
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      *selected_wifi = wifi_idx;
      Selected_Setting_WiFi_State = SETTING_WIFI_PASSWORD_STATE;
      setting_wifi_password_shift_mode = 0;
      print_psw_in_scrn(true);
      wifi_idx = 0;
    }
    break;
  }
  case ERS_KEY:
  {
    wifi_idx = 0;
    if (key == 'D')
    {
      Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_found_ap_count = 0;
      free(setting_found_ap_info);
      vTaskResume(WiFi_Task_Handle);
      setting_entry_action_handler();
      clear_keypad();
    }
    else if (key == 'C')
    {
      Selected_Setting_WiFi_State = SETTING_WIFI_MAX_STATE;
      Selected_Setting_State = SETTING_MAX_STATE;
      setting_found_ap_count = 0;
      free(setting_found_ap_info);
      vTaskResume(WiFi_Task_Handle);
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
      clear_keypad();
    }
    break;
  }
  default:
    break;
  }
}
void setting_wifi_password_state_handler(uint8_t key, uint8_t key_type, int *selected_wifi)
{
  static int64_t setting_wifi_password_last_key_press_time = 0;
  static uint8_t setting_wifi_password_last_key_type = 0;
  static uint8_t setting_wifi_password_last_key = 0;
  switch (setting_wifi_password_shift_mode)
  {
  case 0:
  {
    setting_wifi_password_shift_mode_abc(&setting_wifi_password_last_key_press_time, &setting_wifi_password_last_key_type, &setting_wifi_password_last_key, key, key_type, selected_wifi);
    break;
  }
  case 1:
  {
    setting_wifi_password_shift_mode_ABC(&setting_wifi_password_last_key_press_time, &setting_wifi_password_last_key_type, &setting_wifi_password_last_key, key, key_type, selected_wifi);
    break;
  }
  case 2:
  {
    setting_wifi_password_shift_mode_123(&setting_wifi_password_last_key_press_time, &setting_wifi_password_last_key_type, &setting_wifi_password_last_key, key, key_type, selected_wifi);
    break;
  }
  default:
  {
    break;
  }
  }
}
void setting_gst_0_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    if (key >= '0' && key <= '9')
    {
      add_to_gst_rate(key);
      std::string temp(gst_rate_input, gst_rate_input_idx);
      print_in_gst(true, setting_gst_idx, temp.c_str());
    }
    else if (key == '.')
    {
      add_to_gst_rate(key);
      std::string temp(gst_rate_input, gst_rate_input_idx);
      print_in_gst(true, setting_gst_idx, temp.c_str());
    }
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      Selected_Setting_GST_State = SETTING_GST_MAX_STATE;
      std::string temp(gst_rate_input, gst_rate_input_idx);

      try
      {
        gst_rate[setting_gst_idx] = std::stod(temp);
      }
      catch (std::invalid_argument const& err_thrw)
      {
        gst_rate[setting_gst_idx] = setting_gst_temp_store;
      }
      catch (std::out_of_range const& err_thrw)
      {
        gst_rate[setting_gst_idx] = setting_gst_temp_store;
      }

      setting_gst_idx = 0;
      gst_rate_input_idx = 0;
      memset(gst_rate_input, 0, sizeof(gst_rate_input));
      FILE *f = fopen(GST_RATE, "w");
      if (f == NULL)
      {
        ESP_LOGI(TAG, "Error in file opening");
      }
      else
      {
        fwrite(gst_rate, sizeof(gst_rate), 1, f);
        fclose(f);
      }
      print_gst_rate_set_screen(true, setting_gst_idx, 1, std::to_string(gst_rate[0]).c_str(), std::to_string(gst_rate[1]).c_str());
    }
    clear_keypad();
    break;
  }
  case CHK_CRT_KEY:
  {
    if (key == 'D')
    {
      correct_gst_temp_rate();
      std::string temp(gst_rate_input, gst_rate_input_idx);
      print_in_gst(true, setting_gst_idx, temp.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_GST_State = SETTING_GST_MAX_STATE;
    gst_rate[setting_gst_idx] = setting_gst_temp_store;
    setting_gst_idx = 0;
    setting_gst_temp_store = 0;
    gst_rate_input_idx = 0;
    memset(gst_rate_input, 0, sizeof(gst_rate_input));
    if (key == 'D')
    {
      print_gst_rate_set_screen(true, 0, 1, std::to_string(gst_rate[0]).c_str(), std::to_string(gst_rate[1]).c_str());
    }
    if (key == 'C')
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      clear_calculator();
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    clear_keypad();
    break;
  }
  }
}
void setting_gst_1_handler(uint8_t key, uint8_t key_type)
{
  setting_gst_0_handler(key, key_type);
}
void setting_gst_2_handler(uint8_t key, uint8_t key_type)
{
  setting_gst_0_handler(key, key_type);
}
void setting_gst_3_handler(uint8_t key, uint8_t key_type)
{
  setting_gst_0_handler(key, key_type);
}
void setting_gst_4_handler(uint8_t key, uint8_t key_type)
{
  setting_gst_0_handler(key, key_type);
}
void setting_info_firmware_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    std::string menu_str_1;
    std::string menu_str_2;
    if (key == 'R')
    {
      Selected_Setting_Info_State = SETTING_INFO_HARDWARE_STATE;
      menu_str_1 = "Firmware:" + get_firmware_version();
      menu_str_2 = "Hardware Ver: " + get_hardware_version();
      print_system_info(true, 2, 2, menu_str_1.c_str(), menu_str_2.c_str());
    }
    else
    {
      Selected_Setting_Info_State = SETTING_INFO_HELP_STATE;
      menu_str_1 = "Batch: " + get_batch_no();
      menu_str_2 = "Help: " + std::string(HELP_INFO);
      print_system_info(true, 6, 2, menu_str_1.c_str(), menu_str_2.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_Info_State = SETTING_INFO_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 4);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_info_hardware_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    std::string menu_str_1;
    std::string menu_str_2;
    if (key == 'R')
    {
      Selected_Setting_Info_State = SETTING_INFO_DEVICE_STATE;
      menu_str_1 = "Hardware Ver: " + get_hardware_version();
      menu_str_2 = "Device id: " + get_device_id();
      print_system_info(true, 3, 2, menu_str_1.c_str(), menu_str_2.c_str());
    }
    else
    {
      Selected_Setting_Info_State = SETTING_INFO_FIRMWARE_STATE;
      menu_str_1 = "Firmware:" + get_firmware_version();
      menu_str_2 = "Hardware Ver: " + get_hardware_version();
      print_system_info(true, 1, 1, menu_str_1.c_str(), menu_str_2.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_Info_State = SETTING_INFO_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 4);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_info_device_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    std::string menu_str_1;
    std::string menu_str_2;
    if (key == 'R')
    {
      Selected_Setting_Info_State = SETTING_INFO_MODEL_STATE;
      menu_str_1 = "Device id: " + get_device_id();
      menu_str_2 = "Model: " + get_model_no();
      print_system_info(true, 4, 2, menu_str_1.c_str(), menu_str_2.c_str());
    }
    else
    {
      Selected_Setting_Info_State = SETTING_INFO_HARDWARE_STATE;
      menu_str_1 = "Hardware Ver: " + get_hardware_version();
      menu_str_2 = "Device id: " + get_device_id();
      print_system_info(true, 2, 1, menu_str_1.c_str(), menu_str_2.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_Info_State = SETTING_INFO_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 4);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_info_model_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    std::string menu_str_1;
    std::string menu_str_2;
    if (key == 'R')
    {
      Selected_Setting_Info_State = SETTING_INFO_BATCH_STATE;
      menu_str_1 = "Model: " + get_model_no();
      menu_str_2 = "Batch: " + get_batch_no();
      print_system_info(true, 5, 2, menu_str_1.c_str(), menu_str_2.c_str());
    }
    else
    {
      Selected_Setting_Info_State = SETTING_INFO_DEVICE_STATE;
      menu_str_1 = "Device id: " + get_device_id();
      menu_str_2 = "Model: " + get_model_no();
      print_system_info(true, 3, 1, menu_str_1.c_str(), menu_str_2.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_Info_State = SETTING_INFO_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 4);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_info_batch_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    std::string menu_str_1;
    std::string menu_str_2;
    if (key == 'R')
    {
      Selected_Setting_Info_State = SETTING_INFO_HELP_STATE;
      menu_str_1 = "Batch: " + get_batch_no();
      menu_str_2 = "Help: " + std::string(HELP_INFO);
      print_system_info(true, 6, 2, menu_str_1.c_str(), menu_str_2.c_str());
    }
    else
    {
      Selected_Setting_Info_State = SETTING_INFO_MODEL_STATE;
      menu_str_1 = "Model: " + get_model_no();
      menu_str_2 = "Batch: " + get_batch_no();
      print_system_info(true, 4, 1, menu_str_1.c_str(), menu_str_2.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_Info_State = SETTING_INFO_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 4);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_info_help_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    std::string menu_str_1;
    std::string menu_str_2;
    if (key == 'R')
    {
      Selected_Setting_Info_State = SETTING_INFO_FIRMWARE_STATE;
      menu_str_1 = "Firmware:" + get_firmware_version();
      menu_str_2 = "Hardware Ver: " + get_hardware_version();
      print_system_info(true, 1, 1, menu_str_1.c_str(), menu_str_2.c_str());
    }
    else
    {
      Selected_Setting_Info_State = SETTING_INFO_MODEL_STATE;
      menu_str_1 = "Batch: " + get_batch_no();
      menu_str_2 = "Help: " + std::string(HELP_INFO);
      print_system_info(true, 5, 1, menu_str_1.c_str(), menu_str_2.c_str());
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_Info_State = SETTING_INFO_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 4);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_ideal_state_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case ARW_KEY:
  {
    if (key == 'R')
    {
      setting_selected_menu++;
      if (setting_selected_menu == 7)
        setting_selected_menu = 1;
    }
    else if (key == 'L')
    {
      setting_selected_menu--;
      if (setting_selected_menu == 0)
        setting_selected_menu = 6;
    }
    setting_ideal_state_helper_print_menu(key, setting_selected_menu);
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      setting_select_func_t setting_select_func_list[] = {
          setting_select_none_state,
          setting_select_wifi_state,
          setting_select_log_state,
          setting_select_gst_state,
          setting_select_info_state,
          setting_select_update_state,
          setting_select_help_state};
      (*setting_select_func_list[setting_selected_menu])();
    }
    break;
  }
  case ERS_KEY:
  {
    Selected_Setting_State = SETTING_MAX_STATE;
    mode_now = NORMAL_MODE;
    clear_calculator();
    print_normal_mode_scrn(true);
    display_trans_count(true, total_pending_trans);
    break;
  }
  default:
  {
    break;
  }
  }
}
void setting_log_mobile_state_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    if (key != 'D' && key != '.')
    {
      if (setting_log_mobile_input_idx < 10)
      {
        setting_log_mobile_input[setting_log_mobile_input_idx] = key;
        setting_log_mobile_input_idx++;
        char temp_mobile_buf[setting_log_mobile_input_idx + 1] = {0};
        memcpy(temp_mobile_buf, setting_log_mobile_input, setting_log_mobile_input_idx + 1);
        temp_mobile_buf[setting_log_mobile_input_idx] = '\0';
        print_mobile_no(true, temp_mobile_buf, setting_log_mobile_input_idx);
      }
    }
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      if (setting_log_mobile_input_idx == 10)
      {
        if (get_wifi_status())
        {
          setting_log_mobile_input[10] = '\0';
          print_sending_scrn(true);
          if (send_otp(setting_log_mobile_input))
          {
            print_otp_in_scrn(true);
            Selected_Setting_Log_State = SETTING_LOG_OTP_STATE;
          }
          else
          {
            print_connection_failed_scrn(true);
            vTaskDelay(1000 / portTICK_RATE_MS);
            setting_log_mobile_input_idx = 0;
            memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
            Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
            Selected_Setting_State = SETTING_IDEAL_STATE;
            setting_ideal_state_helper_print_menu('R', 2);
          }
        }
        else
        {
          Selected_Setting_State = SETTING_IDEAL_STATE;
          Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
          setting_log_mobile_input_idx = 0;
          memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
          display_err_wifi_not_connected(true);
          vTaskDelay(1000 / portTICK_RATE_MS);
          setting_ideal_state_helper_print_menu('R', 2);
        }
      }
      else
      {
        print_not_valid_no_scrn(true);
        vTaskDelay(1000 / portTICK_RATE_MS);
        display_setting_log_mobile_input(true);
        char temp_mobile_buf[setting_log_mobile_input_idx + 1] = {0};
        memcpy(temp_mobile_buf, setting_log_mobile_input, setting_log_mobile_input_idx + 1);
        temp_mobile_buf[setting_log_mobile_input_idx] = '\0';
        print_mobile_no(true, temp_mobile_buf, setting_log_mobile_input_idx);
      }
    }
    clear_keypad();
    break;
  }
  case CHK_CRT_KEY:
  {
    if (key == 'D')
    {
      if (setting_log_mobile_input_idx > 0)
      {
        setting_log_mobile_input_idx--;
        setting_log_mobile_input[setting_log_mobile_input_idx] = 0;
        char temp_mobile_buf[setting_log_mobile_input_idx + 1] = {0};
        memcpy(temp_mobile_buf, setting_log_mobile_input, setting_log_mobile_input_idx + 1);
        temp_mobile_buf[setting_log_mobile_input_idx] = '\0';
        print_mobile_no(true, temp_mobile_buf, setting_log_mobile_input_idx);
      }
    }
    break;
  }
  case ERS_KEY:
  {
    setting_log_mobile_input_idx = 0;
    memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
    Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 2);
    }
    else if (key == 'C')
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_log_otp_state_handler(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    if (key != 'D' && key != '.')
    {
      if (setting_log_otp_input_idx < 4)
      {
        setting_log_otp_input[setting_log_otp_input_idx] = key;
        setting_log_otp_input_idx++;
        char temp_otp_buf[setting_log_otp_input_idx + 1] = {0};
        memcpy(temp_otp_buf, setting_log_otp_input, setting_log_otp_input_idx + 1);
        temp_otp_buf[setting_log_otp_input_idx] = '\0';
        print_otp_no(true, temp_otp_buf, setting_log_otp_input_idx);
      }
    }
    break;
  }
  case SIG_KEY:
  {
    if (key == '=')
    {
      if (setting_log_otp_input_idx == 4)
      {
        if (get_wifi_status())
        {
          setting_log_otp_input[4] = '\0';
          print_validating_scrn(true);
          if (veryfy_otp(setting_log_mobile_input, setting_log_otp_input))
          {
            display_setting_log_otp_login_success(true);
            vTaskDelay(1000 / portTICK_RATE_MS);
            File_Operation save_username;
            if (save_username.m_exists(USERNAME))
              save_username.m_remove(USERNAME);
            save_username.m_write(USERNAME, setting_log_mobile_input);
            setting_log_otp_input_idx = 0;
            memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
            setting_log_mobile_input_idx = 0;
            memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
            Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
            Selected_Setting_State = SETTING_IDEAL_STATE;
            setting_ideal_state_helper_print_menu('R', 2);
          }
          else
          {
            display_setting_log_otp_validation_failed(true);
            vTaskDelay(1000 / portTICK_RATE_MS);
            setting_log_otp_input_idx = 0;
            memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
            setting_log_mobile_input_idx = 0;
            memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
            Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
            Selected_Setting_State = SETTING_IDEAL_STATE;
            setting_ideal_state_helper_print_menu('R', 2);
          }
        }
        else
        {
          Selected_Setting_State = SETTING_IDEAL_STATE;
          Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
          display_err_wifi_not_connected(true);
          vTaskDelay(1000 / portTICK_RATE_MS);
          setting_log_otp_input_idx = 0;
          memset(setting_log_otp_input, 0, sizeof(setting_log_otp_input));
          setting_log_mobile_input_idx = 0;
          memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
          setting_ideal_state_helper_print_menu('R', 2);
        }
      }
      else
      {
        display_setting_log_otp_not_valid(true);
        vTaskDelay(1000 / portTICK_RATE_MS);
        print_otp_in_scrn(true);
        char temp_otp_buf[setting_log_otp_input_idx + 1] = {0};
        memcpy(temp_otp_buf, setting_log_otp_input, setting_log_otp_input_idx + 1);
        temp_otp_buf[setting_log_otp_input_idx] = '\0';
        print_otp_no(true, temp_otp_buf, setting_log_otp_input_idx);
      }
    }
    clear_keypad();
    break;
  }
  case CHK_CRT_KEY:
  {
    if (key == 'D')
    {
      if (setting_log_otp_input_idx > 0)
      {
        setting_log_otp_input_idx--;
        setting_log_otp_input[setting_log_otp_input_idx] = 0;
        char temp_otp_buf[setting_log_otp_input_idx + 1] = {0};
        memcpy(temp_otp_buf, setting_log_otp_input, setting_log_otp_input_idx + 1);
        temp_otp_buf[setting_log_otp_input_idx] = '\0';
        print_otp_no(true, temp_otp_buf, setting_log_otp_input_idx);
      }
    }
    break;
  }
  case ERS_KEY:
  {
    setting_log_otp_input_idx = 0;
    memset(setting_log_otp_input, 0, sizeof(setting_log_otp_input));
    setting_log_mobile_input_idx = 0;
    memset(setting_log_mobile_input, 0, sizeof(setting_log_mobile_input));
    Selected_Setting_Log_State = SETTING_LOG_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 2);
    }
    else if (key == 'C')
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
    break;
  }
  }
}
void setting_wifi_state_handler(uint8_t key, uint8_t key_type)
{
  static int selected_wifi = -1;
  if (Selected_Setting_WiFi_State == SETTING_WIFI_SELECTION_STATE)
  {
    setting_wifi_selection_state_handler(key, key_type, &selected_wifi);
  }
  else if (Selected_Setting_WiFi_State == SETTING_WIFI_PASSWORD_STATE)
  {
    setting_wifi_password_state_handler(key, key_type, &selected_wifi);
  }
  else
  {
    return;
  }
}
void setting_log_state_handler(uint8_t key, uint8_t key_type)
{
  if (Selected_Setting_Log_State == SETTING_LOG_MOBILE_STATE)
  {
    setting_log_mobile_state_handler(key, key_type);
  }
  else if (Selected_Setting_Log_State == SETTING_LOG_OTP_STATE)
  {
    setting_log_otp_state_handler(key, key_type);
  }
}
void setting_gst_state_handler(uint8_t key, uint8_t key_type)
{
  if (Selected_Setting_GST_State == SETTING_GST_MAX_STATE)
  {
    switch (key_type)
    {
    case ARW_KEY:
    {
      if (key == 'R')
      {
        if (setting_gst_idx == 4)
        {
          setting_gst_idx = 0;
          print_gst_rate_set_screen(true, setting_gst_idx, 1, std::to_string(gst_rate[setting_gst_idx]).c_str(), std::to_string(gst_rate[setting_gst_idx + 1]).c_str());
        }
        else
        {
          setting_gst_idx++;
          print_gst_rate_set_screen(true, setting_gst_idx, 2, std::to_string(gst_rate[setting_gst_idx - 1]).c_str(), std::to_string(gst_rate[setting_gst_idx]).c_str());
        }
      }
      else
      {
        if (setting_gst_idx == 0)
        {
          setting_gst_idx = 4;
          print_gst_rate_set_screen(true, setting_gst_idx, 2, std::to_string(gst_rate[setting_gst_idx - 1]).c_str(), std::to_string(gst_rate[setting_gst_idx]).c_str());
        }
        else
        {
          setting_gst_idx--;
          print_gst_rate_set_screen(true, setting_gst_idx, 1, std::to_string(gst_rate[setting_gst_idx]).c_str(), std::to_string(gst_rate[setting_gst_idx + 1]).c_str());
        }
      }
      break;
    }
    case SIG_KEY:
    {
      if (key == '=')
      {
        Selected_Setting_GST_State = (Setting_GST_State)setting_gst_idx;
        print_in_gst(true, setting_gst_idx);
        print_in_gst(true, setting_gst_idx, (std::to_string(gst_rate[setting_gst_idx])).c_str());
        setting_gst_temp_store = gst_rate[setting_gst_idx];
      }
      break;
    }
    case ERS_KEY:
    {
      setting_gst_idx = 0;
      if (key == 'D')
      {
        Selected_Setting_State = SETTING_IDEAL_STATE;
        setting_ideal_state_helper_print_menu('R', 3);
      }
      else if (key == 'C')
      {
        Selected_Setting_State = SETTING_MAX_STATE;
        mode_now = NORMAL_MODE;
        print_normal_mode_scrn(true);
        display_trans_count(true, total_pending_trans);
      }
      break;
    }
    }
  }
  else
  {
    static const setting_func_ptr_t setting_gst_handlers[] = {setting_gst_0_handler, setting_gst_1_handler, setting_gst_2_handler, setting_gst_3_handler, setting_gst_4_handler};
    (*setting_gst_handlers[setting_gst_idx])(key, key_type);
  }
}
void setting_info_state_handler(uint8_t key, uint8_t key_type)
{
  static const setting_func_ptr_t setting_info_handlers[] = {setting_info_firmware_handler, setting_info_hardware_handler, setting_info_device_handler, setting_info_model_handler, setting_info_batch_handler, setting_info_help_handler};
  (*setting_info_handlers[(int)Selected_Setting_Info_State])(key, key_type);
}
void setting_update_state_handler(uint8_t key, uint8_t key_type)
{
}
void setting_help_state_handler(uint8_t key, uint8_t key_type)
{
  if (key_type == ERS_KEY)
  {
    Selected_Setting_Help_State = SETTING_HELP_MAX_STATE;
    if (key == 'D')
    {
      Selected_Setting_State = SETTING_IDEAL_STATE;
      setting_ideal_state_helper_print_menu('R', 5);
    }
    else
    {
      Selected_Setting_State = SETTING_MAX_STATE;
      mode_now = NORMAL_MODE;
      print_normal_mode_scrn(true);
      display_trans_count(true, total_pending_trans);
    }
  }
}
void setting_entry_action_handler(void)
{
  ESP_LOGI(TAG, "Executing setting mode entry action");
  clear_calculator();
  Selected_Setting_State = SETTING_IDEAL_STATE;
  setting_selected_menu = 1;
  setting_ideal_state_helper_print_menu('R', 1);
}
void setting_main_func(setting_func_ptr_t setting_handler_func, uint8_t key, uint8_t key_type)
{
  (*setting_handler_func)(key, key_type);
}
void setting_in_event(uint8_t key, uint8_t key_type)
{
  ESP_LOGI(TAG, "Setting event Key: %c, Key_type: %c \t state: %d", key, key_type, (int)Selected_Setting_State);
  setting_func_ptr_t setting_main_func_list[] = {setting_ideal_state_handler, setting_wifi_state_handler, setting_log_state_handler, setting_gst_state_handler, setting_info_state_handler, setting_update_state_handler, setting_help_state_handler};
  (*setting_main_func_list[(int)Selected_Setting_State])(key, key_type);
}
