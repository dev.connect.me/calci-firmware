#include "DB_Manager.h"
#include "Log_Handle.h"

const char *data = "Callback function called";
char *zErrMsg = 0;
sqlite3 *db;

cmd_type Cmd_Type = OTHER;

std::vector<int64_t> id_list;

std::vector<int64_t> log_id_list;

static uint16_t db_rsp_unsync_trans_count = 0;
static uint16_t db_rsp_sync_trans_count = 0;
static uint16_t db_rsp_unsync_logs_count = 0;
static uint16_t db_rsp_sync_logs_count = 0;

void fill_trans_data_struc(const char *col, const char *val, struct Trans_Data_Sqlite_Struc *trans)
{
    if (strcmp(col, "amount") == 0)
    {
        trans->trans_amount = std::stod(val);
    }
    else if (strcmp(col, "transactionType") == 0)
    {
        trans->trans_type = val[0];
    }
    else if (strcmp(col, "paymentMode") == 0)
    {
        trans->payment_mode = val[0];
    }
    else if (strcmp(col, "transactionTimeStamp") == 0)
    {
        trans->trans_time = std::stoi(val);
    }
    else if (strcmp(col, "customerName") == 0)
    {
        memcpy(trans->trans_cust_name, val, strlen(val) + 1);
    }
    else if (strcmp(col, "customerMobile") == 0)
    {
        memcpy(trans->trans_cust_num, val, strlen(val) + 1);
    }
    else if (strcmp(col, "customerRemarks") == 0)
    {
        memcpy(trans->trans_cust_remark, val, strlen(val) + 1);
    }
    else if (strcmp(col, "cashbookUserId") == 0)
    {
        trans->cashbook_user_id = std::stoi(val);
    }
}

void add_in_log_data_struc(const char *col, const char *val, struct Log_Sync_Struct *log_trans)
{
    if (strcmp(col, "userId") == 0)
    {
        log_trans->log_user_id = std::stoi(val);
    }
    else if (strcmp(col, "log") == 0)
    {
        log_trans->log_str = std::string(val);
    }
    else if (strcmp(col, "serialNo") == 0)
    {
        log_trans->log_serial_no = std::string(val);
    }
    else if (strcmp(col, "timestamp") == 0)
    {
        log_trans->log_timestamp = std::string(val);
    }
    else if (strcmp(col, "deviceType") == 0)
    {
        log_trans->log_device_type = std::string(val);
    }
    else if (strcmp(col, "logType") == 0)
    {
        log_trans->log_type = std::string(val);
    }
    else if (strcmp(col, "appOrFirmwareVersion") == 0)
    {
        log_trans->log_firmware_version = std::string(val);
    }
}

static int callback(void *data, int no_of_found_row, char **col_value, char **col_name)
{
    int i = 0;
    Trans_Data_Sqlite_Struc temp_trans;
    ESP_LOGI(DB, "%s: ", (const char *)data);
    switch (Cmd_Type)
    {
    case INSTANT:
    {
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            fill_trans_data_struc(col_name[i], col_value[i], &temp_trans);
        }
        // xQueueSend(instant_sync_data, (void *)&temp_trans, 1000 / portTICK_RATE_MS);
        break;
    }
    case PERIODIC:
    {
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            if (strcmp(col_name[i], "id") == 0)
            {
                id_list.push_back(std::stoi(col_value[i]));
            }
            fill_trans_data_struc(col_name[i], col_value[i], &temp_trans);
        }
        xQueueSend(periodic_sync_data, (void *)&temp_trans, 1000 / portTICK_RATE_MS);
        break;
    }
    case LOG_PERIODIC:
    {
        Log_Sync_Struct temp_log_sync_struct;
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            if (strcmp(col_name[i], "id") == 0)
            {
                log_id_list.push_back(std::stoi(col_value[i]));
            }
            add_in_log_data_struc(col_name[i], col_value[i], &temp_log_sync_struct);
        }
        log_sync_data.push_back(temp_log_sync_struct);
        uint8_t temp_queue_data = 1;
        xQueueSend(log_periodic_sync_data, (void *)&temp_queue_data, 1000 / portTICK_RATE_MS);
        break;
    }
    case GET_ID:
    {
        int64_t send_sync_queue = atoi(col_value[i]);
        // xQueueSend(instant_sync_queue, (void *)&send_sync_queue, 1000 / portTICK_RATE_MS);
        break;
    }
    case GET_UNSYNC_TRANS_COUNT:
    {
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            if (col_value[i] != NULL)
            {
                db_rsp_unsync_trans_count = atoi(col_value[i]);
            }
        }
        break;
    }
    case GET_SYNC_TRANS_COUNT:
    {
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            if (col_value[i] != NULL)
            {
                db_rsp_sync_trans_count = atoi(col_value[i]);
            }
        }
        break;
    }
    case GET_UNSYNC_LOGS_COUNT:
    {
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            if (col_value[i] != NULL)
            {
                db_rsp_unsync_logs_count = atoi(col_value[i]);
            }
        }
        break;
    }
    case GET_SYNC_LOGS_COUNT:
    {
        for (i = 0; i < no_of_found_row; i++)
        {
            ESP_LOGI(DB, "%s = %s\n", col_name[i], col_value[i] ? col_value[i] : "NULL");
            if (col_value[i] != NULL)
            {
                db_rsp_sync_logs_count = atoi(col_value[i]);
            }
        }
        break;
    }
    case OTHER:
    {
        break;
    }
    }
    return 0;
}

int openDb(const char *filename, sqlite3 **db)
{
    int rc = sqlite3_open(filename, db);
    if (rc)
    {
        ESP_LOGE(DB, "Can't open database: %s\n", sqlite3_errmsg(*db));
        char temp_msg[255];
        sprintf(temp_msg, "%s: Can't open database: %s", DB, sqlite3_errmsg(*db));
        return rc;
    }
    else
    {
        ESP_LOGI(DB, "Opened database successfully\n");
    }
    return rc;
}

int db_exec(sqlite3 *db, cmd_type cmd, const char *sql)
{
    ESP_LOGI(DB, "%s", sql);
    ESP_LOGI(DB, "1: %d", esp_get_free_heap_size());
    int64_t start = esp_timer_get_time();
    if (openDb(DATA_BASE, &db))
    {
        ESP_LOGW(DB, "Opening datbase failed");
        return 1;
    }
    ESP_LOGI(DB, "2: %d", esp_get_free_heap_size());
    Cmd_Type = cmd;
    if (Cmd_Type == PERIODIC)
        id_list.clear();
    int rc = sqlite3_exec(db, sql, callback, (void *)data, &zErrMsg);
    if (rc != SQLITE_OK)
    {
        ESP_LOGE(DB, "SQL error: %s", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        ESP_LOGI(DB, "Operation done successfully\n");
        if (Cmd_Type == PERIODIC)
        {
            ESP_LOGI(DB, "%d Message waiting", uxQueueMessagesWaiting(periodic_sync_data));
            if (uxQueueMessagesWaiting(periodic_sync_data))
            {
                Trans_Data_Sqlite_Struc temp_sync_cmd = {0};
                temp_sync_cmd.trans_type = 'S';
                xQueueSend(periodic_sync_data, (void *)&temp_sync_cmd, 1000 / portTICK_RATE_MS);
            }
        }
    }
    ESP_LOGI(DB, "3: %d", esp_get_free_heap_size());
    ESP_LOGI(DB, "Time taken: %lld\n", esp_timer_get_time() - start);
    sqlite3_close(db);
    ESP_LOGI(DB, "4: %d", esp_get_free_heap_size());
    return rc;
}

int get_unsynced_transactions(void)
{
    const char *get_unsync_query = "SELECT COUNT(*) FROM transactions WHERE isSynced=0";
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    int rc = 0;
    rc = db_exec(db, GET_UNSYNC_TRANS_COUNT, get_unsync_query);
    if (rc != SQLITE_OK)
    {
        ESP_LOGE(DB, "RC: %d\n", rc);
    }
    xSemaphoreGive(sqlite_mutex);
    return db_rsp_unsync_trans_count;
}

int get_synced_transactions(void)
{
    const char *get_unsync_query = "SELECT COUNT(*) FROM transactions WHERE isSynced=1";
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    int rc = 0;
    rc = db_exec(db, GET_SYNC_TRANS_COUNT, get_unsync_query);
    if (rc != SQLITE_OK)
    {
        ESP_LOGE(DB, "RC: %d\n", rc);
    }
    xSemaphoreGive(sqlite_mutex);
    return db_rsp_sync_trans_count;
}

int get_unsynced_logs(void)
{
    const char *get_unsync_query = "SELECT COUNT(*) FROM logs WHERE isSynced=0";
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    int rc = 0;
    rc = db_exec(db, GET_UNSYNC_LOGS_COUNT, get_unsync_query);
    if (rc != SQLITE_OK)
    {
        ESP_LOGE(DB, "RC: %d\n", rc);
    }
    xSemaphoreGive(sqlite_mutex);
    return db_rsp_unsync_logs_count;
}

int get_synced_logs(void)
{
    const char *get_unsync_query = "SELECT COUNT(*) FROM logs WHERE isSynced=1";
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    int rc = 0;
    rc = db_exec(db, GET_SYNC_LOGS_COUNT, get_unsync_query);
    if (rc != SQLITE_OK)
    {
        ESP_LOGE(DB, "RC: %d\n", rc);
    }
    xSemaphoreGive(sqlite_mutex);
    return db_rsp_sync_logs_count;
}