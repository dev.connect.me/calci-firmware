#include "Calci_Setup.h"
#include "Display.h"
#include "esp_log.h"
#include "WiFi_Manager.h"
#include "esp_wifi.h"
#include "Calci_Settings.h"

#define MU_TIMEOUT_TIME 500
#define MU_SET_COUNT 3
static const char *TAG = "Calci Setup";
Setup_State Selected_Setup_State = SETUP_MAX_STATE;
Setup_WiFi_State Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
Setup_Log_State Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;

static int mu_counter = 0;
static int64_t last_mu_press_time = 0;

uint16_t setup_found_ap_count = 0;
wifi_ap_record_t *setup_found_ap_info;

static char setup_wifi_password_input[26];
static int setup_wifi_password_input_idx = 0;
static int setup_wifi_password_shift_mode = 1;
static char setup_log_mobile_input[11];
static int setup_log_mobile_input_idx = 0;
static char setup_log_otp_input[5];
static int setup_log_otp_input_idx = 0;

void setup_log_mobile_entry_action(void);

void setup_log_mobile_state_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case NUM_KEY:
    {
        mu_counter = 0;
        if (key != 'D' && key != '.')
        {
            if (setup_log_mobile_input_idx < 10)
            {
                setup_log_mobile_input[setup_log_mobile_input_idx] = key;
                setup_log_mobile_input_idx++;
                char temp_mobile_buf[setup_log_mobile_input_idx + 1] = {0};
                memcpy(temp_mobile_buf, setup_log_mobile_input, setup_log_mobile_input_idx + 1);
                temp_mobile_buf[setup_log_mobile_input_idx] = '\0';
                print_mobile_no(true, temp_mobile_buf, setup_log_mobile_input_idx);
            }
        }
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            if (setup_log_mobile_input_idx == 10)
            {
                if (get_wifi_status())
                {
                    setup_log_mobile_input[10] = '\0';
                    print_sending_scrn(true);
                    if (send_otp(setup_log_mobile_input))
                    {
                        print_otp_in_scrn(true);
                        Selected_Setup_Log_State = SETUP_LOG_OTP_STATE;
                    }
                    else
                    {
                        print_connection_failed_scrn(true);
                        vTaskDelay(1000 / portTICK_RATE_MS);
                        setup_log_mobile_input_idx = 0;
                        memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                        Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                        Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                        print_instruction_scrn(true, 'R', 3);
                    }
                }
                else
                {
                    display_err_wifi_not_connected(true);
                    vTaskDelay(1000 / portTICK_RATE_MS);
                    Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                    Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                    setup_log_mobile_input_idx = 0;
                    memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                    print_instruction_scrn(true, 'R', 3);
                }
            }
            else
            {
                print_not_valid_no_scrn(true);
                vTaskDelay(1000 / portTICK_RATE_MS);
                print_mobile_in_scrn(true);
                char temp_mobile_buf[setup_log_mobile_input_idx + 1] = {0};
                memcpy(temp_mobile_buf, setup_log_mobile_input, setup_log_mobile_input_idx + 1);
                temp_mobile_buf[setup_log_mobile_input_idx] = '\0';
                print_mobile_no(true, temp_mobile_buf, setup_log_mobile_input_idx);
            }
        }
        clear_keypad();
        break;
    }
    case CHK_CRT_KEY:
    {
        mu_counter = 0;
        if (key == 'D')
        {
            if (setup_log_mobile_input_idx > 0)
            {
                setup_log_mobile_input_idx--;
                setup_log_mobile_input[setup_log_mobile_input_idx] = 0;
                char temp_mobile_buf[setup_log_mobile_input_idx + 1] = {0};
                memcpy(temp_mobile_buf, setup_log_mobile_input, setup_log_mobile_input_idx + 1);
                temp_mobile_buf[setup_log_mobile_input_idx] = '\0';
                print_mobile_no(true, temp_mobile_buf, setup_log_mobile_input_idx);
            }
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    setup_log_mobile_input_idx = 0;
                    memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                    Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_log_otp_state_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case NUM_KEY:
    {
        mu_counter = 0;
        if (key != 'D' && key != '.')
        {
            if (setup_log_otp_input_idx < 4)
            {
                setup_log_otp_input[setup_log_otp_input_idx] = key;
                setup_log_otp_input_idx++;
                char temp_otp_buf[setup_log_otp_input_idx + 1] = {0};
                memcpy(temp_otp_buf, setup_log_otp_input, setup_log_otp_input_idx + 1);
                temp_otp_buf[setup_log_otp_input_idx] = '\0';
                print_otp_no(true, temp_otp_buf, setup_log_otp_input_idx);
            }
        }
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            if (setup_log_otp_input_idx == 4)
            {
                if (get_wifi_status())
                {
                    setup_log_otp_input[4] = '\0';
                    print_validating_scrn(true);
                    if (veryfy_otp(setup_log_mobile_input, setup_log_otp_input))
                    {
                        display_setting_log_otp_login_success(true);
                        vTaskDelay(1000 / portTICK_RATE_MS);
                        File_Operation save_username;
                        if (save_username.m_exists(USERNAME))
                            save_username.m_remove(USERNAME);
                        save_username.m_write(USERNAME, setup_log_mobile_input);
                        setup_log_otp_input_idx = 0;
                        memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                        setup_log_mobile_input_idx = 0;
                        memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                        Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                        Selected_Setup_State = SETUP_APP_STATE;
                        print_app_qr_code(true);
                    }
                    else
                    {
                        display_setting_log_otp_validation_failed(true);
                        vTaskDelay(1000 / portTICK_RATE_MS);
                        setup_log_otp_input_idx = 0;
                        memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                        setup_log_mobile_input_idx = 0;
                        memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                        Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                        Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                        print_instruction_scrn(true, 'R', 3);
                    }
                }
                else
                {
                    display_err_wifi_not_connected(true);
                    vTaskDelay(1000 / portTICK_RATE_MS);
                    Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                    Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                    setup_log_otp_input_idx = 0;
                    memset(setup_log_otp_input, 0, sizeof(setup_log_otp_input));
                    setup_log_mobile_input_idx = 0;
                    memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                    print_instruction_scrn(true, 'R', 3);
                }
            }
            else
            {
                display_setting_log_otp_not_valid(true);
                vTaskDelay(1000 / portTICK_RATE_MS);
                print_otp_in_scrn(true);
                char temp_otp_buf[setup_log_otp_input_idx + 1] = {0};
                memcpy(temp_otp_buf, setup_log_otp_input, setup_log_otp_input_idx + 1);
                temp_otp_buf[setup_log_otp_input_idx] = '\0';
                print_otp_no(true, temp_otp_buf, setup_log_otp_input_idx);
            }
        }
        clear_keypad();
        break;
    }
    case CHK_CRT_KEY:
    {
        mu_counter = 0;
        if (key == 'D')
        {
            if (setup_log_otp_input_idx > 0)
            {
                setup_log_otp_input_idx--;
                setup_log_otp_input[setup_log_otp_input_idx] = 0;
                char temp_otp_buf[setup_log_otp_input_idx + 1] = {0};
                memcpy(temp_otp_buf, setup_log_otp_input, setup_log_otp_input_idx + 1);
                temp_otp_buf[setup_log_otp_input_idx] = '\0';
                print_otp_no(true, temp_otp_buf, setup_log_otp_input_idx);
            }
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    setup_log_mobile_input_idx = 0;
                    memset(setup_log_mobile_input, 0, sizeof(setup_log_mobile_input));
                    setup_log_otp_input_idx = 0;
                    memset(setup_log_otp_input, 0, sizeof(setup_log_otp_input));
                    Selected_Setup_Log_State = SETUP_LOG_MAX_STATE;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_wifi_password_shift_mode_abc_helper(int64_t *setup_wifi_password_last_key_press_time, uint8_t *setup_wifi_password_last_key_type, uint8_t *setup_wifi_password_last_key, uint8_t key, uint8_t key_type)
{
    if (*setup_wifi_password_last_key_type == key_type && *setup_wifi_password_last_key == key)
    {
        if ((esp_timer_get_time() - *setup_wifi_password_last_key_press_time) < PSW_INPUT_INTERVAL * 1000)
        {
            char last_char = setup_wifi_password_input[setup_wifi_password_input_idx];
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_parse_key(key, key_type, last_char);
        }
        else
        {
            setup_wifi_password_input_idx++;
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_add_last_char(key, key_type);
        }
    }
    else
    {
        if (setup_wifi_password_input_idx == 0 && *setup_wifi_password_last_key_type == 0 && *setup_wifi_password_last_key == 0 && setup_wifi_password_input[0] == 0)
        {
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_add_last_char(key, key_type);
        }
        else
        {
            setup_wifi_password_input_idx++;
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_abc_add_last_char(key, key_type);
        }
    }
    *setup_wifi_password_last_key_type = key_type;
    *setup_wifi_password_last_key = key;
    *setup_wifi_password_last_key_press_time = esp_timer_get_time();
}
void setup_wifi_password_shift_mode_ABC_helper(int64_t *setup_wifi_password_last_key_press_time, uint8_t *setup_wifi_password_last_key_type, uint8_t *setup_wifi_password_last_key, uint8_t key, uint8_t key_type)
{
    if (*setup_wifi_password_last_key_type == key_type && *setup_wifi_password_last_key == key)
    {
        if ((esp_timer_get_time() - *setup_wifi_password_last_key_press_time) < PSW_INPUT_INTERVAL * 1000)
        {
            char last_char = setup_wifi_password_input[setup_wifi_password_input_idx];
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_parse_key(key, key_type, last_char);
        }
        else
        {
            setup_wifi_password_input_idx++;
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_add_last_char(key, key_type);
        }
    }
    else
    {
        if (setup_wifi_password_input_idx == 0 && *setup_wifi_password_last_key_type == 0 && *setup_wifi_password_last_key == 0 && setup_wifi_password_input[0] == 0)
        {
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_add_last_char(key, key_type);
        }
        else
        {
            setup_wifi_password_input_idx++;
            setup_wifi_password_input[setup_wifi_password_input_idx] = setting_wifi_password_shift_mode_ABC_add_last_char(key, key_type);
        }
    }
    *setup_wifi_password_last_key_type = key_type;
    *setup_wifi_password_last_key = key;
    *setup_wifi_password_last_key_press_time = esp_timer_get_time();
}
void setup_wifi_password_shift_mode_abc(int64_t *setup_wifi_password_last_key_press_time, uint8_t *setup_wifi_password_last_key_type, uint8_t *setup_wifi_password_last_key, uint8_t key, uint8_t key_type, int *selected_wifi)
{
    ESP_LOGI(TAG, "This ABC is calling");
    switch (key_type)
    {
    case NUM_KEY:
    {
        mu_counter = 0;
        setup_wifi_password_shift_mode_abc_helper(setup_wifi_password_last_key_press_time, setup_wifi_password_last_key_type, setup_wifi_password_last_key, key, key_type);
        char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
        temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            ESP_LOGI(TAG, "WiFi: %s", (char *)setup_found_ap_info[*selected_wifi].ssid);
            ESP_LOGI(TAG, "Password: %s", setup_wifi_password_input);
            wifi_config_t setup_wifi_sta_config = {};
            memcpy(setup_wifi_sta_config.sta.ssid, setup_found_ap_info[*selected_wifi].ssid, strlen((char *)setup_found_ap_info[*selected_wifi].ssid));
            memcpy(setup_wifi_sta_config.sta.password, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
            setup_wifi_sta_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
            setup_wifi_sta_config.sta.pmf_cfg.capable = false;
            setup_wifi_sta_config.sta.pmf_cfg.required = false;
            esp_err_t err = esp_wifi_set_config(WIFI_IF_STA, &setup_wifi_sta_config);
            if (err != ESP_OK)
            {
                print_connection_failed_scrn(true);
                *setup_wifi_password_last_key_type = 0;
                *setup_wifi_password_last_key = 0;
                setup_wifi_password_input_idx = 0;
                memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                setup_found_ap_count = 0;
                free(setup_found_ap_info);
                vTaskResume(WiFi_Task_Handle);
                Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                vTaskDelay(3000 / portTICK_RATE_MS);
                print_instruction_scrn(true, 'R', 3);
                clear_keypad();
                return;
            }
            print_connecting_scrn(true);
            err = esp_wifi_connect();
            ESP_LOGI(TAG, "Error: %s", esp_err_to_name(err));
            for (int i = 0; i < 10; i++)
            {
                EventBits_t setup_wifi_bits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10000 / portTICK_PERIOD_MS);
                if (setup_wifi_bits & WIFI_CONNECTED_BIT)
                {
                    ESP_LOGI(TAG, "connected to ap SSID");
                    break;
                }
                else if (setup_wifi_bits & WIFI_FAIL_BIT)
                {
                    ESP_LOGI(TAG, "Failed to connect to SSID");
                    esp_wifi_connect();
                }
                else
                {
                    ESP_LOGE(TAG, "UNEXPECTED EVENT");
                }
                vTaskDelay(1000 / portTICK_RATE_MS);
            }
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            setup_wifi_password_input_idx = 0;
            memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
            setup_found_ap_count = 0;
            free(setup_found_ap_info);
            vTaskResume(WiFi_Task_Handle);
            if (get_wifi_status())
            {
                print_connected_scrn(true);
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                Selected_Setup_State = SETUP_LOG_STATE;
                vTaskDelay(2000 / portTICK_RATE_MS);
                setup_log_mobile_entry_action();
                clear_keypad();
            }
            else
            {
                print_connection_failed_scrn(true);
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                vTaskDelay(3000 / portTICK_RATE_MS);
                print_instruction_scrn(true, 'R', 3);
                clear_keypad();
            }
        }
        else
        {
            setup_wifi_password_shift_mode_abc_helper(setup_wifi_password_last_key_press_time, setup_wifi_password_last_key_type, setup_wifi_password_last_key, key, key_type);
            char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
            memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
            temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
            print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        }
        break;
    }
    case MEM_KEY:
    {
        mu_counter = 0;
        setup_wifi_password_shift_mode_abc_helper(setup_wifi_password_last_key_press_time, setup_wifi_password_last_key_type, setup_wifi_password_last_key, key, key_type);
        char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
        temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        break;
    }
    case CHK_CRT_KEY:
    {
        mu_counter = 0;
        if (key == 'D')
        {
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            if (setup_wifi_password_input_idx >= 0)
            {
                setup_wifi_password_input[setup_wifi_password_input_idx] = 0;
                setup_wifi_password_input_idx--;
                if (setup_wifi_password_input_idx < 0)
                    setup_wifi_password_input_idx = 0;
                char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
                memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
                temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
                print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
            }
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'S')
        {
            setup_wifi_password_shift_mode = 1;
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            print_in_mode(setup_wifi_password_shift_mode);
        }
        else if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    setup_found_ap_count = 0;
                    free(setup_found_ap_info);
                    *setup_wifi_password_last_key_type = 0;
                    *setup_wifi_password_last_key = 0;
                    setup_wifi_password_input_idx = 0;
                    memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
                    vTaskResume(WiFi_Task_Handle);
                    Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_wifi_password_shift_mode_ABC(int64_t *setup_wifi_password_last_key_press_time, uint8_t *setup_wifi_password_last_key_type, uint8_t *setup_wifi_password_last_key, uint8_t key, uint8_t key_type, int *selected_wifi)
{
    ESP_LOGI(TAG, "This ABC is calling");
    switch (key_type)
    {
    case NUM_KEY:
    {
        mu_counter = 0;
        setup_wifi_password_shift_mode_ABC_helper(setup_wifi_password_last_key_press_time, setup_wifi_password_last_key_type, setup_wifi_password_last_key, key, key_type);
        char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
        temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            ESP_LOGI(TAG, "WiFi: %s", (char *)setup_found_ap_info[*selected_wifi].ssid);
            ESP_LOGI(TAG, "Password: %s", setup_wifi_password_input);
            wifi_config_t setup_wifi_sta_config = {};
            memcpy(setup_wifi_sta_config.sta.ssid, setup_found_ap_info[*selected_wifi].ssid, strlen((char *)setup_found_ap_info[*selected_wifi].ssid));
            memcpy(setup_wifi_sta_config.sta.password, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
            setup_wifi_sta_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
            setup_wifi_sta_config.sta.pmf_cfg.capable = false;
            setup_wifi_sta_config.sta.pmf_cfg.required = false;
            esp_err_t err = esp_wifi_set_config(WIFI_IF_STA, &setup_wifi_sta_config);
            if (err != ESP_OK)
            {
                print_connection_failed_scrn(true);
                *setup_wifi_password_last_key_type = 0;
                *setup_wifi_password_last_key = 0;
                setup_wifi_password_input_idx = 0;
                memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                setup_found_ap_count = 0;
                free(setup_found_ap_info);
                vTaskResume(WiFi_Task_Handle);
                Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                vTaskDelay(3000 / portTICK_RATE_MS);
                print_instruction_scrn(true, 'R', 3);
                clear_keypad();
                return;
            }
            print_connecting_scrn(true);
            err = esp_wifi_connect();
            ESP_LOGI(TAG, "Error: %s", esp_err_to_name(err));
            for (int i = 0; i < 10; i++)
            {
                EventBits_t setup_wifi_bits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10000 / portTICK_PERIOD_MS);
                if (setup_wifi_bits & WIFI_CONNECTED_BIT)
                {
                    ESP_LOGI(TAG, "connected to ap SSID");
                    break;
                }
                else if (setup_wifi_bits & WIFI_FAIL_BIT)
                {
                    ESP_LOGI(TAG, "Failed to connect to SSID");
                    esp_wifi_connect();
                }
                else
                {
                    ESP_LOGE(TAG, "UNEXPECTED EVENT");
                }
                vTaskDelay(1000 / portTICK_RATE_MS);
            }
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            setup_wifi_password_input_idx = 0;
            memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
            setup_found_ap_count = 0;
            free(setup_found_ap_info);
            vTaskResume(WiFi_Task_Handle);
            if (get_wifi_status())
            {
                print_connected_scrn(true);
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                Selected_Setup_State = SETUP_LOG_STATE;
                vTaskDelay(2000 / portTICK_RATE_MS);
                setup_log_mobile_entry_action();
                clear_keypad();
            }
            else
            {
                print_connection_failed_scrn(true);
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                vTaskDelay(3000 / portTICK_RATE_MS);
                print_instruction_scrn(true, 'R', 3);
                clear_keypad();
            }
        }
        else
        {
            setup_wifi_password_shift_mode_ABC_helper(setup_wifi_password_last_key_press_time, setup_wifi_password_last_key_type, setup_wifi_password_last_key, key, key_type);
            char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
            memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
            temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
            print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        }
        break;
    }
    case MEM_KEY:
    {
        mu_counter = 0;
        setup_wifi_password_shift_mode_ABC_helper(setup_wifi_password_last_key_press_time, setup_wifi_password_last_key_type, setup_wifi_password_last_key, key, key_type);
        char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
        memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
        temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
        print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        break;
    }
    case CHK_CRT_KEY:
    {
        mu_counter = 0;
        if (key == 'D')
        {
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            if (setup_wifi_password_input_idx >= 0)
            {
                setup_wifi_password_input[setup_wifi_password_input_idx] = 0;
                setup_wifi_password_input_idx--;
                if (setup_wifi_password_input_idx < 0)
                    setup_wifi_password_input_idx = 0;
                char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
                memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
                temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
                print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
            }
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'S')
        {
            setup_wifi_password_shift_mode = 2;
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            print_in_mode(setup_wifi_password_shift_mode);
        }
        else if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    setup_found_ap_count = 0;
                    free(setup_found_ap_info);
                    *setup_wifi_password_last_key_type = 0;
                    *setup_wifi_password_last_key = 0;
                    setup_wifi_password_input_idx = 0;
                    memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
                    vTaskResume(WiFi_Task_Handle);
                    Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_wifi_password_shift_mode_123(int64_t *setup_wifi_password_last_key_press_time, uint8_t *setup_wifi_password_last_key_type, uint8_t *setup_wifi_password_last_key, uint8_t key, uint8_t key_type, int *selected_wifi)
{
    ESP_LOGI(TAG, "This is calling");
    switch (key_type)
    {
    case NUM_KEY:
    {
        mu_counter = 0;
        if (key != 'D' && key != '.')
        {
            if (setup_wifi_password_input_idx == 0 && *setup_wifi_password_last_key_type == 0 && *setup_wifi_password_last_key == 0 && setup_wifi_password_input[0] == 0)
            {
                setup_wifi_password_input[setup_wifi_password_input_idx] = key;
            }
            else
            {
                setup_wifi_password_input_idx++;
                setup_wifi_password_input[setup_wifi_password_input_idx] = key;
            }
            *setup_wifi_password_last_key_type = key_type;
            *setup_wifi_password_last_key = key;
            *setup_wifi_password_last_key_press_time = esp_timer_get_time();
            char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
            memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
            temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
            print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
        }
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            ESP_LOGI(TAG, "WiFi: %s", (char *)setup_found_ap_info[*selected_wifi].ssid);
            ESP_LOGI(TAG, "Password: %s", setup_wifi_password_input);
            wifi_config_t setup_wifi_sta_config = {};
            memcpy(setup_wifi_sta_config.sta.ssid, setup_found_ap_info[*selected_wifi].ssid, strlen((char *)setup_found_ap_info[*selected_wifi].ssid));
            memcpy(setup_wifi_sta_config.sta.password, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
            setup_wifi_sta_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
            setup_wifi_sta_config.sta.pmf_cfg.capable = false;
            setup_wifi_sta_config.sta.pmf_cfg.required = false;
            esp_err_t err = esp_wifi_set_config(WIFI_IF_STA, &setup_wifi_sta_config);
            if (err != ESP_OK)
            {
                print_connection_failed_scrn(true);
                *setup_wifi_password_last_key_type = 0;
                *setup_wifi_password_last_key = 0;
                setup_wifi_password_input_idx = 0;
                memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                setup_found_ap_count = 0;
                free(setup_found_ap_info);
                vTaskResume(WiFi_Task_Handle);
                Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                vTaskDelay(3000 / portTICK_RATE_MS);
                print_instruction_scrn(true, 'R', 3);
                clear_keypad();
                return;
            }
            print_connecting_scrn(true);
            err = esp_wifi_connect();
            ESP_LOGI(TAG, "Error: %s", esp_err_to_name(err));
            for (int i = 0; i < 10; i++)
            {
                EventBits_t setup_wifi_bits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10000 / portTICK_PERIOD_MS);
                if (setup_wifi_bits & WIFI_CONNECTED_BIT)
                {
                    ESP_LOGI(TAG, "connected to ap SSID");
                    break;
                }
                else if (setup_wifi_bits & WIFI_FAIL_BIT)
                {
                    ESP_LOGI(TAG, "Failed to connect to SSID");
                    esp_wifi_connect();
                }
                else
                {
                    ESP_LOGE(TAG, "UNEXPECTED EVENT");
                }
                vTaskDelay(1000 / portTICK_RATE_MS);
            }
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            setup_wifi_password_input_idx = 0;
            memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
            setup_found_ap_count = 0;
            free(setup_found_ap_info);
            vTaskResume(WiFi_Task_Handle);
            if (get_wifi_status())
            {
                print_connected_scrn(true);
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                Selected_Setup_State = SETUP_LOG_STATE;
                vTaskDelay(2000 / portTICK_RATE_MS);
                setup_log_mobile_entry_action();
                clear_keypad();
            }
            else
            {
                print_connection_failed_scrn(true);
                Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
                vTaskDelay(3000 / portTICK_RATE_MS);
                print_instruction_scrn(true, 'R', 3);
                clear_keypad();
            }
        }
        break;
    }
    case CHK_CRT_KEY:
    {
        mu_counter = 0;
        if (key == 'D')
        {
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            if (setup_wifi_password_input_idx >= 0)
            {
                setup_wifi_password_input[setup_wifi_password_input_idx] = 0;
                setup_wifi_password_input_idx--;
                if (setup_wifi_password_input_idx < 0)
                    setup_wifi_password_input_idx = 0;
                char temp_psw_buf[setup_wifi_password_input_idx + 2] = {0};
                memcpy(temp_psw_buf, setup_wifi_password_input, setup_wifi_password_input_idx + 1);
                temp_psw_buf[setup_wifi_password_input_idx + 1] = '\0';
                print_psw(true, temp_psw_buf, setup_wifi_password_input_idx + 1, setup_wifi_password_shift_mode);
            }
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'S')
        {
            setup_wifi_password_shift_mode = 0;
            *setup_wifi_password_last_key_type = 0;
            *setup_wifi_password_last_key = 0;
            print_in_mode(setup_wifi_password_shift_mode);
        }
        else if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    setup_found_ap_count = 0;
                    free(setup_found_ap_info);
                    *setup_wifi_password_last_key_type = 0;
                    *setup_wifi_password_last_key = 0;
                    setup_wifi_password_input_idx = 0;
                    memset(setup_wifi_password_input, 0, sizeof(setup_wifi_password_input));
                    vTaskResume(WiFi_Task_Handle);
                    Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_wifi_selection_entry_action(void)
{
    if (get_wifi_status())
    {
        display_wifi_connected_goto_login(true);
        vTaskDelay(3000 / portTICK_RATE_MS);
        Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
        Selected_Setup_State = SETUP_LOG_STATE;
        setup_log_mobile_entry_action();
        clear_keypad();
    }
    else
    {
        print_scanning(true);
        // suspend wifi manager task so after disconnecting it not retry to connect
        vTaskSuspend(WiFi_Task_Handle);
        vTaskDelay(100 / portTICK_RATE_MS);
        esp_wifi_disconnect();
        esp_err_t err = esp_wifi_scan_start(NULL, true);
        // retrying scanning untill it returns ESP_OK
        while (err != ESP_OK)
        {
            err = esp_wifi_scan_start(NULL, true);
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        err = esp_wifi_scan_get_ap_num(&setup_found_ap_count);
        if (err != ESP_OK)
        {
            print_connection_failed_scrn(true);
            setup_found_ap_count = 0;
            Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
            Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
            vTaskResume(WiFi_Task_Handle);
            vTaskDelay(1000 / portTICK_RATE_MS);
            print_instruction_scrn(true, 'R', 3);
            clear_keypad();
            return;
        }
        setup_found_ap_info = (wifi_ap_record_t *)calloc(setup_found_ap_count, sizeof(wifi_ap_record_t));
        err = esp_wifi_scan_get_ap_records(&setup_found_ap_count, setup_found_ap_info);
        if (err != ESP_OK)
        {
            print_connection_failed_scrn(true);
            setup_found_ap_count = 0;
            Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
            Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
            vTaskResume(WiFi_Task_Handle);
            vTaskDelay(1000 / portTICK_RATE_MS);
            print_instruction_scrn(true, 'R', 3);
            clear_keypad();
            return;
        }
        if (setup_found_ap_count == 0)
        {
            print_wifi_not_found_scrn(true);
            setup_found_ap_count = 0;
            Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
            Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
            vTaskResume(WiFi_Task_Handle);
            vTaskDelay(1000 / portTICK_RATE_MS);
            print_instruction_scrn(true, 'R', 3);
            clear_keypad();
            return;
        }
        ESP_LOGI(TAG, "Scand done and Network found: %d", setup_found_ap_count);
        if (setup_found_ap_count == 1)
        {
            print_ssid(true, (char *)setup_found_ap_info[0].ssid, "", 0, 0, setup_found_ap_count);
        }
        else if (setup_found_ap_count > 1)
        {
            print_ssid(true, (char *)setup_found_ap_info[0].ssid, (char *)setup_found_ap_info[1].ssid, 1, 0, setup_found_ap_count);
        }
    }
}
void setup_log_mobile_entry_action(void)
{
    Selected_Setup_Log_State = SETUP_LOG_MOBILE_STATE;
    print_mobile_in_scrn(true);
}
void setup_wifi_selection_handler(uint8_t key, uint8_t key_type, int *selected_wifi)
{
    static int wifi_idx = 0;
    switch (key_type)
    {
    case ARW_KEY:
    {
        mu_counter = 0;
        if (setup_found_ap_count == 1)
        {
            print_ssid(true, (char *)setup_found_ap_info[0].ssid, "", 0, 0, setup_found_ap_count);
        }
        else
        {
            if (key == 'R')
            {
                wifi_idx++;
                if (wifi_idx == setup_found_ap_count)
                {
                    wifi_idx = 0;
                    print_ssid(true, (char *)setup_found_ap_info[wifi_idx].ssid, (char *)setup_found_ap_info[wifi_idx + 1].ssid, 0, wifi_idx, setup_found_ap_count);
                }
                else
                {
                    ESP_LOGI(TAG, "%d: ssid1: %s\tssid2:%s", wifi_idx, (char *)setup_found_ap_info[wifi_idx].ssid, (char *)setup_found_ap_info[wifi_idx + 1].ssid);
                    print_ssid(true, (char *)setup_found_ap_info[wifi_idx - 1].ssid, (char *)setup_found_ap_info[wifi_idx].ssid, 1, wifi_idx, setup_found_ap_count);
                }
            }
            else if (key == 'L')
            {
                wifi_idx--;
                if (wifi_idx == -1)
                {
                    wifi_idx = setup_found_ap_count - 1;
                    print_ssid(true, (char *)setup_found_ap_info[wifi_idx].ssid, (char *)setup_found_ap_info[wifi_idx - 1].ssid, 1, wifi_idx, setup_found_ap_count);
                }
                else
                {
                    print_ssid(true, (char *)setup_found_ap_info[wifi_idx].ssid, (char *)setup_found_ap_info[wifi_idx + 1].ssid, 0, wifi_idx, setup_found_ap_count);
                }
            }
        }
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            *selected_wifi = wifi_idx;
            Selected_Setup_WiFi_State = SETUP_WIFI_PASSWORD_STATE;
            setup_wifi_password_shift_mode = 0;
            print_psw_in_scrn(true);
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    wifi_idx = 0;
                    setup_found_ap_count = 0;
                    free(setup_found_ap_info);
                    Selected_Setup_WiFi_State = SETUP_WIFI_MAX_STATE;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    vTaskResume(WiFi_Task_Handle);
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_wifi_password_handler(uint8_t key, uint8_t key_type, int *selected_wifi)
{
    static int64_t setup_wifi_password_last_key_press_time = 0;
    static uint8_t setup_wifi_password_last_key_type = 0;
    static uint8_t setup_wifi_password_last_key = 0;
    switch (setup_wifi_password_shift_mode)
    {
    case 0:
    {
        setup_wifi_password_shift_mode_abc(&setup_wifi_password_last_key_press_time, &setup_wifi_password_last_key_type, &setup_wifi_password_last_key, key, key_type, selected_wifi);
        break;
    }
    case 1:
    {
        setup_wifi_password_shift_mode_ABC(&setup_wifi_password_last_key_press_time, &setup_wifi_password_last_key_type, &setup_wifi_password_last_key, key, key_type, selected_wifi);
        break;
    }
    case 2:
    {
        setup_wifi_password_shift_mode_123(&setup_wifi_password_last_key_press_time, &setup_wifi_password_last_key_type, &setup_wifi_password_last_key, key, key_type, selected_wifi);
        break;
    }
    default:
    {
        break;
    }
    }
}
void setup_entry_action(void)
{
    Selected_Setup_State = SETUP_INSTRUCTION1_STATE;
    print_instruction_scrn(true, 'R', 1);
}
void setup_instruction1_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        mu_counter = 0;
        if (key == 'R')
        {
            Selected_Setup_State = SETUP_INSTRUCTION2_STATE;
            print_instruction_scrn(true, key, 2);
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                ESP_LOGI(TAG, "MU counter: %d", mu_counter);
                if (mu_counter >= MU_SET_COUNT)
                {
                    mu_counter = 0;
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        else
        {
            mu_counter = 0;
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_instruction2_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        mu_counter = 0;
        if (key == 'R')
        {
            Selected_Setup_State = SETUP_INSTRUCTION3_STATE;
            print_instruction_scrn(true, key, 3);
        }
        else
        {
            Selected_Setup_State = SETUP_INSTRUCTION1_STATE;
            print_instruction_scrn(true, key, 1);
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                if (mu_counter >= MU_SET_COUNT)
                {
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        else
        {
            mu_counter = 0;
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_instruction3_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        mu_counter = 0;
        if (key == 'R')
        {
            Selected_Setup_State = SETUP_INSTRUCTION4_STATE;
            print_instruction_scrn(true, key, 4);
        }
        else
        {
            Selected_Setup_State = SETUP_INSTRUCTION2_STATE;
            print_instruction_scrn(true, key, 2);
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                if (mu_counter >= MU_SET_COUNT)
                {
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        else
        {
            mu_counter = 0;
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_instruction4_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        mu_counter = 0;
        if (key == 'R')
        {
            Selected_Setup_State = SETUP_WIFI_STATE;
            Selected_Setup_WiFi_State = SETUP_WIFI_SELECTION_STATE;
            setup_wifi_selection_entry_action();
        }
        else
        {
            Selected_Setup_State = SETUP_INSTRUCTION2_STATE;
            print_instruction_scrn(true, key, 3);
        }
        break;
    }
    case SIG_KEY:
    {
        mu_counter = 0;
        if (key == '=')
        {
            Selected_Setup_State = SETUP_WIFI_STATE;
            Selected_Setup_WiFi_State = SETUP_WIFI_SELECTION_STATE;
            setup_wifi_selection_entry_action();
        }
        break;
    }
    case SPC_KEY:
    {
        if (key == 'M')
        {
            if ((esp_timer_get_time() - last_mu_press_time) <= MU_TIMEOUT_TIME * 1000)
            {
                mu_counter++;
                if (mu_counter >= MU_SET_COUNT)
                {
                    Selected_Setup_State = SETUP_MAX_STATE;
                    mode_now = NORMAL_MODE;
                    print_normal_mode_scrn(true);
                    display_trans_count(true, total_pending_trans);
                }
            }
            else
            {
                mu_counter = 0;
                mu_counter++;
            }
            last_mu_press_time = esp_timer_get_time();
        }
        else
        {
            mu_counter = 0;
        }
        break;
    }
    default:
    {
        mu_counter = 0;
        break;
    }
    }
}
void setup_wifi_handler(uint8_t key, uint8_t key_type)
{
    static int selected_wifi = -1;
    switch (Selected_Setup_WiFi_State)
    {
    case SETUP_WIFI_SELECTION_STATE:
    {
        setup_wifi_selection_handler(key, key_type, &selected_wifi);
        break;
    }
    case SETUP_WIFI_PASSWORD_STATE:
    {
        setup_wifi_password_handler(key, key_type, &selected_wifi);
        break;
    }
    default:
        break;
    }
}
void setup_log_handler(uint8_t key, uint8_t key_type)
{
    switch (Selected_Setup_Log_State)
    {
    case SETUP_LOG_MOBILE_STATE:
    {
        setup_log_mobile_state_handler(key, key_type);
        break;
    }
    case SETUP_LOG_OTP_STATE:
    {
        setup_log_otp_state_handler(key, key_type);
        break;
    }
    default:
        break;
    }
}
void setup_app_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        if (key == 'R')
        {
            Selected_Setup_State = SETUP_MAX_STATE;
            mode_now = NORMAL_MODE;
            print_normal_mode_scrn(true);
            display_trans_count(true, total_pending_trans);
        }
        break;
    }
    case SIG_KEY:
    {
        if (key == '=')
        {
            Selected_Setup_State = SETUP_MAX_STATE;
            mode_now = NORMAL_MODE;
            print_normal_mode_scrn(true);
            display_trans_count(true, total_pending_trans);
        }
        break;
    }
    }
}
void setup_max_handler(uint8_t key, uint8_t key_type)
{
    return;
}
void setup_event_in(uint8_t key, uint8_t key_type)
{
    static setup_func_ptr_t setup_handlers[] = {
        setup_instruction1_handler,
        setup_instruction2_handler,
        setup_instruction3_handler,
        setup_instruction4_handler,
        setup_wifi_handler,
        setup_log_handler,
        setup_app_handler,
        setup_max_handler};
    (*setup_handlers[(int)Selected_Setup_State])(key, key_type);
}