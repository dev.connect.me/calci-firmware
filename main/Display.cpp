#include "Display.h"
#include "ST7525.h"
#include "driver/gpio.h"

#define STRING(num) #num

#define DISPLAY_BACKLIGHT 32
#define DISPLAY_SID 14
#define DISPLAY_SCLK 12
#define DISPLAY_A0 26
#define DISPLAY_RST 27
#define DISPLAY_CS 25
#define LCD_HOST HSPI_HOST

xQueueHandle display_queue;

ST7525 Display(DISPLAY_SID, DISPLAY_SCLK, DISPLAY_A0, DISPLAY_RST, DISPLAY_CS,
               DISPLAY_BACKLIGHT, LCD_HOST);

void display_init()
{
  display_queue = xQueueCreate(10, sizeof(display_data));
  xTaskCreate(display_task, "Display control task", 10240, NULL, 10, &Display_Task_Handle);
  Display.begin(0x80);
  Display.clear_display();
}
std::string remove_tailing_zeros(const char *double_str)
{
  std::string str(double_str);
  str.erase(str.find_last_not_of('0') + 1, std::string::npos);
  str.erase(str.find_last_not_of('.') + 1, std::string::npos);
  return str;
}
std::string double_to_str(double num, int8_t precision)
{
  std::stringstream stream;
  std::string str = "";
  if (precision == -1)
  {
    stream << std::fixed;
    stream << std::setprecision(5);
    stream << num;
    str = stream.str();
    std::size_t found = str.find('.');
    if (found != std::string::npos)
    {
      char last_digit = str.at(str.length() - 1);
      while (last_digit == '0' || last_digit == '.')
      {
        str.pop_back();
        if (last_digit == '.')
          break;
        last_digit = str.at(str.length() - 1);
      }
    }
  }
  else if (precision == 0)
  {
    stream << std::fixed;
    stream << std::setprecision(0);
    stream << num;
    str = stream.str();
  }
  else if (precision == -2)
  {
    stream << std::fixed << std::setprecision(0) << num;
    str = stream.str();
  }
  else
  {
    stream << std::fixed << std::setprecision(precision) << num;
    str = stream.str();
  }

  if (precision == -2)
    return str + ".";
  return str;
}

static void display_task(void *arg)
{
  struct display_data recv_data;
  while (1)
  {
    if (xQueueReceive(display_queue, &(recv_data), portMAX_DELAY))
    {
      switch (recv_data.p0)
      {
      case 1:
      {
        back_light(recv_data.p1);
        break;
      }
      case 2:
      {
        print_hello_scrn();
        break;
      }
      case 3:
      {
        select_setup();
        break;
      }
      case 4:
      {
        select_letmetry();
        break;
      }
      case 5:
      {
        show_update_popup(recv_data.p7);
        break;
      }
      case 6:
      {
        select_update(recv_data.p7);
        break;
      }
      case 7:
      {
        select_later(recv_data.p7);
        break;
      }
      case 8:
      {
        print_ssid(recv_data.p7, recv_data.p8, recv_data.p3, recv_data.p1, recv_data.p2);
        break;
      }
      case 9:
      {
        print_in_mode(recv_data.p1);
        break;
      }
      case 10:
      {
        print_psw(recv_data.p7, recv_data.p1, recv_data.p2);
        break;
      }
      case 11:
      {
        print_connected_scrn();
        break;
      }
      case 12:
      {
        print_psw_in_scrn();
        break;
      }
      case 13:
      {
        print_mobile_in_scrn();
        break;
      }
      case 14:
      {
        print_otp_in_scrn();
        break;
      }
      case 15:
      {
        print_normal_mode_scrn();
        break;
      }
      case 16:
      {
        print_screen_no(recv_data.p5);
        break;
      }
      case 17:
      {
        print_operand(recv_data.p7);
        break;
      }
      case 18:
      {
        blank_calci_scrn();
        break;
      }
      case 19:
      {
        print_operator(recv_data.p4);
        break;
      }
      case 20:
      {
        print_mem_operator(recv_data.p4);
        break;
      }
      case 21:
      {
        print_result(recv_data.p6);
        break;
      }
      case 22:
      {
        print_mrc_scrn(recv_data.p6);
        break;
      }
      case 23:
      {
        print_mobile_no(recv_data.p7, recv_data.p1);
        break;
      }
      case 24:
      {
        print_otp_no(recv_data.p7, recv_data.p1);
        break;
      }
      case 25:
      {
        print_main_menu(recv_data.p1);
        break;
      }
      case 26:
      {
        print_gst_rate_set_screen(recv_data.p1, recv_data.p2, recv_data.p7, recv_data.p8);
        break;
      }
      case 27:
      {
        print_connecting_scrn();
        break;
      }
      case 28:
      {
        print_syncing_scrn();
        break;
      }
      case 29:
      {
        print_network_error_scrn();
        break;
      }
      case 30:
      {
        print_validating_scrn();
        break;
      }
      case 31:
      {
        print_sending_scrn();
        break;
      }
      case 32:
      {
        print_not_valid_no_scrn();
        break;
      }
      case 33:
      {
        print_connection_failed_scrn();
        break;
      }
      case 34:
      {
        print_login_again_scrn();
        break;
      }
      case 35:
      {
        print_synced_scrn();
        break;
      }
      case 36:
      {
        print_scanning();
        break;
      }
      case 37:
      {
        print_found();
        break;
      }
      case 38:
      {
        clear_operator();
        break;
      }
      case 39:
      {
        clear_mem_operator();
        break;
      }
      case 40:
      {
        print_signal_strength(recv_data.p3);
        break;
      }
      case 41:
      {
        print_try_update_again();
        break;
      }
      case 42:
      {
        print_no_update_available();
        break;
      }
      case 43:
      {
        print_checking_update();
        break;
      }
      case 44:
      {
        print_downloading(recv_data.p5);
        break;
      }
      case 45:
      {
        print_last_calculation(recv_data.p7, recv_data.p4, recv_data.p8);
        break;
      }
      case 46:
      {
        print_last_calculation(recv_data.p7, recv_data.p4, "");
        break;
      }
      case 47:
      {
        clear_last_calculation();
        break;
      }
      case 48:
      {
        print_operand(recv_data.p6, recv_data.p3);
        break;
      }
      case 49:
      {
        print_last_calculation(recv_data.p6, recv_data.p4);
        break;
      }
      case 50:
      {
        print_last_calculation(recv_data.p6, recv_data.p4, recv_data.p9);
        break;
      }
      case 51:
      {
        print_equal_sign();
        break;
      }
      case 52:
      {
        remove_equal_perc_sign();
        break;
      }
      case 53:
      {
        print_perc_sign();
        break;
      }
      case 54:
      {
        print_gst_sign(recv_data.p3, recv_data.p4);
        break;
      }
      case 55:
      {
        remove_gst_sign();
        break;
      }
      case 56:
      {
        print_mem_sign(recv_data.p4);
        break;
      }
      case 57:
      {
        remove_mem_sign();
        break;
      }
      case 58:
      {
        print_cash_sign(recv_data.p4);
        break;
      }
      case 59:
      {
        remove_cash_sign();
        break;
      }
      case 60:
      {
        print_start_setup_scrn(recv_data.p1);
        break;
      }
      case 61:
      {
        print_battery_charge(recv_data.p1);
        break;
      }
      case 62:
      {
        print_error();
        break;
      }
      case 63:
      {
        print_main_menu(recv_data.p1, recv_data.p7, recv_data.p8);
        break;
      }
      case 64:
      {
        print_in_gst(recv_data.p1, recv_data.p7);
        break;
      }
      case 65:
      {
        print_in_gst(recv_data.p1);
        break;
      }
      case 66:
      {
        print_system_info(recv_data.p1, recv_data.p2, recv_data.p7, recv_data.p8);
        break;
      }
      case 67:
      {
        print_check_mode();
        break;
      }
      case 68:
      {
        clear_check_mode();
        break;
      }
      case 69:
      {
        clear_operator_block();
        break;
      }
      case 70:
      {
        print_result_operator(recv_data.p1);
        break;
      }
      case 71:
      {
        print_online_trans_instruct(recv_data.p1);
        break;
      }
      case 72:
      {
        remove_online_trans_instruct();
        break;
      }
      case 73:
      {
        print_gt_sign(recv_data.p1);
        break;
      }
      case 74:
      {
        remove_gt_sign();
        break;
      }
      case 75:
      {
        print_correct_mode();
        break;
      }
      case 76:
      {
        clear_correct_mode();
        break;
      }
      case 77:
      {
        print_onboarding_complete();
        break;
      }
      case 78:
      {
        print_update_done();
        break;
      }
      case 79:
      {
        print_invalid_amount_err();
        break;
      }
      case 80:
      {
        clear_invalid_amount_err();
        break;
      }
      case 81:
      {
        print_fct_rst_msg(recv_data.p1);
        break;
      }
      case 82:
      {
        print_sync();
        break;
      }
      case 83:
      {
        clear_sync();
        break;
      }
      case 84:
      {
        print_normal_mode_ac_scrn();
        break;
      }
      case 85:
      {
        print_cust_details();
        break;
      }
      case 86:
      {
        select_cust_name();
        break;
      }
      case 87:
      {
        select_cust_num();
        break;
      }
      case 88:
      {
        select_cust_remark();
        break;
      }
      case 89:
      {
        select_cust_done();
        break;
      }
      case 90:
      {
        select_cust_pay_later();
        break;
      }
      case 91:
      {
        print_cust_details_in_name(recv_data.p7, recv_data.p1, recv_data.p2);
        break;
      }
      case 92:
      {
        print_cust_details_in_num(recv_data.p7, recv_data.p1, recv_data.p2);
        break;
      }
      case 93:
      {
        print_cust_details_in_remark(recv_data.p7, recv_data.p1, recv_data.p2);
        break;
      }
      case 94:
      {
        print_trans_saved();
        break;
      }
      case 95:
      {
        print_instruction_scrn(recv_data.p1, recv_data.p2);
        break;
      }
      case 96:
      {
        print_app_qr_code();
        break;
      }
      case 97:
      {
        print_help_qr_code();
        break;
      }
      case 98:
      {
        print_trans_saved_from_main();
        break;
      }
      case 99:
      {
        remove_trans_saved_from_main();
        break;
      }
      case 100:
      {
        print_back_scrn();
        break;
      }
      case 101:
      {
        show_update_ntfy_popup(recv_data.p7);
        break;
      }
      case 102:
      {
        select_upd_ntfy__update();
        break;
      }
      case 103:
      {
        select_upd_ntfy_later();
        break;
      }
      case 104:
      {
        print_wifi_not_found_scrn();
        break;
      }
      case 105:
      {
        show_initializing();
        break;
      }
      case 106:
      {
        display_show_setting_menu(recv_data.p3, recv_data.p1, recv_data.p7, recv_data.p8);
        break;
      }
      case 107:
      {
        display_show_setting_menu(recv_data.p3, recv_data.p1, recv_data.p7);
        break;
      }
      case 108:
      {
        display_show_setting_menu(recv_data.p3, recv_data.p1);
        break;
      }
      case 109:
      {
        display_setting_ssid(recv_data.p7);
        break;
      }
      case 110:
      {
        display_setting_ssid(recv_data.p3, recv_data.p1, recv_data.p2, recv_data.p7, recv_data.p8);
        break;
      }
      case 111:
      {
        display_setting_wifi_scanning();
        break;
      }
      case 112:
      {
        display_err_wifi_not_connected();
        break;
      }
      case 113:
      {
        display_setting_log_mobile_input();
        break;
      }
      case 114:
      {
        display_setting_log_otp_not_valid();
        break;
      }
      case 115:
      {
        display_setting_log_otp_validation_failed();
        break;
      }
      case 116:
      {
        display_setting_log_otp_login_success();
        break;
      }
      case 117:
      {
        display_err_not_login();
        break;
      }
      case 118:
      {
        display_wifi_connected_goto_login();
        break;
      }
      case 119:
      {
        display_battery_low();
        break;
      }
      case 120:
      {
        remove_battery_charge_symbol();
        break;
      }
      case 121:
      {
        print_last_calculation(recv_data.p6, recv_data.p1, recv_data.p9, recv_data.p2, "Hi");
        break;
      }
      case 122:
      {
        display_trans_count(recv_data.p5);
        break;
      }
      case 123:
      {
        save_current_scrn();
        break;
      }
      case 124:
      {
        display_error_entry_wifi(recv_data.p1);
        break;
      }
      case 125:
      {
        display_error_entry_login(recv_data.p1);
        break;
      }
      case 126:
      {
        display_error_wifi(recv_data.p1);
        break;
      }
      case 127:
      {
        display_error_login(recv_data.p1);
        break;
      }
      case 128:
      {
        display_message_when_coying_file_data_to_db();
        break;
      }
      case 129:
      {
        display_wait_syncing();
        break;
      }
      case 130:
      {
        dispaly_shutting_down();
        break;
      }
      }
    }
    if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
    {
      printf("Display task deleting\n");
      vTaskDelete(NULL);
    }
  }
}

void back_light(bool bg_light)
{
  Display.back_light(bg_light);
  if (!bg_light)
  {
    Display.clear_display();
  }
}

void display_message_when_coying_file_data_to_db()
{
  Display.clear_display();
  Display.drawstring_center_arial14(4, 0, 191, "Updating...");
}
void print_hello_scrn()
{
  Display.clear_display();
  Display.set_page_address(0);
  Display.set_column_address(3);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);
  Display.st7525_data(0x78);
  Display.st7525_data(0x30);
  Display.set_page_address(0);
  Display.set_column_address(20);
  Display.st7525_data(0xC0);
  Display.st7525_data(0xC0);
  Display.st7525_data(0x00);
  Display.st7525_data(0xF0);
  Display.st7525_data(0xF0);
  Display.st7525_data(0x00);
  Display.st7525_data(0xFC);
  Display.st7525_data(0xFC);

  Display.drawstring_callite24(1, 70, "Hello!");
  Display.drawstring_arial14(4, 5, "I'm Your SMART Calculator");
}

void print_start_setup_scrn(uint8_t opt)
{
  Display.clear_display();
  if (opt == 1)
  {
    Display.drawstring_callite24(2, 0, "Let's setup Device");
    // Display.drawstring_arial14(6, 20, "It'll only takes 2 mins :)");
  }
  else if (opt == 2)
  {
    Display.drawstring_arial14(2, 5, "1.");
    Display.drawsinvertedtring_arial14(2, 20, "Setup");
    Display.drawstring_arial14(5, 5, "2.");
    Display.drawstring_arial14(5, 20, "Let me try");
  }
}
void select_setup()
{
  Display.drawstring_arial14(2, 5, "1.");
  Display.drawsinvertedtring_arial14(2, 20, "Setup");
  Display.drawstring_arial14(5, 5, "2.");
  Display.drawstring_arial14(5, 20, "Let me try");
}
void select_letmetry()
{
  Display.drawstring_arial14(2, 5, "1.");
  Display.drawstring_arial14(2, 20, "Setup");
  Display.drawstring_arial14(5, 5, "2.");
  Display.drawsinvertedtring_arial14(5, 20, "Let me try");
}
void show_update_popup(const char *s1)
{
  Display.copy_current_screen();
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  std::string update_str = "Update Now(" + std::string(s1) + ")";
  Display.drawstring_arial14(2, 5, "1.");
  Display.drawsinvertedtring_arial14(2, 20, update_str.c_str());
  Display.drawstring_arial14(5, 5, "2.");
  Display.drawstring_arial14(5, 20, "Later");
}
void select_update(const char *s1)
{
  std::string update_str = "Update Now(" + std::string(s1) + ")";
  Display.drawstring_arial14(2, 5, "1.");
  Display.drawsinvertedtring_arial14(2, 20, update_str.c_str());
  Display.drawstring_arial14(5, 5, "2.");
  Display.drawstring_arial14(5, 20, "Later");
}
void select_later(const char *s1)
{
  std::string update_str = "Update Now(" + std::string(s1) + ")";
  Display.drawstring_arial14(2, 5, "1.");
  Display.drawstring_arial14(2, 20, update_str.c_str());
  Display.drawstring_arial14(5, 5, "2.");
  Display.drawsinvertedtring_arial14(5, 20, "Later");
}
void print_ssid(const char *ssid1, const char *ssid2, int8_t rssi, uint8_t idx, uint8_t ssid_idx)
{
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);

  if (idx == 0)
  {
    std::string wifi_ssid1_str = "1. ";
    std::string wifi_ssid2_str = "2. ";
    if (strlen(ssid1) > 10)
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
    }
    if (strlen(ssid2) > 10)
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
    }
    Display.drawsinvertedtring_arial14(2, 0, wifi_ssid1_str.c_str());
    Display.drawstring_arial14(5, 0, wifi_ssid2_str.c_str());
  }
  else if (idx == (ssid_idx - 1))
  {
    std::string wifi_ssid1_str = std::to_string(idx) + ". ";
    std::string wifi_ssid2_str = std::to_string(idx + 1) + ". ";
    if (strlen(ssid1) > 10)
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
    }
    if (strlen(ssid2) > 10)
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
    }
    Display.drawstring_arial14(2, 0, wifi_ssid1_str.c_str());
    Display.drawsinvertedtring_arial14(5, 0, wifi_ssid2_str.c_str());
  }
  else
  {
    if (rssi == 1)
    {
      std::string wifi_ssid1_str = std::to_string(idx) + ". ";
      std::string wifi_ssid2_str = std::to_string(idx + 1) + ". ";
      if (strlen(ssid1) > 10)
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
      }
      if (strlen(ssid2) > 10)
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
      }
      Display.drawstring_arial14(2, 0, wifi_ssid1_str.c_str());
      Display.drawsinvertedtring_arial14(5, 0, wifi_ssid2_str.c_str());
    }
    else
    {
      std::string wifi_ssid1_str = std::to_string(idx + 1) + ". ";
      std::string wifi_ssid2_str = std::to_string(idx + 2) + ". ";
      if (strlen(ssid1) > 10)
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
      }
      if (strlen(ssid2) > 10)
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
      }
      Display.drawsinvertedtring_arial14(2, 0, wifi_ssid1_str.c_str());
      Display.drawstring_arial14(5, 0, wifi_ssid2_str.c_str());
    }
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}

void print_in_mode(uint8_t in_mode)
{
  Display.clear_single_page_box(0, 156, 191);
  if (in_mode == 0)
    Display.drawstring(0, 172, "abc");
  else if (in_mode == 1)
    Display.drawstring(0, 172, "ABC");
  else if (in_mode == 2)
    Display.drawstring(0, 172, "123");
}
void print_psw(const char *psw, uint8_t len, uint8_t in_mode)
{
  // if (len == 1)
  // {
  //   Display.clear_page(2);
  //   Display.clear_page(3);
  //   Display.clear_page(4);
  //   Display.clear_page(5);
  // }

  Display.clear_page(5);
  Display.clear_page(6);

  print_in_mode(in_mode);

  Display.drawline(7, 1, 0, 191);
  Display.drawinvertedstring(7, 2, "Press shift to switch A/a/0");
  if (len == 0)
    return;
  Display.drawstring_center_arial14(5, 0, 191, psw);
}
void print_connected_scrn()
{
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawmark(2, 86);
  Display.drawstring_arial14(5, 45, "Wi-Fi connected");
  // Display.drawstring_arial14(5, 50, "to network");
}
void print_psw_in_scrn()
{
  Display.clear_display();
  print_in_mode(0);
  Display.drawstring_arial14(2, 20, "Enter Wi-Fi password");
  // Display.drawstring_arial14(4, 68, "Password");
  Display.drawline(7, 1, 0, 191);
  Display.drawinvertedstring(7, 2, "Press shift to switch A/a/0");
}

void print_mobile_in_scrn()
{
  Display.clear_display();
  Display.drawstring_arial14(0, 70, "Step-2");
  Display.drawstring_arial14(2, 10, "Enter your phone number");
  // Display.drawstring_arial14(5, 50, "Mobile no");
  Display.drawline(7, 1, 0, 191);
}
void print_otp_in_scrn()
{
  Display.clear_single_page_box(0, 69, 191);
  Display.clear_single_page_box(1, 69, 191);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(3, 60, "Enter OTP");
  Display.drawline(7, 1, 0, 191);
}
void print_normal_mode_scrn()
{
  Display.clear_display();
  Display.drawline(7, 1, 0, 191);
  Display.drawstring_right_callite24(4, 190, "0");
}
void print_normal_mode_ac_scrn()
{
  Display.clear_single_page_box(0, 100, 191);
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawline(7, 1, 0, 191);
  Display.drawstring_right_callite24(4, 190, "0");
}
void print_screen_no(uint16_t num)
{
  Display.clear_single_page_box(6, 0, 24);
  Display.drawstring(6, 5, std::to_string(num).c_str());
}
void print_operand(const char *data)
{
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.drawstring_right_callite24(4, 190, data);
}
void blank_calci_scrn()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
}
void print_operator(char data)
{
  Display.clear_single_page_box(2, 20, 191);
  Display.drawchar(2, 185, data);
}
void print_mem_operator(char data)
{
  Display.drawchar(3, 5, 'M');
  Display.drawchar(3, 12, data);
}
void print_result(double data)
{
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.drawstring_right_callite24(4, 190, double_to_str(data, -1).c_str());
}
void print_mrc_scrn(double mem_value)
{
  Display.drawstring(3, 172, "MRC");
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_right_callite24(4, 190, double_to_str(mem_value, -1).c_str());
}
void print_mobile_no(const char *mo_no, uint8_t len)
{
  // Display.clear_display();
  // Display.drawstring_arial14(1, 5, "Please enter your");
  // Display.drawstring_arial14(3, 5, "10-digit mobile number ");
  // Display.drawline(7, 1, 0, 191);
  Display.clear_page(5);
  Display.clear_page(6);
  if (len == 0)
    return;
  Display.drawstring_center_arial14(5, 0, 191, mo_no);
}
void print_otp_no(const char *otp_no, uint8_t len)
{
  Display.clear_page(5);
  Display.clear_page(6);
  if (len == 0)
    return;
  Display.drawstring_center_arial14(5, 0, 191, otp_no);
}

void print_main_menu(uint8_t menu_idx)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  if (menu_idx == 1)
  {
    Display.drawstring_arial14(2, 5, "1.");
    Display.drawsinvertedtring_arial14(2, 20, "Wi-Fi");
    Display.drawstring_arial14(5, 5, "2.");
    Display.drawstring_arial14(5, 20, "Log In");
  }
  else if (menu_idx == 3)
  {
    Display.drawstring_arial14(2, 5, "1.");
    Display.drawstring_arial14(2, 20, "Wi-Fi");
    Display.drawstring_arial14(5, 5, "2.");
    Display.drawsinvertedtring_arial14(5, 20, "Log In");
  }
  else if (menu_idx == 5)
  {
    Display.drawstring_arial14(2, 5, "2.");
    Display.drawstring_arial14(2, 20, "Log In");
    Display.drawstring_arial14(5, 5, "3.");
    Display.drawsinvertedtring_arial14(5, 20, "Set GST rate");
  }
  else if (menu_idx == 7)
  {
    Display.drawstring_arial14(2, 5, "3.");
    Display.drawstring_arial14(2, 20, "Set GST rate");
    Display.drawstring_arial14(5, 5, "4.");
    Display.drawsinvertedtring_arial14(5, 20, "System info");
  }
  else if (menu_idx == 8)
  {
    Display.drawstring_arial14(2, 5, "4.");
    Display.drawstring_arial14(2, 20, "System info");
    Display.drawstring_arial14(5, 5, "5.");
    Display.drawsinvertedtring_arial14(5, 20, "System Update");
  }
  else if (menu_idx == 9)
  {
    Display.drawstring_arial14(2, 5, "5.");
    Display.drawstring_arial14(2, 20, "System Update");
    Display.drawstring_arial14(5, 5, "6.");
    Display.drawsinvertedtring_arial14(5, 20, "Help videos");
  }
}
void print_main_menu(uint8_t menu_idx, const char *s1, const char *s2)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  std::string wifi_menu_str = "Wi-Fi";
  if (strlen(s1) > 0)
  {
    if (strlen(s1) > 15)
    {
      wifi_menu_str = wifi_menu_str + " (" + std::string(s1).substr(0, 15) + "...)";
    }
    else
    {
      wifi_menu_str = wifi_menu_str + " (" + std::string(s1) + ")";
    }
  }

  std::string login_menu_str = "";
  if (strlen(s2) > 0)
  {
    login_menu_str = "Log Out (" + std::string(s2) + ")";
  }
  else
  {
    login_menu_str = "Log In";
  }

  if (menu_idx == 1)
  {
    Display.drawstring_arial14(2, 5, "1.");
    Display.drawsinvertedtring_arial14(2, 20, wifi_menu_str.c_str());
    Display.drawstring_arial14(5, 5, "2.");
    Display.drawstring_arial14(5, 20, login_menu_str.c_str());
  }
  else if (menu_idx == 3)
  {
    Display.drawstring_arial14(2, 5, "1.");
    Display.drawstring_arial14(2, 20, wifi_menu_str.c_str());
    Display.drawstring_arial14(5, 5, "2.");
    Display.drawsinvertedtring_arial14(5, 20, login_menu_str.c_str());
  }
  else if (menu_idx == 5)
  {
    Display.drawstring_arial14(2, 5, "2.");
    Display.drawstring_arial14(2, 20, login_menu_str.c_str());
    Display.drawstring_arial14(5, 5, "3.");
    Display.drawsinvertedtring_arial14(5, 20, "Set GST rate");
  }
}
void print_system_info(uint8_t menu_idx, uint8_t selected, const char *s1, const char *s2)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  std::string menu_idx_str1 = "";
  std::string menu_idx_str2 = "";

  if (selected == 1)
  {
    menu_idx_str1 = std::to_string(menu_idx) + ".";
    menu_idx_str2 = std::to_string(menu_idx + 1) + ".";
    Display.drawstring_arial14(2, 5, menu_idx_str1.c_str());
    Display.drawsinvertedtring_arial14(2, 20, s1);
    Display.drawstring_arial14(5, 5, menu_idx_str2.c_str());
    Display.drawstring_arial14(5, 20, s2);
  }
  else if (selected == 2)
  {
    menu_idx_str1 = std::to_string(menu_idx - 1) + ".";
    menu_idx_str2 = std::to_string(menu_idx) + ".";
    Display.drawstring_arial14(2, 5, menu_idx_str1.c_str());
    Display.drawstring_arial14(2, 20, s1);
    Display.drawstring_arial14(5, 5, menu_idx_str2.c_str());
    Display.drawsinvertedtring_arial14(5, 20, s2);
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 178, "->");
}
void print_gst_rate_set_screen(uint8_t gst_index, uint8_t selected, const char *menu_gst_1, const char *menu_gst_2)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);

  if (selected == 1)
  {
    std::string menu_gst_str_ind_1 = std::to_string(gst_index + 1) + ".";
    std::string menu_gst_str_ind_2 = std::to_string(gst_index + 2) + ".";

    std::string menu_gst_str_1 = "GST+" + std::to_string(gst_index) + "          " + remove_tailing_zeros(menu_gst_1);
    std::string menu_gst_str_2 = "GST+" + std::to_string(gst_index + 1) + "          " + remove_tailing_zeros(menu_gst_2);

    Display.drawstring_arial14(2, 5, menu_gst_str_ind_1.c_str());
    Display.drawsinvertedtring_arial14(2, 20, menu_gst_str_1.c_str());
    Display.drawstring_arial14(5, 5, menu_gst_str_ind_2.c_str());
    Display.drawstring_arial14(5, 20, menu_gst_str_2.c_str());
  }
  else if (selected == 2)
  {
    std::string menu_gst_str_ind_1 = std::to_string(gst_index) + ".";
    std::string menu_gst_str_ind_2 = std::to_string(gst_index + 1) + ".";

    std::string menu_gst_str_1 = "GST+" + std::to_string(gst_index - 1) + "          " + remove_tailing_zeros(menu_gst_1);
    std::string menu_gst_str_2 = "GST+" + std::to_string(gst_index) + "          " + remove_tailing_zeros(menu_gst_2);
    Display.drawstring_arial14(2, 5, menu_gst_str_ind_1.c_str());
    Display.drawstring_arial14(2, 20, menu_gst_str_1.c_str());
    Display.drawstring_arial14(5, 5, menu_gst_str_ind_2.c_str());
    Display.drawsinvertedtring_arial14(5, 20, menu_gst_str_2.c_str());
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}

void print_connecting_scrn()
{
  Display.clear_display();
  Display.drawstring_arial14(4, 45, "Connecting....");
}
void print_syncing_scrn()
{
  Display.clear_display();
  Display.drawstring_center_arial14(4, 0, 191, "Syncing....");
}
void print_network_error_scrn()
{
  Display.clear_display();
  Display.drawcrossmark(2, 88);
  Display.drawstring_center_arial14(5, 0, 191, "Network Error");
  Display.drawstring(7, 10, "Retry or Check connection");
}
void print_validating_scrn()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(4, 45, "Validating....");
}
void print_sending_scrn()
{
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(4, 45, "Sending OTP");
}
void print_not_valid_no_scrn()
{
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(2, 88);
  Display.drawstring_center_arial14(5, 0, 191, "Number is not valid");
}
void print_connection_failed_scrn()
{
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(1, 88);
  Display.drawstring_center_arial14(3, 0, 191, "Something went wrong");
  Display.drawstring_center_arial14(5, 0, 191, "Please retry");
}
void print_login_again_scrn()
{
  Display.clear_display();
  Display.drawstring_center_arial14(4, 0, 191, "Please Login again");
}
void print_synced_scrn()
{
  Display.clear_display();
  Display.drawmark(2, 86);
  Display.drawstring_center_arial14(5, 0, 191, "Synced");
}
void print_scanning()
{
  Display.clear_display();
  Display.drawstring_arial14(0, 70, "Step-1");
  Display.drawstring_callite24(3, 0, "Connect Network");
}
void print_found()
{
  Display.clear_display();
  Display.drawstring(0, 78, "Wi-Fi");
}
void clear_operator()
{
  Display.clear_single_page_box(4, 65, 191);
  Display.clear_single_page_box(5, 65, 191);
  Display.clear_single_page_box(6, 65, 191);
}
void clear_mem_operator() { Display.clear_single_page_box(3, 5, 17); }
void print_signal_strength(int8_t rssi)
{
  Display.set_page_address(0);
  Display.set_column_address(20);
  if (rssi <= -80)
  {
    Display.st7525_data(0xC2);
    Display.st7525_data(0xC4);
    Display.st7525_data(0x00);
    Display.st7525_data(0xF8);
    Display.st7525_data(0xF0);
    Display.st7525_data(0x00);
    Display.st7525_data(0xFC);
    Display.st7525_data(0xFC);
  }
  else if (rssi <= -60)
  {
    Display.st7525_data(0xC0);
    Display.st7525_data(0xC0);
    Display.st7525_data(0x00);
    Display.st7525_data(0x0);
    Display.st7525_data(0x0);
    Display.st7525_data(0x00);
    Display.st7525_data(0x0);
    Display.st7525_data(0x0);
  }
  else if (rssi <= -40)
  {
    Display.st7525_data(0xC0);
    Display.st7525_data(0xC0);
    Display.st7525_data(0x00);
    Display.st7525_data(0xF0);
    Display.st7525_data(0xF0);
    Display.st7525_data(0x00);
    Display.st7525_data(0x0);
    Display.st7525_data(0x0);
  }
  else if (rssi <= -1)
  {
    Display.st7525_data(0xC0);
    Display.st7525_data(0xC0);
    Display.st7525_data(0x00);
    Display.st7525_data(0xF0);
    Display.st7525_data(0xF0);
    Display.st7525_data(0x00);
    Display.st7525_data(0xFC);
    Display.st7525_data(0xFC);
  }
  else
  {
    Display.st7525_data(0xC9);
    Display.st7525_data(0xC6);
    Display.st7525_data(0x06);
    Display.st7525_data(0xF9);
    Display.st7525_data(0xF0);
    Display.st7525_data(0x00);
    Display.st7525_data(0xFC);
    Display.st7525_data(0xFC);
  }
}

void print_try_update_again()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(4, 0, 191, "Try again");
}
void print_no_update_available()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(4, 0, 191, "No update available");
}
void print_checking_update()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(4, 0, 191, "Checking....");
}
void print_downloading(uint16_t perc)
{
  if (perc > 0)
  {
    Display.clear_single_page_box(4, 70, 109);
    Display.drawstring_arial14(4, 70, std::to_string(perc).c_str());
  }
  else
  {
    Display.clear_page(1);
    Display.clear_page(2);
    Display.clear_page(3);
    Display.clear_page(4);
    Display.clear_page(5);
    Display.clear_page(6);
    Display.clear_page(7);
    Display.drawstring_arial14(1, 40, "Downloading....");
    Display.drawstring_arial14(4, 70, std::to_string(perc).c_str());
    Display.drawstring_arial14(4, 110, "KB");
  }
}
void print_last_calculation(const char *operand_1, char sign_op, const char *operand_2)
{
  Display.clear_single_page_box(2, 65, 191);
  int left_space = 187 - (strlen(operand_1) * 5) - 7 - (strlen(operand_2) * 5);
  Display.drawstring(2, left_space, operand_1);
  Display.drawchar(2, left_space + (strlen(operand_1) * 5), sign_op);
  Display.drawstring(2, left_space + 7 + (strlen(operand_1) * 5), operand_2);
}
void print_last_calculation(double operand_1, char sign_op)
{
  Display.clear_single_page_box(2, 20, 191);
  std::string op_str1 = double_to_str(operand_1, -1);
  int left_space = 191 - (op_str1.length() * 7) - 7;
  Display.drawstring(2, left_space, op_str1.c_str());
  Display.drawchar(2, left_space + (op_str1.length() * 7), sign_op);
}
void print_last_calculation(double operand_1, char sign_op, double operand_2)
{
  Display.clear_single_page_box(2, 20, 191);
  std::string op_str1 = double_to_str(operand_1, -1);
  std::string op_str2 = double_to_str(operand_2, -1);
  int left_space = 191 - (op_str1.length() * 7) - 7 - (op_str2.length() * 7);
  Display.drawstring(2, left_space, op_str1.c_str());
  Display.drawchar(2, left_space + (op_str1.length() * 7), sign_op);
  Display.drawstring(2, left_space + 7 + (op_str1.length() * 7), op_str2.c_str());
}

void print_last_calculation(double operand_1, char sign_op1, double operand_2, char sign_op2, const char *ovld)
{
  Display.clear_single_page_box(2, 20, 191);
  std::string op_str1 = double_to_str(operand_1, -1);
  std::string op_str2 = double_to_str(operand_2, -1);
  int left_space = 191 - (op_str1.length() * 7) - 14 - (op_str2.length() * 7);
  Display.drawstring(2, left_space, op_str1.c_str());
  Display.drawchar(2, left_space + (op_str1.length() * 7), sign_op1);
  Display.drawstring(2, left_space + 7 + (op_str1.length() * 7), op_str2.c_str());
  Display.drawchar(2, left_space + 7 + (op_str1.length() * 7) + (op_str2.length() * 7), sign_op2);
}

void clear_last_calculation()
{
  Display.clear_single_page_box(2, 25, 191);
}
void print_operand(double data, int8_t precision)
{
  Display.clear_single_page_box(4, 25, 191);
  Display.clear_single_page_box(5, 25, 191);
  Display.clear_single_page_box(6, 25, 191);
  Display.drawstring_right_callite24(4, 190, double_to_str(data, precision).c_str());
}
void print_equal_sign()
{
  Display.clear_single_page_box(5, 0, 24);
  Display.drawstring(5, 5, "=");
}
void print_perc_sign()
{
  Display.clear_single_page_box(5, 0, 24);
  Display.drawstring(5, 5, "%");
}
void remove_equal_perc_sign()
{
  Display.clear_single_page_box(5, 0, 24);
}
void print_gst_sign(int8_t gst_type, char key)
{
  Display.clear_single_page_box(4, 0, 24);
  Display.drawchar(4, 3, 'G');
  if (gst_type == 1)
    Display.drawchar(4, 10, '+');
  else if (gst_type == -1)
    Display.drawchar(4, 10, '-');
  Display.drawchar(4, 16, key);
}
void remove_gst_sign()
{
  Display.clear_single_page_box(4, 0, 24);
}

void print_mem_sign(char key)
{
  Display.clear_single_page_box(3, 0, 24);
  Display.drawchar(3, 3, 'M');
  Display.drawchar(3, 10, key);
}

void remove_mem_sign()
{
  Display.clear_single_page_box(3, 0, 24);
}

void print_cash_sign(char key)
{
  Display.clear_page(7);
  Display.drawline(7, 1, 0, 191);
  if (key == '+')
    Display.drawinvertedstring(7, 143, "CASH IN");
  else
    Display.drawinvertedstring(7, 136, "CASH OUT");
}

void remove_cash_sign()
{
  Display.clear_page(7);
  Display.drawline(7, 1, 0, 191);
}

void print_battery_charge(uint8_t charge)
{
  Display.set_page_address(0);
  Display.set_column_address(3);
  Display.st7525_data(0xFC);
  int i = 0;
  for (i = 0; i <= charge; i++)
    Display.st7525_data(0xFC);
  for (int j = i; j <= 6; j++)
    Display.st7525_data(0x84);
  Display.st7525_data(0xFC);
  Display.st7525_data(0x78);
  Display.st7525_data(0x30);
}

void print_error()
{
  Display.clear_single_page_box(4, 25, 191);
  Display.clear_single_page_box(5, 25, 191);
  Display.clear_single_page_box(6, 25, 191);
  Display.drawsinvertedtring_arial14(4, 65, "ERROR");
}

void print_in_gst(uint8_t gst_idx, const char *show_gst)
{
  Display.clear_page(5);
  Display.clear_page(6);
  Display.drawstring_center_arial14(5, 0, 191, show_gst);
}

void print_in_gst(uint8_t gst_idx)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);

  std::string gst_instruc_str = "Type to set GST+" + std::to_string(gst_idx);
  Display.drawstring_arial14(2, 20, gst_instruc_str.c_str());
  Display.drawline(7, 1, 0, 191);
  Display.drawinvertedstring(7, 0, " =: to set     C: to cancel");
}

void print_check_mode()
{
  Display.clear_single_page_box(0, 170, 191);
  Display.drawinvertedstring(0, 170, "CHK");
}

void clear_check_mode()
{
  Display.clear_single_page_box(0, 170, 191);
}

void clear_operator_block()
{
  Display.clear_single_page_box(1, 0, 24);
  Display.clear_single_page_box(2, 0, 24);
  Display.clear_single_page_box(3, 0, 24);
  Display.clear_single_page_box(4, 0, 24);
  Display.clear_single_page_box(5, 0, 24);
  Display.clear_single_page_box(6, 0, 24);
}

void print_result_operator(char data)
{
  Display.clear_single_page_box(5, 0, 24);
  Display.drawchar(5, 5, data);
}

void print_online_trans_instruct(uint8_t pay_type)
{
  if (pay_type == 1)
    Display.drawinvertedstring(7, 0, "PT: CASH");
  else if (pay_type == 2)
    Display.drawinvertedstring(7, 0, "PT: ONLINE");
  else if (pay_type == 3)
    Display.drawinvertedstring(7, 0, "PT: DUE");
}
void remove_online_trans_instruct()
{
  Display.clear_page(7);
  Display.drawline(7, 1, 0, 192);
}

void print_invalid_amount_err()
{
  Display.drawinvertedstring(7, 0, "PT: INVALID");
}

void clear_invalid_amount_err()
{
  Display.clear_page(7);
  Display.drawline(7, 1, 0, 192);
}

void print_gt_sign(uint8_t gt_op_sign)
{
  Display.clear_single_page_box(2, 0, 20);
  switch (gt_op_sign)
  {
  case 1:
  {
    Display.drawstring(2, 0, "G +");
    break;
  }
  case 2:
  {
    Display.drawstring(2, 0, "G -");
    break;
  }
  case 3:
  {
    Display.drawstring(2, 0, "GST");
    break;
  }
  case 4:
  {
    Display.drawstring(2, 0, "GT");
    break;
  }
  }
}

void print_correct_mode()
{
  Display.clear_single_page_box(0, 170, 191);
  Display.drawinvertedstring(0, 170, "CRT");
}

void clear_correct_mode()
{
  Display.clear_single_page_box(0, 170, 191);
}

void remove_gt_sign()
{
  Display.clear_single_page_box(2, 0, 20);
}

void print_onboarding_complete()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawmark(2, 85);
  Display.drawstring_center_arial14(5, 0, 191, "Set Up Completed :)");
}

void print_update_done()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_callite24(1, 0, 191, "Update done!");
  Display.drawstring_center_arial14(5, 0, 191, "Device restarting...");
}

void print_fct_rst_msg(uint8_t msg_type)
{
  if (msg_type == 1)
  {
    Display.clear_page(1);
    Display.clear_page(2);
    Display.clear_page(3);
    Display.clear_page(4);
    Display.clear_page(5);
    Display.clear_page(6);
    Display.clear_page(7);
    Display.drawstring_callite24(1, 20, "Factory Reset!");
  }
  switch (msg_type)
  {
  case 1:
    Display.drawstring_arial14(5, 70, "Erasing...");
    break;
  case 2:
    Display.drawstring_arial14(5, 80, "Erased");
    break;
  case 3:
    Display.drawstring_arial14(5, 40, "Done restarting...");
    break;
  case 4:
    Display.drawstring_arial14(5, 40, "Failed restarting...");
    break;
  case 5:
    Display.drawstring_arial14(5, 30, "Not found restarting...");
    break;
  }
}

void print_sync()
{
  Display.set_page_address(0);
  Display.set_column_address(35);
  Display.st7525_data(0x1A);
  Display.st7525_data(0x22);
  Display.st7525_data(0x22);
  Display.st7525_data(0x22);
  Display.st7525_data(0x2C);
  // Display.st7525_data(0x48);
  // Display.st7525_data(0xA0);
}

void clear_sync()
{
  Display.clear_single_page_box(0, 35, 42);
}

void print_cust_details()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(1, 0, "Name:");
  Display.drawstring_arial14(3, 0, "Number:");
  Display.drawstring_arial14(5, 0, "Remark:");
  Display.drawinvertedstring(7, 0, "DONE");
  Display.drawstring(7, 85, "DUE");
}

void select_cust_name()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(2, 0, 191, "(2/3)Customer Name");
  Display.drawline(7, 1, 0, 191);
  Display.drawinvertedstring(7, 0, "<-");
  Display.drawinvertedstring(7, 80, "Enter");
  Display.drawinvertedstring(7, 178, "->");
}

void select_cust_num()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);

  Display.drawstring_center_arial14(2, 0, 191, "(1/3)Mobile No/Customer ID");
  Display.drawline(7, 1, 0, 191);
  Display.drawinvertedstring(7, 0, "<-");
  Display.drawinvertedstring(7, 80, "Enter");
  Display.drawinvertedstring(7, 178, "->");
}

void select_cust_remark()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(2, 0, 191, "(3/3)Customer Remark");
  Display.drawline(7, 1, 0, 191);
  Display.drawinvertedstring(7, 0, "<-");
  Display.drawinvertedstring(7, 80, "Enter");
  Display.drawinvertedstring(7, 178, "->");
}

void select_cust_done()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  // Display.drawstring_arial14(1, 0, "Number:");
  // Display.drawline(3, 1, 61, 190);
  Display.drawstring_arial14(2, 0, "Remark:");
  Display.drawline(4, 1, 61, 190);
  Display.drawsinvertedtring_arial14(5, 35, "Done");
  Display.drawstring_arial14(5, 90, "DUE");
}

void select_cust_pay_later()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  // Display.drawstring_arial14(1, 0, "Number:");
  // Display.drawline(3, 1, 61, 190);
  Display.drawstring_arial14(2, 0, "Remark:");
  Display.drawline(4, 1, 61, 190);
  Display.drawstring_arial14(5, 35, "Done");
  Display.drawsinvertedtring_arial14(5, 90, "DUE");
}

void print_cust_details_in_name(const char *psw, uint8_t len, uint8_t in_mode)
{
  Display.clear_page(5);
  Display.clear_page(6);

  print_in_mode(in_mode);

  Display.drawstring_center_arial14(5, 0, 191, psw);
}

void print_cust_details_in_num(const char *psw, uint8_t len, uint8_t in_mode)
{
  Display.clear_page(5);
  Display.clear_page(6);

  print_in_mode(in_mode);

  Display.drawstring_center_arial14(5, 0, 191, psw);
}

void print_cust_details_in_remark(const char *psw, uint8_t len, uint8_t in_mode)
{
  Display.clear_page(5);
  Display.clear_page(6);

  print_in_mode(in_mode);

  Display.drawstring_center_arial14(5, 0, 191, psw);
}

void print_trans_saved()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawmark(2, 85);
  Display.drawstring_arial14(5, 30, "Transaction Saved");
}

void print_instruction_scrn(uint8_t key, uint8_t select_insruct)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring(0, 60, "Instruction");
  if (key == 'R')
  {
    if (select_insruct == 1)
    {
      Display.drawinvertedstring(2, 0, "Correct");
      Display.drawstring(2, 60, "-To fix any mistake");
      Display.drawinvertedstring(5, 0, "Shift");
      Display.drawstring(5, 60, "-To switch A/a/1");
      Display.drawinvertedstring(7, 120, "Forward ->");
    }
    else if (select_insruct == 2)
    {
      Display.drawinvertedstring(2, 0, "Menu");
      Display.drawstring(2, 60, "-Long press button");
      Display.drawinvertedstring(5, 0, "Enter");
      Display.drawstring(5, 60, "-To select");
      Display.drawinvertedstring(7, 120, "Forward ->");
      Display.drawinvertedstring(7, 0, "<- Back");
    }
    else if (select_insruct == 3)
    {
      Display.drawinvertedstring(2, 0, "Next Extra");
      Display.drawstring(2, 70, " -Customer info");
      Display.drawinvertedstring(4, 0, "CashIn/Out");
      Display.drawstring(4, 70, "-Multi press for");
      Display.drawstring(5, 70, "Cash/Online/Due");
      Display.drawinvertedstring(7, 120, "Forward ->");
      Display.drawinvertedstring(7, 0, "<- Back");
    }
    else if (select_insruct == 4)
    {
      Display.drawstring_center_arial14(3, 0, 191, "Let's setup the device");
      Display.drawinvertedstring(7, 135, "Enter ->");
      Display.drawinvertedstring(7, 0, "<- Back");
    }
  }
  else if (key == 'L')
  {
    if (select_insruct == 1)
    {
      Display.drawinvertedstring(2, 0, "Correct");
      Display.drawstring(2, 60, "-To fix any mistake");
      Display.drawinvertedstring(5, 0, "Shift");
      Display.drawstring(5, 60, "-To switch A/a/1");
      Display.drawinvertedstring(7, 120, "Forward ->");
    }
    else if (select_insruct == 2)
    {
      Display.drawinvertedstring(2, 0, "Menu");
      Display.drawstring(2, 60, "-Long press button");
      Display.drawinvertedstring(5, 0, "Enter");
      Display.drawstring(5, 60, "-To select");
      Display.drawinvertedstring(7, 120, "Forward ->");
      Display.drawinvertedstring(7, 0, "<- Back");
    }
    else if (select_insruct == 3)
    {
      Display.drawinvertedstring(2, 0, "Next Extra");
      Display.drawstring(2, 70, " -Customer info");
      Display.drawinvertedstring(5, 0, "Multi Press");
      Display.drawstring(5, 74, "-Cash/Online/Due");
      Display.drawinvertedstring(7, 120, "Forward ->");
      Display.drawinvertedstring(7, 0, "<- Back");
    }
    else if (select_insruct == 4)
    {
      Display.drawstring_center_arial14(3, 0, 191, "Let's setup the device");
      Display.drawinvertedstring(7, 135, "Enter ->");
      Display.drawinvertedstring(7, 0, "<- Back");
    }
  }
}

void print_app_qr_code()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(2, 0, "Download Tohands");
  Display.drawstring_arial14(4, 0, "App Scan This QR");
  // Display.drawstring(5, 60, "");
  Display.drawinvertedstring(7, 64, "Enter");
  Display.draw_app_qr(1, 140);
}

void print_help_qr_code()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(2, 0, "How to use");
  Display.drawstring_arial14(4, 0, "Videos Scan This QR");
  Display.draw_help_qr(1, 148);
}

void print_trans_saved_from_main()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawmark(2, 85);
  Display.drawstring_arial14(5, 30, "Transaction Saved");
}
void remove_trans_saved_from_main()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
}
void print_back_scrn()
{
  Display.print_saved_screen();
}

void show_update_ntfy_popup(const char *s1)
{
  Display.copy_current_screen();
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  std::string update_str = "(" + std::string(s1) + ")" + "Update available";
  if (update_str.length() > 27)
  {
    std::string temp_s1 = update_str.substr(0, 24);
    temp_s1 = temp_s1 + "...";
    Display.drawstring(3, 10, temp_s1.c_str());
  }
  else
  {
    Display.drawstring(3, 10, update_str.c_str());
  }
  Display.drawstring(7, 0, "Later");
  Display.drawinvertedstring(7, 122, "Update now");
}

void select_upd_ntfy__update()
{
  Display.drawstring(7, 0, "Later");
  Display.drawinvertedstring(7, 122, "Update now");
}

void select_upd_ntfy_later()
{
  Display.drawinvertedstring(7, 0, "Later");
  Display.drawstring(7, 122, "Update now");
}

void print_wifi_not_found_scrn()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(1, 88);
  Display.drawstring_center_arial14(3, 0, 191, "WiFi not found");
  Display.drawstring_center_arial14(5, 0, 191, "Please retry");
}
void show_initializing()
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(4, 0, 191, "Initializing...");
}
void display_show_setting_menu(int show_menu, uint8_t key, const char *ssid, const char *login)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  if (show_menu == 1)
  {
    if (strlen(ssid) > 0)
    {
      std::string ssid_menu_str = "1. WiFi(";
      if (strlen(ssid) > 10)
      {
        ssid_menu_str = ssid_menu_str + std::string(ssid).substr(0, 10) + "...)";
      }
      else
      {
        ssid_menu_str = ssid_menu_str + std::string(ssid) + ")";
      }
      Display.drawsinvertedtring_arial14(2, 0, ssid_menu_str.c_str());
    }
    else
    {
      Display.drawsinvertedtring_arial14(2, 0, "1. WiFi");
    }
    if (strlen(login) > 0)
    {
      std::string login_menu_str = "2. Logout(" + std::string(login) + ")";
      Display.drawstring_arial14(5, 0, login_menu_str.c_str());
    }
    else
    {
      Display.drawstring_arial14(5, 0, "2. Login");
    }
  }
  else if (show_menu == 2)
  {
    if (key == 'R')
    {
      if (strlen(ssid) > 0)
      {
        std::string ssid_menu_str = "1. WiFi(";
        if (strlen(ssid) > 10)
        {
          ssid_menu_str = ssid_menu_str + std::string(ssid).substr(0, 10) + "...)";
        }
        else
        {
          ssid_menu_str = ssid_menu_str + std::string(ssid) + ")";
        }
        Display.drawstring_arial14(2, 0, ssid_menu_str.c_str());
      }
      else
      {
        Display.drawstring_arial14(2, 0, "1. WiFi");
      }
      if (strlen(login) > 0)
      {
        std::string login_menu_str = "2. Logout(" + std::string(login) + ")";
        Display.drawsinvertedtring_arial14(5, 0, login_menu_str.c_str());
      }
      else
      {
        Display.drawsinvertedtring_arial14(5, 0, "2. Login");
      }
    }
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}
void display_show_setting_menu(int show_menu, uint8_t key, const char *ssid_login)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  if (show_menu == 2)
  {
    if (key == 'R')
    {
    }
    else
    {
      if (strlen(ssid_login) > 0)
      {
        std::string login_menu_str = "2. Logout(" + std::string(ssid_login) + ")";
        Display.drawsinvertedtring_arial14(2, 0, login_menu_str.c_str());
      }
      else
      {
        Display.drawsinvertedtring_arial14(2, 0, "2. Login");
      }
      Display.drawstring_arial14(5, 0, "3. Set GST rate");
    }
  }
  else if (show_menu == 3)
  {
    if (key == 'R')
    {
      if (strlen(ssid_login) > 0)
      {
        std::string login_menu_str = "2. Logout(" + std::string(ssid_login) + ")";
        Display.drawstring_arial14(2, 0, login_menu_str.c_str());
      }
      else
      {
        Display.drawstring_arial14(2, 0, "2. Login");
      }
      Display.drawsinvertedtring_arial14(5, 0, "3. Set GST rate");
    }
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}
void display_show_setting_menu(int show_menu, uint8_t key)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  if (show_menu == 3)
  {
    if (key == 'R')
    {
    }
    else
    {
      Display.drawsinvertedtring_arial14(2, 0, "3. Set GST rate");
      Display.drawstring_arial14(5, 0, "4. System info");
    }
  }
  else if (show_menu == 4)
  {
    if (key == 'R')
    {
      Display.drawstring_arial14(2, 0, "3. Set GST rate");
      Display.drawsinvertedtring_arial14(5, 0, "4. System info");
    }
    else
    {
      Display.drawsinvertedtring_arial14(2, 0, "4. System info");
      Display.drawstring_arial14(5, 0, "5. System update");
    }
  }
  else if (show_menu == 5)
  {
    if (key == 'R')
    {
      Display.drawstring_arial14(2, 0, "4. System info");
      Display.drawsinvertedtring_arial14(5, 0, "5. System update");
    }
    else
    {
      Display.drawsinvertedtring_arial14(2, 0, "5. System update");
      Display.drawstring_arial14(5, 0, "6. Help videos");
    }
  }
  else if (show_menu == 6)
  {
    Display.drawstring_arial14(2, 0, "5. System update");
    Display.drawsinvertedtring_arial14(5, 0, "6. Help videos");
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}

void display_setting_ssid(const char *ssid1)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  std::string wifi_ssid_str = "1. ";
  if (strlen(ssid1) > 10)
  {
    wifi_ssid_str = wifi_ssid_str + std::string(ssid1).substr(0, 10) + "...";
  }
  else
  {
    wifi_ssid_str = wifi_ssid_str + std::string(ssid1).substr(0, 10);
  }
  Display.drawsinvertedtring_arial14(2, 0, wifi_ssid_str.c_str());
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}
void display_setting_ssid(int wifi_idx, uint8_t total_wifi, uint8_t key, const char *ssid1, const char *ssid2)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  if (wifi_idx == 0)
  {
    std::string wifi_ssid1_str = "1. ";
    std::string wifi_ssid2_str = "2. ";
    if (strlen(ssid1) > 10)
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
    }
    if (strlen(ssid2) > 10)
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
    }
    Display.drawsinvertedtring_arial14(2, 0, wifi_ssid1_str.c_str());
    Display.drawstring_arial14(5, 0, wifi_ssid2_str.c_str());
  }
  else if (wifi_idx == (total_wifi - 1))
  {
    std::string wifi_ssid1_str = std::to_string(wifi_idx) + ". ";
    std::string wifi_ssid2_str = std::to_string(wifi_idx + 1) + ". ";
    if (strlen(ssid1) > 10)
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
    }
    if (strlen(ssid2) > 10)
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
    }
    else
    {
      wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
    }
    Display.drawstring_arial14(2, 0, wifi_ssid1_str.c_str());
    Display.drawsinvertedtring_arial14(5, 0, wifi_ssid2_str.c_str());
  }
  else
  {
    if (key == 'R')
    {
      std::string wifi_ssid1_str = std::to_string(wifi_idx) + ". ";
      std::string wifi_ssid2_str = std::to_string(wifi_idx + 1) + ". ";
      if (strlen(ssid1) > 10)
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
      }
      if (strlen(ssid2) > 10)
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
      }
      Display.drawstring_arial14(2, 0, wifi_ssid1_str.c_str());
      Display.drawsinvertedtring_arial14(5, 0, wifi_ssid2_str.c_str());
    }
    else
    {
      std::string wifi_ssid1_str = std::to_string(wifi_idx + 1) + ". ";
      std::string wifi_ssid2_str = std::to_string(wifi_idx + 2) + ". ";
      if (strlen(ssid1) > 10)
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid1_str = wifi_ssid1_str + std::string(ssid1).substr(0, 10);
      }
      if (strlen(ssid2) > 10)
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10) + "...";
      }
      else
      {
        wifi_ssid2_str = wifi_ssid2_str + std::string(ssid2).substr(0, 10);
      }
      Display.drawsinvertedtring_arial14(2, 0, wifi_ssid1_str.c_str());
      Display.drawstring_arial14(5, 0, wifi_ssid2_str.c_str());
    }
  }
  Display.drawstring(7, 0, "<-");
  Display.drawstring(7, 80, "Enter");
  Display.drawstring(7, 178, "->");
}

void display_err_wifi_not_connected(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(2, 88);
  Display.drawstring_center_arial14(5, 0, 191, "Wi-Fi not connected");
}

void display_setting_wifi_scanning(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_callite24(3, 0, 191, "Connect Network");
}

void display_setting_log_mobile_input(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(2, 10, "Enter your phone number");
  Display.drawline(7, 1, 0, 191);
}

void display_setting_log_otp_not_valid(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(2, 88);
  Display.drawstring_center_arial14(5, 0, 191, "OTP must be 4 digit");
}

void display_setting_log_otp_validation_failed(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(2, 88);
  Display.drawstring_center_arial14(5, 0, 191, "OTP validation failed");
}

void display_setting_log_otp_login_success(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawmark(2, 86);
  Display.drawstring_center_arial14(5, 0, 191, "Login successful");
}

void display_err_not_login(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawcrossmark(1, 88);
  Display.drawstring_center_arial14(3, 0, 191, "Not logged in");
  Display.drawstring_center_arial14(5, 0, 191, "Login first");
}

void display_wifi_connected_goto_login(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(3, 0, 191, "WiFi already connected");
  Display.drawstring_center_arial14(5, 0, 191, "Next step login");
}

void display_battery_low(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drwa_battery_low(2, 0, 191);
  Display.drawstring(5, 2, "Battery Low connect charger");
}

void display_trans_count(uint16_t count)
{
  Display.clear_single_page_box(0, 156, 192);
  if (count > 0)
  {
    uint16_t temp = count;
    int digit = 0;
    do
    {
      temp /= 10;
      ++digit;
    } while (temp != 0);
    Display.set_page_address(0);
    Display.set_column_address(182 - (7 * digit));
    Display.st7525_data(0x40);
    Display.st7525_data(0x3A);
    Display.st7525_data(0x32);
    Display.st7525_data(0x2A);
    Display.st7525_data(0x26);
    Display.st7525_data(0x2E);
    Display.st7525_data(0x01);
    Display.drawstring(0, 191 - (7 * digit), std::to_string(count).c_str());
  }
}

void remove_battery_charge_symbol(void)
{
  Display.clear_single_page_box(0, 3, 15);
}

void save_current_scrn()
{
  Display.copy_current_screen();
}

void display_error_entry_wifi(uint8_t error_select)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(3, 25, "Wi-Fi not connected");
  Display.drawstring(7, 0, "<Later");
  Display.drawinvertedstring(7, 109, "Connect now>");
}

void display_error_entry_login(uint8_t error_select)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_arial14(3, 50, "Logged out");
  Display.drawstring(7, 0, "<Cancel");
  Display.drawinvertedstring(7, 123, "Login now>");
}

void display_error_wifi(uint8_t error_select)
{
  if (error_select == 0)
  {
    Display.drawstring(7, 0, "<Later");
    Display.drawinvertedstring(7, 109, "Connect now>");
  }
  else if (error_select == 1)
  {
    Display.drawinvertedstring(7, 0, "<Later");
    Display.drawstring(7, 109, "Connect now>");
  }
}

void display_error_login(uint8_t error_select)
{
  if (error_select == 0)
  {
    Display.drawstring(7, 0, "<Cancel");
    Display.drawinvertedstring(7, 123, "Login now>");
  }
  else if (error_select == 1)
  {
    Display.drawinvertedstring(7, 0, "<Cancel");
    Display.drawstring(7, 123, "Login now>");
  }
}

void display_wait_syncing(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(2, 0, 191, "Syncing...");
  Display.drawstring_center_arial14(5, 0, 191, "Please wait...");
}

void dispaly_shutting_down(void)
{
  Display.clear_page(1);
  Display.clear_page(2);
  Display.clear_page(3);
  Display.clear_page(4);
  Display.clear_page(5);
  Display.clear_page(6);
  Display.clear_page(7);
  Display.drawstring_center_arial14(2, 0, 191, "Shutting down...");
  Display.drawstring_center_arial14(5, 0, 191, "Please wait...");
}

void back_light(bool is_pub, bool bg_light, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = bg_light;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void display_message_when_coying_file_data_to_db(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_hello_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_setup(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_letmetry(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void show_update_popup(bool is_pub, const char *s1, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  memcpy(dis_sturct.p7, s1, strlen(s1) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_update(bool is_pub, const char *s1, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  memcpy(dis_sturct.p7, s1, strlen(s1) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_later(bool is_pub, const char *s1, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  memcpy(dis_sturct.p7, s1, strlen(s1) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_ssid(bool is_pub, const char *ssid1, const char *ssid2, int8_t rssi, uint8_t idx, uint8_t ssid_idx, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = idx;
  dis_sturct.p2 = ssid_idx;
  dis_sturct.p3 = rssi;
  memcpy(dis_sturct.p7, ssid1, strlen(ssid1) + 1);
  memcpy(dis_sturct.p8, ssid2, strlen(ssid2) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_in_mode(bool is_pub, uint8_t in_mode, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = in_mode;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_psw(bool is_pub, const char *psw, uint8_t len, uint8_t in_mode,
               const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = len;
  dis_sturct.p2 = in_mode;
  memcpy(dis_sturct.p7, psw, strlen(psw) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_connected_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_psw_in_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_mobile_in_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_otp_in_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_normal_mode_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_normal_mode_ac_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_screen_no(bool is_pub, uint16_t num, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p5 = num;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_operand(bool is_pub, const char *data, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  memcpy(dis_sturct.p7, data, strlen(data) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void blank_calci_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_operator(bool is_pub, char data, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = data;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_mem_operator(bool is_pub, char data, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = data;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_result(bool is_pub, double data, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p6 = data;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_mrc_scrn(bool is_pub, double mem_value, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p6 = mem_value;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_mobile_no(bool is_pub, const char *mo_no, uint8_t len,
                     const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = len;
  memcpy(dis_sturct.p7, mo_no, strlen(mo_no) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_otp_no(bool is_pub, const char *otp_no, uint8_t len,
                  const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = len;
  memcpy(dis_sturct.p7, otp_no, strlen(otp_no) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_main_menu(bool is_pub, uint8_t menu_idx, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = menu_idx;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_gst_rate_set_screen(bool is_pub, uint8_t gst_index, uint8_t selected, const char *menu_gst_1, const char *menu_gst_2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = gst_index;
  dis_sturct.p2 = selected;
  memcpy(dis_sturct.p7, menu_gst_1, strlen(menu_gst_1) + 1);
  memcpy(dis_sturct.p8, menu_gst_2, strlen(menu_gst_2) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_connecting_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_syncing_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_network_error_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_validating_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_sending_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_not_valid_no_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_connection_failed_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_login_again_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_synced_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_scanning(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_found(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void clear_operator(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void clear_mem_operator(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_signal_strength(bool is_pub, int8_t rssi, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  int8_t strength = rssi;
  dis_sturct.p3 = strength;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_try_update_again(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_no_update_available(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_checking_update(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_downloading(bool is_pub, uint16_t perc, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p5 = perc;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_last_calculation(bool is_pub, const char *operand_1, char sign_op, const char *operand_2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = sign_op;
  memcpy(dis_sturct.p7, operand_1, strlen(operand_1) + 1);
  memcpy(dis_sturct.p8, operand_2, strlen(operand_2) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void print_last_calculation(bool is_pub, const char *operand_1, char sign_op, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = sign_op;
  memcpy(dis_sturct.p7, operand_1, strlen(operand_1) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void clear_last_calculation(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_operand(bool is_pub, double data, int8_t precision, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p6 = data;
  dis_sturct.p3 = precision;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_last_calculation(bool is_pub, double operand_1, char sign_op, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = sign_op;
  dis_sturct.p6 = operand_1;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_last_calculation(bool is_pub, double operand_1, char sign_op, double operand_2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = sign_op;
  dis_sturct.p6 = operand_1;
  dis_sturct.p9 = operand_2;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_last_calculation(bool is_pub, double operand_1, char sign_op1, double operand_2, char sign_op2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = sign_op1;
  dis_sturct.p2 = sign_op2;
  dis_sturct.p6 = operand_1;
  dis_sturct.p9 = operand_2;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_equal_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void remove_equal_perc_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_perc_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_gst_sign(bool is_pub, int8_t gst_type, char key, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p3 = gst_type;
  dis_sturct.p4 = key;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void remove_gst_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_mem_sign(bool is_pub, char key, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = key;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void remove_mem_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_cash_sign(bool is_pub, char key, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p4 = key;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void remove_cash_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_start_setup_scrn(bool is_pub, uint8_t opt, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = opt;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_battery_charge(bool is_pub, uint8_t charge, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = charge;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_error(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_main_menu(bool is_pub, uint8_t menu_idx, const char *s1, const char *s2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = menu_idx;
  memcpy(dis_sturct.p7, s1, strlen(s1) + 1);
  memcpy(dis_sturct.p8, s2, strlen(s2) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_in_gst(bool is_pub, uint8_t gst_idx, const char *show_gst, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = gst_idx;
  memcpy(dis_sturct.p7, show_gst, strlen(show_gst) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_in_gst(bool is_pub, uint8_t gst_idx, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = gst_idx;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_system_info(bool is_pub, uint8_t menu_idx, uint8_t selected, const char *s1, const char *s2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = menu_idx;
  dis_sturct.p2 = selected;
  memcpy(dis_sturct.p7, s1, strlen(s1) + 1);
  memcpy(dis_sturct.p8, s2, strlen(s2) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_check_mode(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void clear_check_mode(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void clear_operator_block(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_result_operator(bool is_pub, char data, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = data;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_online_trans_instruct(bool is_pub, uint8_t pay_type, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = pay_type;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void remove_online_trans_instruct(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_gt_sign(bool is_pub, uint8_t gt_op_sign, uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = gt_op_sign;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void remove_gt_sign(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_correct_mode(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void clear_correct_mode(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_onboarding_complete(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_update_done(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_invalid_amount_err(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void clear_invalid_amount_err(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_fct_rst_msg(bool is_pub, uint8_t msg_type, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = msg_type;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_sync(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void clear_sync(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_cust_details(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void select_cust_name(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_cust_num(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_cust_remark(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void select_cust_done(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void select_cust_pay_later(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_cust_details_in_name(bool is_pub, const char *psw, uint8_t len, uint8_t in_mode, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = len;
  dis_sturct.p2 = in_mode;
  memcpy(dis_sturct.p7, psw, strlen(psw) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_cust_details_in_num(bool is_pub, const char *psw, uint8_t len, uint8_t in_mode, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = len;
  dis_sturct.p2 = in_mode;
  memcpy(dis_sturct.p7, psw, strlen(psw) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_cust_details_in_remark(bool is_pub, const char *psw, uint8_t len, uint8_t in_mode, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = len;
  dis_sturct.p2 = in_mode;
  memcpy(dis_sturct.p7, psw, strlen(psw) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_trans_saved(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_instruction_scrn(bool is_pub, uint8_t key, uint8_t select_insruct, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = key;
  dis_sturct.p2 = select_insruct;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_app_qr_code(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_help_qr_code(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_trans_saved_from_main(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void remove_trans_saved_from_main(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_back_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void show_update_ntfy_popup(bool is_pub, const char *s1, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  memcpy(dis_sturct.p7, s1, strlen(s1) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_upd_ntfy__update(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void select_upd_ntfy_later(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void print_wifi_not_found_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void show_initializing(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_show_setting_menu(bool is_pub, int show_menu, uint8_t key, const char *ssid, const char *login, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = key;
  dis_sturct.p3 = show_menu;
  memcpy(dis_sturct.p7, ssid, strlen(ssid) + 1);
  memcpy(dis_sturct.p8, login, strlen(login) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void display_show_setting_menu(bool is_pub, int show_menu, uint8_t key, const char *ssid_login, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = key;
  dis_sturct.p3 = show_menu;
  memcpy(dis_sturct.p7, ssid_login, strlen(ssid_login) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void display_show_setting_menu(bool is_pub, int show_menu, uint8_t key, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = key;
  dis_sturct.p3 = show_menu;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_setting_ssid(bool is_pub, const char *ssid1, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  memcpy(dis_sturct.p7, ssid1, strlen(ssid1) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void display_setting_ssid(bool is_pub, int wifi_idx, uint8_t total_wifi, uint8_t key, const char *ssid1, const char *ssid2, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = total_wifi;
  dis_sturct.p2 = key;
  dis_sturct.p3 = wifi_idx;
  memcpy(dis_sturct.p7, ssid1, strlen(ssid1) + 1);
  memcpy(dis_sturct.p8, ssid2, strlen(ssid2) + 1);
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_setting_wifi_scanning(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_err_wifi_not_connected(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_setting_log_mobile_input(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_setting_log_otp_not_valid(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_setting_log_otp_validation_failed(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_setting_log_otp_login_success(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_err_not_login(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_wifi_connected_goto_login(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_battery_low(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
void remove_battery_charge_symbol(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_trans_count(bool is_pub, uint16_t count, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p5 = count;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void save_current_scrn(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_error_entry_wifi(bool is_pub, uint8_t error_select, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = error_select;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_error_entry_login(bool is_pub, uint8_t error_select, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = error_select;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_error_wifi(bool is_pub, uint8_t error_select, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = error_select;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_error_login(bool is_pub, uint8_t error_select, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  dis_sturct.p1 = error_select;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void display_wait_syncing(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}

void dispaly_shutting_down(bool is_pub, const uint8_t func)
{
  struct display_data dis_sturct;
  dis_sturct.p0 = func;
  xQueueSend(display_queue, (void *)&dis_sturct, (TickType_t)0);
}
