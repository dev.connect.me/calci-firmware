#include "Factory_Reset.h"
#include "Display.h"
#include "nvs.h"
#include "nvs_flash.h"

int64_t button_down;
bool fct_rst_running = false;

void erase_reset_calci(void)
{
    fct_rst_running = true;
    if (Keypad_Task_Handle != NULL)
        vTaskSuspend(Keypad_Task_Handle);
    if (Sync_Task_Handle != NULL)
        vTaskSuspend(Sync_Task_Handle);
    if (WiFi_Task_Handle != NULL)
        vTaskSuspend(WiFi_Task_Handle);
    if (Upd_Ntfy_Task_Handle != NULL)
        vTaskSuspend(Upd_Ntfy_Task_Handle);
    if (Log_Sender_Task_Handle != NULL)
        vTaskDelete(Log_Sender_Task_Handle);
    ESP_LOGI(F_TAG, "Factory reset start\n");
    esp_partition_iterator_t pi = esp_partition_find(
        ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_FACTORY, NULL);

    esp_partition_iterator_t pi2 = esp_partition_find(
        ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_SPIFFS, NULL);

    esp_partition_iterator_t pi3 = esp_partition_find(
        ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_FAT, "storage");

    if (pi != NULL)
    {
        ESP_LOGI(F_TAG, "Got Factory partition");
        print_fct_rst_msg(true, 1);
        esp_err_t err;
        if (nvs_flash_erase() == ESP_OK)
            ESP_LOGI(F_TAG, "NVS partition erased");

        const esp_partition_t *fat_part = esp_partition_get(pi3);
        esp_partition_iterator_release(pi3);
        err = esp_partition_erase_range(fat_part, 0, fat_part->size);
        if (err == ESP_OK)
            ESP_LOGI(F_TAG, "External flash erased");
        else
            ESP_LOGI(F_TAG, "External flash erase error: %s", esp_err_to_name(err));

        const esp_partition_t *spiffs_part = esp_partition_get(pi2);
        esp_partition_iterator_release(pi2);
        if (spiffs_part == NULL)
            ESP_LOGI(F_TAG, "SPIFFS partition not found");

        err = esp_partition_erase_range(spiffs_part, 0, spiffs_part->size);
        if (err == ESP_OK)
            ESP_LOGI(F_TAG, "SPIFFS partition erased");
        else
            ESP_LOGI(F_TAG, "SPIFFS erase error: %s", esp_err_to_name(err));
        print_fct_rst_msg(true, 2);
        const esp_partition_t *factory = esp_partition_get(pi);
        esp_partition_iterator_release(pi);
        err = esp_ota_set_boot_partition(factory);
        ESP_LOGI(F_TAG, "Error: %s\n", esp_err_to_name(err));
        if (err == ESP_OK)
        {
            print_fct_rst_msg(true, 3); // rebooting
            vTaskDelay(500 / portTICK_RATE_MS);
            esp_restart();
        }
        else
        {
            print_fct_rst_msg(true, 4);
            ESP_LOGI(F_TAG, "Boot partition not set successful");
            vTaskDelay(500 / portTICK_RATE_MS);
            esp_restart();
        }
    }
    else
    {
        print_fct_rst_msg(true, 5);
        ESP_LOGI(F_TAG, "Factory partition not found:\n");
        vTaskDelay(500 / portTICK_RATE_MS);
        esp_restart();
    }
}

void factory_reset_task(void *pvParameter)
{
    while (1)
    {
        uint8_t fct_rst_pin = gpio_get_level(FACTORY_RESET);
        if (fct_rst_pin == 0)
        {
            ESP_LOGI(F_TAG, "Factory reset button pressed");
            if (button_down == 0)
                button_down = esp_timer_get_time();
            else
            {
                if ((esp_timer_get_time() - button_down) > FACTORY_RESET_SECONDS * 1000 * 1000)
                {
                    erase_reset_calci();
                    button_down = 0;
                }
            }
        }
        else
        {
            if (button_down > 0)
            {
                if ((esp_timer_get_time() - button_down) > FACTORY_RESET_SECONDS * 1000 * 1000)
                    erase_reset_calci();
                button_down = 0;
            }
        }
        if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
        {
            ESP_LOGI(F_TAG, "Task delete notification received");
            vTaskDelete(NULL);
        }
    }
}
void init_factory_reset(void)
{
    gpio_pad_select_gpio(FACTORY_RESET);
    gpio_set_direction(FACTORY_RESET, GPIO_MODE_INPUT);

    ESP_LOGI(F_TAG, "Factory reset pin: %d\n", gpio_get_level(FACTORY_RESET));

    button_down = 0;

    xTaskCreate(factory_reset_task, "factory_reset_task", 10240, NULL, 10, &Factory_Reset_Task_Handle);
}