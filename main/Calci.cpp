#include "Calci.h"
#include "Calci_Setup.h"
#include "Calci_Error.h"
#include "Multi_Tap_Input.h"

#define KEYPAD_DEBOUNCE_DELAY 100

static const char *TAG = "Calci";

const esp_timer_create_args_t mode_timer_args = {.callback = &mode_callback,
                                                 .name = "mode"};
esp_timer_handle_t mode_timer;

//*************************Calci Mode**************************************

//******************************Data Base************************************
uint8_t change_mode = 0;
int8_t gst = 0;
uint8_t gst_count = 0;
char last_gst_rate = 0;
int8_t last_gst = 0;
uint8_t default_tax_pressed = 0;
uint8_t cust_details_pnt = 0;
std::string cust_name_buff = "";
std::string cust_num_buff = "";
std::string cust_remark_buff = "";

uint8_t err_sel_opt = 1;
//******************************Data Base************************************

Multi_Tap *multi_tap_cust_detail_buff;

void init_calci()
{
    menu_pin.menu = 0;
    menu_pin.sub_menu = 0;
    ESP_ERROR_CHECK(esp_timer_create(&mode_timer_args, &mode_timer));
    ESP_ERROR_CHECK(esp_timer_create(&psw_in_timer_args, &psw_in_timer));
    ESP_ERROR_CHECK(esp_timer_create(&invalid_cash_value_timer_args, &invalid_cash_value_timer));
    FILE *f = fopen(GST_RATE, "r");
    if (f == NULL)
    {
        f = fopen(GST_RATE, "w");
        fwrite(gst_rate, sizeof(gst_rate), 1, f);
        fclose(f);
    }
    else
    {
        fread(gst_rate, sizeof(gst_rate), 1, f);
        fclose(f);
    }
}

static void mode_callback(void *arg)
{
    if (mode_now == NORMAL_MODE)
    {
        mode_now = SETTING_MODE;
        change_mode = 1;
    }
}

void calci_parse(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    switch (mode_now)
    {
    case NORMAL_MODE:
        exec_normal_mode(key, key_type, event_type);
        break;
    case SETUP_MODE:
    {
        if (event_type)
        {
            setup_event_in(key, key_type);
        }
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case SETTING_MODE:
    {
        if (event_type)
        {
            setting_in_event(key, key_type);
        }
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case UPDATE_MODE:
    {
        exec_update_mode(key, key_type, event_type);
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case STEP_CHECK_MODE:
    {
        exec_step_check_mode(key, key_type, event_type);
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case STEP_CORRECT_MODE:
    {
        exec_step_correct_mode(key, key_type, event_type);
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case ADD_CUST_DETAILS_MODE:
    {
        exec_add_cust_details_mode(key, key_type, event_type);
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case UPDATE_NTFY_MODE:
    {
        exec_update_ntfy_mode(key, key_type, event_type);
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    case ERROR_MODE:
    {
        exec_error_mode(key, key_type, event_type);
        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
        clear_keypad();
        break;
    }
    }
}

void exec_normal_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        if (gst == 0)
        {
            if (!err_flag)
            {
                if (invalid_cash_value)
                {
                    last_gst = 0;
                    if (key_type == ERS_KEY)
                    {
                        if (key == 'C')
                        {
                            esp_timer_stop(invalid_cash_value_timer);
                            invalid_cash_value = 0;
                            clear_calculator();
                        }
                        else if (key == 'D')
                        {
                            esp_timer_stop(invalid_cash_value_timer);
                            invalid_cash_value = 0;
                            clear_invalid_amount_err(true);
                            clear_screen();
                        }
                    }
                }
                else
                {
                    switch (key_type)
                    {
                    case NUM_KEY:
                    {
                        last_gst = 0;
                        get_num_input(key);
                        vTaskDelay(KEYPAD_DEBOUNCE_DELAY / portTICK_RATE_MS);
                        clear_keypad();
                        break;
                    }
                    case SIG_KEY:
                    {
                        last_gst = 0;
                        if (key == '=')
                            get_equal_input(key);
                        else if (key == '%')
                            get_perc_input(key);
                        else
                            get_sign_input(key);
                        break;
                    }
                    case GT_KEY:
                    {
                        last_gst = 0;
                        calculate_gt(key);
                        break;
                    }
                    case GST_KEY:
                    {
                        if (gst == 0)
                        {
                            if (key == '+')
                                gst = 1;
                            else
                                gst = -1;
                        }
                        break;
                    }
                    case CHK_CRT_KEY:
                    {
                        last_gst = 0;
                        if (key == 'C')
                        {
                            if (c_flag)
                            {
                                mode_now = ADD_CUST_DETAILS_MODE;
                                select_cust_num(true);
                                cust_name_buff.clear();
                                cust_num_buff.clear();
                                cust_remark_buff.clear();
                                multi_tap_cust_detail_buff = new Multi_Tap(cust_num_buff, 10);
                                multi_tap_cust_detail_buff->set_keypad_mode(2);
                                print_in_mode(true, multi_tap_cust_detail_buff->get_keypad_mode());
                                cust_details_pnt = 1;
                            }
                        }
                        else
                        {
                            correct_operand();
                        }
                        break;
                    }
                    case ARW_KEY:
                    {
                        last_gst = 0;
                        change_mode_to_step_check(key);
                        if (key == 'R')
                            step_it = operation.begin();
                        else if (key == 'L')
                            step_it = --operation.end();
                        print_check_mode(true);
                        clear_last_calculation(true);
                        clear_operator_block(true);
                        remove_cash_sign(true);
                        show_check_step();
                        mode_now = STEP_CHECK_MODE;
                        break;
                    }
                    case MEM_KEY:
                    {
                        last_gst = 0;
                        if (key == '+')
                            add_in_mem();
                        else if (key == '-')
                            sub_in_mem();
                        else if (key == '=')
                            mem_recal();
                        break;
                    }
                    case CASH_KEY:
                    {
                        last_gst = 0;
                        cash_entry(key);
                        break;
                    }
                    case ERS_KEY:
                    {
                        last_gst = 0;
                        if (key == 'C')
                        {
                            clear_calculator();
                        }
                        else
                            clear_screen();
                        break;
                    }
                    case SPC_KEY:
                    {
                        last_gst = 0;
                        if (key == KEY_MENU)
                        {
                            esp_timer_stop(mode_timer);
                            ESP_ERROR_CHECK(esp_timer_start_once(mode_timer, 500000));
                        }
                        else if (key == 'M')
                            mark_up(key);
                        break;
                    }
                    }
                }
            }
            else
            {
                if (key_type == ERS_KEY && key == 'C')
                {
                    err_flag = 0;
                    clear_calculator();
                }
            }
        }
        else
        {
            if (key_type == NUM_KEY)
            {
                if (!gst_flag)
                {
                    gst_count = 0;
                    last_gst_rate = 0;
                }
                if (key == '0' || key == '1' || key == '2' || key == '3' || key == '4')
                {
                    default_tax_pressed = 1;
                    if (last_gst_rate == key)
                    {
                        if (gst_count > 3)
                        {
                            get_input(gst, gst_count, key);
                            if (gst == 1)
                                gst_count = 2;
                            else if (gst == -1)
                                gst_count = 3;
                        }
                        else
                        {
                            get_input(gst, gst_count, key);
                            gst_count++;
                        }
                    }
                    else
                    {
                        gst_count = 1;
                        get_input(gst, gst_count, key);
                        gst_count++;
                    }
                    last_gst_rate = key;
                }
            }
        }
    }
    else
    {
        if (key_type == GST_KEY)
        {
            if (default_tax_pressed)
            {
                default_tax_pressed = 0;
            }
            else
            {
                if (last_gst == gst)
                {
                }
                else
                {
                    gst_count = 1;
                    last_gst_rate = 0;
                }
                if (gst_count == 0)
                {
                    gst_count = 1;
                    last_gst_rate = 0;
                }

                if (gst_count > 3)
                {
                    get_input(gst, gst_count, '1');
                    if (gst == 1)
                        gst_count = 2;
                    else if (gst == -1)
                        gst_count = 3;
                }
                else
                {
                    get_input(gst, gst_count, '1');
                    gst_count++;
                }
                last_gst_rate = '1';
            }
            last_gst = gst;
            gst = 0;
        }
        else if (key_type == SPC_KEY)
        {
            if (key == KEY_MENU)
                esp_err_t err = esp_timer_stop(mode_timer);
        }
    }
}

void exec_update_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        if (menu_pin.menu == 0)
        {
            switch (key_type)
            {
            case ARW_KEY:
            {
                if (key == 'R')
                {
                    menu_pin.sub_menu = 1;
                    select_later(true, fm_ver.c_str());
                }
                else if (key == 'L')
                {
                    menu_pin.sub_menu = 0;
                    select_update(true, fm_ver.c_str());
                }
                break;
            }
            case SIG_KEY:
            {
                if (key == '=')
                {
                    ESP_LOGI(TAG, "menu_pin.sub_menu: %d", menu_pin.sub_menu);
                    if (menu_pin.sub_menu == 1)
                    {
                        ESP_LOGI(TAG, "UPdate Ignored");
                        mode_now = NORMAL_MODE;
                        menu_pin.sub_menu = 0;
                        menu_pin.menu = 0;
                        print_normal_mode_ac_scrn();
                        display_trans_count(true, total_pending_trans);
                    }
                    else
                    {
                        ESP_LOGI(TAG, "OTA task created");
                        xTaskCreate(&ota_task, "ota_task", 8192, NULL, 5, &OTA_Task_Handle);
                    }
                }
                break;
            }
            }
        }
    }
}

void exec_step_check_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        switch (key_type)
        {
        case ARW_KEY:
        {
            if (key == 'R')
            {
                if (step_it == (--operation.end()))
                    step_it = operation.begin();
                else
                    step_it++;
                std::cout << "Operand: " << step_it->val << "\tOperator: " << step_it->arith_op << std::endl;
                show_check_step();
            }
            else if (key == 'L')
            {
                if (step_it == operation.begin())
                    step_it = --operation.end();
                else
                    step_it--;

                show_check_step();
            }
            break;
        }
        case ERS_KEY:
        {
            if (key == 'D')
            {
                clear_check_mode(true);
                mode_now = NORMAL_MODE;
                print_normal_mode_scrn(true);
                print_screen_no(true, screen_no);
                print_operand(true, operation.back().val, -1);
                display_trans_count(true, total_pending_trans);
            }
            else if (key == 'C')
            {
                clear_check_mode(true);
                clear_calculator();
                mode_now = NORMAL_MODE;
                print_normal_mode_scrn(true);
                print_screen_no(true, screen_no);
                print_operand(true, operation.back().val, -1);
                display_trans_count(true, total_pending_trans);
            }
            break;
        }
        case CHK_CRT_KEY:
        {
            if (key == 'D')
            {
                if (once_cash)
                    break;
                print_correct_mode(true);
                clear_correct_var();
                mode_now = STEP_CORRECT_MODE;
            }
            break;
        }
        default:
        {
            clear_check_mode(true);
            mode_now = NORMAL_MODE;
            print_normal_mode_scrn(true);
            display_trans_count(true, total_pending_trans);
            print_screen_no(true, screen_no);
            print_operand(true, operation.back().val, -1);
            break;
        }
        }
    }
}

void exec_step_correct_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        switch (key_type)
        {
        case NUM_KEY:
        {
            parse_and_add_to_corrected_operand(key);
            break;
        }
        case SIG_KEY:
        {
            if (key != '=' && key != '%')
                change_correct_operator_sign(key);
            break;
        }
        case ERS_KEY:
        {
            if (key == 'D')
            {
                clear_correct_var();
                print_operand(true, step_it->val, -1);
            }
            else if (key == 'C')
            {
                clear_check_mode(true);
                mode_now = NORMAL_MODE;
                print_normal_mode_scrn(true);
                display_trans_count(true, total_pending_trans);
                clear_calculator();
            }
            break;
        }
        case CHK_CRT_KEY:
        {
            if (key == 'D')
            {
                simplify_operations_with_correction();
                print_check_mode(true);
                clear_last_calculation(true);
                clear_operator_block(true);
                remove_cash_sign(true);
                show_check_step();
                mode_now = STEP_CHECK_MODE;
            }
            break;
        }
        }
    }
}

void exec_add_cust_details_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        switch (key_type)
        {
        case NUM_KEY:
        {
            multi_tap_cust_detail_buff->add_tap(key, key_type);
            std::string temp_detail = multi_tap_cust_detail_buff->get_buffer();
            if (cust_details_pnt == 1)
            {
                print_cust_details_in_num(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
            }
            else if (cust_details_pnt == 2)
            {
                print_cust_details_in_name(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
            }
            else if (cust_details_pnt == 3)
            {
                print_cust_details_in_remark(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
            }
            break;
        }
        case SIG_KEY:
        {
            if (key == '=')
            {
                mode_now = NORMAL_MODE;
                if (cust_details_pnt == 1)
                {
                    cust_num_buff.clear();
                    cust_num_buff = multi_tap_cust_detail_buff->get_buffer();
                }
                else if (cust_details_pnt == 2)
                {
                    cust_name_buff.clear();
                    cust_name_buff = multi_tap_cust_detail_buff->get_buffer();
                }
                else if (cust_details_pnt == 3)
                {
                    cust_remark_buff.clear();
                    cust_remark_buff = multi_tap_cust_detail_buff->get_buffer();
                }

                int save_sts = save_transaction(accum, (TRANS_TYPE)c_flag, (PAYMENT_MODE)payment_mode_status, cust_name_buff.c_str(), cust_num_buff.c_str(), cust_remark_buff.c_str());
                if (save_sts)
                {
                    print_normal_mode_ac_scrn(true);
                    display_trans_count(true, total_pending_trans);
                    print_operand(true, accum, -1);
                    print_screen_no(true, screen_no);
                }
                cust_name_buff.clear();
                cust_num_buff.clear();
                cust_remark_buff.clear();
                c_flag = 0;
            }
            else
            {
                if (cust_details_pnt == 2)
                {
                    multi_tap_cust_detail_buff->add_tap(key, key_type);
                    std::string temp_detail = multi_tap_cust_detail_buff->get_buffer();
                    print_cust_details_in_name(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
                }
                else if (cust_details_pnt == 3)
                {
                    multi_tap_cust_detail_buff->add_tap(key, key_type);
                    std::string temp_detail = multi_tap_cust_detail_buff->get_buffer();
                    print_cust_details_in_remark(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
                }
            }
            break;
        }
        case GT_KEY:
        {
            break;
        }
        case GST_KEY:
        {
            break;
        }
        case CHK_CRT_KEY:
        {
            if (key == 'D')
            {
                multi_tap_cust_detail_buff->single_back_space();
                std::string temp_detail = multi_tap_cust_detail_buff->get_buffer();
                if (cust_details_pnt == 1)
                {
                    print_cust_details_in_num(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
                }
                else if (cust_details_pnt == 2)
                {
                    print_cust_details_in_name(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
                }
                else if (cust_details_pnt == 3)
                {
                    print_cust_details_in_remark(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
                }
            }
            break;
        }
        case ARW_KEY:
        {
            if (key == 'R')
            {
                cust_details_pnt++;
                if (cust_details_pnt == 4)
                    cust_details_pnt = 1;
            }
            else if (key == 'L')
            {
                cust_details_pnt--;
                if (cust_details_pnt == 0)
                    cust_details_pnt = 3;
            }
            if (cust_details_pnt == 1)
            {
                if (key == 'R')
                {
                    cust_remark_buff.clear();
                    cust_remark_buff = multi_tap_cust_detail_buff->get_buffer();
                }
                else if (key == 'L')
                {
                    cust_name_buff.clear();
                    cust_name_buff = multi_tap_cust_detail_buff->get_buffer();
                }

                select_cust_num(true);
                delete multi_tap_cust_detail_buff;
                multi_tap_cust_detail_buff = new Multi_Tap(cust_num_buff, 10);
                multi_tap_cust_detail_buff->set_keypad_mode(2);
                print_in_mode(true, multi_tap_cust_detail_buff->get_keypad_mode());
                std::string temp_name = multi_tap_cust_detail_buff->get_buffer();
                print_cust_details_in_name(true, temp_name.c_str(), temp_name.size() + 1, 2);
            }
            else if (cust_details_pnt == 2)
            {
                if (key == 'R')
                {
                    cust_num_buff.clear();
                    cust_num_buff = multi_tap_cust_detail_buff->get_buffer();
                }
                else if (key == 'L')
                {
                    cust_remark_buff.clear();
                    cust_remark_buff = multi_tap_cust_detail_buff->get_buffer();
                }

                select_cust_name(true);
                delete multi_tap_cust_detail_buff;
                multi_tap_cust_detail_buff = new Multi_Tap(cust_name_buff, 15);
                multi_tap_cust_detail_buff->set_keypad_mode(0);
                print_in_mode(true, multi_tap_cust_detail_buff->get_keypad_mode());
                std::string temp_name = multi_tap_cust_detail_buff->get_buffer();
                print_cust_details_in_name(true, temp_name.c_str(), temp_name.size() + 1, 0);
            }
            else if (cust_details_pnt == 3)
            {
                if (key == 'R')
                {
                    cust_name_buff.clear();
                    cust_name_buff = multi_tap_cust_detail_buff->get_buffer();
                }
                else if (key == 'L')
                {
                    cust_num_buff.clear();
                    cust_num_buff = multi_tap_cust_detail_buff->get_buffer();
                }

                select_cust_remark(true);
                delete multi_tap_cust_detail_buff;
                multi_tap_cust_detail_buff = new Multi_Tap(cust_remark_buff, 15);
                multi_tap_cust_detail_buff->set_keypad_mode(0);
                print_in_mode(true, multi_tap_cust_detail_buff->get_keypad_mode());
                std::string temp_name = multi_tap_cust_detail_buff->get_buffer();
                print_cust_details_in_name(true, temp_name.c_str(), temp_name.size() + 1, 0);
            }
            break;
        }
        case MEM_KEY:
        {
            if (cust_details_pnt == 2)
            {
                multi_tap_cust_detail_buff->add_tap(key, key_type);
                std::string temp_detail = multi_tap_cust_detail_buff->get_buffer();
                print_cust_details_in_name(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
            }
            else if (cust_details_pnt == 3)
            {
                multi_tap_cust_detail_buff->add_tap(key, key_type);
                std::string temp_detail = multi_tap_cust_detail_buff->get_buffer();
                print_cust_details_in_remark(true, temp_detail.c_str(), temp_detail.size() + 1, multi_tap_cust_detail_buff->get_keypad_mode());
            }
            break;
        }
        case CASH_KEY:
        {
            break;
        }
        case ERS_KEY:
        {
            cust_name_buff.clear();
            cust_num_buff.clear();
            cust_remark_buff.clear();
            c_flag = 0;
            clear_calculator();
            mode_now = NORMAL_MODE;
            break;
        }
        case SPC_KEY:
        {
            if (key == 'S')
            {
                if (cust_details_pnt != 1)
                {
                    multi_tap_cust_detail_buff->shift_keypad_mode(key, key_type);
                    print_in_mode(true, multi_tap_cust_detail_buff->get_keypad_mode());
                }
            }
            break;
        }
        }
    }
}

void exec_update_ntfy_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        switch (key_type)
        {
        case ARW_KEY:
        {
            if (key == 'R')
            {
                menu_pin.sub_menu = 1;
                select_upd_ntfy__update(true);
            }
            else if (key == 'L')
            {
                menu_pin.sub_menu = 0;
                select_upd_ntfy_later(true);
            }
            break;
        }
        case SIG_KEY:
        {
            if (key == '=')
            {
                if (menu_pin.sub_menu == 0)
                {
                    File_Operation temp_file;
                    Json_Operation temp_json;

                    std::string temp = temp_file.m_read(UPDATE_STATUS);
                    int temp_checked = temp_json.get_int(temp.c_str(), "checked");
                    if (temp_checked == 0)
                    {
                        temp = temp_json.add_int(temp.c_str(), "checked", 1);
                        temp_file.m_write(UPDATE_STATUS, temp.c_str());
                    }
                    mode_now = NORMAL_MODE;
                    print_back_scrn(true);
                }
                else
                {
                    xTaskCreate(&ota_task, "ota_task", 8192, NULL, 5, NULL);
                }
            }
            break;
        }
        }
    }
}

void exec_error_mode(uint8_t key, uint8_t key_type, uint8_t event_type)
{
    if (event_type)
    {
        error_mode_keypad_event_in(key, key_type);
    }
}