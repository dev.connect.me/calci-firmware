#include "HTTP_Client.h"
#include "DB_Manager.h"
#include "Log_Handle.h"

#ifdef CONFIG_FIRMWARE_BUILD_DEV
const char *BASE_URL = "http://52.35.64.203:8089/cashbook/baseUrl";
#endif
#ifdef CONFIG_FIRMWARE_BUILD_PROD
const char *BASE_URL = "https://tohands.in/cashbook/baseUrl";
#endif

std::string url;
std::string path;

bool autho_save_check = false;
bool accs_save_check = false;
bool id_save_check = false;
bool username_save_check = false;

esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
  static char otp_verify_rsp[1024]; // Buffer to store response of http request from event handler
  static int output_len;            // Stores number of bytes read
  switch (evt->event_id)
  {
  case HTTP_EVENT_ERROR:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_ERROR");
    break;
  }
  case HTTP_EVENT_ON_CONNECTED:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_ON_CONNECTED");
    break;
  }
  case HTTP_EVENT_HEADER_SENT:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_SENT_HEADER");
    break;
  }
  case HTTP_EVENT_ON_HEADER:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key,
             evt->header_value);
    File_Operation token_file;
    if ("Authorization" == std::string(evt->header_key))
      autho_save_check = token_file.m_write(AUTH_TOKEN, evt->header_value);
    else if ("AccessToken" == std::string(evt->header_key))
      accs_save_check = token_file.m_write(ACCS_TOKEN, evt->header_value);
    break;
  }
  case HTTP_EVENT_ON_DATA:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
    if (esp_http_client_is_chunked_response(evt->client))
    {
      // If user_data buffer is configured, copy the response into the buffer
      memcpy(&otp_verify_rsp[output_len], evt->data, evt->data_len);
      output_len += evt->data_len;
    }
    else
    {
      cJSON *doc = cJSON_Parse((char *)evt->data);
      cJSON *data = cJSON_GetObjectItemCaseSensitive(doc, "data");
      cJSON *val = cJSON_GetObjectItemCaseSensitive(data, "id");
      if (cJSON_IsNumber(val) && (val->valueint != NULL))
      {
        FILE *f = fopen(CASHBOOK_ID, "w+");
        if (f != NULL)
        {
          id_save_check = true;
          fwrite(&val->valueint, sizeof(int), 1, f);
          fclose(f);
        }
        else
        {
          ESP_LOGE(CLIENT, "Failed to open Cashbook ID for writing");
          char temp_msg[255];
          sprintf(temp_msg, "%s: Failed to open Cashbook ID for writing", CLIENT);
          send_log(temp_msg, "FIRMWARE");
        }
      }
      cJSON_Delete(doc);
    }
    break;
  }
  case HTTP_EVENT_ON_FINISH:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_ON_FINISH");
    otp_verify_rsp[output_len] = '\0';
    ESP_LOGI(CLIENT, "DATA: %s", otp_verify_rsp);
    cJSON *doc = cJSON_Parse(otp_verify_rsp);
    cJSON *data = cJSON_GetObjectItemCaseSensitive(doc, "data");
    cJSON *val = cJSON_GetObjectItemCaseSensitive(data, "id");
    if (cJSON_IsNumber(val) && (val->valueint != NULL))
    {
      FILE *f = fopen(CASHBOOK_ID, "w+");
      if (f != NULL)
      {
        id_save_check = true;
        fwrite(&val->valueint, sizeof(int), 1, f);
        fclose(f);
      }
      else
      {
        ESP_LOGE(CLIENT, "Failed to open Cashbook ID for writing");
        char temp_msg[255];
        sprintf(temp_msg, "%s: Failed to open Cashbook ID for writing", CLIENT);
        send_log(temp_msg, "FIRMWARE");
      }
    }
    cJSON_Delete(doc);
    output_len = 0;
    break;
  }
  case HTTP_EVENT_DISCONNECTED:
  {
    ESP_LOGI(CLIENT, "HTTP_EVENT_DISCONNECTED");
    break;
  }
  }
  return ESP_OK;
}

void get_base_url()
{
  char output_buffer[200] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = BASE_URL,
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .user_data =
          output_buffer, // Pass address of local buffer to get response

  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(CLIENT, "Failed to initialise HTTP connection");
    char temp_msg[255];
    sprintf(temp_msg, "%s: get_base_url Failed to initialise HTTP connection", CLIENT);
    send_log(temp_msg, "FIRMWARE");
  }
  esp_http_client_set_method(client, HTTP_METHOD_GET);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  esp_err_t err = esp_http_client_open(client, 0);
  if (err != ESP_OK)
  {
    ESP_LOGE(CLIENT, "Failed to open HTTP connection: %s", esp_err_to_name(err));
    char temp_msg[255];
    sprintf(temp_msg, "%s: get_base_url Failed to open HTTP connection: %s", CLIENT, esp_err_to_name(err));
    send_log(temp_msg, "FIRMWARE");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return;
  }

  esp_http_client_fetch_headers(client);

  int data_read = esp_http_client_read(client, output_buffer, 200);
  if (data_read < 0)
  {
  }
  else if (data_read > 0)
  {
    ESP_LOGI(CLIENT, "Data: %s", output_buffer);
    const cJSON *nesValue = NULL;
    cJSON *doc = cJSON_Parse(output_buffer);
    if (doc == NULL)
    {
      return;
    }
    cJSON *nesDoc = cJSON_GetObjectItemCaseSensitive(doc, "data");
    if (nesDoc)
    {
      nesValue = cJSON_GetObjectItemCaseSensitive(nesDoc, "url");
      if (cJSON_IsString(nesValue) && (nesValue->valuestring != NULL))
      {
#ifdef CONFIG_FIRMWARE_BUILD_DEV
        url = "http://" + std::string(nesValue->valuestring);
#endif
#ifdef CONFIG_FIRMWARE_BUILD_PROD
        url = "https://" + std::string(nesValue->valuestring);
#endif
      }
      nesValue = cJSON_GetObjectItemCaseSensitive(nesDoc, "path");
      if (cJSON_IsString(nesValue) && (nesValue->valuestring != NULL))
      {
        path = std::string(nesValue->valuestring);
      }
      cJSON_Delete(doc);
    }
  }
  else if (data_read == 0)
  {
    // esp_restart();
  }
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
}

bool send_otp(const char *num)
{
  if (url.length() == 0)
    return false;
  std::string send_otp_url = url + path + "register/login" + "?username=" + std::string(num);
  char output_buffer[200] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = send_otp_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .user_data =
          output_buffer, // Pass address of local buffer to get response
  };

  esp_http_client_handle_t client = esp_http_client_init(&config);
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);

  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK)
  {
    ESP_LOGI(CLIENT, "Perform success");
    ESP_LOGI(CLIENT, "Status code: %d", esp_http_client_get_status_code(client));
    if (esp_http_client_get_status_code(client) == 200)
    {
      esp_http_client_cleanup(client);
      return true;
    }
    else
    {
      esp_http_client_cleanup(client);
      return true;
    }
  }
  else
  {
    ESP_LOGE(CLIENT, "Perform failed");
    char temp_msg[255];
    sprintf(temp_msg, "%s: send_otp Perform failed", CLIENT);
    send_log(temp_msg, "API");
    esp_http_client_cleanup(client);
    return false;
  }
  return false;
}

bool veryfy_otp(const char *num1, const char *num2)
{
  if (url.length() == 0)
    return false;
  std::string verify_otp_url = url + path + "register/login" +
                               "?username=" + std::string(num1) +
                               "&otp=" + std::string(num2);
  // char output_buffer[1024] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = verify_otp_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .event_handler = _http_event_handle,
      // .user_data =
      // output_buffer, // Pass address of local buffer to get response
  };

  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(CLIENT, "Failed to initialise HTTP connection");
    char temp_msg[255];
    sprintf(temp_msg, "%s: verify_otp Failed to initialise HTTP connection", CLIENT);
    send_log(temp_msg, "FIRMWARE");
  }
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK)
  {
    ESP_LOGI(CLIENT, "Verify OTP Status = %d, content_length = %d",
             esp_http_client_get_status_code(client),
             esp_http_client_get_content_length(client));
    if (autho_save_check && accs_save_check && id_save_check)
    {
      ESP_LOGI(CLIENT, "Both token and ID saved successfuly");

      char temp_query[100];
      int64_t cashbook_id = 0;
      FILE *fid = fopen(CASHBOOK_ID, "r");
      if (fid == NULL)
      {
        ESP_LOGE(CLIENT, "Failed to open Cashbook ID file");
        char temp_msg[255];
        sprintf(temp_msg, "%s: verify otp Failed to open Cashbook ID for writing", CLIENT);
        send_log(temp_msg, "FIRMWARE");
      }
      else
      {
        fread(&cashbook_id, sizeof(int64_t), 1, fid);
        fclose(fid);
      }
      sprintf(temp_query, "UPDATE transactions SET cashbookUserId=%lld WHERE cashbookUserId=0", cashbook_id);
      int rc = 0;

      xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
      rc = db_exec(db, OTHER, temp_query);
      xSemaphoreGive(sqlite_mutex);
      if (rc != SQLITE_OK)
      {
        ESP_LOGE(CLIENT, "RC: %d\n", rc);
        char temp_msg[255];
        sprintf(temp_msg, "%s: DB exe failed : %d", CLIENT, rc);
        send_log(temp_msg, "FIRMWARE");
      }
      

      autho_save_check = false;
      accs_save_check = false;
      id_save_check = false;
      esp_http_client_cleanup(client);
      set_is_relogging_happen(0);
      return true;
    }
  }
  else
  {
    ESP_LOGE(CLIENT, "Verify OTP request failed: %s", esp_err_to_name(err));
    char temp_msg[255];
    sprintf(temp_msg, "%s: Verify OTP request failed: %s", CLIENT, esp_err_to_name(err));
    send_log(temp_msg, "API");
    // esp_http_client_cleanup(client);
  }
  esp_http_client_cleanup(client);
  autho_save_check = false;
  accs_save_check = false;
  id_save_check = false;
  return false;
}

int send_transaction(struct Trans_Data_Sqlite_Struc &trans_data, std::string &req_body)
{
  ESP_LOGI(CLIENT, "Transaction Amount: %f Type: %c Name: %s Number: %s", trans_data.trans_amount, trans_data.trans_type, trans_data.trans_cust_name, trans_data.trans_cust_num);
  if (url.length() == 0)
    return 0;
  std::string send_transaction_url =
      url + path + "saveTransaction?deviceId=" + get_device_id();
  ESP_LOGI(CLIENT, "URL: %s", send_transaction_url.c_str());
  char output_buffer[1024] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = send_transaction_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .user_data =
          output_buffer, // Pass address of local buffer to get response
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(CLIENT, "Failed to initialise HTTP connection");
    return 0;
  }
  cJSON *parent = cJSON_CreateObject();
  cJSON *doc = cJSON_CreateObject();
  cJSON *cashbookUserTransactions = NULL;
  cJSON *transactionType = NULL;
  cJSON *amount_send = NULL;
  cJSON *cashbookUserId = NULL;
  cJSON *deviceType = NULL;
  cJSON *transactionTime = NULL;
  cJSON *transactionMode = NULL;
  cJSON *customerName = NULL;
  cJSON *customerMobile = NULL;
  cJSON *remarks = NULL;
  if (trans_data.trans_type == '+')
    transactionType = cJSON_CreateString("Credit");
  if (trans_data.trans_type == '-')
    transactionType = cJSON_CreateString("Debit");
  cJSON_AddItemToObject(doc, "transactionType", transactionType);
  amount_send = cJSON_CreateNumber(trans_data.trans_amount);
  cJSON_AddItemToObject(doc, "amount", amount_send);
  deviceType = cJSON_CreateString("CALCULATOR");
  cJSON_AddItemToObject(doc, "deviceType", deviceType);
  std::string mili_tras_time = std::to_string(trans_data.trans_time) + "000";
  transactionTime = cJSON_CreateString(mili_tras_time.c_str());
  cJSON_AddItemToObject(doc, "transactionTime", transactionTime);
  if (trans_data.payment_mode == char(1))
    transactionMode = cJSON_CreateString("CASH");
  else if (trans_data.payment_mode == char(2))
    transactionMode = cJSON_CreateString("ONLINE");
  else if ((char)trans_data.payment_mode == char(3))
    transactionMode = cJSON_CreateString("PAY_LATER");
  cJSON_AddItemToObject(doc, "paymentMode", transactionMode);

  if (strlen(trans_data.trans_cust_name) > 0)
  {
    customerName = cJSON_CreateString(trans_data.trans_cust_name);
    cJSON_AddItemToObject(doc, "customerName", customerName);
  }
  if (strlen(trans_data.trans_cust_num) > 0)
  {
    customerMobile = cJSON_CreateString(trans_data.trans_cust_num);
    cJSON_AddItemToObject(doc, "customerMobile", customerMobile);
  }
  if (strlen(trans_data.trans_cust_remark) > 0)
  {
    remarks = cJSON_CreateString(trans_data.trans_cust_remark);
    cJSON_AddItemToObject(doc, "remarks", remarks);
  }

  cashbookUserId = cJSON_CreateNumber(trans_data.cashbook_user_id);
  cJSON_AddItemToObject(doc, "cashbookUserId", cashbookUserId);
  cashbookUserTransactions = cJSON_CreateArray();
  cJSON_AddItemToArray(cashbookUserTransactions, doc);
  cJSON_AddItemToObject(parent, "cashbookUserTransactions",
                        cashbookUserTransactions);
  char *body_data = cJSON_PrintUnformatted(parent);
  // cJSON_Delete(parent);
  File_Operation auth_file;
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "Content-Type", "application/json");
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  std::string temp_auth = auth_file.m_read(AUTH_TOKEN);
  esp_http_client_set_header(client, "Authorization", temp_auth.c_str());
  esp_http_client_set_post_field(client, body_data, strlen(body_data));
  ESP_LOGI(CLIENT, "BODY: %s", body_data);
  esp_err_t err = esp_http_client_open(client, strlen(body_data));
  if (err != ESP_OK)
  {
    ESP_LOGE(CLIENT, "Failed to open HTTP connection: %s",
             esp_err_to_name(err));
    char temp_msg[255];
    sprintf(temp_msg, "%s: send_transaction Failed to open HTTP connection: %s", CLIENT, esp_err_to_name(err));
    send_log(temp_msg, "API");
    req_body = std::string(body_data);
    cJSON_free(body_data);
    cJSON_Delete(cashbookUserTransactions);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
  }
  int wlen = esp_http_client_write(client, body_data, strlen(body_data));
  if (wlen < 0)
  {
    ESP_LOGE(CLIENT, "Write failed");
    char temp_msg[255];
    sprintf(temp_msg, "%s: send_transaction Write failed", CLIENT);
    send_log(temp_msg, "API");
    req_body = std::string(body_data);
    cJSON_free(body_data);
    cJSON_Delete(cashbookUserTransactions);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
  }
  esp_http_client_fetch_headers(client);

  int data_read = esp_http_client_read(client, output_buffer, 200);
  if (data_read < 0)
  {
    req_body = std::string(body_data);
    cJSON_free(body_data);
    cJSON_Delete(cashbookUserTransactions);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
  }
  else if (data_read > 0)
  {
    ESP_LOGI(CLIENT, "CREDIT Status = %d, content_length = %d",
             esp_http_client_get_status_code(client),
             esp_http_client_get_content_length(client));
    ESP_LOGI(CLIENT, "Response: %s", output_buffer);
    req_body = std::string(body_data);
    cJSON_free(body_data);
    cJSON_Delete(cashbookUserTransactions);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return esp_http_client_get_status_code(client);
  }
  ESP_LOGI(CLIENT, "CREDIT Status = %d, content_length = %d",
           esp_http_client_get_status_code(client),
           esp_http_client_get_content_length(client));
  req_body = std::string(body_data);
  cJSON_free(body_data);
  cJSON_Delete(cashbookUserTransactions);
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
  return 0;
}

int send_transactions(const char *trans_list)
{
  if (url.length() == 0)
    return 0;
  std::string send_transaction_url = url + path + "saveTransaction?deviceId=" + get_device_id();
  ESP_LOGI(CLIENT, "URL: %s", send_transaction_url.c_str());
  char output_buffer[1024] = {0}; // Buffer to store response of http request
  esp_http_client_config_t config = {
      .url = send_transaction_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .user_data =
          output_buffer, // Pass address of local buffer to get response
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(CLIENT, "Failed to initialise HTTP connection");
    return 0;
  }
  File_Operation auth_file;
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "Content-Type", "application/json");
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  std::string temp_auth = auth_file.m_read(AUTH_TOKEN);
  esp_http_client_set_header(client, "Authorization", temp_auth.c_str());
  ESP_LOGI(CLIENT, "BODY len: %d", strlen(trans_list));
  esp_http_client_set_post_field(client, trans_list, strlen(trans_list));
  esp_err_t err = esp_http_client_open(client, strlen(trans_list));
  if (err != ESP_OK)
  {
    ESP_LOGE(CLIENT, "Failed to open HTTP connection: %s",
             esp_err_to_name(err));
    char temp_msg[255];
    sprintf(temp_msg, "%s: Failed to open HTTP connection: %s", CLIENT, esp_err_to_name(err));
    send_log(temp_msg, "API");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
  }
  int wlen = esp_http_client_write(client, trans_list, strlen(trans_list));
  if (wlen < 0)
  {
    ESP_LOGE(CLIENT, "Write failed");
    char temp_msg[255];
    sprintf(temp_msg, "%s: send_transactions Write failed", CLIENT);
    send_log(temp_msg, "API");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
  }
  esp_http_client_fetch_headers(client);

  int data_read = esp_http_client_read(client, output_buffer, 200);
  if (data_read < 0)
  {
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
  }
  else if (data_read > 0)
  {
    ESP_LOGI(CLIENT, "CREDIT Status = %d, content_length = %d",
             esp_http_client_get_status_code(client),
             esp_http_client_get_content_length(client));
    ESP_LOGI(CLIENT, "Response: %s", output_buffer);
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return esp_http_client_get_status_code(client);
  }
  ESP_LOGI(CLIENT, "CREDIT Status = %d, content_length = %d",
           esp_http_client_get_status_code(client),
           esp_http_client_get_content_length(client));
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
  return 0;
}

int refresh_token()
{
  if (url.length() == 0)
    return 0;
  std::string send_transaction_url = url + path + "refreshToken";

  char output_buffer[1024] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = send_transaction_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .event_handler = _http_event_handle,
      .user_data =
          output_buffer, // Pass address of local buffer to get response
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(CLIENT, "Failed to initialise HTTP connection");
  }

  File_Operation auth_file;
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  esp_http_client_set_header(client, "Authorization",
                             auth_file.m_read(AUTH_TOKEN).c_str());
  esp_http_client_set_header(client, "AccessToken",
                             auth_file.m_read(ACCS_TOKEN).c_str());

  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK)
  {
    ESP_LOGI(CLIENT, "HTTP POST Status = %d, content_length = %d",
             esp_http_client_get_status_code(client),
             esp_http_client_get_content_length(client));
    int res_status = esp_http_client_get_status_code(client);
    esp_http_client_cleanup(client);
    return res_status;
  }
  else
  {
    ESP_LOGE(CLIENT, "HTTP POST request failed: %s", esp_err_to_name(err));
    char temp_msg[255];
    sprintf(temp_msg, "%s: refresh token HTTP POST request failed: %s", CLIENT, esp_err_to_name(err));
    send_log(temp_msg, "API");
    esp_http_client_cleanup(client);
    return 0;
  }
}

int check_for_update(std::string &avail, std::string &rsp_ver)
{
  if (url.length() == 0)
    return 0;
  const esp_partition_t *running = esp_ota_get_running_partition();
  std::string send_transaction_url = url + path + "v2/checkFirmwareUpdate?hardwareVersion=" + get_hardware_version() + "&serialNo=" + get_serial_no();
  ESP_LOGI(CLIENT, "URL: %s", send_transaction_url.c_str());
  char output_buffer[1024] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = send_transaction_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .user_data =
          output_buffer, // Pass address of local buffer to get response
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(CLIENT, "Failed to initialise HTTP connection");
    return 0;
  }
  File_Operation auth_file;
  esp_http_client_set_method(client, HTTP_METHOD_GET);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  esp_http_client_set_header(client, "Authorization",
                             auth_file.m_read(AUTH_TOKEN).c_str());

  esp_err_t err = esp_http_client_open(client, 0);
  if (err != ESP_OK)
  {
    ESP_LOGE(CLIENT, "Failed to open HTTP connection: %s", esp_err_to_name(err));
    esp_http_client_cleanup(client);
    return 0;
  }

  esp_http_client_fetch_headers(client);
  int data_read = esp_http_client_read(client, output_buffer, 1024);

  int temp_rsp = 0;
  if (data_read < 0)
  {
    temp_rsp = 0;
    avail = "0";
  }
  else if (data_read > 0)
  {
    ESP_LOGI(CLIENT, "Response: %s", output_buffer);
    const cJSON *nesValue = NULL;
    cJSON *doc = cJSON_Parse(output_buffer);
    if (doc != NULL)
    {
      cJSON *nesDoc = cJSON_GetObjectItemCaseSensitive(doc, "data");
      if (nesDoc)
      {
        nesValue = cJSON_GetObjectItemCaseSensitive(nesDoc, "versionCode");
        if (cJSON_IsString(nesValue) && (nesValue->valuestring != NULL))
        {
          int letest_ver = std::stoi(nesValue->valuestring);
          if (letest_ver > std::stoi(CONFIG_FIRMWARE_VERSION_CODE))
          {
            avail = std::string(nesValue->valuestring);
            cJSON *get_ver = cJSON_GetObjectItemCaseSensitive(nesDoc, "versionName");
            if (cJSON_IsString(get_ver) && (get_ver->valuestring != NULL))
              rsp_ver = std::string(get_ver->valuestring);
            cJSON_Delete(doc);
          }
          else
          {
            avail = "0";
            cJSON_Delete(doc);
          }
        }
        else
        {
          avail = "0";
          cJSON_Delete(doc);
        }
      }
    }
    else
    {
      avail = "0";
      cJSON_Delete(doc);
    }
    temp_rsp = esp_http_client_get_status_code(client);
  }
  else if (data_read == 0)
  {
    temp_rsp = 0;
    avail = "0";
  }
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
  return temp_rsp;
}

void send_firmware_version(void)
{
  if (url.length() == 0)
    return;
  std::string send_fw_ver_url = url + path + "saveFirmwareVersion";
  char output_buffer[200] = {0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      .url = send_fw_ver_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .user_data =
          output_buffer, // Pass address of local buffer to get response
  };

  File_Operation auth_file;
  esp_http_client_handle_t client = esp_http_client_init(&config);
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  esp_http_client_set_header(client, "Content-Type", "application/json");
  esp_http_client_set_header(client, "Authorization",
                             auth_file.m_read(AUTH_TOKEN).c_str());

  cJSON *doc = cJSON_CreateObject();
  cJSON *ser_doc = cJSON_CreateString(get_serial_no().c_str());
  cJSON *fw_code_doc = cJSON_CreateString(CONFIG_FIRMWARE_VERSION_CODE);
  cJSON *fw_name_doc = cJSON_CreateString(CONFIG_FIRMWARE_VERSION_NAME);
  cJSON_AddItemToObject(doc, "serialNo", ser_doc);
  cJSON_AddItemToObject(doc, "versionCode", fw_code_doc);
  cJSON_AddItemToObject(doc, "versionName", fw_name_doc);
  char *post_data = cJSON_PrintUnformatted(doc);
  esp_http_client_set_post_field(client, post_data, strlen(post_data));
  ESP_LOGI(CLIENT, "BODY: %s", post_data);
  esp_err_t err = esp_http_client_open(client, strlen(post_data));
  if (err != ESP_OK)
  {
    ESP_LOGE(CLIENT, "Failed to open HTTP connection: %s",
             esp_err_to_name(err));
  }
  else
  {
    int wlen = esp_http_client_write(client, post_data, strlen(post_data));
    esp_http_client_fetch_headers(client);
    int data_read = esp_http_client_read(client, output_buffer, 200);
    if (data_read > 0)
    {
      ESP_LOGI(CLIENT, "send_firmware_version Status = %d, content_length = %d",
               esp_http_client_get_status_code(client),
               esp_http_client_get_content_length(client));
    }
  }
  cJSON_free(post_data);
  cJSON_Delete(doc);
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
}