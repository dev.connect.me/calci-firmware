#include "Json_Operation.h"

Json_Operation::Json_Operation() {}

Json_Operation::~Json_Operation() {}

bool Json_Operation::validate_json(const char *json) {
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    return false;
  }
  return true;
}

bool Json_Operation::get_bool(const char *json, const char *key) {
  const cJSON *value = NULL;
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    return -1;
  }
  value = cJSON_GetObjectItemCaseSensitive(doc, key);
  if (cJSON_IsTrue(value)) {
    cJSON_Delete(doc);
    return true;
  } else {
    cJSON_Delete(doc);
    return false;
  }
}

int Json_Operation::get_int(const char *json, const char *key) {
  const cJSON *value = NULL;
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    return -1;
  }
  value = cJSON_GetObjectItemCaseSensitive(doc, key);
  if (cJSON_IsNumber(value) && (value->valueint != NULL)) {
    int temp = value->valueint;
    cJSON_Delete(doc);
    return temp;
  }
  // else if(cJSON_IsTrue(value) == 0) {
  // 	printf("Checking monitor \"%d\"\n", value->valueint);
  // 	cJSON_Delete(doc);
  // 	return 0;
  // }
  cJSON_Delete(doc);
  return 0;
}

float Json_Operation::get_float(const char *json, const char *key) {
  const cJSON *value = NULL;
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    return -1;
  }
  value = cJSON_GetObjectItemCaseSensitive(doc, key);
  if (cJSON_IsNumber(value) && (value->valuedouble != NULL)) {
    float temp = value->valuedouble;
    cJSON_Delete(doc);
    return temp;
  } else {
    return -2;
  }
}
std::string Json_Operation::get_string(const char *json, const char *key) {
  cJSON *value = NULL;
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    return "-1";
  }
  value = cJSON_GetObjectItemCaseSensitive(doc, key);
  if (cJSON_IsString(value) && (value->valuestring != NULL)) {
    char *copyData = value->valuestring;
    std::string data = std::string(copyData);
    // cJSON_Delete(value);
    // char *copyData = cJSON_PrintUnformatted(doc);
    // std::string data = std::string(copyData);
    // cJSON_free(copyData);
    // std::string data = value->valuestring;
    cJSON_Delete(doc);
    return data;
  } else {
    return "-2";
  }
}

// Get count of total element in array
int Json_Operation::get_ary_size(const char *json, const char *ary) {
  const cJSON *value = NULL;
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    const char *error_ptr = cJSON_GetErrorPtr();
    if (error_ptr != NULL) {
      fprintf(stderr, "Error before: %s\n", error_ptr);
    }
    delete error_ptr;
    return -1;
  }
  value = cJSON_GetObjectItemCaseSensitive(doc, ary);
  if (value) {
    int temp = cJSON_GetArraySize(value);
    cJSON_Delete(doc);
    return temp;
  } else {
    return -2;
  }
}

// Add key and value to json if value is integer
std::string Json_Operation::add_int(const char *json, const char *key,
                                   int value) {
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    doc = cJSON_CreateObject();
    cJSON *item = NULL;
    item = cJSON_CreateNumber(value);
    cJSON_AddItemToObject(doc, key, item);
  } else {
    cJSON *item = NULL;
    item = cJSON_GetObjectItemCaseSensitive(doc, key);
    if (cJSON_IsNumber(item) && (item->valueint != NULL)) {
      cJSON_SetNumberValue(item, value);
    } else {
      item = cJSON_CreateNumber(value);
      cJSON_AddItemToObject(doc, key, item);
    }
  }

  char *copyData = cJSON_PrintUnformatted(doc);
  std::string data = std::string(copyData);
  cJSON_free(copyData);
  cJSON_Delete(doc);
  return data;
}

// Add key and value to json if value is float
std::string Json_Operation::add_float(const char *json, const char *key,
                                     float value) {
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    doc = cJSON_CreateObject();
    cJSON *item = NULL;
    item = cJSON_CreateNumber(value);
    cJSON_AddItemToObject(doc, key, item);
  } else {
    cJSON *item = NULL;
    item = cJSON_CreateNumber(value);
    cJSON_AddItemToObject(doc, key, item);
  }
  char *copyData = cJSON_PrintUnformatted(doc);
  std::string data = std::string(copyData);
  cJSON_free(copyData);
  cJSON_Delete(doc);
  return data;
}

// Add key and value to json if value is string
std::string Json_Operation::add_string(const char *json, const char *key,
                                      const char *value) {
  cJSON *doc = cJSON_Parse(json);
  if (doc == NULL) {
    doc = cJSON_CreateObject();
    cJSON *item = NULL;
    item = cJSON_CreateString(value);
    cJSON_AddItemToObject(doc, key, item);
  } else {
    cJSON *item = NULL;
    item = cJSON_GetObjectItemCaseSensitive(doc, key);
    if (cJSON_IsString(item) && (item->valuestring != NULL)) {
      cJSON *item2 = NULL;
      item2 = cJSON_CreateString(value);
      cJSON_ReplaceItemViaPointer(doc, item, item2);
    } else {
      item = cJSON_CreateString(value);
      cJSON_AddItemToObject(doc, key, item);
    }
  }

  char *copyData = cJSON_PrintUnformatted(doc);
  std::string data = std::string(copyData);
  cJSON_free(copyData);
  cJSON_Delete(doc);
  return data;
}

std::string Json_Operation::add_nes_json(const char *json, const char *nesJson,
                                        const char *data) {
  cJSON *doc = cJSON_Parse(json);
  cJSON *nesDoc = cJSON_Parse(nesJson);
  if (doc == NULL) {
    doc = cJSON_CreateObject();
    if (nesDoc == NULL) {
      nesDoc = cJSON_CreateObject();
      cJSON_AddItemToObject(doc, data, nesDoc);
    } else {
      cJSON_AddItemToObject(doc, data, nesDoc);
    }
  } else {
    if (nesDoc == NULL) {
      nesDoc = cJSON_CreateObject();
      cJSON_AddItemToObject(doc, data, nesDoc);
    } else {
      cJSON_AddItemToObject(doc, data, nesDoc);
    }
  }
  char *copyData = cJSON_PrintUnformatted(doc);
  std::string retData = std::string(copyData);
  cJSON_free(copyData);
  cJSON_Delete(doc);
  return retData;
}
