#include "Multi_Tap_Input.h"

static const char *TAG = "Multi Tap";

std::map<uint8_t, std::vector<uint8_t>> multi_tap_mapping[3]{
    {{'1', {'a', 'b', 'c'}},
     {'2', {'d', 'e', 'f'}},
     {'3', {'g', 'h', 'i'}},
     {'4', {'j', 'k', 'l'}},
     {'5', {'m', 'n', 'o'}},
     {'6', {'p', 'q', 'r'}},
     {'7', {'s', 't', 'u'}},
     {'8', {'v', 'w', 'x'}},
     {'9', {'y', 'z'}},
     {'0', {'!', '@', '#'}},
     {'D', {'$', '^', '&'}},
     {'.', {'.', ' ', '?'}}},
    {{'+', {'+', '{', '}'}},
     {'-', {'-', '[', ']'}},
     {'*', {'*', '(', ')'}},
     {'/', {'/', '"', '\\'}},
     {'%', {'%', ':', '|'}}},
    {{'+', {'<', ';', '>'}},
     {'-', {'\'', '_', ','}},
     {'=', {'=', '`', '~'}}}};

Multi_Tap::Multi_Tap(std::string str, uint8_t str_limit)
{
    ESP_LOGI(TAG, "str: %s :limit: %d", str.c_str(), str_limit);
    limit = str_limit;
    if (str.size() > limit)
    {
        printf("\nString length is bigger not supportted\n");
        state = MULTI_TAP_ERROR;
        return;
    }
    len = str.size();
    memset(buffer, 0, 20);
    memcpy(buffer, str.c_str(), len);
    last_key = 0;
    last_key_type = 0;
    prev_press_time = 0;
    keypad_mode = 0;
    ESP_LOGW(TAG, "str: %s len: %d", str.c_str(), len);
}

void Multi_Tap::add_tap(uint8_t key, uint8_t key_type)
{
    if (len >= (limit - 1))
    {
        return;
    }
    uint8_t key_val = 0;
    // if (last_key == 0 && last_key_type == 0)
    if (buffer[len] == 0)
    {
        if (keypad_mode == 2)
        {
            ESP_LOGI(TAG, "Keypad type: %d \t Key: %d", key_type, key);
            if (key_type == NUM_KEY && key != '.' && key != 'D')
            {
                buffer[len] = key;
                last_key = key;
                last_key_type = key_type;
                prev_press_time = esp_timer_get_time();
            }
        }
        else
        {
            key_val = get_key_first_tap(key, key_type);
            buffer[len] = key_val;
            prev_press_time = esp_timer_get_time();
        }
    }
    else
    {
        if (keypad_mode == 2)
        {
            ESP_LOGI(TAG, "Keypad type: %d \t Key: %d", key_type, key);
            if (key_type == NUM_KEY && key != '.' && key != 'D')
            {
                len++;
                buffer[len] = key;
                last_key = key;
                last_key_type = key_type;
                prev_press_time = esp_timer_get_time();
            }
        }
        else
        {
            // ESP_LOGW(TAG, "last_key_type: %d \t last_key: %d", last_key_type, last_key);
            // ESP_LOGW(TAG, "key_type: %d \t key: %d", key_type, key);
            if (last_key == key && last_key_type == key_type)
            {
                // ESP_LOGW(TAG, "previous time: %lld \t current time: %lld", prev_press_time, esp_timer_get_time());
                if ((esp_timer_get_time() - prev_press_time) <= MULTI_TAP_TIMEOUT * 1000)
                {
                    ESP_LOGW(TAG, "Same key press");
                    key_val = get_key_same_tap(key, key_type);
                    // ESP_LOGW(TAG, "key value: %c, len: %d", key_val, len);
                    buffer[len] = key_val;
                }
                else
                {
                    key_val = get_key(key, key_type);
                    len++;
                    buffer[len] = key_val;
                }
            }
            else
            {
                key_val = get_key(key, key_type);
                len++;
                buffer[len] = key_val;
            }
            prev_press_time = esp_timer_get_time();
        }
    }
}

uint8_t Multi_Tap::get_key(uint8_t key, uint8_t key_type)
{
    last_key = key;
    last_key_type = key_type;
    if (key_type == NUM_KEY)
    {
        last_iter = multi_tap_mapping[0][key].begin();
        if (key != '.' && key != 'D' && keypad_mode == 1)
        {
            return toupper(*last_iter);
        }
        return *last_iter;
    }
    else if (key_type == SIG_KEY)
    {
        last_iter = multi_tap_mapping[1][key].begin();
        return *last_iter;
    }
    else if (key_type == MEM_KEY)
    {
        last_iter = multi_tap_mapping[2][key].begin();
        return *last_iter;
    }
    else
    {
        printf("\nKey type is not valid for multi tap input\n");
    }
    return 0;
}

uint8_t Multi_Tap::get_key_first_tap(uint8_t key, uint8_t key_type)
{
    return get_key(key, key_type);
}

uint8_t Multi_Tap::get_key_same_tap(uint8_t key, uint8_t key_type)
{
    last_key = key;
    last_key_type = key_type;
    if (key_type == NUM_KEY)
    {
        last_iter++;
        if (last_iter == multi_tap_mapping[0][key].end())
        {
            last_iter = multi_tap_mapping[0][key].begin();
        }
        if (key != '.' && key != 'D' && keypad_mode == 1)
        {
            return toupper(*last_iter);
        }
        return *last_iter;
    }
    else if (key_type == SIG_KEY)
    {
        last_iter++;
        if (last_iter == multi_tap_mapping[1][key].end())
        {
            last_iter = multi_tap_mapping[1][key].begin();
        }
        return *last_iter;
    }
    else if (key_type == MEM_KEY)
    {
        last_iter++;
        if (last_iter == multi_tap_mapping[2][key].end())
        {
            last_iter = multi_tap_mapping[2][key].begin();
        }
        return *last_iter;
    }
    else
    {
        printf("\nKey type is not valid for multi tap input\n");
    }
    return 0;
}

void Multi_Tap::set_keypad_mode(uint8_t mode_val)
{
    keypad_mode = mode_val;
}

void Multi_Tap::shift_keypad_mode(uint8_t key, uint8_t key_type)
{
    ESP_LOGW(TAG, "1: keypad mode: %d \t len: %d", keypad_mode, len);
    if (keypad_mode >= 2)
        keypad_mode = 0;
    else
        keypad_mode++;
    last_key = 0;
    last_key_type = 0;
    ESP_LOGW(TAG, "2: keypad mode: %d \t len: %d", keypad_mode, len);
}

uint8_t Multi_Tap::get_keypad_mode(void)
{
    return keypad_mode;
}

std::string Multi_Tap::get_buffer(void)
{
    ESP_LOGW(TAG, "len 1: %d", len);
    for (int i = 0; i <= len; i++)
    {
        printf("\n");
        printf("%c", buffer[i]);
        printf("\n");
    }
    if (len == 0 && buffer[0] == 0)
    {
        return std::string(buffer, 0);
    }
    else
    {
        while (buffer[len] == 0)
        {
            len--;
            if (len == 0)
            {
                break;
            }
        }
    }
    ESP_LOGW(TAG, "len 2: %d", len);
    return std::string(buffer, len + 1);
}

void Multi_Tap::single_back_space(void)
{
    ESP_LOGW(TAG, "1: len: %d", len);
    if (len > 0)
    {
        ESP_LOGW(TAG, "2: len: %d", len);
        if (buffer[len] == 0)
        {
            while (buffer[len] == 0)
            {
                len--;
                ESP_LOGW(TAG, "3: len: %d", len);
                if (len == 0)
                {
                    break;
                }
            }
            buffer[len] = 0;
        }
        else
        {
            buffer[len] = 0;
            len--;
        }
    }
    else
    {
        buffer[len] = 0;
        last_key = 0;
        last_key_type = 0;
    }
    ESP_LOGW(TAG, "Last key: %d key type: %d", last_key, last_key_type);
}