/*
 * SPDX-FileCopyrightText: 2017-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "sdkconfig.h"
#include "esp_efuse.h"
#include <assert.h>
#include "esp_efuse_custom_table.h"

// md5_digest_table 3aad295ade45adf9feac4a9ba5f75c17
// This file was generated from the file esp_efuse_custom_table.csv. DO NOT CHANGE THIS FILE MANUALLY.
// If you want to change some fields, you need to change esp_efuse_custom_table.csv file
// then run `efuse_common_table` or `efuse_custom_table` command it will generate this file.
// To show efuse_table run the command 'show_efuse_table'.

#define MAX_BLK_LEN CONFIG_EFUSE_MAX_BLK_LEN

// The last free bit in the block is counted over the entire file.
#define LAST_FREE_BIT_BLK3 184

_Static_assert(LAST_FREE_BIT_BLK3 <= MAX_BLK_LEN, "The eFuse table does not match the coding scheme. Edit the table and restart the efuse_common_table or efuse_custom_table command to regenerate the new files.");

static const esp_efuse_desc_t COUNTRY_CODE[] = {
    {EFUSE_BLK3, 56, 8}, 	 // Country Code,
};

static const esp_efuse_desc_t HARDWARE_VER[] = {
    {EFUSE_BLK3, 64, 12}, 	 // Hardware Version,
};

static const esp_efuse_desc_t BATCH_NO[] = {
    {EFUSE_BLK3, 76, 5}, 	 // Batch No,
};

static const esp_efuse_desc_t WI_FI[] = {
    {EFUSE_BLK3, 81, 1}, 	 // Wi-Fi bit,
};

static const esp_efuse_desc_t BL[] = {
    {EFUSE_BLK3, 82, 1}, 	 // BL bit,
};

static const esp_efuse_desc_t GSM[] = {
    {EFUSE_BLK3, 83, 1}, 	 // GSM bit,
};

static const esp_efuse_desc_t BATCH_YEAR[] = {
    {EFUSE_BLK3, 84, 7}, 	 // Batch Year,
};

static const esp_efuse_desc_t BATCH_MONTH[] = {
    {EFUSE_BLK3, 91, 4}, 	 // Batch Month,
};

static const esp_efuse_desc_t DATA_SET[] = {
    {EFUSE_BLK3, 95, 1}, 	 // Set Bit,
};

static const esp_efuse_desc_t DEVICE_ID[] = {
    {EFUSE_BLK3, 160, 24}, 	 // Device ID,
};





const esp_efuse_desc_t* ESP_EFUSE_COUNTRY_CODE[] = {
    &COUNTRY_CODE[0],    		// Country Code
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_HARDWARE_VER[] = {
    &HARDWARE_VER[0],    		// Hardware Version
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_BATCH_NO[] = {
    &BATCH_NO[0],    		// Batch No
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_WI_FI[] = {
    &WI_FI[0],    		// Wi-Fi bit
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_BL[] = {
    &BL[0],    		// BL bit
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_GSM[] = {
    &GSM[0],    		// GSM bit
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_BATCH_YEAR[] = {
    &BATCH_YEAR[0],    		// Batch Year
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_BATCH_MONTH[] = {
    &BATCH_MONTH[0],    		// Batch Month
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_DATA_SET[] = {
    &DATA_SET[0],    		// Set Bit
    NULL
};

const esp_efuse_desc_t* ESP_EFUSE_DEVICE_ID[] = {
    &DEVICE_ID[0],    		// Device ID
    NULL
};
