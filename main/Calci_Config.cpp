#include "Calci_Config.h"
#include <cstring>
#include <iomanip>
#include <sstream>

static const char *TAG = "Calci Config";

calci_mode mode_now;

static int is_relogging_happen = 0;

//*************************Task Handler************************************
TaskHandle_t Battery_Task_Handle = NULL;
TaskHandle_t OTA_Task_Handle = NULL;
TaskHandle_t Display_Task_Handle = NULL;
TaskHandle_t Factory_Reset_Task_Handle = NULL;
TaskHandle_t Keypad_Task_Handle = NULL;
TaskHandle_t Sync_Task_Handle = NULL;
TaskHandle_t WiFi_Task_Handle = NULL;
TaskHandle_t Upd_Ntfy_Task_Handle = NULL;
TaskHandle_t Log_Sender_Task_Handle = NULL;
//*************************Task Handler************************************

//******************************Setting parameters***************************

struct menu_pointer menu_pin;

std::string input_ssid = "";

static void psw_in_callback(void *arg);

const esp_timer_create_args_t psw_in_timer_args = {.callback = &psw_in_callback,
                                                   .name = "char_enter"};
esp_timer_handle_t psw_in_timer;

struct psw_in_struct psw_in;

char mobile_in[11];
uint8_t mobile_in_ele = 0;
char otp_in[5];
uint8_t otp_in_ele = 0;

std::string gst_in[5];

uint16_t total_pending_trans = 0;

xSemaphoreHandle sqlite_mutex;

std::list<Log_Sync_Struct> log_sync_data;

//******************************Setting parameters***************************
xQueueHandle keypad_queue;
// xQueueHandle instant_sync_queue;
// xQueueHandle instant_sync_data;
xQueueHandle periodic_sync_data;
xQueueHandle log_periodic_sync_data;

static void timeout_callback(void *arg)
{
  uint8_t temp = 2;
  xQueueSendFromISR(keypad_queue, &temp, NULL);
}

const esp_timer_create_args_t timeout_timer_args = {.callback = &timeout_callback,
                                                    .name = "mode"};
esp_timer_handle_t timeout_timer;

//********************************system API*********************************
static void print_device_desc(device_desc_t *desc)
{
  ESP_LOGI(TAG, "Country  = %d", desc->country_code);
  ESP_LOGI(TAG, "Hardware Ver  = %d", desc->hardware_ver);
  ESP_LOGI(TAG, "Batch No  = %d", desc->batch_no);
  ESP_LOGI(TAG, "Wi-Fi  = %d", desc->wi_fi_bit);
  ESP_LOGI(TAG, "Bluetooth  = %d", desc->bl_bit);
  ESP_LOGI(TAG, "GSM  = %d", desc->gsm_bit);
  ESP_LOGI(TAG, "Batch Year  = %d", desc->batch_year);
  ESP_LOGI(TAG, "Batch Month  = %d", desc->batch_month);
  ESP_LOGI(TAG, "Data Set  = %d", desc->data_set);
  ESP_LOGI(TAG, "Device ID  = %d", desc->device_id);
}

static void read_device_desc_efuse_fields(device_desc_t *desc)
{
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_COUNTRY_CODE, &desc->country_code, 8));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_HARDWARE_VER, &desc->hardware_ver, 12));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_BATCH_NO, &desc->batch_no, 5));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_WI_FI, &desc->wi_fi_bit, 1));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_BL, &desc->bl_bit, 1));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_GSM, &desc->gsm_bit, 1));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_BATCH_YEAR, &desc->batch_year, 7));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_BATCH_MONTH, &desc->batch_month, 4));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_DATA_SET, &desc->data_set, 1));
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_DEVICE_ID, &desc->device_id, 24));
  print_device_desc(desc);
}

static void read_efuse_fields(device_desc_t *desc)
{
  ESP_LOGI(TAG, "read efuse fields");

  uint8_t mac[6];
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_MAC_FACTORY, &mac, sizeof(mac) * 8));
  ESP_LOGI(TAG, "1. read MAC address: %02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  size_t secure_version = 0;
  ESP_ERROR_CHECK(esp_efuse_read_field_cnt(ESP_EFUSE_SECURE_VERSION, &secure_version));
  ESP_LOGI(TAG, "2. read secure_version: %d", secure_version);

  ESP_LOGI(TAG, "3. read custom fields");
  read_device_desc_efuse_fields(desc);
}

static void write_efuse_fields(device_desc_t *desc, esp_efuse_coding_scheme_t coding_scheme)
{
  const esp_efuse_coding_scheme_t coding_scheme_for_batch_mode = EFUSE_CODING_SCHEME_3_4;

  ESP_LOGI(TAG, "write custom efuse fields");
  if (coding_scheme == coding_scheme_for_batch_mode)
  {
    ESP_LOGI(TAG, "In the case of 3/4 or RS coding scheme, you cannot write efuse fields separately");
    ESP_LOGI(TAG, "You should use the batch mode of writing fields for this");
    ESP_ERROR_CHECK(esp_efuse_batch_write_begin());
  }
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_COUNTRY_CODE, &desc->country_code, 8));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_HARDWARE_VER, &desc->hardware_ver, 12));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_BATCH_NO, &desc->batch_no, 5));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_WI_FI, &desc->wi_fi_bit, 1));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_BL, &desc->bl_bit, 1));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_GSM, &desc->gsm_bit, 1));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_BATCH_YEAR, &desc->batch_year, 7));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_BATCH_MONTH, &desc->batch_month, 4));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_DATA_SET, &desc->data_set, 1));
  ESP_ERROR_CHECK(esp_efuse_write_field_blob(ESP_EFUSE_DEVICE_ID, &desc->device_id, 24));
  if (coding_scheme == coding_scheme_for_batch_mode)
  {
    ESP_ERROR_CHECK(esp_efuse_batch_write_commit());
  }
}

static esp_efuse_coding_scheme_t get_coding_scheme(void)
{
  // The coding scheme is used for EFUSE_BLK1, EFUSE_BLK2 and EFUSE_BLK3.
  // We use EFUSE_BLK3 (custom block) to verify it.
  esp_efuse_coding_scheme_t coding_scheme = esp_efuse_get_coding_scheme(EFUSE_BLK3);
  if (coding_scheme == EFUSE_CODING_SCHEME_NONE)
  {
    ESP_LOGI(TAG, "Coding Scheme NONE");
  }
  else if (coding_scheme == EFUSE_CODING_SCHEME_3_4)
  {
    ESP_LOGI(TAG, "Coding Scheme 3/4");
  }
  else
  {
    ESP_LOGI(TAG, "Coding Scheme REPEAT");
  }
  return coding_scheme;
}
//********************************system API*********************************

// void connect_wifi()
// {
//   psw_in.input_psw[psw_in.input_psw_ele + 1] = '\0';
//   char temp_psw[psw_in.input_psw_ele + 1];
//   memcpy(temp_psw, psw_in.input_psw, psw_in.input_psw_ele + 1);
//   wifi_disconnect();
//   // clear event bits
//   loadSTAconfig(input_ssid.c_str(), psw_in.input_psw);
//   esp_wifi_connect();
//   for (int i = 0; i < 3; i++)
//   {
//     bits = xEventGroupWaitBits(s_wifi_event_group,
//                                WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE,
//                                pdFALSE, 10000 / portTICK_PERIOD_MS);
//     if (bits & WIFI_CONNECTED_BIT)
//     {
//       ESP_LOGI(TAG, "connected to ap SSID");
//       break;
//     }
//     else if (bits & WIFI_FAIL_BIT)
//     {
//       ESP_LOGI(TAG, "Failed to connect to SSID");
//       xEventGroupClearBits(s_wifi_event_group, WIFI_FAIL_BIT);
//       esp_wifi_connect();
//     }
//     else
//     {
//       ESP_LOGE(TAG, "UNEXPECTED EVENT");
//     }
//   }
// }

uint8_t pars_psw_key(uint8_t key, uint8_t key_type)
{
  switch (key_type)
  {
  case NUM_KEY:
  {
    switch (key)
    {
    case '1':
    {
      if (psw_in.shift_mode == 0)
        return 'a' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'A' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '2':
    {
      if (psw_in.shift_mode == 0)
        return 'd' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'D' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '3':
    {
      if (psw_in.shift_mode == 0)
        return 'g' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'G' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '4':
    {
      if (psw_in.shift_mode == 0)
        return 'j' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'J' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '5':
    {
      if (psw_in.shift_mode == 0)
        return 'm' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'M' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '6':
    {
      if (psw_in.shift_mode == 0)
        return 'p' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'P' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '7':
    {
      if (psw_in.shift_mode == 0)
        return 's' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'S' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '8':
    {
      if (psw_in.shift_mode == 0)
        return 'v' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'V' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '9':
    {
      if (psw_in.shift_mode == 0)
        return 'y' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 1)
        return 'Y' + psw_in.same_key_press_no;
      else if (psw_in.shift_mode == 2)
        return key;
      break;
    }
    case '0':
    {
      if ((psw_in.shift_mode == 0) || (psw_in.shift_mode == 1))
      {
        if (psw_in.same_key_press_no == 0)
          return '!';
        else if (psw_in.same_key_press_no == 1)
          return '@';
        else if (psw_in.same_key_press_no == 2)
          return '#';
      }
      else
        return key;
      break;
    }
    case 'D':
    {
      if (psw_in.same_key_press_no == 0)
        return '$';
      else if (psw_in.same_key_press_no == 1)
        return '^';
      else if (psw_in.same_key_press_no == 2)
        return '&';
      break;
    }
    case '.':
    {
      if (psw_in.same_key_press_no == 0)
        return '.';
      else if (psw_in.same_key_press_no == 1)
        return ' ';
      else if (psw_in.same_key_press_no == 2)
        return '?';
      break;
      break;
    }
    }
    break;
  }
  case SIG_KEY:
  {
    switch (key)
    {
    case '+':
    {
      if (psw_in.same_key_press_no == 0)
        return key;
      else if (psw_in.same_key_press_no == 1)
        return '{';
      else if (psw_in.same_key_press_no == 2)
        return '}';
      break;
    }
    case '-':
    {
      if (psw_in.same_key_press_no == 0)
        return key;
      else if (psw_in.same_key_press_no == 1)
        return '[';
      else if (psw_in.same_key_press_no == 2)
        return ']';
      break;
    }
    case '*':
    {
      if (psw_in.same_key_press_no == 0)
        return key;
      else if (psw_in.same_key_press_no == 1)
        return '(';
      else if (psw_in.same_key_press_no == 2)
        return ')';
      break;
    }
    case '/':
    {
      if (psw_in.same_key_press_no == 0)
        return key;
      else if (psw_in.same_key_press_no == 1)
        return '\\';
      else if (psw_in.same_key_press_no == 2)
        return '"';
      break;
    }
    case '%':
    {
      if (psw_in.same_key_press_no == 0)
        return key;
      else if (psw_in.same_key_press_no == 1)
        return ':';
      else if (psw_in.same_key_press_no == 2)
        return '|';
      break;
    }
    }
    break;
  }
  case MEM_KEY:
  {
    switch (key)
    {
    case '+':
    {
      if (psw_in.same_key_press_no == 0)
        return '<';
      else if (psw_in.same_key_press_no == 1)
        return '>';
      else if (psw_in.same_key_press_no == 2)
        return ';';
      break;
    }
    case '-':
    {
      if (psw_in.same_key_press_no == 0)
        return '_';
      else if (psw_in.same_key_press_no == 1)
        return '\'';
      else if (psw_in.same_key_press_no == 2)
        return '~';
      break;
    }
    case '=':
    {
      if (psw_in.same_key_press_no == 0)
        return '=';
      else if (psw_in.same_key_press_no == 1)
        return '^';
      else if (psw_in.same_key_press_no == 2)
        return '`';
      break;
    }
    }
    break;
  }
  }
  return 0;
}

static void psw_in_callback(void *arg)
{
  psw_in.same_key_press_no = 0;
  psw_in.input_psw_ele++;
}

void clear_keypad()
{
  uint8_t temp = 1;
  xQueueSend(keypad_queue, (void *)&temp, (TickType_t)0);
}

void send_ota_failed()
{
  uint8_t temp = 3;
  xQueueSend(keypad_queue, (void *)&temp, (TickType_t)0);
}

std::string get_firmware_version()
{
  return std::string(CONFIG_FIRMWARE_VERSION_NAME);
}

std::string get_hardware_version()
{
  uint16_t temp_hard_ver;
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_HARDWARE_VER, &temp_hard_ver, 12));
  std::ostringstream ss;
  ss << std::setfill('0') << std::setw(3) << std::hex << temp_hard_ver;
  return ss.str();
}

std::string get_device_id()
{
  uint16_t temp_device_id;
  ESP_ERROR_CHECK(esp_efuse_read_field_blob(ESP_EFUSE_DEVICE_ID, &temp_device_id, 24));
  std::ostringstream ss;
  ss << std::setfill('0') << std::setw(6) << std::hex << temp_device_id;
  return ss.str();
}

std::string get_model_no()
{
  device_desc_t device_desc = {0};
  read_device_desc_efuse_fields(&device_desc);
  print_device_desc(&device_desc);
  char buffer[18];
  sprintf(buffer, "SC-%02X-%X%X%X", device_desc.country_code, device_desc.wi_fi_bit, device_desc.bl_bit, device_desc.gsm_bit);

  ESP_LOGI(TAG, "Model No: %s", buffer);
  return std::string(buffer);
}

std::string get_serial_no(void)
{
  device_desc_t device_desc = {0};
  read_device_desc_efuse_fields(&device_desc);
  print_device_desc(&device_desc);
  char buffer[40];
  sprintf(buffer, "SC-%02X-%03d-B%02X%X%X%X-%02X%02X-%06X", device_desc.country_code, device_desc.hardware_ver, device_desc.batch_no, device_desc.wi_fi_bit, device_desc.bl_bit, device_desc.gsm_bit, device_desc.batch_year, device_desc.batch_month, device_desc.device_id);
  ESP_LOGI(TAG, "Serial No: %s", buffer);
  return std::string(buffer);
}

std::string get_batch_no(void)
{
  device_desc_t device_desc = {0};
  read_device_desc_efuse_fields(&device_desc);
  print_device_desc(&device_desc);
  char buffer[10];
  sprintf(buffer, "B%02X-%02X%02X", device_desc.batch_no, device_desc.batch_year, device_desc.batch_month);

  ESP_LOGI(TAG, "Batch No: %s", buffer);
  return std::string(buffer);
}

void log_out_user(void)
{
  File_Operation del_token;
  del_token.m_remove(AUTH_TOKEN);
  del_token.m_remove(ACCS_TOKEN);
  del_token.m_remove(CASHBOOK_ID);
  del_token.m_remove(USERNAME);
  is_relogging_happen = 1;
}

uint8_t is_user_log_in(void)
{
  File_Operation check_login_file;
  if (check_login_file.m_exists(AUTH_TOKEN) && check_login_file.m_exists(ACCS_TOKEN) && check_login_file.m_exists(USERNAME) && check_login_file.m_exists(CASHBOOK_ID))
  {
    return 1;
  }
  return 0;
}

uint8_t is_just_logout(void)
{
  if (is_relogging_happen == 1)
  {
    return 1;
  }
  return 0;
}

void set_is_relogging_happen(int relogging_sts)
{
  is_relogging_happen = relogging_sts;
}

void wait_untill_task_delete(TaskHandle_t* x_check_task_handle)
{
  ESP_LOGI(TAG, "Waiting for task to delete");
  TaskStatus_t xTaskDetails;
  vTaskGetInfo(*x_check_task_handle, &xTaskDetails, pdTRUE, eInvalid);
  while(xTaskDetails.eCurrentState != eDeleted)
  {
    vTaskGetInfo(*x_check_task_handle, &xTaskDetails, pdTRUE, eInvalid);
    vTaskDelay(100 / portTICK_RATE_MS);
  }
  if(xTaskDetails.eCurrentState == eDeleted)
  {
    ESP_LOGI(TAG, "%s: Task deleted", xTaskDetails.pcTaskName);
  }
}
