#include "Sys_Time.h"

const char *RTC_TAG = "RTC_TAG";

std::string data;
ds1302_t dev;

void sntp_sync_time(struct timeval *tv)
{
    settimeofday(tv, NULL);
    ESP_LOGI(RTC_TAG, "Time is synchronized from custom code");
    sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
    time_t now;
    struct tm timeinfo;
    time(&now);
    ESP_LOGI(RTC_TAG, "System Time: %lld", (int64_t)now);
    localtime_r(&now, &timeinfo);
    ESP_LOGI(RTC_TAG, "System Time: %s", timeToString(timeinfo).c_str());
    set_rtc_time(&timeinfo);
    get_rtc_time();
}

void time_sync_notification_cb(struct timeval *tv)
{
    ESP_LOGI(RTC_TAG, "Notification of a time synchronization event");
}

static void initialize_sntp(void)
{
    ESP_LOGI(RTC_TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    ESP_LOGI(RTC_TAG, "Your NTP Server is %s", NTP_SERVER);
    sntp_setservername(0, NTP_SERVER);
    sntp_set_time_sync_notification_cb(time_sync_notification_cb);
    //   sntp_set_sync_mode(CONFIG_SNTP_TIME_SYNC_METHOD_CUSTOM);
    sntp_init();
}

void set_rtc_time(struct tm *time)
{
    ds1302_set_time(&dev, time);
    ESP_LOGI(RTC_TAG, "Set initial date time done");
}

struct tm get_rtc_time()
{
    struct tm rtcinfo;

    ds1302_get_time(&dev, &rtcinfo);
    ESP_LOGI(RTC_TAG, "%04d-%02d-%02d %02d:%02d:%02d", rtcinfo.tm_year + 1900,
             rtcinfo.tm_mon + 1, rtcinfo.tm_mday, rtcinfo.tm_hour, rtcinfo.tm_min,
             rtcinfo.tm_sec);
    return rtcinfo;
}

void set_sys_time(struct tm *tm)
{
    time_t epoch = mktime(tm);
    struct timeval now;
    int rc;

    now.tv_sec = epoch;
    now.tv_usec = 0;

    rc = settimeofday(&now, NULL);
    if (rc == 0)
    {
        // printf("settimeofday() successful.\n");
    }
    else
    {
        // printf("settimeofday() failed, "
            //    "errno = %d\n",
            //    errno);
    }
}

std::string timeToString(struct tm timeString)
{
    std::string timeData = std::to_string(timeString.tm_year + 1900);
    timeData = timeData.append("-");
    timeData = timeData.append(std::to_string(timeString.tm_mon + 1));
    timeData = timeData.append("-");
    timeData = timeData.append(std::to_string(timeString.tm_mday));
    timeData = timeData.append("T");
    timeData = timeData.append(std::to_string(timeString.tm_hour));
    timeData = timeData.append(":");
    timeData = timeData.append(std::to_string(timeString.tm_min));
    timeData = timeData.append(":");
    timeData = timeData.append(std::to_string(timeString.tm_sec));
    timeData = timeData.append("Z");
    return timeData;
}

// function to store time stamp
time_t timeStemp()
{
    time_t now;
    time(&now);
    // printf("time stamp :%ld", now);
    return now;
}

void start_sys_time()
{
    dev.ce_pin = CE_GPIO;
    dev.io_pin = IO_GPIO;
    dev.sclk_pin = SCLK_GPIO;
    ESP_ERROR_CHECK(ds1302_init(&dev));
    ESP_ERROR_CHECK(ds1302_set_write_protect(&dev, false));
    vTaskDelay(100/ portTICK_RATE_MS);
    ESP_ERROR_CHECK(ds1302_start(&dev, true));
    ESP_LOGI(RTC_TAG, "CONFIG_TIMEZONE=  %lf", CONFIG_TIMEZONE);

    struct tm time = get_rtc_time();
    ESP_LOGI(RTC_TAG, "RTC Time: %s", timeToString(time).c_str());
    set_sys_time(&time);
    initialize_sntp();
}