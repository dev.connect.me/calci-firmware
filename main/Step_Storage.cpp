#include "Step_Storage.h"

std::list<Operand> operation;

/// @brief
/// @param reg
/// @param op_sign
void push_arith_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.arith_op = op_sign;
    operation.push_back(new_operand);
}

void push_tax_operation(double reg, int tax_type, uint8_t val_type, uint8_t tax_perc)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.tax_op.tax_type = tax_type;
    new_operand.tax_op.val_type = val_type;
    new_operand.tax_op.tax_perc = tax_perc;
    operation.push_back(new_operand);
}

void push_mem_operation(double reg, int op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.mem_op = op_sign;
    operation.push_back(new_operand);
}

void push_cash_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.cash_op = op_sign;
    operation.push_back(new_operand);
}

void push_gt_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.gt_op = op_sign;
    operation.push_back(new_operand);
}

void push_mu_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.mu_op = op_sign;
    operation.push_back(new_operand);
}

void push_arith_result_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.arith_op = op_sign;
    new_operand.result_flag = 1;
    operation.push_back(new_operand);
}

void push_mem_result_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.mem_op = op_sign;
    new_operand.result_flag = 1;
    operation.push_back(new_operand);
}

void push_cash_result_operation(double reg, uint8_t op_sign)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.cash_op = op_sign;
    new_operand.result_flag = 1;
    operation.push_back(new_operand);
}

void push_tax_result_operation(double reg, int tax_type, uint8_t val_type, uint8_t tax_perc)
{
    Operand new_operand = {0};
    new_operand.val = reg;
    new_operand.tax_op.tax_type = tax_type;
    new_operand.tax_op.val_type = val_type;
    new_operand.tax_op.tax_perc = tax_perc;
    new_operand.result_flag = 1;
    operation.push_back(new_operand);
}

void show_operations()
{
    for (auto it = operation.begin(); it != operation.end(); it++)
    {
        std::cout << "Operand: " << it->val << "\tOperator: " << it->arith_op << std::endl;
        std::cout << "Tax type: " << it->tax_op.tax_type << "\tVal type: " << it->tax_op.val_type << "\tTax perc: " << it->tax_op.tax_perc << std::endl;
        std::cout << "Mem: " << it->mem_op << std::endl;
    }
}

void change_last_operation(double reg, uint8_t op_sign)
{
    operation.back().val = reg;
    operation.back().arith_op = op_sign;
}

void change_last_tax_operation(int tax_type, uint8_t val_type, uint8_t tax_perc)
{
    operation.back().tax_op.tax_type = tax_type;
    operation.back().tax_op.val_type = val_type;
    operation.back().tax_op.tax_perc = tax_perc;
}