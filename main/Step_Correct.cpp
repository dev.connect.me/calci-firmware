#include "Step_Correct.h"

double corrected_operand = 0;
uint8_t corrected_sign = 0;
double corrected_pnt1 = 1;
uint8_t changed_operand = 0;

double correct_accum = 0;
double correct_s_reg = 0;

uint8_t correct_a_flag, correct_e_flag, correct_p_flag, correct_m_flag, correct_gst_flag;

std::list<Operand> copy_operation;
std::list<Operand>::iterator simplify_it;

static Grand_Total correct_grand_total;
double correct_mem;

double correct_addition(double op1, double op2)
{
    return op1 + op2;
}
double correct_substraction(double op1, double op2)
{
    return op1 - op2;
}
double correct_multiplication(double op1, double op2)
{
    return op1 * op2;
}
double correct_division(double op1, double op2)
{
    return op1 / op2;
}
double correct_addition_percen(double op1, double op2)
{
    return op1 + ((op1 * op2) / 100);
}
double correct_substraction_percen(double op1, double op2)
{
    return op1 - ((op1 * op2) / 100);
}
double correct_multiplication_percen(double op1, double op2)
{
    return op1 * (op2 / 100);
}
double correct_division_percen(double op1, double op2)
{
    return (100 * op1) / op2;
}
double correct_calculate_inclusive_tax(double amount, uint8_t cost_type, char key)
{
    int x = (int)key; // type cast the character into double to perforn further operations
    x -= 48;
    if (cost_type == 1)
    {
        return amount + (amount * (gst_rate[x] / 100.0));
    }
    else if (cost_type == 2)
    {
        return amount * 100.0 / (gst_rate[x] + 100.0);
    }
    else if (cost_type == 3)
    {
        return amount * gst_rate[x] / 100.0;
    }
    else
    {
        return (100.0 + gst_rate[x]) * amount / gst_rate[x];
    }
}
double correct_calculate_exclusive_tax(double amount, uint8_t cost_type, char key)
{
    int x = (int)key; // type cast the character into double to perforn further operations
    x -= 48;
    if (cost_type == 1)
    {
        return amount * 100.0 / (gst_rate[x] + 100.0);
    }
    else if (cost_type == 2)
    {
        return amount * gst_rate[x] / 100.0;
    }
    else if (cost_type == 3)
    {
        return amount * 100.0 / gst_rate[x];
    }
    else
    {
        return amount * gst_rate[x] / 100.0;
    }
}

void clear_correct_var()
{
    corrected_operand = 0;
    corrected_sign = 0;
    correct_grand_total = {0};
    correct_mem = 0;

    corrected_pnt1 = 1;
    changed_operand = 0;

    correct_accum = 0;
    correct_s_reg = 0;

    correct_a_flag = 0;
    correct_e_flag = 0;
    correct_p_flag = 0;
    correct_m_flag = 0;
    correct_gst_flag = 0;
}

void add_to_corrected_operand(char key)
{
    double x = (double)key; // type cast the character into double to perforn further operations
    x -= 48;                // since character digit indoubleing -48 gives the corresponding ASCII
    if (corrected_pnt1 == 1)
    {
        corrected_operand = (corrected_operand * 10) + x; // add it in result
        if (corrected_operand > 999999999999)
            corrected_operand = (corrected_operand - x) / 10;
        print_operand(true, corrected_operand, 0);
    }
    else
    {
        corrected_operand = corrected_operand + (x * corrected_pnt1); // add it in result
        double temp_prec = 1 / corrected_pnt1;
        int temp2 = 0;
        while (temp_prec > 1)
        {
            temp_prec = temp_prec / 10;
            temp2++;
        }
        if ((int)corrected_operand > (pow(10, (12 - temp2))) - 1)
        {
            corrected_operand = corrected_operand - (x * corrected_pnt1);
            corrected_pnt1 *= 10;
            temp2--;
        }
        else if (temp2 > 5)
        {
            corrected_operand = corrected_operand - (x * corrected_pnt1);
            corrected_pnt1 *= 10;
            temp2--;
        }
        print_operand(true, corrected_operand, temp2);
    }
}

void parse_and_add_to_corrected_operand(char key)
{
    changed_operand = 1;
    if (key <= '9' && key >= '0')
    {
        add_to_corrected_operand(key);
        if (corrected_pnt1 <= 0.1)
            corrected_pnt1 = corrected_pnt1 / 10;
    }
    else if (key == 'D')
    {
        add_to_corrected_operand('0');
        if (corrected_pnt1 <= 0.1)
            corrected_pnt1 = corrected_pnt1 / 10;
        add_to_corrected_operand('0');
        if (corrected_pnt1 <= 0.1)
            corrected_pnt1 = corrected_pnt1 / 10;
    }
    else if (key == '.')
    {
        if (corrected_pnt1 == 1)
        {
            corrected_pnt1 = 0.1;
            print_operand(true, corrected_operand, -2);
        }
    }
}

void change_correct_operator_sign(char key)
{
    if (step_it->result_flag)
        return;
    else if (step_it->arith_op)
    {
        step_it->arith_op = key;
        print_operator(true, key);
    }
}

void simplify_operations_with_correction()
{
    if (changed_operand)
        step_it->val = corrected_operand;
    for (simplify_it = operation.begin(); simplify_it != operation.end(); simplify_it++)
    {
        if (simplify_it->result_flag)
        {
            if (simplify_it->arith_op)
            {
                if (simplify_it->arith_op == '=')
                {
                }
                else if (simplify_it->arith_op == '=')
                {
                }
            }
            else if (simplify_it->tax_op.tax_type)
            {
            }
            else if (simplify_it->mem_op)
            {
                if (simplify_it->mem_op == '=')
                {
                }
                else
                {
                }
            }
            else if (simplify_it->cash_op)
            {
            }
        }
        else
        {
            if (simplify_it->arith_op)
            {
                if (simplify_it->arith_op != '=' && simplify_it->arith_op != '%')
                {
                    correct_add_sign_input(simplify_it->arith_op);
                }
                else if (simplify_it->arith_op == '=')
                {
                    correct_equal_input(simplify_it->arith_op);
                }
                else if (simplify_it->arith_op == '%')
                {
                    correct_perc_input(simplify_it->arith_op);
                }
            }
            else if (simplify_it->tax_op.tax_type)
            {
                correct_gst_input(simplify_it->tax_op.tax_type, simplify_it->tax_op.val_type, simplify_it->tax_op.tax_perc);
            }
            else if (simplify_it->mem_op)
            {
                if (simplify_it->mem_op == '=')
                {
                }
                else
                {
                    correct_in_mem(simplify_it->mem_op);
                }
            }
            else if (simplify_it->cash_op)
            {
            }
            else if (simplify_it->gt_op)
            {
            }
            else
            {
                Operand new_operand = {0};
                new_operand.val = simplify_it->val;
                new_operand.arith_op = 0;
                copy_operation.push_back(new_operand);
            }
        }
    }
    std::copy(copy_operation.begin(), copy_operation.end(), operation.begin());
    copy_operation.clear();

    accum = correct_accum;
    mem = correct_mem;
    s_reg = 0;
    memcpy((void *)&grand_total, (void *)&correct_grand_total, sizeof(correct_grand_total));
    a_flag = correct_a_flag;
    e_flag = correct_e_flag;
    p_flag = correct_p_flag;
    m_flag = correct_m_flag;
    gst_flag = correct_gst_flag;
    clear_correct_var();
}

void correct_add_sign_input(char key)
{
    if (!correct_a_flag && !correct_e_flag && !correct_p_flag && !correct_m_flag && !correct_gst_flag)
    {
        correct_a_flag = simplify_it->arith_op;
        correct_accum = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = correct_a_flag;
        copy_operation.push_back(new_operand);
    }
    else if (correct_a_flag)
    {
        if (correct_a_flag == '+')
        {
            correct_a_flag = simplify_it->arith_op;
            correct_s_reg = simplify_it->val;
            Operand new_operand = {0};
            new_operand.val = correct_s_reg;
            new_operand.arith_op = correct_a_flag;
            copy_operation.push_back(new_operand);
            correct_accum = correct_addition(correct_accum, correct_s_reg);
            correct_s_reg = 0;
        }
        else if (correct_a_flag == '-')
        {
            correct_a_flag = simplify_it->arith_op;
            correct_s_reg = simplify_it->val;
            Operand new_operand = {0};
            new_operand.val = correct_s_reg;
            new_operand.arith_op = correct_a_flag;
            copy_operation.push_back(new_operand);
            correct_accum = correct_substraction(correct_accum, correct_s_reg);
            correct_s_reg = 0;
        }
        else if (correct_a_flag == '*')
        {
            correct_a_flag = simplify_it->arith_op;
            correct_s_reg = simplify_it->val;
            Operand new_operand = {0};
            new_operand.val = correct_s_reg;
            new_operand.arith_op = correct_a_flag;
            copy_operation.push_back(new_operand);
            correct_accum = correct_multiplication(correct_accum, correct_s_reg);
            correct_s_reg = 0;
        }
        else if (correct_a_flag == '/')
        {
            correct_a_flag = simplify_it->arith_op;
            correct_s_reg = simplify_it->val;
            Operand new_operand = {0};
            new_operand.val = correct_s_reg;
            new_operand.arith_op = correct_a_flag;
            copy_operation.push_back(new_operand);
            correct_accum = correct_division(correct_accum, correct_s_reg);
            correct_s_reg = 0;
        }
    }
    else if (correct_e_flag)
    {
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = simplify_it->arith_op;
        copy_operation.push_back(new_operand);
        correct_a_flag = simplify_it->arith_op;
        correct_e_flag = 0;
    }
    else if (correct_p_flag)
    {
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = simplify_it->arith_op;
        copy_operation.push_back(new_operand);
        correct_a_flag = simplify_it->arith_op;
        correct_e_flag = 0;
    }
    else if (correct_m_flag)
    {
        if (correct_m_flag == '+')
        {
            Operand new_operand = {0};
            new_operand.val = correct_accum;
            new_operand.arith_op = simplify_it->arith_op;
            copy_operation.push_back(new_operand);
            correct_a_flag = simplify_it->arith_op;
            correct_e_flag = 0;
        }
        else if (correct_m_flag == '-')
        {
            Operand new_operand = {0};
            new_operand.val = correct_accum;
            new_operand.arith_op = simplify_it->arith_op;
            copy_operation.push_back(new_operand);
            correct_a_flag = simplify_it->arith_op;
            correct_e_flag = 0;
        }
    }
    else if (correct_gst_flag)
    {
    }
}

void correct_equal_input(char key)
{
    if (!correct_a_flag && !correct_e_flag && !correct_p_flag && !correct_m_flag && !correct_gst_flag)
    {
        correct_e_flag = simplify_it->arith_op;
        correct_accum = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = correct_a_flag;
        copy_operation.push_back(new_operand);
        correct_grand_total.total += correct_accum;
        correct_e_flag = 1;
    }
    else if (correct_a_flag)
    {
        correct_e_flag = simplify_it->arith_op;
        correct_s_reg = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_s_reg;
        new_operand.arith_op = correct_e_flag;
        copy_operation.push_back(new_operand);
        simplify_it++;
        if (correct_a_flag == '+')
        {
            correct_accum = correct_addition(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '-')
        {
            correct_accum = correct_substraction(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '*')
        {
            correct_accum = correct_multiplication(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '/')
        {
            correct_accum = correct_division(correct_accum, correct_s_reg);
        }

        new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = key;
        new_operand.result_flag = 1;
        copy_operation.push_back(new_operand);
        correct_grand_total.total += correct_accum;
        correct_a_flag = 0;
    }
    else if (correct_e_flag)
    {
    }
    else if (correct_p_flag)
    {
    }
    else if (correct_m_flag)
    {
    }
    else if (correct_gst_flag)
    {
    }
}

void correct_perc_input(char key)
{
    if (!correct_a_flag && !correct_e_flag && !correct_p_flag && !correct_m_flag && !correct_gst_flag)
    {
        correct_p_flag = simplify_it->arith_op;
        correct_accum = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = correct_a_flag;
        copy_operation.push_back(new_operand);
        correct_grand_total.total += correct_accum;
        correct_p_flag = 1;
    }
    else if (correct_a_flag)
    {
        correct_p_flag = simplify_it->arith_op;
        correct_s_reg = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_s_reg;
        new_operand.arith_op = correct_p_flag;
        copy_operation.push_back(new_operand);
        simplify_it++;
        if (correct_a_flag == '+')
        {
            correct_accum = correct_addition_percen(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '-')
        {
            correct_accum = correct_substraction_percen(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '*')
        {
            correct_accum = correct_multiplication_percen(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '/')
        {
            correct_accum = correct_division_percen(correct_accum, correct_s_reg);
        }

        new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.arith_op = key;
        new_operand.result_flag = 1;
        copy_operation.push_back(new_operand);
        correct_grand_total.total += correct_accum;
        correct_a_flag = 0;
    }
    else if (correct_e_flag)
    {
    }
    else if (correct_p_flag)
    {
    }
    else if (correct_m_flag)
    {
    }
    else if (correct_gst_flag)
    {
    }
}

void correct_gst_input(int8_t gst_type, uint8_t cost_type, char key)
{
    if (!correct_a_flag && !correct_e_flag && !correct_p_flag && !correct_m_flag && !correct_gst_flag)
    {
        correct_gst_flag = 1;
        correct_accum = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.tax_op.tax_perc = key;
        new_operand.tax_op.tax_type = gst_type;
        new_operand.tax_op.val_type = cost_type;
        copy_operation.push_back(new_operand);

        if (gst_type == 1)
        {
            double temp_accum = correct_accum;
            correct_grand_total.net += correct_accum;
            correct_accum = correct_calculate_inclusive_tax(correct_accum, cost_type, key);
            correct_grand_total.gross += correct_accum;
            correct_grand_total.tare += correct_accum - temp_accum;
        }
        else if (gst_type == -1)
        {
            double temp_accum = correct_accum;
            correct_grand_total.gross += correct_accum;
            correct_accum = correct_calculate_exclusive_tax(correct_accum, cost_type, key);
            correct_grand_total.net += correct_accum;
            correct_grand_total.tare += temp_accum - correct_accum;
        }

        new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.tax_op.tax_perc = key;
        new_operand.tax_op.tax_type = gst_type;
        new_operand.tax_op.val_type = cost_type;
        copy_operation.push_back(new_operand);
    }
    else if (correct_a_flag)
    {
        correct_gst_flag = 1;
        correct_s_reg = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = simplify_it->val;
        new_operand.tax_op.tax_perc = key;
        new_operand.tax_op.tax_type = gst_type;
        new_operand.tax_op.val_type = cost_type;
        copy_operation.push_back(new_operand);
        if (correct_a_flag == '+')
        {
            correct_accum = correct_addition(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '-')
        {
            correct_accum = correct_substraction(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '*')
        {
            correct_accum = correct_multiplication(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '/')
        {
            correct_accum = correct_division(correct_accum, correct_s_reg);
        }
        if (gst_type == 1)
        {
            double temp_accum = correct_accum;
            correct_grand_total.net += correct_accum;
            correct_accum = correct_calculate_inclusive_tax(correct_accum, cost_type, key);
            correct_grand_total.gross += correct_accum;
            correct_grand_total.tare += correct_accum - temp_accum;
        }
        else if (gst_type == -1)
        {
            double temp_accum = correct_accum;
            correct_grand_total.gross += correct_accum;
            correct_accum = correct_calculate_exclusive_tax(correct_accum, cost_type, key);
            correct_grand_total.net += correct_accum;
            correct_grand_total.tare += temp_accum - correct_accum;
        }
        new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.tax_op.tax_perc = key;
        new_operand.tax_op.tax_type = gst_type;
        new_operand.tax_op.val_type = cost_type;
        copy_operation.push_back(new_operand);
        correct_s_reg = 0;
        correct_a_flag = 0;
    }
    else if (correct_e_flag)
    {
    }
    else if (correct_p_flag)
    {
    }
    else if (correct_m_flag)
    {
    }
    else if (correct_gst_flag)
    {
        if (correct_accum != 0)
        {
            if (gst_type == 1)
                correct_accum = correct_calculate_inclusive_tax(correct_accum, cost_type, key);
            else if (gst_type == -1)
                correct_accum = correct_calculate_exclusive_tax(correct_accum, cost_type, key);
            Operand new_operand = {0};
            new_operand.val = correct_accum;
            new_operand.tax_op.tax_perc = key;
            new_operand.tax_op.tax_type = gst_type;
            new_operand.tax_op.val_type = cost_type;
            copy_operation.push_back(new_operand);
        }
    }
}

void correct_in_mem(char key)
{
    if (!correct_a_flag && !correct_e_flag && !correct_p_flag && !correct_m_flag && !correct_gst_flag)
    {
        correct_accum = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.mem_op = key;
        copy_operation.push_back(new_operand);
        if (key == '+')
            correct_mem += correct_accum;
        else if (key == '-')
            correct_mem -= correct_accum;
        correct_m_flag = key;
    }
    else if (correct_a_flag)
    {
        correct_s_reg = simplify_it->val;
        Operand new_operand = {0};
        new_operand.val = correct_s_reg;
        new_operand.mem_op = key;
        copy_operation.push_back(new_operand);
        if (correct_a_flag == '+')
        {
            correct_accum = correct_addition(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '-')
        {
            correct_accum = correct_substraction(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '*')
        {
            correct_accum = correct_multiplication(correct_accum, correct_s_reg);
        }
        else if (correct_a_flag == '/')
        {
            correct_accum = correct_division(correct_accum, correct_s_reg);
        }
        correct_s_reg = 0;
        if (key == '+')
            correct_mem += correct_accum;
        else if (key == '-')
            correct_mem -= correct_accum;

        correct_a_flag = 0;
        correct_m_flag = key;
    }
    else if (correct_e_flag)
    {
        if (key == '+')
            correct_mem += correct_accum;
        else if (key == '-')
            correct_mem -= correct_accum;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.mem_op = key;
        copy_operation.push_back(new_operand);
        correct_e_flag = 0;
        correct_m_flag = key;
    }
    else if (correct_p_flag)
    {
        if (key == '+')
            correct_mem += correct_accum;
        else if (key == '-')
            correct_mem -= correct_accum;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.mem_op = key;
        copy_operation.push_back(new_operand);
        correct_p_flag = 0;
        correct_m_flag = key;
    }
    else if (correct_m_flag)
    {
        if (key == '+')
            correct_mem += correct_accum;
        else if (key == '-')
            correct_mem -= correct_accum;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.mem_op = key;
        copy_operation.push_back(new_operand);
        correct_m_flag = key;
    }
    else if (correct_gst_flag)
    {
        if (key == '+')
            correct_mem += correct_accum;
        else if (key == '-')
            correct_mem -= correct_accum;
        Operand new_operand = {0};
        new_operand.val = correct_accum;
        new_operand.mem_op = key;
        copy_operation.push_back(new_operand);
        correct_gst_flag = 0;
        correct_m_flag = key;
    }
}