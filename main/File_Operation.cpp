#include "File_Operation.h"
#include "nvs.h"
#include "nvs_flash.h"

File_Operation::File_Operation() {}

File_Operation::~File_Operation() {}

void File_Operation::m_initSpiff() {
  esp_vfs_spiffs_conf_t conf = {.base_path = "/spiffs",
                                .partition_label = NULL,
                                .max_files = 5,
                                .format_if_mount_failed = true};

  // Use settings defined above to initialize and mount SPIFFS filesystem.
  // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
  esp_err_t ret = esp_vfs_spiffs_register(&conf);

  if (ret != ESP_OK) {
    if (ret == ESP_FAIL) {
    } else if (ret == ESP_ERR_NOT_FOUND) {
    } else {
    }
    return;
  }
}
void File_Operation::m_list() {
  DIR *dirPointer;
  struct dirent *direntPointer;
  dirPointer = opendir("/spiffs");
  if (dirPointer) {
    while ((direntPointer = readdir(dirPointer)) != NULL) {
      // printf("%s\n", direntPointer->d_name);
    }
    closedir(dirPointer);
  }
}

bool File_Operation::m_write(const char *fileName, const char *data) {
  FILE *f = fopen(fileName, "w");
  if (f == NULL) {
    // printf("file writing fail");
    return false;
  }
  fprintf(f, "%s\n", data);
  fclose(f);
  return true;
}

std::string File_Operation::m_read(const char *fileName) {
  FILE *f = fopen(fileName, "r");
  int count = 0;
  if (f == NULL) {
    // printf("Failed to open file for reading");
    return "-1";
  } else {
    while (1) {
      char c = fgetc(f); // reading the file
      if (c == '\n') {
        break;
      }
      count++;
    }
    rewind(f);
    char line[count + 1];
    fscanf(f, "%[^\n]", line);
    fclose(f);
    return std::string(line);
  }
}

// return zero if delete successful
void File_Operation::m_remove(const char *fileName) {
  if (remove(fileName) == 0) {
    // printf("Deleted successfully");
  } else {
    // printf("Unable to delete the file");
  }
}

bool File_Operation::m_exists(const char *fileName) {
  struct stat buffer;
  int exist = stat(fileName, &buffer);
  if (!exist) {
    return true;
  } else { // -1
    return false;
  }
}

int File_Operation::m_size(const char *fileName) {
  // opening the file in read mode
  FILE *file = fopen(fileName, "r");

  // checking if the file exist or not
  if (file == NULL) {
    return -1;
  }

  fseek(file, 0, SEEK_END);

  // calculating the size of the file
  int res = ftell(file);

  // closing the file
  fclose(file);

  return res;
}
