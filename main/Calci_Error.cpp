#include "Calci_Error.h"
#include "Calci_Settings.h"

static const char *TAG = "ERROR MODE";

Error_State Selected_Error_State = ERROR_MAX;
Error_State_Wifi Selected_Error_Wifi_State = ERROR_WIFI_MAX;
Error_State_Login Selected_Error_Login_State = ERROR_LOGIN_MAX;

static uint32_t is_first_trans = 0;

void erro_mode_entry_action_handle(Error_State err_type)
{
    mode_now = ERROR_MODE; // change mode
    switch (err_type)
    {
    case ERROR_WIFI:
    {
        ESP_LOGI(TAG, "WIFI ERROR");
        Selected_Error_State = ERROR_WIFI;
        Selected_Error_Wifi_State = ERROR_WIFI_CONNECT;
        display_error_entry_wifi(true, ERROR_WIFI_CONNECT);
        break;
    }
    case ERROR_LOGIN:
    {
        ESP_LOGI(TAG, "LOGIN ERROR");
        Selected_Error_State = ERROR_LOGIN;
        Selected_Error_Login_State = ERROR_LOGIN_NOW;
        display_error_entry_login(true, ERROR_LOGIN_NOW);
        break;
    }
    case ERROR_MAX:
    {
        break;
    }
    }
}

int error_mode_entry_action(Error_State err_type)
{
    if (is_first_trans == 0)
    {
        erro_mode_entry_action_handle(err_type);
        is_first_trans++;
        return 0;
    }
    if(is_just_logout())
    {
        erro_mode_entry_action_handle(err_type);
        set_is_relogging_happen(2);
        return 0;
    }
    return 1;
}
void error_mode_wifi_connect_action(void)
{
    clear_calculator();
    mode_now = SETTING_MODE;
    setting_entry_action_handler();
}

void error_mode_wifi_later_action(void)
{
    clear_calculator();
    mode_now = NORMAL_MODE;
    print_normal_mode_ac_scrn(true);
    display_trans_count(true, total_pending_trans);
}

void error_mode_wifi_keypad_event_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        if (key == 'R')
        {
            Selected_Error_Wifi_State = ERROR_WIFI_CONNECT;
        }
        else if (key == 'L')
        {
            Selected_Error_Wifi_State = ERROR_WIFI_LATER;
        }
        display_error_wifi(true, Selected_Error_Wifi_State);
        break;
    }
    case SIG_KEY:
    {
        if (key == '=')
        {
            static error_action_func_ptr_t error_action_handlers[] = {
                error_mode_wifi_connect_action,
                error_mode_wifi_later_action};
            (*error_action_handlers[(int)Selected_Error_Wifi_State])();
        }
        break;
    }
    }
}

void error_mode_login_now_action(void)
{
    clear_calculator();
    mode_now = SETTING_MODE;
    setting_entry_action_handler();
}

void error_mode_login_later_action(void)
{
    clear_calculator();
    mode_now = NORMAL_MODE;
    print_normal_mode_ac_scrn(true);
    display_trans_count(true, total_pending_trans);
}

void error_mode_login_keypad_event_handler(uint8_t key, uint8_t key_type)
{
    switch (key_type)
    {
    case ARW_KEY:
    {
        if (key == 'R')
        {
            Selected_Error_Login_State = ERROR_LOGIN_NOW;
        }
        else if (key == 'L')
        {
            Selected_Error_Login_State = ERROR_LOGIN_LATER;
        }
        display_error_login(true, Selected_Error_Login_State);
        break;
    }
    case SIG_KEY:
    {
        if (key == '=')
        {
            static error_action_func_ptr_t error_action_handlers[] = {
                error_mode_login_now_action,
                error_mode_login_later_action};
            (*error_action_handlers[(int)Selected_Error_Login_State])();
        }
        break;
    }
    }
}

void error_mode_keypad_event_in(uint8_t key, uint8_t key_type)
{
    static error_func_ptr_t error_handlers[] = {
        error_mode_wifi_keypad_event_handler,
        error_mode_login_keypad_event_handler};
    (*error_handlers[(int)Selected_Error_State])(key, key_type);
}