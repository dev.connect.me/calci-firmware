#include "Display.h"
#include "Keypad.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "File_Operation.h"
#include "Back_Light.h"
#include "esp_system.h"
#include "Battery_Monitor.h"
#include "Sys_Time.h"
#include "Factory_Reset.h"
#include "Update_Notify.h"
#include "Calci_Setup.h"
#include "Log_Handle.h"

#define PWR_OFF_TIME 1
static const char *MAIN_TAG = "MAIN";

uint8_t pwr_btn_status = 1;

extern "C" void app_main(void)
{
  init_tohands_light();
  display_init();
  back_light(true, true);
  print_hello_scrn(true);
  vTaskDelay(500 / portTICK_RATE_MS);
  int temp_bat = start_battery_monitoring();
  ESP_LOGI(MAIN_TAG, "Battery status: %d", temp_bat);
  if (temp_bat == 1)
  {
    display_battery_low(true);
    vTaskDelay(2000 / portTICK_RATE_MS);
  }
  else if (temp_bat == 2)
  {
    display_battery_low(true);
    vTaskDelay(2000 / portTICK_RATE_MS);
  }

  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
  {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  File_Operation main_file;
  main_file.m_initSpiff();

  wifi_init();
  start_syncing();
  init_factory_reset();
  init_calci();
  init_keypad();
  clear_keypad();

  if (main_file.m_exists(ACCS_TOKEN) && main_file.m_exists(AUTH_TOKEN) && main_file.m_exists(CASHBOOK_ID) && main_file.m_exists(USERNAME))
  {
    mode_now = NORMAL_MODE;
    print_normal_mode_scrn(true);
    display_trans_count(true, total_pending_trans);
  }
  else
  {
    print_start_setup_scrn(true, 1);
    vTaskDelay(1000 / portTICK_RATE_MS);
    setup_entry_action();
    mode_now = SETUP_MODE;
  }

  start_sys_time();
  start_update_notifier();
  init_log_handle();
  init_on_off_button();

  while (1)
  {
    if (get_on_off_voltage() < 2500)
    {
      ESP_LOGI(MAIN_TAG, "pwr_btn_status = %d", pwr_btn_status);
      if (pwr_btn_status == 1)
      {
        ESP_LOGI(MAIN_TAG, "Power button OFF pressed");
        while (uxQueueMessagesWaiting(display_queue))
        {
          vTaskDelay(100 / portTICK_RATE_MS);
        }
        dispaly_shutting_down(true);
        if (Sync_Task_Handle != NULL)
        {
          xTaskNotifyGive(Sync_Task_Handle);
          wait_untill_task_delete(&Sync_Task_Handle);
        }
        if (Log_Sender_Task_Handle != NULL)
        {
          xTaskNotifyGive(Log_Sender_Task_Handle);
          wait_untill_task_delete(&Log_Sender_Task_Handle);
        }
        if (Upd_Ntfy_Task_Handle != NULL)
        {
          xTaskNotifyGive(Upd_Ntfy_Task_Handle);
        }
        if (WiFi_Task_Handle != NULL)
          xTaskNotifyGive(WiFi_Task_Handle);
        if (Battery_Task_Handle != NULL)
          xTaskNotifyGive(Battery_Task_Handle);
        if (OTA_Task_Handle != NULL)
          vTaskDelete(OTA_Task_Handle);
        if (Factory_Reset_Task_Handle != NULL)
          xTaskNotifyGive(Factory_Reset_Task_Handle);
        if (mode_now != SETUP_MODE)
        {
          wait_untill_task_delete(&WiFi_Task_Handle);
          wait_untill_task_delete(&Battery_Task_Handle);
          wait_untill_task_delete(&Factory_Reset_Task_Handle);
        }

        tohands_light(false);
        if (Display_Task_Handle != NULL)
          xTaskNotifyGive(Display_Task_Handle);
        back_light(true, false);
        wait_untill_task_delete(&Display_Task_Handle);
        pwr_btn_status = 0;
        ESP_LOGI(KEYPAD_TAG, "Powered OFF");
      }
      else
      {
        ESP_LOGI(MAIN_TAG, "Power button ON pressed");
        pwr_btn_status = 1;
        esp_restart();
      }
    }
    vTaskDelay(100 / portTICK_RATE_MS);
  }
}
