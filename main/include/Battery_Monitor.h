#ifndef BATTERY_MONITOR_H_
#define BATTERY_MONITOR_H_

#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "Display.h"

static void battery_monitoring_task_example(void *arg);
int start_battery_monitoring(void);

#endif