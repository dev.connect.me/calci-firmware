#ifndef LOG_HANDLE_H_
#define LOG_HANDLE_H_

void init_log_handle(void);
int send_log_to_server(const char *body_data);
char *prepare_send_log_body(void);
void send_log(const char *e_log, const char *e_log_type);

#endif