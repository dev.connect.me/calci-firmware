#ifndef STEP_CORRECT_H_
#define STEP_CORRECT_H_

#include "stdio.h"
#include <math.h>
#include "Display.h"
#include "Step_Check.h"
#include "Calculator.h"

void clear_correct_var();
void parse_and_add_to_corrected_operand(char key);
void change_correct_operator_sign(char key);
void simplify_operations_with_correction();
void correct_add_sign_input(char key);
void correct_equal_input(char key);
void correct_perc_input(char key);
void correct_gst_input(int8_t gst_type, uint8_t cost_type, char key);
void correct_in_mem(char key);

#endif