#define KEY_00 57
#define KEY_0 56
#define KEY_1 46
#define KEY_2 47
#define KEY_3 48
#define KEY_4 35
#define KEY_5 36
#define KEY_6 37
#define KEY_7 24
#define KEY_8 25
#define KEY_9 26
#define Key_PT 58

#define KEY_ADD 60
#define KEY_SUB 49
#define KEY_MUL 38
#define KEY_DIV 27
#define KEY_MOD 39
#define KEY_EQU 59

#define KEY_MRC 28
#define KEY_M_PLUS 16
#define KEY_M_MINUS 17

#define KEY_CASH_IN 50
#define KEY_CASH_OUT 61

#define KEY_LEFT 5
#define KEY_RIGHT 6

#define KEY_CRT 2
#define KEY_NXT 3
#define KEY_SHIFT 4
#define KEY_GST_PLUS 12
#define KEY_GST_MINUS 13
#define KEY_GT 14
#define KEY_MU 15
#define KEY_MENU 23
#define KEY_DEL 34
#define KEY_AC 45

#define NUM_KEY 1
#define SIG_KEY 2
#define MEM_KEY 3
#define ARW_KEY 4
#define GST_KEY 5
#define SPC_KEY 6
#define CHK_CRT_KEY 7
#define GT_KEY 8
#define ERS_KEY 9
#define CASH_KEY 10
