#ifndef OTA_H_
#define OTA_H_

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "errno.h"
#include "HTTP_Client.h"
#include "WiFi_Manager.h"
#include "File_Operation.h"
#include "Calci_Config.h"

#define BUFFSIZE 1024

static const char *OTA = "OTA";
static char ota_write_data[BUFFSIZE + 1] = {0};

static void http_cleanup(esp_http_client_handle_t client)
{
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
}

static void __attribute__((noreturn)) task_fatal_error(void)
{
  ESP_LOGE(OTA, "Exiting task due to fatal error...");
  (void)vTaskDelete(NULL);

  while (1)
  {
    ;
  }
}

static void ota_task(void *pvParameter)
{
  if (get_unsynced_transactions() > 0)
  {
    Trans_Data_Sqlite_Struc syncing_data = {0};
    syncing_data.trans_type = 'I';
    xQueueSend(periodic_sync_data, (void *)&syncing_data, 0);
    display_wait_syncing(true);
    while (get_unsynced_transactions() > 0)
    {
      vTaskDelay(10000 / portTICK_RATE_MS);
    }
  }
  xTaskNotifyGive(Sync_Task_Handle);
  xTaskNotifyGive(Log_Sender_Task_Handle);
  wait_untill_task_delete(&Sync_Task_Handle);
  wait_untill_task_delete(&Log_Sender_Task_Handle);
  print_downloading(true, 0);
  esp_err_t err;
  /* update handle : set by esp_ota_begin(), must be freed via esp_ota_end() */
  esp_ota_handle_t update_handle = 0;
  const esp_partition_t *update_partition = NULL;

  ESP_LOGI(OTA, "Starting OTA example");

  const esp_partition_t *configured = esp_ota_get_boot_partition();
  const esp_partition_t *running = esp_ota_get_running_partition();

  if (configured != running)
  {
    ESP_LOGW(OTA, "Configured OTA boot partition at offset 0x%08x, but running from "
                  "offset 0x%08x",
             configured->address, running->address);
    ESP_LOGW(OTA, "(This can happen if either the OTA boot data or preferred "
                  "boot image become corrupted somehow.)");
  }

  ESP_LOGI(OTA, "Running partition type %d subtype %d (offset 0x%08x)", running->type, running->subtype, running->address);
  if (url.length() == 0)
  {
    send_ota_failed();
    vTaskDelete(NULL);
  }
  std::string ota_url = url + path + "v2/downloadLatestFirmware?hardwareVersion=" + get_hardware_version() + "&serialNo=" + get_serial_no();
  ESP_LOGI(OTA, "Download link: %s", ota_url.c_str());

  esp_http_client_config_t config = {
      .url = ota_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
      .cert_pem = server_cert_pem_start,
#endif
      .timeout_ms = 5000,
  };

  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (client == NULL)
  {
    ESP_LOGE(OTA, "Failed to initialise HTTP connection");
    send_ota_failed();
    task_fatal_error();
  }
  File_Operation auth_file;
  esp_http_client_set_method(client, HTTP_METHOD_GET);
  esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
  esp_http_client_set_header(client, "Authorization",
                             auth_file.m_read(AUTH_TOKEN).c_str());

  err = esp_http_client_open(client, 0);
  if (err != ESP_OK)
  {
    ESP_LOGE(OTA, "Failed to open HTTP connection: %s", esp_err_to_name(err));
    esp_http_client_cleanup(client);
    send_ota_failed();
    task_fatal_error();
  }
  esp_http_client_fetch_headers(client);

  update_partition = esp_ota_get_next_update_partition(NULL);
  ESP_LOGI(OTA, "Writing to partition subtype %d at offset 0x%x",
           update_partition->subtype, update_partition->address);
  assert(update_partition != NULL);

  int binary_file_length = 0;
  /*deal with all receive packet*/
  bool image_header_was_checked = false;
  while (1)
  {
    int data_read = esp_http_client_read(client, ota_write_data, BUFFSIZE);
    if (data_read < 0)
    {
      ESP_LOGE(OTA, "Error: SSL data read error");
      http_cleanup(client);
      send_ota_failed();
      task_fatal_error();
    }
    else if (data_read > 0)
    {
      if (image_header_was_checked == false)
      {
        esp_app_desc_t new_app_info;
        if (data_read > sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t) + sizeof(esp_app_desc_t))
        {
          // check current version with downloading
          memcpy(&new_app_info, &ota_write_data[sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t)], sizeof(esp_app_desc_t));
          ESP_LOGI(OTA, "New firmware version: %s", new_app_info.version);

          esp_app_desc_t running_app_info;
          if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK)
            ESP_LOGI(OTA, "Running firmware version: %s", running_app_info.version);

          image_header_was_checked = true;

          err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
          if (err != ESP_OK)
          {
            ESP_LOGE(OTA, "esp_ota_begin failed (%s)", esp_err_to_name(err));
            http_cleanup(client);
            send_ota_failed();
            task_fatal_error();
          }
          ESP_LOGI(OTA, "esp_ota_begin succeeded");
        }
        else
        {
          ESP_LOGE(OTA, "received package is not fit len");
          http_cleanup(client);
          send_ota_failed();
          task_fatal_error();
        }
      }
      err =
          esp_ota_write(update_handle, (const void *)ota_write_data, data_read);
      if (err != ESP_OK)
      {
        http_cleanup(client);
        send_ota_failed();
        task_fatal_error();
      }
      binary_file_length += data_read;
      vTaskDelay(1 / portTICK_PERIOD_MS);
      print_downloading(true, binary_file_length / 1024);
      ESP_LOGI(OTA, "Written image length %d", binary_file_length);
    }
    else if (data_read == 0)
    {
      /*
       * As esp_http_client_read never returns negative error code, we rely on
       * `errno` to check for underlying transport connectivity closure if any
       */
      if (errno == ECONNRESET || errno == ENOTCONN)
      {
        ESP_LOGE(OTA, "Connection closed, errno = %d", errno);
        break;
      }
      if (esp_http_client_is_complete_data_received(client) == true)
      {
        ESP_LOGI(OTA, "Connection closed");
        break;
      }
    }
    vTaskDelay(1 / portTICK_PERIOD_MS);
  }
  ESP_LOGI(OTA, "Total Write binary data length: %d", binary_file_length);
  if (esp_http_client_is_complete_data_received(client) != true)
  {
    ESP_LOGE(OTA, "Error in receiving complete file");
    // String recvComplete = "";
    // recvComplete = otaJson.addInt(recvComplete.c_str(), "recvComplete", 1);
    // p_rsp_v = recvComplete;
    // bleNotify("1");
    http_cleanup(client);
    send_ota_failed();
    task_fatal_error();
  }

  err = esp_ota_end(update_handle);
  if (err != ESP_OK)
  {
    if (err == ESP_ERR_OTA_VALIDATE_FAILED)
    {
      ESP_LOGE(OTA, "Image validation failed, image is corrupted");
    }
    ESP_LOGE(OTA, "esp_ota_end failed (%s)!", esp_err_to_name(err));
    http_cleanup(client);
    send_ota_failed();
    task_fatal_error();
  }
  err = esp_ota_set_boot_partition(update_partition);
  if (err != ESP_OK)
  {
    ESP_LOGE(OTA, "esp_ota_set_boot_partition failed (%s)!",
             esp_err_to_name(err));
    http_cleanup(client);
    send_ota_failed();
    task_fatal_error();
  }
  ESP_LOGI(OTA, "Prepare to restart system!");
  print_update_done(true);
  vTaskDelay(1000 / portTICK_RATE_MS);
  esp_restart();
  return;
}

#endif
