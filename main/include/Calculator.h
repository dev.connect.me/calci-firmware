#ifndef CALCULATOR_H_
#define CALCULATOR_H_

#include "stdio.h"
#include "Display.h"
#include "Sync_Handel.h"
#include <math.h>
#include <string>
#include <iostream>
#include <iterator>
#include <list>
#include "Step_Storage.h"

struct Grand_Total{
    double gross;
    double net;
    double tare;
    double total;
};

extern double gst_rate[5];
extern uint8_t err_flag;
extern uint8_t screen_no;
extern uint8_t once_cash;

extern double accum;
extern double s_reg;
extern double pnt1;
extern double mem;
extern Grand_Total grand_total;
extern uint8_t a_flag, e_flag, p_flag, m_flag, mrc_flag, gst_flag, gt_flag, c_flag, mu_flag, s_flag;
extern uint8_t once_equal, once_tax;
extern uint8_t gt_pointor;

extern uint8_t invalid_cash_value;
extern esp_timer_handle_t invalid_cash_value_timer;
extern const esp_timer_create_args_t invalid_cash_value_timer_args;

enum TRANS_TYPE
{
    TRANS_TYPE_NONE = 0,
    TRANS_TYPE_CREDIT = '+',
    TRANS_TYPE_DEBIT = '-'
};

extern TRANS_TYPE selected_trans_type;

enum PAYMENT_MODE
{
    PAYMENT_MODE_INVALID = 0,
    PAYMENT_MODE_CASH,
    PAYMENT_MODE_ONLINE,
    PAYMENT_MODE_LATER,
    PAYMENT_MODE_MAX
};

extern PAYMENT_MODE payment_mode_status;

static const char *CALC_TAG = "CALCULATOR";

int save_transaction(Trans_Data_Sqlite_Struc *data_str, uint8_t sync_sts);
int save_transaction(double cash_amount, TRANS_TYPE trans_type, PAYMENT_MODE payment_mode, const char *cust_name, const char *cust_num, const char *cust_remark);

void add_to_operand(char key);
void get_num_input(char key);
void get_equal_input(char key);
void get_perc_input(char key);
void get_sign_input(char key);
void get_input(char key);
void get_input(int8_t gst_type, uint8_t cost_type, char key);
void clear_calculator();
void clear_screen();
void correct_operand();
void add_in_mem();
void sub_in_mem();
void mem_recal();
void cash_entry(char key);
void change_mode_to_step_check(char key);
void calculate_gt(char key);
void mark_up(char key);

#endif