#ifndef UPDATE_NOTIFY_H_
#define UPDATE_NOTIFY_H_

#include "WiFi_Manager.h"
#include "HTTP_Client.h"
#include "Calci_Config.h"

extern std::string update_version_name;
extern std::string update_version_code;

static const char* UPD_NTFY = "UPDATE NTFY";
#define UPDATE_STATUS "/spiffs/updatestatus"

void start_update_notifier();

#endif