#ifndef STEP_STORAGE_H_
#define STEP_STORAGE_H_

#include <iostream>
#include <iterator>
#include <list>

struct Tax_Op
{
    int tax_type;
    uint8_t val_type;
    uint8_t tax_perc;
};

struct Operand
{
    double val;
    uint8_t arith_op;
    struct Tax_Op tax_op;
    uint8_t mem_op;
    uint8_t cash_op;
    uint8_t result_flag;
    uint8_t gt_op;
    uint8_t mu_op;
};

extern std::list<Operand> operation;

void push_arith_operation(double reg, uint8_t op_sign);

void push_tax_operation(double reg, int tax_type, uint8_t val_type, uint8_t tax_perc);

void push_mem_operation(double reg, int op_sign);

void push_cash_operation(double reg, uint8_t op_sign);

void push_mu_operation(double reg, uint8_t op_sign);

void push_arith_result_operation(double reg, uint8_t op_sign);

void push_mem_result_operation(double reg, uint8_t op_sign);

void push_cash_result_operation(double reg, uint8_t op_sign);

void push_gt_operation(double reg, uint8_t op_sign);

void push_tax_result_operation(double reg, int tax_type, uint8_t val_type, uint8_t tax_perc);

void show_operations();

void change_last_operation(double reg, uint8_t op_sign);

void change_last_tax_operation(int tax_type, uint8_t val_type, uint8_t tax_perc);

#endif