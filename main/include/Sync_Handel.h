#ifndef SYNC_HANDEL_H_
#define SYNC_HANDEL_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "esp_flash.h"
#include "esp_flash_spi_init.h"
#include "esp_partition.h"
#include "esp_vfs.h"
#include "esp_vfs_fat.h"
#include "esp_system.h"
#include "HTTP_Client.h"
#include "WiFi_Manager.h"

#define CREDIT "/fat/s_c"
#define DEBIT "/fat/s_d"

#define BACKUP_CREDIT "/fat/b_c"
#define BACKUP_DEBIT "/fat/b_d"

#define CREATE_TABLE "CREATE TABLE IF NOT EXISTS transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, amount DOUBLE, transactionType TEXT, paymentMode TEXT, transactionTimeStamp INTEGER, isSynced BOOLEAN, customerName TEXT, customerMobile TEXT, customerRemarks TEXT, cashbookUserId INTEGER)"
#define TABLE_NAME "transactions"

static const char *SYNC = "SYNC HANDEL";

extern esp_flash_t *ext_flash;

// Handle of the wear levelling library instance
static wl_handle_t s_wl_handle = WL_INVALID_HANDLE;
static void sync_handel_task(void *arg);

// Mount path for the partition
extern const char *storage_base_path;
extern const char *backup_base_path;

static esp_flash_t *init_ext_flash(void);
static const esp_partition_t *add_partition(esp_flash_t *ext_flash, const char *partition_label);
static void list_data_partitions(void);
static bool mount_fatfs(const char *partition_label);
static void get_fatfs_usage(size_t *out_total_bytes, size_t *out_free_bytes);
void move_file(const char *src, const char *trg);
void start_syncing();

#endif
