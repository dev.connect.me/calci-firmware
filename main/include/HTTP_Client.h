#ifndef HTTP_CLIENT_H_
#define HTTP_CLIENT_H_

#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_system.h"
#include "esp_tls.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <stdlib.h>
#include <string.h>
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "sdkconfig.h"
#include "File_Operation.h"
#include "Json_Operation.h"
#include "Constant.h"
#include "Calci_Config.h"
#include "DB_Manager.h"

#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 2048
static const char *CLIENT = "HTTP_CLIENT";

extern const char* BASE_URL;

extern std::string url;
extern std::string path;

extern const char server_cert_pem_start[] asm("_binary_bybuy_prod_pem_start");
extern const char server_cert_pem_end[]   asm("_binary_bybuy_prod_pem_end");

esp_err_t _http_event_handle(esp_http_client_event_t *evt);

void get_base_url();

bool send_otp(const char *num);

bool veryfy_otp(const char *num1, const char *num2);

int send_transaction(struct Trans_Data_Sqlite_Struc &trans_data, std::string &req_body);

int send_transactions(const char *trans_list);

int refresh_token();

int check_for_update(std::string &avail, std::string &rsp_ver);

void send_firmware_version(void);

#endif