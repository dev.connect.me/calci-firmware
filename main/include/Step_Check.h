#ifndef STEP_CHECK_H_
#define STEP_CHECK_H_

#include <iostream>
#include <iterator>
#include <list>
#include <string>
#include "Step_Storage.h"
#include "Display.h"

extern std::list<Operand>::iterator step_it;

void show_check_step();

#endif