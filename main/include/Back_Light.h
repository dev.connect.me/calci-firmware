#ifndef BACK_LIGHT_H_
#define BACK_LIGHT_H_

#include "driver/gpio.h"
#include "driver/rtc_io.h"
#include "esp_sleep.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"

void init_tohands_light();
void tohands_light(bool on_off);
void init_on_off_button(void);
float get_on_off_voltage(void);

#endif
