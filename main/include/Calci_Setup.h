#ifndef CALCI_SETUP_H_
#define CALCI_SETUP_H_

#include <stdint.h>

enum Setup_State
{
    SETUP_INSTRUCTION1_STATE = 0,
    SETUP_INSTRUCTION2_STATE,
    SETUP_INSTRUCTION3_STATE,
    SETUP_INSTRUCTION4_STATE,
    SETUP_WIFI_STATE,
    SETUP_LOG_STATE,
    SETUP_APP_STATE,
    SETUP_MAX_STATE
};

enum Setup_WiFi_State
{
    SETUP_WIFI_SELECTION_STATE = 0,
    SETUP_WIFI_PASSWORD_STATE,
    SETUP_WIFI_MAX_STATE
};

enum Setup_Log_State
{
    SETUP_LOG_MOBILE_STATE = 0,
    SETUP_LOG_OTP_STATE,
    SETUP_LOG_MAX_STATE
};

typedef void (*setup_func_ptr_t)(uint8_t key, uint8_t key_type);

void setup_entry_action(void);
void setup_event_in(uint8_t key, uint8_t key_type);

#endif