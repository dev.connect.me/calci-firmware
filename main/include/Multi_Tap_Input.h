#ifndef MULTI_TAP_INPUT_H_
#define MULTI_TAP_INPUT_H_

#include <string>
#include <string.h>
#include "esp_timer.h"
#include <iostream>
#include <vector>
#include <map>
#include "Constant.h"
#include "esp_log.h"

#define MULTI_TAP_TIMEOUT 700

enum Multi_Tap_State
{
  MULTI_TAP_OK = 0,
  MULTI_TAP_ERROR
};

class Multi_Tap
{
public:
  Multi_Tap(std::string str, uint8_t str_limit);

  void add_tap(uint8_t key, uint8_t key_type);
  uint8_t get_key_first_tap(uint8_t key, uint8_t key_type);
  uint8_t get_key_same_tap(uint8_t key, uint8_t key_type);
  uint8_t get_key(uint8_t key, uint8_t key_type);
  void shift_keypad_mode(uint8_t key, uint8_t key_type);
  uint8_t get_keypad_mode(void);
  std::string get_buffer(void);
  void set_keypad_mode(uint8_t mode_val);
  void single_back_space(void);

private:
  Multi_Tap_State state = MULTI_TAP_OK;
  char buffer[20];
  uint8_t len;
  uint8_t last_key;
  uint8_t last_key_type;
  uint8_t keypad_mode;
  uint8_t limit;
  int64_t prev_press_time;
  std::vector<uint8_t>::iterator last_iter;
};

#endif // MULTI_TAP_INPUT_H_
