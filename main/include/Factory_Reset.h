#ifndef FACTORY_RESET_H_
#define FACTORY_RESET_H_

#include <stdio.h>
#include "sdkconfig.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_partition.h"
#include "esp_ota_ops.h"
#include "Sync_Handel.h"
#include "File_Operation.h"
#include "Calci_Config.h"

#define FACTORY_RESET (gpio_num_t)39
#define FACTORY_RESET_SECONDS 3

static const char *F_TAG = "FCT_RST";
extern bool fct_rst_running;

void init_factory_reset(void);

#endif