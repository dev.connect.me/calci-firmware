#ifndef FILEOPERATION_H_
#define FILEOPERATION_H_

#include "esp_spiffs.h"
#include <dirent.h>
#include <iostream>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

class File_Operation {
public:
  File_Operation();
  virtual ~File_Operation();
  void m_initSpiff();
  void m_list();
  bool m_write(const char *fileName, const char *data);
  std::string m_read(const char *fileName);
  void m_remove(const char *fileName);
  bool m_exists(const char *fileName);
  int m_size(const char *fileName);
};

#endif /* MAIN_FILEOPERATION_H_ */
