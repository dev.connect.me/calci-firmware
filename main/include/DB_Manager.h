#ifndef DB_MANAGER_H_
#define DB_MANAGER_H_

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <string>
#include <vector>
#include "esp_log.h"
#include "esp_timer.h"
#include "sqlite3.h"
#include "Calci_Config.h"

static const char *DB = "DB Manager";

enum cmd_type
{
  INSTANT = 1,
  PERIODIC,
  GET_ID,
  GET_UNSYNC_TRANS_COUNT,
  GET_SYNC_TRANS_COUNT,
  GET_UNSYNC_LOGS_COUNT,
  GET_SYNC_LOGS_COUNT,
  OTHER,
  LOG_PERIODIC,
};

extern std::vector<int64_t> id_list;
extern std::vector<int64_t> log_id_list;
extern sqlite3 *db;

static int callback(void *data, int argc, char **argv, char **azColName);
int openDb(const char *filename, sqlite3 **db);
int db_exec(sqlite3 *db, cmd_type cmd, const char *sql);
int get_unsynced_transactions(void);
int get_synced_transactions(void);
int get_unsynced_logs(void);
int get_synced_logs(void);

#endif
