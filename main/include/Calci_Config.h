#ifndef CALCI_CONFIG_H_
#define CALCI_CONFIG_H_

#include <stdio.h>
#include <string>
#include <iostream>
#include <iterator>
#include <list>
#include "esp_timer.h"
#include "Constant.h"
#include "WiFi_Manager.h"
#include "esp_efuse.h"
#include "esp_efuse_table.h"
#include "esp_efuse_custom_table.h"
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "File_Operation.h"

#define USERNAME "/spiffs/cred/username"
#define HELP_INFO "hello@tohands.in"
#define GST_RATE "/spiffs/gst_rate"
#define DATA_BASE "/fat/data.db"
#define OLD_DB_CHEKC "/spiffs/old_db_check"

#define CONSUMER_ID "cashbookconsumerid"
#define AUTH_TOKEN "/spiffs/token/auth"
#define ACCS_TOKEN "/spiffs/token/accs"
#define CASHBOOK_ID "/spiffs/cashbookid"

//*************************Task Handler************************************
extern TaskHandle_t Battery_Task_Handle;
extern TaskHandle_t OTA_Task_Handle;
extern TaskHandle_t Display_Task_Handle;
extern TaskHandle_t Factory_Reset_Task_Handle;
extern TaskHandle_t Keypad_Task_Handle;
extern TaskHandle_t Sync_Task_Handle;
extern TaskHandle_t WiFi_Task_Handle;
extern TaskHandle_t Upd_Ntfy_Task_Handle;
extern TaskHandle_t Log_Sender_Task_Handle;
//*************************Task Handler************************************

//*************************Calci Mode**************************************
// calci_mode simple enum for provide human understandable mode information
enum calci_mode
{
  NORMAL_MODE = 0,
  SETUP_MODE = 1,
  SETTING_MODE,
  UPDATE_MODE,
  STEP_CHECK_MODE,
  STEP_CORRECT_MODE,
  ADD_CUST_DETAILS_MODE,
  UPDATE_NTFY_MODE,
  ERROR_MODE
};

// mode_now variable that save's current calci mode
extern calci_mode mode_now;
//******************************Setting parameters***************************
struct menu_pointer
{
  int menu;
  int sub_menu;
};

extern uint16_t total_pending_trans;

extern struct menu_pointer menu_pin;

extern std::string input_ssid;

static void psw_in_callback(void *arg);

extern const esp_timer_create_args_t psw_in_timer_args;
extern esp_timer_handle_t psw_in_timer;

struct psw_in_struct
{
  uint8_t shift_mode;
  uint8_t last_key;
  uint8_t last_key_type;
  char input_psw[25];
  uint8_t input_psw_ele;
  uint8_t same_key_press_no;
};

extern struct psw_in_struct psw_in;

extern char mobile_in[11];
extern uint8_t mobile_in_ele;
extern char otp_in[5];
extern uint8_t otp_in_ele;

extern std::string gst_in[5];

extern xSemaphoreHandle sqlite_mutex;

//******************************Setting parameters***************************
extern xQueueHandle keypad_queue;
// extern xQueueHandle instant_sync_queue;
// extern xQueueHandle instant_sync_data;
extern xQueueHandle periodic_sync_data;
extern xQueueHandle log_periodic_sync_data;

extern const esp_timer_create_args_t timeout_timer_args;
extern esp_timer_handle_t timeout_timer;

typedef struct
{
  uint8_t country_code;  /*!< Module version: length 8 bits */
  uint16_t hardware_ver; /*!< Device role: length 3 bits */
  uint8_t batch_no;      /*!< Setting 1: length 6 bits */
  bool wi_fi_bit;        /*!< Setting 2: length 5 bits */
  bool bl_bit;           /*!< Setting 2: length 5 bits */
  bool gsm_bit;          /*!< Setting 2: length 5 bits */
  uint8_t batch_year;    /*!< Custom secure version: length 16 bits */
  uint8_t batch_month;
  uint8_t data_set;
  uint32_t device_id; /*!< Reserv */
} device_desc_t;

struct Trans_Data_0
{
  char trans_type;
  double trans_amount;
};

struct Trans_Data_1
{
  char trans_type;
  double trans_amount;
  char trans_cust_name[16];
  char trans_cust_num[11];
  char trans_cust_remark[16];
};

struct Trans_Data_Sqlite_Struc
{
  char trans_type;
  char payment_mode;
  double trans_amount;
  int64_t trans_time;
  char trans_cust_name[16];
  char trans_cust_num[11];
  char trans_cust_remark[16];
  int64_t cashbook_user_id;
};

struct Log_Sync_Struct
{
  int64_t log_user_id;
  std::string log_str;
  std::string log_serial_no;
  std::string log_timestamp;
  std::string log_device_type;
  std::string log_type;
  std::string log_firmware_version;
};

extern std::list<Log_Sync_Struct> log_sync_data;

struct Instant_Sync_Queue
{
  time_t file_time;
  char trans_type;
};

void connect_wifi();
uint8_t pars_psw_key(uint8_t key, uint8_t key_type);
static void psw_in_callback(void *arg);
void clear_keypad();
void send_ota_failed();
void log_out_user(void);
uint8_t is_user_log_in(void);
std::string get_firmware_version();
std::string get_hardware_version();
std::string get_device_id();
std::string get_model_no();
std::string get_serial_no(void);
std::string get_batch_no(void);

void set_is_relogging_happen(int relogging_sts);
uint8_t is_just_logout(void);
void wait_untill_task_delete(TaskHandle_t* x_check_task_handle);

#endif
