#ifndef KEYPAD_H_
#define KEYPAD_H_

#include "ADP5589.h"
#include "Calci.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_sleep.h"
#include "Calci_Config.h"
#include "File_Operation.h"
#include "Back_Light.h"

#define AUTO_OFF_TIME 10

static const char *KEYPAD_TAG = "KEYPAD";
ADP5589 Keypad;
calci_mode last_mode = NORMAL_MODE;

bool get_key_and_type(uint8_t input_event, uint8_t *key, uint8_t *key_type, uint8_t *event_type)
{
  *event_type = (input_event >> 7) & 0x01;
  input_event &= ~(0x01 << 7);

  switch (input_event)
  {
  case KEY_00:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = 'D';
    return true;
  }
  case KEY_0:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '0';
    return true;
  }
  case KEY_1:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '1';
    return true;
  }
  case KEY_2:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '2';
    return true;
  }
  case KEY_3:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '3';
    return true;
  }
  case KEY_4:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '4';
    return true;
  }
  case KEY_5:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '5';
    return true;
  }
  case KEY_6:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '6';
    return true;
  }
  case KEY_7:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '7';
    return true;
  }
  case KEY_8:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '8';
    return true;
  }
  case KEY_9:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '9';
    return true;
  }
  case Key_PT:
  {
    *key_type = (uint8_t)NUM_KEY;
    *key = '.';
    return true;
  }
  case KEY_ADD:
  {
    *key_type = (uint8_t)SIG_KEY;
    *key = '+';
    return true;
  }
  case KEY_SUB:
  {
    *key_type = (uint8_t)SIG_KEY;
    *key = '-';
    return true;
  }
  case KEY_MUL:
  {
    *key_type = (uint8_t)SIG_KEY;
    *key = '*';
    return true;
  }
  case KEY_DIV:
  {
    *key_type = (uint8_t)SIG_KEY;
    *key = '/';
    return true;
  }
  case KEY_MOD:
  {
    *key_type = (uint8_t)SIG_KEY;
    *key = '%';
    return true;
  }
  case KEY_EQU:
  {
    *key_type = (uint8_t)SIG_KEY;
    *key = '=';
    return true;
  }
  case KEY_MRC:
  {
    *key_type = (uint8_t)MEM_KEY;
    *key = '=';
    return true;
  }
  case KEY_M_PLUS:
  {
    *key_type = (uint8_t)MEM_KEY;
    *key = '+';
    return true;
  }
  case KEY_M_MINUS:
  {
    *key_type = (uint8_t)MEM_KEY;
    *key = '-';
    return true;
  }
  case KEY_CASH_IN:
  {
    *key_type = (uint8_t)CASH_KEY;
    *key = '+';
    return true;
  }
  case KEY_CASH_OUT:
  {
    *key_type = (uint8_t)CASH_KEY;
    *key = '-';
    return true;
  }
  case KEY_LEFT:
  {
    *key_type = (uint8_t)ARW_KEY;
    *key = 'L';
    return true;
  }
  case KEY_RIGHT:
  {
    *key_type = (uint8_t)ARW_KEY;
    *key = 'R';
    return true;
  }
  case KEY_CRT:
  {
    *key_type = (uint8_t)CHK_CRT_KEY;
    *key = 'D';
    return true;
  }
  case KEY_NXT:
  {
    *key_type = (uint8_t)CHK_CRT_KEY;
    *key = 'C';
    return true;
  }
  case KEY_SHIFT:
  {
    *key_type = (uint8_t)SPC_KEY;
    *key = 'S';
    return true;
  }
  case KEY_GST_PLUS:
  {
    *key_type = (uint8_t)GST_KEY;
    *key = '+';
    return true;
  }
  case KEY_GST_MINUS:
  {
    *key_type = (uint8_t)GST_KEY;
    *key = '-';
    return true;
  }
  case KEY_GT:
  {
    *key_type = (uint8_t)GT_KEY;
    *key = '=';
    return true;
  }
  case KEY_MU:
  {
    *key_type = (uint8_t)SPC_KEY;
    *key = 'M';
    return true;
  }
  case KEY_MENU:
  {
    *key_type = (uint8_t)SPC_KEY;
    *key = KEY_MENU;
    return true;
  }
  case KEY_DEL:
  {
    *key_type = (uint8_t)ERS_KEY;
    *key = 'D';
    return true;
  }
  case KEY_AC:
  {
    *key_type = (uint8_t)ERS_KEY;
    *key = 'C';
    return true;
  }
  default:
    return false;
  }
}

static void keypad_input(void *arg)
{
  uint8_t input_key;
  uint8_t input_key_type;
  uint8_t input_event_type;
  uint8_t clear_queue;
  while (1)
  {
    uint8_t keypad_event = Keypad.readRegister(ADP5589_ADR_STATUS);
    if (xQueueReceive(keypad_queue, &(clear_queue), (TickType_t)1))
    {
      ESP_LOGI(KEYPAD_TAG, "Queue received: %d", clear_queue);
      while (keypad_event)
      {
        Keypad.readRegister(ADP5589_ADR_FIFO1);
        keypad_event = Keypad.readRegister(ADP5589_ADR_STATUS);
        vTaskDelay(1 / portTICK_RATE_MS);
      }
      if (clear_queue == 1)
      {
        ESP_LOGI(KEYPAD_TAG, "Queue received dummy clear data");
      }
      else if (clear_queue == 2)
      {
        if (mode_now == NORMAL_MODE || mode_now == SETUP_MODE)
        {
          extern uint8_t pwr_btn_status;
          if (pwr_btn_status == 1)
          {
            ESP_LOGI(KEYPAD_TAG, "Auto Powering OFF");
            while (uxQueueMessagesWaiting(display_queue))
            {
              vTaskDelay(100 / portTICK_RATE_MS);
            }
            dispaly_shutting_down(true);
            if (Sync_Task_Handle != NULL)
            {
              xTaskNotifyGive(Sync_Task_Handle);
              wait_untill_task_delete(&Sync_Task_Handle);
            }
            if (Log_Sender_Task_Handle != NULL)
            {
              xTaskNotifyGive(Log_Sender_Task_Handle);
              wait_untill_task_delete(&Log_Sender_Task_Handle);
            }
            if (Upd_Ntfy_Task_Handle != NULL)
            {
              xTaskNotifyGive(Upd_Ntfy_Task_Handle);
            }
            if (WiFi_Task_Handle != NULL)
              xTaskNotifyGive(WiFi_Task_Handle);
            if (Battery_Task_Handle != NULL)
              xTaskNotifyGive(Battery_Task_Handle);
            if (OTA_Task_Handle != NULL)
              vTaskDelete(OTA_Task_Handle);
            if (Factory_Reset_Task_Handle != NULL)
              xTaskNotifyGive(Factory_Reset_Task_Handle);
            wait_untill_task_delete(&WiFi_Task_Handle);
            wait_untill_task_delete(&Battery_Task_Handle);
            wait_untill_task_delete(&Factory_Reset_Task_Handle);

            tohands_light(false);
            if (Display_Task_Handle != NULL)
              xTaskNotifyGive(Display_Task_Handle);
            back_light(true, false);
            wait_untill_task_delete(&Display_Task_Handle);
            pwr_btn_status = 0;
            ESP_LOGI(KEYPAD_TAG, "Powered OFF");
            vTaskDelete(NULL);
          }
        }
      }
      else if (clear_queue == 3)
      {
        if (mode_now == UPDATE_MODE)
        {
          print_connection_failed_scrn(true);
          vTaskDelay(1000 / portTICK_RATE_MS);
          mode_now = NORMAL_MODE;
          menu_pin.sub_menu = 0;
          menu_pin.menu = 0;
          print_normal_mode_ac_scrn(true);
          display_trans_count(true, total_pending_trans);
        }
      }
      else if (clear_queue == 4)
      {
        invalid_cash_value = 0;
        clear_invalid_amount_err(true);
        clear_screen();
      }
    }
    if (keypad_event)
    {
      if (last_mode != mode_now)
      {
        if (mode_now == NORMAL_MODE || mode_now == SETUP_MODE)
        {
          esp_timer_stop(timeout_timer);
          ESP_ERROR_CHECK(esp_timer_start_once(timeout_timer, AUTO_OFF_TIME * 60 * 1000 * 1000));
        }
        else
        {
          esp_timer_stop(timeout_timer);
        }
      }
      else
      {
        if (mode_now == NORMAL_MODE || mode_now == SETUP_MODE)
        {
          esp_timer_stop(timeout_timer);
          ESP_ERROR_CHECK(esp_timer_start_once(timeout_timer, AUTO_OFF_TIME * 60 * 1000 * 1000));
        }
      }
      uint8_t recv_event = Keypad.readRegister(ADP5589_ADR_FIFO1);
      if (get_key_and_type(recv_event, &input_key, &input_key_type, &input_event_type))
      {
        ESP_LOGI(KEYPAD_TAG, "Recv event \tkey: %c, \tkey type: %d, \tevent type: %d", input_key, input_key_type, input_event_type);
        calci_parse(input_key, input_key_type, input_event_type);
      }
    }
    if (change_mode)
    {
      ESP_LOGI(KEYPAD_TAG, "Mode: SETTING MODE");
      setting_entry_action_handler();
      change_mode = 0;
    }
    vTaskDelay(1 / portTICK_RATE_MS);
  }
}

void init_keypad()
{
  ESP_LOGI(KEYPAD_TAG, "Initilizing");
  Keypad.begin();
  for (int i = 0; i <= 6; ++i)
  {
    Keypad.activateRow(i);
    Keypad.activateColumn(i);
  }
  keypad_queue = xQueueCreate(1, sizeof(uint8_t));
  xTaskCreate(keypad_input, "Keypad input monitoring task", 10240, NULL, 10, &Keypad_Task_Handle);
  ESP_ERROR_CHECK(esp_timer_create(&timeout_timer_args, &timeout_timer));
  esp_timer_stop(timeout_timer);
  ESP_ERROR_CHECK(esp_timer_start_once(timeout_timer, AUTO_OFF_TIME * 60 * 1000 * 1000));
}

void restart_keypad()
{
  Keypad.begin();
  for (int i = 0; i <= 6; ++i)
  {
    Keypad.activateRow(i);
    Keypad.activateColumn(i);
  }
}

#endif