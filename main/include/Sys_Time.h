#ifndef SYS_TIME_H_
#define SYS_TIME_H_

#include "driver/gpio.h"
#include "esp_attr.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_sntp.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include <ds1302.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <sys/time.h>
#include <time.h>

#define CONFIG_SNTP_TIME_SYNC_METHOD_CUSTOM
#define IO_GPIO GPIO_NUM_22
#define SCLK_GPIO GPIO_NUM_21
#define CE_GPIO GPIO_NUM_23
#define CONFIG_TIMEZONE 5.5
#define NTP_SERVER "pool.ntp.org"

void set_rtc_time(struct tm *time);
struct tm get_rtc_time();
void set_sys_time(struct tm *tm);
time_t timeStemp();
std::string timeToString(struct tm timeString);
void start_sys_time();

#endif