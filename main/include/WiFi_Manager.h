#ifndef WIFI_MANAGER_H_
#define WIFI_MANAGER_H_

#include <string>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#define DEFAULT_SCAN_LIST_SIZE 25

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

extern EventGroupHandle_t s_wifi_event_group;

void wifi_init(void);
void wifi_disconnect(void);
bool get_wifi_status(void);
std::string get_wifi_ssid(void);

#endif