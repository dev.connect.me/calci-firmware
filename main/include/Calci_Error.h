#ifndef CALCI_ERROR_H_
#define CALCI_ERROR_H_

#include "Calculator.h"

enum Error_State
{
    ERROR_WIFI = 0,
    ERROR_LOGIN,
    ERROR_MAX
};

enum Error_State_Wifi
{
    ERROR_WIFI_CONNECT = 0,
    ERROR_WIFI_LATER,
    ERROR_WIFI_MAX
};

enum Error_State_Login
{
    ERROR_LOGIN_NOW = 0,
    ERROR_LOGIN_LATER,
    ERROR_LOGIN_MAX
};

typedef void (*error_func_ptr_t)(uint8_t key, uint8_t key_type);
typedef void (*error_action_func_ptr_t)(void);

int error_mode_entry_action(Error_State err_type);
void error_mode_keypad_event_in(uint8_t key, uint8_t key_type);

#endif