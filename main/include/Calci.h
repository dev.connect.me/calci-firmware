#ifndef CALCI_H_
#define CALCI_H_

#include "Constant.h"
#include "Display.h"
#include "esp_timer.h"
#include <cctype>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "Calculator.h"
#include "Calci_Config.h"
#include "WiFi_Manager.h"
#include "Calci_Settings.h"
#include "Step_Check.h"
#include "Step_Correct.h"
#include "OTA.h"
#include "Update_Notify.h"


extern const esp_timer_create_args_t mode_timer_args;
extern esp_timer_handle_t mode_timer;


//*************************Calci Mode**************************************

//******************************Data Base************************************
extern uint8_t change_mode;
extern int8_t gst;
extern uint8_t gst_count;
extern char last_gst_rate;
//******************************Data Base************************************

static void mode_callback(void *arg);
void init_calci();
void calci_parse(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_normal_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_setup_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_setting_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_update_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_step_check_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_step_correct_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_add_cust_details_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_update_ntfy_mode(uint8_t key, uint8_t key_type, uint8_t event_type);
void exec_error_mode(uint8_t key, uint8_t key_type, uint8_t event_type);

#endif