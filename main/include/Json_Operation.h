#ifndef JSONOPERATION_H_
#define JSONOPERATION_H_

#include "cJSON.h"
#include <stdio.h>
#include <string>

class Json_Operation {
public:
  Json_Operation();
  virtual ~Json_Operation();

  bool validate_json(const char *json);

  // Get value of key which is boolean type
  bool get_bool(const char *json, const char *key);

  // Get value of key which is integer type
  int get_int(const char *json, const char *key);

  // Get value of key which is float type
  float get_float(const char *json, const char *key);

  // Get value of key which is string type
  std::string get_string(const char *json, const char *key);

  // Get count of total element in array
  int get_ary_size(const char *json, const char *ary);

  // Add key and value to json if value is integer
  std::string add_int(const char *json, const char *key, int value);

  // Add key and value to json if value is float
  std::string add_float(const char *json, const char *key, float value);

  // Add key and value to json if value is string
  std::string add_string(const char *json, const char *key, const char *value);

  std::string add_nes_json(const char *json, const char *nesJson,
                           const char *data);
};

#endif /* MAIN_JSONOPERATION_H_ */
