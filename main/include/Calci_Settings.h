#ifndef CALCI_SETTINGS_H_
#define CALCI_SETTINGS_H_

#include "Calculator.h"
#include "File_Operation.h"

#define PSW_INPUT_INTERVAL 700

extern std::string fm_ver;

enum Setting_State
{
    SETTING_IDEAL_STATE = 0,
    SETTING_WIFI_STATE,
    SETTING_LOG_STATE,
    SETTING_GST_STATE,
    SETTING_INFO_STATE,
    SETTING_UPDATE_STATE,
    SETTING_HELP_STATE,
    SETTING_MAX_STATE
};

enum Setting_WiFi_State
{
    SETTING_WIFI_SELECTION_STATE=0,
    SETTING_WIFI_PASSWORD_STATE,
    SETTING_WIFI_MAX_STATE
};

enum Setting_Log_State
{
    SETTING_LOG_MOBILE_STATE=0,
    SETTING_LOG_OTP_STATE,
    SETTING_LOG_MAX_STATE
};

enum Setting_GST_State
{
    SETTING_GST_0_STATE=0,
    SETTING_GST_1_STATE,
    SETTING_GST_2_STATE,
    SETTING_GST_3_STATE,
    SETTING_GST_4_STATE,
    SETTING_GST_MAX_STATE
};

enum Setting_Info_State
{
    SETTING_INFO_FIRMWARE_STATE=0,
    SETTING_INFO_HARDWARE_STATE,
    SETTING_INFO_DEVICE_STATE,
    SETTING_INFO_MODEL_STATE,
    SETTING_INFO_BATCH_STATE,
    SETTING_INFO_HELP_STATE,
    SETTING_INFO_MAX_STATE
};

enum Setting_Update_State
{
    SETTING_UPDATE_CHECK_STATE,
    SETTING_UPDATE_MAX_STATE
};

enum Setting_Help_State
{
    SETTING_HELP_SHOW_STATE,
    SETTING_HELP_MAX_STATE
};

typedef void (*setting_func_ptr_t)(uint8_t key, uint8_t key_type);

void setting_ideal_state_handler(uint8_t key, uint8_t key_type);
void setting_entry_action_handler(void);
void setting_in_event(uint8_t key, uint8_t key_type);

uint8_t setting_wifi_password_shift_mode_abc_add_last_char(uint8_t key, uint8_t key_type);
uint8_t setting_wifi_password_shift_mode_ABC_add_last_char(uint8_t key, uint8_t key_type);
uint8_t setting_wifi_password_shift_mode_abc_parse_key(uint8_t key, uint8_t key_type, char last_key);
uint8_t setting_wifi_password_shift_mode_ABC_parse_key(uint8_t key, uint8_t key_type, char last_key);

void setting_wifi_password_shift_mode_abc_helper(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type);
void setting_wifi_password_shift_mode_ABC_helper(int64_t *setting_wifi_password_last_key_press_time, uint8_t *setting_wifi_password_last_key_type, uint8_t *setting_wifi_password_last_key, uint8_t key, uint8_t key_type);

#endif // CALCI_SETTINGS_H_