/*
 * SPDX-FileCopyrightText: 2017-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifdef __cplusplus
extern "C" {
#endif


// md5_digest_table 3aad295ade45adf9feac4a9ba5f75c17
// This file was generated from the file esp_efuse_custom_table.csv. DO NOT CHANGE THIS FILE MANUALLY.
// If you want to change some fields, you need to change esp_efuse_custom_table.csv file
// then run `efuse_common_table` or `efuse_custom_table` command it will generate this file.
// To show efuse_table run the command 'show_efuse_table'.


extern const esp_efuse_desc_t* ESP_EFUSE_COUNTRY_CODE[];
extern const esp_efuse_desc_t* ESP_EFUSE_HARDWARE_VER[];
extern const esp_efuse_desc_t* ESP_EFUSE_BATCH_NO[];
extern const esp_efuse_desc_t* ESP_EFUSE_WI_FI[];
extern const esp_efuse_desc_t* ESP_EFUSE_BL[];
extern const esp_efuse_desc_t* ESP_EFUSE_GSM[];
extern const esp_efuse_desc_t* ESP_EFUSE_BATCH_YEAR[];
extern const esp_efuse_desc_t* ESP_EFUSE_BATCH_MONTH[];
extern const esp_efuse_desc_t* ESP_EFUSE_DATA_SET[];
extern const esp_efuse_desc_t* ESP_EFUSE_DEVICE_ID[];

#ifdef __cplusplus
}
#endif
