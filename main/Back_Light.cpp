#include "Back_Light.h"

#define TOHANDS_LIGHT_PIN (gpio_num_t)17
#define PWR_BTN GPIO_NUM_34

static esp_adc_cal_characteristics_t on_off_adc_chars;

float get_on_off_voltage(void)
{
    int adc_value = 0;
    for (int i = 0; i < 10; i++)
    {
        adc_value += adc1_get_raw(ADC1_CHANNEL_6);
    }
    float avg_adc = adc_value / 10.0;
    avg_adc = esp_adc_cal_raw_to_voltage(avg_adc, &on_off_adc_chars);
    // printf("\non off detact voltage: %f mv\n", avg_adc);
    return avg_adc;
}

void init_on_off_button(void)
{
    rtc_gpio_deinit(PWR_BTN);
    esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, (adc_bits_width_t)ADC_WIDTH_BIT_DEFAULT, 0, &on_off_adc_chars);
    adc1_config_width((adc_bits_width_t)ADC_WIDTH_BIT_DEFAULT);
    adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
}

void init_tohands_light()
{
    gpio_pad_select_gpio(TOHANDS_LIGHT_PIN);
    gpio_set_direction(TOHANDS_LIGHT_PIN, GPIO_MODE_OUTPUT);
    gpio_set_level(TOHANDS_LIGHT_PIN, 1);
}

void tohands_light(bool on_off)
{
    gpio_set_level(TOHANDS_LIGHT_PIN, on_off);
}