#include "Step_Check.h"

std::list<Operand>::iterator step_it;

int get_it_position(void)
{
    return std::distance(operation.begin(), step_it)+1;
}

void get_iterator(int step_no)
{
    int temp_step = 0;
    for (auto temp_it = operation.begin(); temp_it != operation.end(); ++temp_it)
    {
        if (temp_step == step_no)
        {
            step_it = temp_it;
            break;
        }
        temp_step++;
    }
}

void show_check_step()
{
    if (step_it->result_flag)
    {
        if (step_it->arith_op)
        {
            print_operand(true, step_it->val, -1);
            clear_last_calculation(true);
            remove_cash_sign(true);
            print_result_operator(true, step_it->arith_op);
            print_screen_no(true, get_it_position());
        }
        else if (step_it->tax_op.tax_type)
        {
            clear_operator_block(true);
            clear_last_calculation(true);
            remove_cash_sign(true);
            print_operand(true, step_it->val, -1);
            print_result_operator(true, '=');
            print_gst_sign(true, step_it->tax_op.tax_type, step_it->tax_op.tax_perc);
            print_screen_no(true, get_it_position());
        }
        else if (step_it->mem_op)
        {
            if (step_it->mem_op == '=')
            {
                print_operand(true, step_it->val, -1);
                clear_last_calculation(true);
                remove_cash_sign(true);
                print_result_operator(true, '=');
                print_mem_sign(true, step_it->mem_op);
                print_screen_no(true, get_it_position());
            }
            else
            {
                print_operand(true, step_it->val, -1);
                clear_last_calculation(true);
                remove_cash_sign(true);
                print_result_operator(true, '=');
                print_mem_sign(true, step_it->mem_op);
                print_screen_no(true, get_it_position());
            }
        }
        else if (step_it->cash_op)
        {
            clear_operator_block(true);
            clear_last_calculation(true);
            print_result_operator(true, '=');
            print_operand(true, step_it->val, -1);
            print_cash_sign(true, step_it->cash_op);
            print_screen_no(true, get_it_position());
        }
    }
    else
    {
        if (step_it->arith_op)
        {
            clear_operator_block(true);
            remove_cash_sign(true);
            print_operand(true, step_it->val, -1);
            print_operator(true, step_it->arith_op);
            print_screen_no(true, get_it_position());
        }
        else if (step_it->tax_op.tax_type)
        {
            clear_operator_block(true);
            remove_cash_sign(true);
            clear_last_calculation(true);
            print_operand(true, step_it->val, -1);
            print_gst_sign(true, step_it->tax_op.tax_type, step_it->tax_op.tax_perc);
            print_screen_no(true, get_it_position());
        }
        else if (step_it->mem_op)
        {
            if (step_it->mem_op == '=')
            {
                print_operand(true, step_it->val, -1);
                clear_last_calculation(true);
                remove_cash_sign(true);
                print_mem_sign(true, step_it->mem_op);
                print_screen_no(true, get_it_position());
            }
            else
            {
                print_operand(true, step_it->val, -1);
                clear_last_calculation(true);
                remove_cash_sign(true);
                print_mem_sign(true, step_it->mem_op);
                print_screen_no(true, get_it_position());
            }
        }
        else if (step_it->cash_op)
        {
            clear_operator_block(true);
            clear_last_calculation(true);
            remove_cash_sign(true);
            print_operand(true, step_it->val, -1);
            print_cash_sign(true, step_it->cash_op);
            print_screen_no(true, get_it_position());
        }
        else if (step_it->gt_op)
        {
            clear_operator_block(true);
            clear_last_calculation(true);
            remove_cash_sign(true);
            print_operand(true, step_it->val, -1);
            print_gt_sign(true, step_it->gt_op);
            print_screen_no(true, get_it_position());
        }
        else if (step_it->mu_op)
        {
            clear_operator_block(true);
            clear_last_calculation(true);
            remove_cash_sign(true);
            print_operand(true, step_it->val, -1);
            print_operator(true, step_it->mu_op);
            print_screen_no(true, get_it_position());
        }
    }
}
