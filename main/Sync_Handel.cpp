#include "Sync_Handel.h"
#include "Display.h"
#include "DB_Manager.h"
#include "Log_Handle.h"
#include <string>

#define SYNC_PERIOD_MS 1000

// Mount path for the partition
const char *storage_base_path = "/fat";
esp_flash_t *ext_flash;

static esp_flash_t *init_ext_flash(void)
{
  spi_bus_config_t bus_config = {};
  bus_config.mosi_io_num = 15;
  bus_config.miso_io_num = 4;
  bus_config.sclk_io_num = 13;
  // .quadhd_io_num = VSPI_IOMUX_PIN_NUM_HD,
  // .quadwp_io_num = VSPI_IOMUX_PIN_NUM_WP,

  esp_flash_spi_device_config_t device_config = {};
  device_config.host_id = VSPI_HOST;
  device_config.cs_id = 0;
  device_config.cs_io_num = 16;
  device_config.io_mode = SPI_FLASH_DIO;
  device_config.speed = ESP_FLASH_40MHZ;

  ESP_LOGI(SYNC, "Initializing external SPI Flash");
  ESP_LOGI(SYNC, "Pin assignments:");
  ESP_LOGI(SYNC, "MOSI: %2d   MISO: %2d   SCLK: %2d   CS: %2d",
           bus_config.mosi_io_num, bus_config.miso_io_num,
           bus_config.sclk_io_num, device_config.cs_io_num);

  // Initialize the SPI bus
  ESP_ERROR_CHECK(spi_bus_initialize(VSPI_HOST, &bus_config, SPI_DMA_CH_AUTO));

  // Add device to the SPI bus
  esp_flash_t *flash;
  ESP_ERROR_CHECK(spi_bus_add_flash_device(&flash, &device_config));

  // Probe the Flash chip and initialize it
  esp_err_t err = esp_flash_init(flash);
  if (err != ESP_OK)
  {
    ESP_LOGE(SYNC, "Failed to initialize external Flash: %s (0x%x)", esp_err_to_name(err), err);
    char temp_msg[255];
    sprintf(temp_msg, "%s: Failed to initialize external Flash: %s (0x%x)", SYNC, esp_err_to_name(err), err);
    return NULL;
  }

  // Print out the ID and size
  uint32_t id;
  ESP_ERROR_CHECK(esp_flash_read_id(flash, &id));
  ESP_LOGI(SYNC, "Initialized external Flash, size=%d KB, ID=0x%x", flash->size / 1024, id);

  return flash;
}

static const esp_partition_t *add_partition(esp_flash_t *flash, const char *partition_label)
{
  ESP_LOGI(SYNC, "Adding external Flash as a partition, label=\"%s\", size=%d KB", partition_label, flash->size / 1024);
  const esp_partition_t *fat_partition;
  ESP_ERROR_CHECK(esp_partition_register_external(flash, 0, flash->size, partition_label, ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_FAT, &fat_partition));
  return fat_partition;
}

static void list_data_partitions(void)
{
  ESP_LOGI(SYNC, "Listing data partitions:");
  esp_partition_iterator_t it = esp_partition_find(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_ANY, NULL);

  for (; it != NULL; it = esp_partition_next(it))
  {
    const esp_partition_t *part = esp_partition_get(it);
    ESP_LOGI(SYNC, "- partition '%s', subtype %d, offset 0x%x, size %d kB",
             part->label, part->subtype, part->address, part->size / 1024);
  }

  esp_partition_iterator_release(it);
}

static bool mount_fatfs(const char *partition_label)
{
  ESP_LOGI(SYNC, "Mounting FAT filesystem");
  esp_vfs_fat_mount_config_t mount_config = {};
  mount_config.max_files = 4;
  mount_config.format_if_mount_failed = true;
  mount_config.allocation_unit_size = CONFIG_WL_SECTOR_SIZE;
  ESP_LOGI(SYNC, "Path: %s\t Lable: %s", storage_base_path, partition_label);
  esp_err_t err = esp_vfs_fat_spiflash_mount(storage_base_path, partition_label, &mount_config, &s_wl_handle);
  if (err != ESP_OK)
  {
    ESP_LOGE(SYNC, "Failed to mount FATFS (%s)", esp_err_to_name(err));
    return false;
  }
  return true;
}

static void get_fatfs_usage(size_t *out_total_bytes, size_t *out_free_bytes)
{
  FATFS *fs;
  size_t free_clusters;
  int res = f_getfree("0:", &free_clusters, &fs);
  assert(res == FR_OK);
  size_t total_sectors = (fs->n_fatent - 2) * fs->csize;
  size_t free_sectors = free_clusters * fs->csize;

  // assuming the total size is < 4GiB, should be true for SPI Flash
  if (out_total_bytes != NULL)
  {
    *out_total_bytes = total_sectors * fs->ssize;
  }
  if (out_free_bytes != NULL)
  {
    *out_free_bytes = free_sectors * fs->ssize;
  }
}

void move_file(const char *src_file_name, const char *trg_file_name)
{
  Trans_Data_0 cpy_data_0;
  Trans_Data_1 cpy_data_1;
  void *cpy_data;
  FILE *f = fopen(src_file_name, "r");
  if (f == NULL)
  {
    return;
  }
  fseek(f, 0L, SEEK_END);
  int res = ftell(f); // counting the size of the file
  rewind(f);
  if (res == sizeof(Trans_Data_0))
  {
    fread(&cpy_data_0, sizeof(cpy_data_0), 1, f);

    FILE *ft = fopen(trg_file_name, "w");
    if (ft == NULL)
    {
      fclose(ft);
      return;
    }
    fwrite(&cpy_data_0, sizeof(cpy_data_0), 1, ft);
    fclose(ft);
  }
  else if (res == sizeof(Trans_Data_1))
  {
    fread(&cpy_data_1, sizeof(cpy_data_1), 1, f);

    FILE *ft = fopen(trg_file_name, "w");
    if (ft == NULL)
    {
      fclose(ft);
      return;
    }
    fwrite(&cpy_data_1, sizeof(cpy_data_1), 1, ft);
    fclose(ft);
  }
  fclose(f);
  remove(src_file_name);
}

void call_unsync_trans_get_query(void)
{
  xQueueReset(periodic_sync_data);
  id_list.clear();
  char temp_query[100];
  sprintf(temp_query, "SELECT * FROM %s WHERE isSynced=0 AND cashbookUserId!=0 LIMIT 20", TABLE_NAME);
  int rc = 0;
  xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
  rc = db_exec(db, PERIODIC, temp_query);
  xSemaphoreGive(sqlite_mutex);
  if (rc != SQLITE_OK)
  {
    ESP_LOGE(SYNC, "RC: %d\n", rc);
    char temp_msg[255];
    sprintf(temp_msg, "%s: SQLite exe failed %d", SYNC, rc);
    send_log(temp_msg, "FIRMWARE");
  }
}

void call_transactions_delete_query(void)
{
  const char *delete_query = "DELETE FROM transactions WHERE isSynced=1";
  xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
  int rc = 0;
  rc = db_exec(db, OTHER, delete_query);
  if (rc != SQLITE_OK)
  {
    ESP_LOGE(DB, "RC: %d\n", rc);
  }
  xSemaphoreGive(sqlite_mutex);
}

void save_transaction_success_handle(void)
{
  ESP_LOGI(SYNC, "Successfuly sync transactions");
  std::string sync_flag_set_list = "UPDATE " + std::string(TABLE_NAME) + " SET isSynced=1 WHERE id=";
  for (int i = 0; i < id_list.size(); i++)
  {
    if (i < (id_list.size() - 1))
      sync_flag_set_list += std::to_string(id_list[i]) + " OR id=";
    else
      sync_flag_set_list += std::to_string(id_list[i]);
  }
  ESP_LOGI(SYNC, "Sync flag update list: %s", sync_flag_set_list.c_str());
  int rc = 0;
  xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
  rc = db_exec(db, OTHER, sync_flag_set_list.c_str());
  xSemaphoreGive(sqlite_mutex);
  if (rc != SQLITE_OK)
  {
    remove(DATA_BASE);
    FILE *fp;
    fp = fopen(DATA_BASE, "w");
    fclose(fp);
    if (openDb(DATA_BASE, &db))
    {
      ESP_LOGE(SYNC, "Opening datbase failed");
    }
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    rc = db_exec(db, OTHER, CREATE_TABLE);
    xSemaphoreGive(sqlite_mutex);
    if (rc != SQLITE_OK)
    {
      ESP_LOGE(SYNC, "RC: %d\n", rc);
    }
  }
}

static void sync_handel_task(void *arg)
{
  Trans_Data_Sqlite_Struc sync_data_buf = {0};
  std::string set_sync_flag_query = "";
  cJSON *array = NULL;

  cJSON *transactionType = NULL;
  cJSON *amount_send = NULL;
  cJSON *cashbookUserId = NULL;
  cJSON *deviceType = NULL;
  cJSON *transactionTime = NULL;
  cJSON *paymentMode = NULL;
  cJSON *customerName = NULL;
  cJSON *customerMobile = NULL;
  cJSON *remarks = NULL;

  while (1)
  {
    struct stat buffer;
    int exist = stat(CASHBOOK_ID, &buffer);
    if (get_wifi_status() && (!exist))
    {
      if (xQueueReceive(periodic_sync_data, (void *)&sync_data_buf, SYNC_PERIOD_MS / portTICK_RATE_MS))
      {
        print_sync(true);
        ESP_LOGI(SYNC, "Queue received: %c\t Size: %d", sync_data_buf.trans_type, id_list.size());
        if (sync_data_buf.trans_type == 'S')
        {
          if (id_list.size() > 0)
          {
            cJSON *doc = cJSON_CreateObject();
            cJSON_AddItemToObject(doc, "cashbookUserTransactions", array);
            char *sync_data_json = cJSON_PrintUnformatted(doc);
            ESP_LOGI(CLIENT, "BODY: %s", sync_data_json);
            // cJSON_Delete(doc);
            int rsp_vrfy = send_transactions(sync_data_json);
            if (rsp_vrfy == 200)
            {
              save_transaction_success_handle();
            }
            else if (rsp_vrfy == 401)
            {
              ESP_LOGI(SYNC, "Token refreshing");
              int res_token = refresh_token();
              if (res_token == 200)
              {
                ESP_LOGI(SYNC, "Token refreshed");
                rsp_vrfy = send_transactions(sync_data_json);
                if (rsp_vrfy == 200)
                {
                  save_transaction_success_handle();
                }
                else
                {
                  ESP_LOGE(SYNC, "Save transaction status code: %d", rsp_vrfy);
                  char temp_msg[255];
                  sprintf(temp_msg, "%s: Save transaction status code: %d", SYNC, rsp_vrfy);
                  send_log(temp_msg, "API");
                }
              }
              else if (res_token == 401)
              {
                ESP_LOGI(SYNC, "Token expired");
                char temp_msg[255];
                sprintf(temp_msg, "%s:  transaction sync task : synced token expired", SYNC);
                send_log(temp_msg, "API");
                log_out_user();
              }
              else
              {
                ESP_LOGE(SYNC, "Token refresh status code: %d", res_token);
                char temp_msg[255];
                sprintf(temp_msg, "%s: Token refresh status code: %d", SYNC, res_token);
                send_log(temp_msg, "API");
              }
            }
            else
            {
              ESP_LOGE(SYNC, "Save transaction status code: %d", rsp_vrfy);
              char temp_msg[255];
              sprintf(temp_msg, "%s: Save transaction status code: %d", SYNC, rsp_vrfy);
              send_log(temp_msg, "API");
            }
            cJSON_Delete(doc);
            cJSON_free(sync_data_json);
            array = cJSON_CreateArray();
          }
          total_pending_trans = get_unsynced_transactions();
          if (mode_now == NORMAL_MODE)
          {
            display_trans_count(true, total_pending_trans);
          }
          int temp_sync_trans = get_synced_transactions();
          ESP_LOGI(SYNC, "Total synced transactions= %d", temp_sync_trans);
          if (temp_sync_trans > 500)
          {
            call_transactions_delete_query();
          }
          call_unsync_trans_get_query();
          clear_sync(true);
          if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
          {
            ESP_LOGI(SYNC, "Task delete notification received");
            vTaskDelete(NULL);
          }
        }
        else if (sync_data_buf.trans_type == 'I')
        {
          ESP_LOGI(SYNC, "Sync transaction instant");
          call_unsync_trans_get_query();
        }
        else
        {
          if (array == NULL)
          {
            ESP_LOGI(SYNC, "array is NULL creating object");
            array = cJSON_CreateArray();
          }
          if (sync_data_buf.trans_amount != 0 && sync_data_buf.cashbook_user_id != 0)
          {
            if (sync_data_buf.trans_type == '+' || sync_data_buf.trans_type == '-')
            {
              if (sync_data_buf.payment_mode == char(1) || sync_data_buf.payment_mode == char(2) || sync_data_buf.payment_mode == char(3))
              {
                cJSON *nesDoc = cJSON_CreateObject();
                if (sync_data_buf.trans_type == '+')
                  transactionType = cJSON_CreateString("Credit");
                else if (sync_data_buf.trans_type == '-')
                  transactionType = cJSON_CreateString("Debit");
                cJSON_AddItemToObject(nesDoc, "transactionType", transactionType);
                amount_send = cJSON_CreateNumber(sync_data_buf.trans_amount);
                cJSON_AddItemToObject(nesDoc, "amount", amount_send);
                if (sync_data_buf.payment_mode == char(1))
                  paymentMode = cJSON_CreateString("CASH");
                else if (sync_data_buf.payment_mode == char(2))
                  paymentMode = cJSON_CreateString("ONLINE");
                else if ((char)sync_data_buf.payment_mode == char(3))
                  paymentMode = cJSON_CreateString("PAY_LATER");
                cJSON_AddItemToObject(nesDoc, "paymentMode", paymentMode);
                cashbookUserId = cJSON_CreateNumber(sync_data_buf.cashbook_user_id);
                cJSON_AddItemToObject(nesDoc, "cashbookUserId", cashbookUserId);

                deviceType = cJSON_CreateString("CALCULATOR");
                cJSON_AddItemToObject(nesDoc, "deviceType", deviceType);
                if (sync_data_buf.trans_time != NULL && sync_data_buf.trans_time != 0)
                {
                  std::string mili_tras_time = std::to_string(sync_data_buf.trans_time) + "000";
                  transactionTime = cJSON_CreateString(mili_tras_time.c_str());
                  cJSON_AddItemToObject(nesDoc, "transactionTime", transactionTime);
                }
                if (strlen(sync_data_buf.trans_cust_name) > 0)
                {
                  customerName = cJSON_CreateString(sync_data_buf.trans_cust_name);
                  cJSON_AddItemToObject(nesDoc, "customerName", customerName);
                }
                if (strlen(sync_data_buf.trans_cust_num) > 0)
                {
                  customerMobile = cJSON_CreateString(sync_data_buf.trans_cust_num);
                  cJSON_AddItemToObject(nesDoc, "customerMobile", customerMobile);
                }
                if (strlen(sync_data_buf.trans_cust_remark) > 0)
                {
                  remarks = cJSON_CreateString(sync_data_buf.trans_cust_remark);
                  cJSON_AddItemToObject(nesDoc, "remarks", remarks);
                }

                cJSON_AddItemToArray(array, nesDoc);
              }
            }
          }
        }
      }
      else
      {
        if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
        {
          ESP_LOGI(SYNC, "Task delete notification received");
          vTaskDelete(NULL);
        }
        call_unsync_trans_get_query();
      }
    }
    if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
    {
      ESP_LOGI(SYNC, "Task delete notification received");
      break;
    }
  }
  vTaskDelete(NULL);
}

int move_file_data_to_db(Trans_Data_Sqlite_Struc *data_str, uint8_t sync_sts)
{
  char *temp_query = (char *)malloc(300 * sizeof(char));
  if (temp_query != NULL)
  {
    int rc = 0;
    sprintf(temp_query, "INSERT INTO %s (amount, transactionType, paymentMode, transactionTimeStamp, isSynced, customerName, customerMobile, customerRemarks, cashbookUserId)VALUES(%lf, '%c', '%c', %lld, %d, '%s',  '%s', '%s', %lld)",
            TABLE_NAME, data_str->trans_amount, data_str->trans_type, data_str->payment_mode, data_str->trans_time, sync_sts, data_str->trans_cust_name, data_str->trans_cust_num, data_str->trans_cust_remark, data_str->cashbook_user_id);

    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    rc = db_exec(db, OTHER, temp_query);
    xSemaphoreGive(sqlite_mutex);
    if (rc != SQLITE_OK)
    {
      remove(DATA_BASE);
      FILE *fp;
      fp = fopen(DATA_BASE, "w");
      fclose(fp);
      if (openDb(DATA_BASE, &db))
      {
        ESP_LOGE(SYNC, "Opening datbase failed");
      }
      xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
      rc = db_exec(db, OTHER, CREATE_TABLE);
      xSemaphoreGive(sqlite_mutex);
      if (rc != SQLITE_OK)
      {
        ESP_LOGE(SYNC, "RC: %d\n", rc);
      }
    }
    free(temp_query);
    return rc;
  }
  return 1;
}

void move_files_to_db(const char *files_dir)
{
  Trans_Data_0 syncing_trans_0;
  Trans_Data_1 syncing_trans_1;

  // Trans_Data syncing_trans;
  DIR *move_dir;
  struct dirent *move_dirent;
  move_dir = opendir(files_dir);
  if (move_dir)
  {
    int file_count = 0;
    while ((move_dirent = readdir(move_dir)) != NULL)
    {
      ESP_LOGI(SYNC, "Credit transaction pending");
      Trans_Data_Sqlite_Struc file_read_data = {0};
      if (strcmp(files_dir, CREDIT) == 0)
      {
        file_read_data.trans_type = '+';
      }
      else if (strcmp(files_dir, DEBIT) == 0)
      {
        file_read_data.trans_type = '-';
      }

      int64_t cashbook_id = 0;
      FILE *fid = fopen(CASHBOOK_ID, "r");
      if (fid == NULL)
      {
        ESP_LOGW(SYNC, "Failed to open Cashbook ID file");
      }
      else
      {
        fread(&cashbook_id, sizeof(int64_t), 1, fid);
        fclose(fid);
      }
      file_read_data.cashbook_user_id = cashbook_id;

      std::string file_name = std::string(files_dir) + "/" + std::string(move_dirent->d_name);
      FILE *f = fopen(file_name.c_str(), "r");
      if (f == NULL)
      {
        ESP_LOGI(SYNC, "file reading fail");
        break;
      }
      //******************************************************************************************************
      fseek(f, 0L, SEEK_END);
      int res = ftell(f); // counting the size of the file
      rewind(f);
      if (res == sizeof(Trans_Data_0))
      {
        fread(&syncing_trans_0, sizeof(syncing_trans_0), 1, f);
        fclose(f);

        file_read_data.trans_amount = syncing_trans_0.trans_amount;
        if (syncing_trans_0.trans_type == 'C')
          file_read_data.payment_mode = 1;
        else if (syncing_trans_0.trans_type == 'O')
          file_read_data.payment_mode = 2;
        else if (syncing_trans_0.trans_type == 'L')
          file_read_data.payment_mode = 3;
        file_read_data.trans_time = std::stoll(move_dirent->d_name, nullptr, 10);
        if (move_file_data_to_db(&file_read_data, 0) == 0)
        {
          remove(file_name.c_str());
        }
      }
      else if (res == sizeof(Trans_Data_1))
      {
        fread(&syncing_trans_1, sizeof(syncing_trans_1), 1, f);
        fclose(f);

        file_read_data.trans_amount = syncing_trans_1.trans_amount;
        if (syncing_trans_1.trans_type == 'C')
          file_read_data.payment_mode = 1;
        else if (syncing_trans_1.trans_type == 'O')
          file_read_data.payment_mode = 2;
        else if (syncing_trans_1.trans_type == 'L')
          file_read_data.payment_mode = 3;
        file_read_data.trans_time = std::stoll(move_dirent->d_name, nullptr, 10);
        memcpy(file_read_data.trans_cust_name, syncing_trans_1.trans_cust_name, sizeof(syncing_trans_1.trans_cust_name));
        memcpy(file_read_data.trans_cust_num, syncing_trans_1.trans_cust_num, sizeof(syncing_trans_1.trans_cust_num));
        memcpy(file_read_data.trans_cust_remark, syncing_trans_1.trans_cust_remark, sizeof(syncing_trans_1.trans_cust_remark));
        if (move_file_data_to_db(&file_read_data, 0) == 0)
        {
          remove(file_name.c_str());
        }
      }
      //******************************************************************************************************
    }
    closedir(move_dir);
    int remov = rmdir(files_dir);
    if (remov == 0)
    {
      ESP_LOGI(SYNC, "%s Directory removed successfuly", files_dir);
    }
    else
    {
      ESP_LOGW(SYNC, "%s Directory removed failed", files_dir);
    }
  }
}

void start_syncing()
{
  display_message_when_coying_file_data_to_db(true);
  ext_flash = init_ext_flash();
  if (ext_flash == NULL)
  {
    ESP_LOGI(SYNC, "External flash initialization failed");
    return;
  }

  // esp_err_t err = esp_flash_erase_chip(ext_flash);
  // if (err == ESP_OK)
  //   ESP_LOGI(SYNC, "External flash erased");
  // else
  //   ESP_LOGI(SYNC, "External flash erase error: %s", esp_err_to_name(err));
  // Add the entire external flash chip as a partition
  const char *partition_label = "storage";
  add_partition(ext_flash, partition_label);

  list_data_partitions();

  if (!mount_fatfs("storage"))
  {
    ESP_LOGI(SYNC, "FATFS partition mounting failed");
    return;
  }

  size_t bytes_total, bytes_free;
  get_fatfs_usage(&bytes_total, &bytes_free);
  ESP_LOGI(SYNC, "FAT FS: %d kB total, %d kB free", bytes_total / 1024, bytes_free / 1024);

  sqlite_mutex = xSemaphoreCreateMutex();

  if (sqlite_mutex == NULL)
  {
    ESP_LOGW(SYNC, "Failed to initialize the sqlite mutex");
    // need to better handle this
  }
  xSemaphoreGive(sqlite_mutex);
  struct stat st;
  if (stat(DATA_BASE, &st) == 0)
  {
    ESP_LOGI(SYNC, "DATA_BASE exist");
    if (stat(OLD_DB_CHEKC, &st) != 0)
    {
      ESP_LOGW(SYNC, "Non valid DATA_BASE exist");
      rmdir(CREDIT);
      rmdir(DEBIT);
      FILE *fp;
      remove(DATA_BASE);
      fp = fopen(DATA_BASE, "w");
      fclose(fp);
      fp = fopen(OLD_DB_CHEKC, "w");
      fclose(fp);
    }
    else
    {
      ESP_LOGW(SYNC, "Valid DATA_BASE exist");
    }
  }
  else
  {
    ESP_LOGW(SYNC, "DATA_BASE NOT exist so creating file");
    FILE *fp;
    fp = fopen(DATA_BASE, "w");
    fclose(fp);
  }

  sqlite3_initialize();
  if (openDb(DATA_BASE, &db))
  {
    ESP_LOGW(SYNC, "Opening datbase failed");
    return;
  }

  sqlite3_close(db);

  int rc = 0;
  xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
  rc = db_exec(db, OTHER, CREATE_TABLE);
  xSemaphoreGive(sqlite_mutex);
  if (rc != SQLITE_OK)
  {
    ESP_LOGE(SYNC, "RC: %d\n", rc);
  }

  DIR *past_fs_dir;
  past_fs_dir = opendir(BACKUP_CREDIT);
  if (past_fs_dir)
  {
    closedir(past_fs_dir);
    int remov = rmdir(BACKUP_CREDIT);
    if (remov == 0)
    {
      ESP_LOGI(SYNC, "%s Directory removed successfuly", BACKUP_CREDIT);
    }
    else
    {
      ESP_LOGW(SYNC, "%s Directory removed failed", BACKUP_CREDIT);
    }
  }

  past_fs_dir = opendir(BACKUP_DEBIT);
  if (past_fs_dir)
  {
    closedir(past_fs_dir);
    int remov = rmdir(BACKUP_DEBIT);
    if (remov == 0)
    {
      ESP_LOGI(SYNC, "%s Directory removed successfuly", BACKUP_DEBIT);
    }
    else
    {
      ESP_LOGW(SYNC, "%s Directory removed failed", BACKUP_DEBIT);
    }
  }

  // move_files_to_db(CREDIT);
  // move_files_to_db(DEBIT);

  total_pending_trans = 0;
  total_pending_trans = get_unsynced_transactions();

  if (mode_now == NORMAL_MODE)
  {
    display_trans_count(true, total_pending_trans);
  }

  periodic_sync_data = xQueueCreate(51, sizeof(Trans_Data_Sqlite_Struc));

  if (periodic_sync_data == NULL)
  {
    ESP_LOGE(SYNC, "Failed to initialize the periodic_sync_data queue");
  }

  xTaskCreate(sync_handel_task, "syncing data task", 10240, NULL, 10, &Sync_Task_Handle);
}