#include "Log_Handle.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_http_client.h"
#include "cJSON.h"
#include "Calci_Config.h"
#include "WiFi_Manager.h"
#include "File_Operation.h"
#include "DB_Manager.h"
#include "HTTP_Client.h"
#include "vector"

#define LOG_SYNC_PERIOD_MS 1000
#define LOG_DATA_BASE "/fat/log_data.db"
#define CREATE_LOG_TABLE "CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, userId INTEGER, log TEXT, serialNo TEXT, timestamp TEXT, deviceType TEXT, logType TEXT, appOrFirmwareVersion TEXT, isSynced BOOLEAN)"
static const char *L_TAG = "Log Handle";

void call_logs_delete_query(void)
{
    const char *delete_query = "DELETE FROM logs WHERE isSynced=1";
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    int rc = 0;
    rc = db_exec(db, OTHER, delete_query);
    if (rc != SQLITE_OK)
    {
        ESP_LOGE(DB, "RC: %d\n", rc);
    }
    xSemaphoreGive(sqlite_mutex);
}

void log_sender_task(void *pvParameter)
{
    uint8_t log_sync_data_buf = 0;
    const char *log_get_query = "SELECT * FROM logs WHERE isSynced=0 LIMIT 20";

    while (1)
    {
        struct stat buffer;
        int exist = stat(CASHBOOK_ID, &buffer);
        if (get_wifi_status() && (!exist))
        {
            if (xQueueReceive(log_periodic_sync_data, (void *)&log_sync_data_buf, LOG_SYNC_PERIOD_MS / portTICK_RATE_MS))
            {
                char *temp_prepared_body = prepare_send_log_body();
                if (temp_prepared_body != NULL)
                {
                    if (!log_id_list.empty() && strlen(temp_prepared_body) > 0)
                    {
                        int rsp = send_log_to_server(temp_prepared_body);
                        if (rsp == 200)
                        {
                            ESP_LOGI(L_TAG, "Successfuly sync error log");
                            std::string sync_flag_set_list = "UPDATE logs SET isSynced=1 WHERE id=";
                            for (int i = 0; i < log_id_list.size(); i++)
                            {
                                if (i < (log_id_list.size() - 1))
                                    sync_flag_set_list += std::to_string(log_id_list[i]) + " OR id=";
                                else
                                    sync_flag_set_list += std::to_string(log_id_list[i]);
                            }
                            ESP_LOGI(L_TAG, "Sync error flag update list: %s", sync_flag_set_list.c_str());
                            int rc = 0;
                            xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
                            rc = db_exec(db, OTHER, sync_flag_set_list.c_str());
                            xSemaphoreGive(sqlite_mutex);
                            if (rc != SQLITE_OK)
                            {
                                ESP_LOGE(L_TAG, "RC: %d\n", rc);
                            }
                            log_id_list.clear();
                        }
                    }
                    free(temp_prepared_body);
                }
                int temp_sync_logs = get_synced_logs();
                ESP_LOGI(L_TAG, "Total synced logs= %d", temp_sync_logs);
                if (temp_sync_logs > 500)
                {
                    call_logs_delete_query();
                }
            }
            else
            {
                int rc = 0;
                xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
                rc = db_exec(db, LOG_PERIODIC, log_get_query);
                ESP_LOGW(L_TAG, "RC: %d\n", rc);
                xSemaphoreGive(sqlite_mutex);
            }
        }
        else
        {
            vTaskDelay(1000 / portTICK_RATE_MS);
        }
        if (ulTaskNotifyTake(pdTRUE, 100 / portTICK_RATE_MS))
        {
            ESP_LOGI(L_TAG, "Task delete notification received");
            vTaskDelete(NULL);
        }
    }
    vTaskDelete(NULL);
}

void init_log_handle(void)
{
    struct stat st;
    if (stat(LOG_DATA_BASE, &st) == 0)
    {
        ESP_LOGI(L_TAG, "LOG_DATA_BASE exist");
        remove(LOG_DATA_BASE);
    }

    int rc = 0;
    xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
    rc = db_exec(db, OTHER, CREATE_LOG_TABLE);
    xSemaphoreGive(sqlite_mutex);

    if (rc != SQLITE_OK)
    {
        ESP_LOGE(L_TAG, "RC: %d\n", rc);
    }

    log_periodic_sync_data = xQueueCreate(5, sizeof(uint8_t));

    if (log_periodic_sync_data == NULL)
    {
        ESP_LOGE(L_TAG, "Failed to initialize the periodic_sync_data queue");
    }

    xTaskCreate(log_sender_task, "log_sending", 10240, NULL, 8, &Log_Sender_Task_Handle);
}

char *prepare_send_log_body(void)
{
    if (!log_sync_data.empty())
    {
        cJSON *cashbookIncidents_array = cJSON_CreateArray();
        for (auto itr : log_sync_data)
        {
            cJSON *doc = cJSON_CreateObject();
            if (itr.log_user_id != 0)
            {
                cJSON *user_id_doc = cJSON_CreateNumber(itr.log_user_id);
                cJSON_AddItemToObject(doc, "userId", user_id_doc);
            }
            if (!itr.log_str.empty())
            {
                cJSON *log_doc = cJSON_CreateString(itr.log_str.c_str());
                cJSON_AddItemToObject(doc, "log", log_doc);
            }
            if (!itr.log_serial_no.empty())
            {
                cJSON *s_no = cJSON_CreateString(itr.log_serial_no.c_str());
                cJSON_AddItemToObject(doc, "serialNo", s_no);
            }
            if (!itr.log_timestamp.empty())
            {
                cJSON *t_stamp = cJSON_CreateString(itr.log_timestamp.c_str());
                cJSON_AddItemToObject(doc, "timestamp", t_stamp);
            }
            if (!itr.log_device_type.empty())
            {
                cJSON *device_type = cJSON_CreateString(itr.log_device_type.c_str());
                cJSON_AddItemToObject(doc, "deviceType", device_type);
            }
            if (!itr.log_type.empty())
            {
                cJSON *log_type = cJSON_CreateString(itr.log_type.c_str());
                cJSON_AddItemToObject(doc, "logType", log_type);
            }
            if (!itr.log_firmware_version.empty())
            {
                cJSON *firmware_version = cJSON_CreateString(itr.log_firmware_version.c_str());
                cJSON_AddItemToObject(doc, "appOrFirmwareVersion", firmware_version);
            }

            cJSON_AddItemToArray(cashbookIncidents_array, doc);
        }
        cJSON *main_doc = cJSON_CreateObject();
        cJSON_AddItemToObject(main_doc, "cashbookIncidents", cashbookIncidents_array);
        char *temp_body = cJSON_PrintUnformatted(main_doc);
        cJSON_Delete(cashbookIncidents_array);
        log_sync_data.clear();
        return temp_body;
    }
    return "";
}

int send_log_to_server(const char *body_data)
{
    if (url.length() == 0)
        return 0;
    std::string send_transaction_url =
        url + path + "saveIncident";
    ESP_LOGI(L_TAG, "URL: %s", send_transaction_url.c_str());
    char output_buffer[1024] = {0}; // Buffer to store response of http request
    int content_length = 0;
    esp_http_client_config_t config = {
        .url = send_transaction_url.c_str(),
#ifdef CONFIG_FIRMWARE_BUILD_PROD
        .cert_pem = server_cert_pem_start,
#endif
        .user_data =
            output_buffer, // Pass address of local buffer to get response
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    if (client == NULL)
    {
        ESP_LOGE(L_TAG, "Failed to initialise HTTP connection");
        return 0;
    }
    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_header(client, "Content-Type", "application/json");
    esp_http_client_set_header(client, "consumer-id", CONSUMER_ID);
    esp_http_client_set_post_field(client, body_data, strlen(body_data));
    ESP_LOGI(L_TAG, "BODY: %s", body_data);
    esp_err_t err = esp_http_client_open(client, strlen(body_data));
    if (err != ESP_OK)
    {
        ESP_LOGE(L_TAG, "Failed to open HTTP connection: %s",
                 esp_err_to_name(err));
        esp_http_client_close(client);
        esp_http_client_cleanup(client);
        return 0;
    }
    int wlen = esp_http_client_write(client, body_data, strlen(body_data));
    if (wlen < 0)
    {
        ESP_LOGE(L_TAG, "Write failed");
        esp_http_client_close(client);
        esp_http_client_cleanup(client);
        return 0;
    }
    esp_http_client_fetch_headers(client);

    int data_read = esp_http_client_read(client, output_buffer, 1024);
    if (data_read < 0)
    {
        esp_http_client_close(client);
        esp_http_client_cleanup(client);
        return 0;
    }
    else if (data_read > 0)
    {
        ESP_LOGI(L_TAG, "Status = %d, content_length = %d",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));
        ESP_LOGI(L_TAG, "Response: %s", output_buffer);
        esp_http_client_close(client);
        esp_http_client_cleanup(client);
        return esp_http_client_get_status_code(client);
    }
    ESP_LOGI(L_TAG, "Status = %d, content_length = %d",
             esp_http_client_get_status_code(client),
             esp_http_client_get_content_length(client));
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return 0;
}

void send_log(const char *e_log, const char *e_log_type)
{
    int64_t cashbook_id = 0;
    FILE *fid = fopen(CASHBOOK_ID, "r");
    if (fid == NULL)
    {
        ESP_LOGE(L_TAG, "Failed to open Cashbook ID file");
    }
    else
    {
        fread(&cashbook_id, sizeof(int64_t), 1, fid);
        fclose(fid);
    }

    time_t seconds;
    seconds = time(NULL);

    char temp_t_stamp[30];
    sprintf(temp_t_stamp, "%ld000", seconds);

    std::string temp_sr_no = get_serial_no();

    char *temp_qry = (char *)malloc(1024 * sizeof(char));
    if (temp_qry != NULL && e_log != NULL && e_log_type != NULL && !temp_sr_no.empty() && strlen(e_log) > 0 && strlen(e_log_type) > 0)
    {
        sprintf(temp_qry, "INSERT INTO logs (userId, log, serialNo, timestamp, deviceType, logType, appOrFirmwareVersion, isSynced)VALUES(%lld, '%s', '%s', '%s', 'CALCULATOR', '%s', '%s', 0)",
                cashbook_id, e_log, temp_sr_no.c_str(), temp_t_stamp, e_log_type, CONFIG_FIRMWARE_VERSION_NAME);
        ESP_LOGI(L_TAG, "Log Insert= %s", temp_qry);

        xSemaphoreTake(sqlite_mutex, portMAX_DELAY);
        int rc = 0;
        rc = db_exec(db, OTHER, temp_qry);
        if (rc != SQLITE_OK)
        {
            ESP_LOGE(L_TAG, "RC: %d\n", rc);
        }
        xSemaphoreGive(sqlite_mutex);

        free(temp_qry);
    }
}