#include "Arial14.h"
#include "CalLite24.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "glcdfont.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define swap(a, b) \
  {                \
    uint8_t t = a; \
    a = b;         \
    b = t;         \
  }

#define BLACK 1
#define WHITE 0

#define LCDWIDTH 192
#define LCDHEIGHT 64

#define CMD_DISPLAY_OFF 0xAE
#define CMD_DISPLAY_ON 0xAF

#define CMD_SET_DISP_START_LINE 0x40
#define CMD_SET_PAGE 0xB0

#define CMD_SET_COLUMN_UPPER 0x10
#define CMD_SET_COLUMN_LOWER 0x00

#define CMD_SET_ADC_NORMAL 0xA0
#define CMD_SET_ADC_REVERSE 0xA1

#define CMD_SET_DISP_NORMAL 0xA6
#define CMD_SET_DISP_REVERSE 0xA7

#define CMD_SET_ALLPTS_NORMAL 0xA4
#define CMD_SET_ALLPTS_ON 0xA5
#define CMD_SET_BIAS_9 0xA2
#define CMD_SET_BIAS_7 0xA3

#define CMD_SET_CONTRAST 0x81

class ST7525
{
public:
  static uint8_t st7525_buffer[8][192];
  static uint8_t st7525_saved_buffer[8][192];
  ST7525(int8_t SID, int8_t SCLK, int8_t A0, int8_t RST, int8_t CS, int8_t BKL,
         spi_host_device_t lcd_host)
      : sid((gpio_num_t)SID), sclk((gpio_num_t)SCLK), a0((gpio_num_t)A0),
        rst((gpio_num_t)RST), cs((gpio_num_t)CS), bck_light((gpio_num_t)BKL),
        spi_host(lcd_host) {}

  void st7525_init(void);
  void begin(uint8_t contrast);
  void st7525_command(uint8_t c);
  void st7525_data(uint8_t c);
  void st7525_set_brightness(uint8_t val);
  void clear_display(void);
  void clear();
  void display();
  void back_light(bool light);

  void set_page_address(uint8_t add);
  void set_column_address(uint8_t add);

  void drawchar(uint8_t page, uint8_t col, char c);
  void drawinvertedchar(uint8_t page, uint8_t col, char c);
  void drawstring(uint8_t page, uint8_t col, const char *c);
  void drawinvertedstring(uint8_t page, uint8_t col, const char *c);
  void drawcharary(uint8_t page, uint8_t col, const char *c, int len);

  uint8_t drawchar_arial14(uint8_t page, uint8_t col, char c);
  uint8_t drawinvertedchar_arial14(uint8_t page, uint8_t col, char c);
  uint8_t drawchar_right_arial14(uint8_t page, uint8_t col, char c);
  void drawstring_arial14(uint8_t page, uint8_t col, const char *c);
  void drawsinvertedtring_arial14(uint8_t page, uint8_t col, const char *c);
  void drawstring_right_arial14(uint8_t page, uint8_t col, const char *c);

  uint8_t drawchar_callite24(uint8_t page, uint8_t col, char c);
  uint8_t drawchar_right_callite24(uint8_t page, uint8_t col, char c);
  void drawstring_callite24(uint8_t page, uint8_t col, const char *c);
  void drawstring_right_callite24(uint8_t page, uint8_t col, const char *c);

  void drawstring_center_arial14(uint8_t page, uint8_t x1, uint8_t x2, const char* c);
  void drawstring_center_callite24(uint8_t page, uint8_t x1, uint8_t x2, const char *c);

  void clear_page(uint8_t add);
  void drawline(uint8_t page, uint8_t y, uint8_t x0, uint8_t x1);
  void clear_single_page_box(uint8_t add, uint8_t x1, uint8_t x2);

  void setpixel(uint8_t x, uint8_t y, uint8_t color);
  uint8_t getpixel(uint8_t x, uint8_t y);
  void fillcircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color);
  void drawcircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color);
  void drawrect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color);
  void fillrect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color);
  void drawline(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color);

  void drawstring_P(uint8_t x, uint8_t line, const char *c);

  void drawbitmap(uint8_t x, uint8_t y, const uint8_t *bitmap, uint8_t w,
                  uint8_t h, uint8_t color);

  void drawmark(uint8_t page, uint8_t col);
  void drawcrossmark(uint8_t page, uint8_t col);

  void draw_app_qr(uint8_t page, uint8_t col);
  void draw_help_qr(uint8_t page, uint8_t col);
  void copy_current_screen();
  void print_saved_screen();

  void drwa_battery_low(uint8_t page, uint8_t x1, uint8_t x2);

private:
  gpio_num_t sid, sclk, a0, rst, cs, bck_light;

  int _col, _page;

  spi_host_device_t spi_host;

  spi_device_handle_t spi;

  void spiwrite(uint8_t c);

  void my_setpixel(uint8_t x, uint8_t y, uint8_t color);

  // uint8_t buffer[128*64/8];
};
