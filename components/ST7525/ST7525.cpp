#include "ST7525.h"
#include <stdlib.h>

#define ST7525_STARTBYTES 1

uint8_t is_reversed = 0;
// a handy reference to where the pages are on the screen
const uint8_t pagemap[] = {3, 2, 1, 0, 7, 6, 5, 4};

uint8_t app_qr[][49] = {
    0x00, 0x00, 0x00, 0xfc, 0xfc, 0xfc, 0x3c, 0xbc, 0xbc, 0xbc, 0xbc, 0xbc, 0x3c, 0xfc, 0x7c, 0xbc,
    0x7c, 0xbc, 0xbc, 0x3c, 0x7c, 0xbc, 0xbc, 0xfc, 0xfc, 0x7c, 0x3c, 0xfc, 0xbc, 0xfc, 0xfc, 0xbc,
    0x7c, 0x3c, 0x7c, 0xfc, 0x3c, 0xbc, 0xbc, 0xbc, 0xbc, 0xbc, 0x3c, 0x3c, 0xfc, 0xfc, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xa0, 0xef, 0xa8, 0xe8, 0x28, 0xef, 0xa0, 0x7f, 0xeb,
    0x96, 0xe9, 0x9e, 0x43, 0xf0, 0xcd, 0x5d, 0xce, 0xfb, 0xe3, 0x51, 0x2c, 0x57, 0x0d, 0x32, 0xc5,
    0x1f, 0xc9, 0xb1, 0x06, 0xff, 0x60, 0xef, 0xa8, 0xe8, 0xe8, 0xaf, 0x60, 0x60, 0xff, 0xff, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xdd, 0xb8, 0x15, 0x65, 0x49, 0x9e, 0xaa, 0xb5,
    0x19, 0x52, 0x30, 0xea, 0x44, 0x84, 0xcf, 0x1d, 0x07, 0x50, 0x0d, 0x5a, 0xe4, 0xe9, 0xff, 0x1d,
    0x1b, 0x98, 0xed, 0xff, 0x13, 0x31, 0x32, 0xe9, 0xdf, 0xb9, 0x6e, 0xe0, 0x62, 0x62, 0xff, 0xff,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xd9, 0x94, 0x6f, 0xbb, 0xd8, 0x03, 0xaa,
    0x98, 0xc0, 0x19, 0x31, 0x02, 0xe3, 0xcc, 0x28, 0x1a, 0xf0, 0x69, 0xff, 0x5d, 0x37, 0x50, 0x05,
    0x3a, 0x90, 0x01, 0xef, 0xbb, 0x80, 0x90, 0x03, 0x67, 0xdd, 0x36, 0xee, 0x24, 0x46, 0x46, 0xff,
    0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x0a, 0xed, 0x29, 0x29, 0x2f, 0xe8,
    0x0a, 0xfa, 0xb2, 0xca, 0x9a, 0x4e, 0xf0, 0x84, 0xb6, 0x5d, 0x33, 0x51, 0x05, 0x3a, 0xd0, 0x68,
    0xfe, 0xfb, 0x33, 0x89, 0xd9, 0xc1, 0x83, 0xbb, 0xaa, 0x3a, 0x00, 0x86, 0x7f, 0x7c, 0x82, 0x82,
    0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0x7f, 0x7f, 0x78, 0x7b, 0x7a, 0x7a, 0x7a,
    0x7b, 0x78, 0x7f, 0x7a, 0x7f, 0x7c, 0x7c, 0x78, 0x7f, 0x7f, 0x7a, 0x78, 0x79, 0x7f, 0x7d, 0x7b,
    0x79, 0x79, 0x7b, 0x79, 0x78, 0x7f, 0x7b, 0x79, 0x7a, 0x7b, 0x7d, 0x7c, 0x7e, 0x7f, 0x78, 0x7a,
    0x7a, 0x7f, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

uint8_t help_qr[][43] =
    {
        0x00, 0x00, 0x00, 0x00, 0xf8, 0xf8, 0xf8, 0x78, 0x78, 0x78, 0x78, 0x78, 0x78, 0x78, 0xf8, 0x78,
        0x78, 0xf8, 0xf8, 0x78, 0xf8, 0x78, 0x78, 0x78, 0xf8, 0x78, 0xf8, 0xf8, 0xf8, 0x78, 0x78, 0x78,
        0x78, 0x78, 0x78, 0x78, 0x78, 0xf8, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff,
        0xff, 0xff, 0x40, 0xdf, 0x51, 0xd1, 0xd1, 0xdf, 0x40, 0x7f, 0xcd, 0xb2, 0x5c, 0x67, 0x43, 0x76,
        0x10, 0x7b, 0x06, 0xe9, 0x43, 0x3b, 0xc7, 0xff, 0xc0, 0x5f, 0xd1, 0xd1, 0x51, 0xdf, 0x40, 0x40,
        0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x2b, 0x6e, 0x03,
        0x97, 0xdf, 0x45, 0x55, 0x39, 0xf5, 0x7e, 0xb9, 0x20, 0xb5, 0x52, 0x1e, 0x40, 0xae, 0xce, 0xdb,
        0x84, 0x00, 0xf6, 0x24, 0x6c, 0xdf, 0xf9, 0xfd, 0xee, 0x44, 0x44, 0xff, 0xff, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x15, 0xd5, 0x57, 0x5c, 0x52, 0xd6, 0x15, 0xf8,
        0xca, 0x1d, 0x14, 0x95, 0x8b, 0x52, 0x1e, 0x8c, 0x32, 0xfe, 0x2b, 0x34, 0x06, 0x76, 0x54, 0x72,
        0x01, 0x65, 0xee, 0xf6, 0x4c, 0x4c, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xff, 0xff, 0xff, 0xf0, 0xf7, 0xf4, 0xf4, 0xf4, 0xf7, 0xf0, 0xff, 0xf5, 0xf4, 0xf8, 0xfb, 0xf8,
        0xf9, 0xf0, 0xf4, 0xf8, 0xfc, 0xfb, 0xfd, 0xf3, 0xf8, 0xfb, 0xf3, 0xf0, 0xfa, 0xfe, 0xfd, 0xf4,
        0xf4, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00};

uint8_t mark_buffer[3][20] = {0xFC, 0xFE, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x83, 0xC3, 0xE3, 0x70, 0x38, 0x1C, 0x0C, 0xFF, 0xFF, 0x00, 0x38, 0x70, 0xE0, 0xC0, 0xE0, 0x70, 0x38, 0x1C, 0x0E, 0x07, 0x03, 0x01, 0x00, 0x00, 0x00, 0xFC, 0xFC, 0x03, 0x07, 0x0C, 0x0C, 0x0C, 0x0C, 0x0D, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x07, 0x03};
uint8_t cross_mark_buffer[][16] = {0xf7, 0xe3, 0xc1, 0x80, 0x01, 0x03, 0x07, 0x0f, 0x0f, 0x07, 0x03, 0x01, 0x80, 0xc1, 0xe3, 0xf7, 
	                                  0xef, 0xc7, 0x83, 0x01, 0x80, 0xc0, 0xe0, 0xf0, 0xf0, 0xe0, 0xc0, 0x80, 0x01, 0x83, 0xc7, 0xef};
// the memory buffer for the LCD

uint8_t low_bat[][26] = {
    0xFF, 0xFF, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0xFF,0xFF, 0xF0, 0xF0,
    0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xFF,0xFF, 0x0F, 0x0F};
uint8_t ST7525::st7525_buffer[8][192] = {0};
uint8_t ST7525::st7525_saved_buffer[8][192] = {0};

void ST7525::set_page_address(uint8_t add)
{
  _page = add;
  add = 0xb0 | add;
  st7525_command(add);
  return;
}

void ST7525::set_column_address(uint8_t add)
{
  _col = add;
  st7525_command(0x10 | (add >> 4));
  st7525_command(0x0f & add);
  return;
}

void ST7525::drawchar(uint8_t page, uint8_t col, char c)
{
  set_page_address(page);
  set_column_address(col);
  st7525_data(0x00);
  for (uint8_t i = 0; i < 5; i++)
  {
    st7525_data(font[((c - 32) * 5) + i]);
  }
  st7525_data(0x00);
}

void ST7525::drawinvertedchar(uint8_t page, uint8_t col, char c)
{
  set_page_address(page);
  set_column_address(col);
  st7525_data(0xFF);
  for (uint8_t i = 0; i < 5; i++)
  {
    st7525_data(~font[((c - 32) * 5) + i]);
  }
  st7525_data(0xFF);
}

void ST7525::drawstring(uint8_t page, uint8_t col, const char *c)
{
  int i = 0;
  while (c[0] != 0)
  {
    drawchar(page, col + (i * 7), c[0]);
    c++;
    i++;
  }
}

void ST7525::drawinvertedstring(uint8_t page, uint8_t col, const char *c)
{
  int i = 0;
  while (c[0] != 0)
  {
    drawinvertedchar(page, col + (i * 7), c[0]);
    c++;
    i++;
  }
}

void ST7525::drawcharary(uint8_t page, uint8_t col, const char *c, int len)
{
  int i = 0;
  for (i = 0; i < len; i++)
  {
    drawchar(page, col + (i * 7), c[i]);
  }
}

uint8_t ST7525::drawchar_arial14(uint8_t page, uint8_t col, char c)
{
  int char_size = Arial_14[6 + (c - 32)];
  int char_loc = 0;
  for (int l = 0; l < (c - 32); l++)
    char_loc += Arial_14[6 + l];
  char_loc *= 2;
  char_loc += 102;
  set_page_address(page);
  set_column_address(col);
  st7525_data(0x00);
  st7525_data(0x00);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(Arial_14[char_loc + i]);
  }
  st7525_data(0x00);
  st7525_data(0x00);
  set_page_address(page + 1);
  set_column_address(col);
  st7525_data(0x00);
  st7525_data(0x00);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(Arial_14[char_loc + char_size + i] >> 2);
  }
  st7525_data(0x00);
  st7525_data(0x00);
  return char_size;
}

uint8_t ST7525::drawinvertedchar_arial14(uint8_t page, uint8_t col, char c)
{
  int char_size = Arial_14[6 + (c - 32)];
  int char_loc = 0;
  for (int l = 0; l < (c - 32); l++)
    char_loc += Arial_14[6 + l];
  char_loc *= 2;
  char_loc += 102;
  set_page_address(page);
  set_column_address(col);
  st7525_data(0xFF);
  st7525_data(0xFF);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(~Arial_14[char_loc + i]);
  }
  st7525_data(0xFF);
  st7525_data(0xFF);
  set_page_address(page + 1);
  set_column_address(col);
  st7525_data(0xFF);
  st7525_data(0xFF);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(~(Arial_14[char_loc + char_size + i] >> 2));
  }
  st7525_data(0xFF);
  st7525_data(0xFF);
  return char_size;
}

uint8_t ST7525::drawchar_right_arial14(uint8_t page, uint8_t col, char c)
{
  int char_size = Arial_14[6 + (c - 32)];
  int char_loc = 0;
  for (int l = 0; l < (c - 32); l++)
    char_loc += Arial_14[6 + l];
  char_loc *= 2;
  char_loc += 102;
  set_page_address(page);
  for (uint8_t i = char_size; i > 0; i--)
  {
    set_column_address(col - (char_size - i));
    st7525_data(Arial_14[char_loc + (i - 1)]);
  }
  set_page_address(page + 1);
  for (uint8_t i = char_size; i > 0; i--)
  {
    set_column_address(col - (char_size - i));
    st7525_data(Arial_14[char_loc + char_size + (i - 1)] >> 2);
  }
  return char_size;
}

void ST7525::drawstring_arial14(uint8_t page, uint8_t col, const char *c)
{
  uint8_t next_pos = 0;
  while (c[0] != 0)
  {
    next_pos += drawchar_arial14(page, col + next_pos, c[0]);
    c++;
    next_pos = next_pos + 2;
  }
}

void ST7525::drawsinvertedtring_arial14(uint8_t page, uint8_t col, const char *c)
{
  uint8_t next_pos = 0;
  while (c[0] != 0)
  {
    next_pos += drawinvertedchar_arial14(page, col + next_pos, c[0]);
    c++;
    next_pos = next_pos + 2;
  }
}

void ST7525::drawstring_right_arial14(uint8_t page, uint8_t col, const char *c)
{
  uint8_t next_pos = col;
  for (int i = strlen(c); i > 0; i--)
  {
    next_pos -= drawchar_right_arial14(page, next_pos, c[i - 1]);
    next_pos--;
  }
}

uint8_t ST7525::drawchar_callite24(uint8_t page, uint8_t col, char c)
{
  int char_size = CalLite24[6 + (c - 32)];
  int char_loc = 0;
  for (int l = 0; l < (c - 32); l++)
    char_loc += CalLite24[6 + l];
  char_loc *= 4;
  char_loc += 102;
  set_page_address(page);
  set_column_address(col);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(CalLite24[char_loc + i]);
  }
  set_page_address(page + 1);
  set_column_address(col);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(CalLite24[char_loc + char_size + i]);
  }
  set_page_address(page + 2);
  set_column_address(col);
  for (uint8_t i = 0; i < char_size; i++)
  {
    st7525_data(CalLite24[char_loc + (char_size * 2) + i]);
  }
  return char_size;
}

uint8_t ST7525::drawchar_right_callite24(uint8_t page, uint8_t col, char c)
{
  int char_size = CalLite24[6 + (c - 32)];
  int char_loc = 0;
  for (int l = 0; l < (c - 32); l++)
    char_loc += CalLite24[6 + l];
  char_loc *= 4;
  char_loc += 102;
  set_page_address(page);
  for (uint8_t i = char_size; i > 0; i--)
  {
    set_column_address(col - (char_size - i));
    st7525_data(CalLite24[char_loc + (i - 1)]);
  }
  set_page_address(page + 1);
  for (uint8_t i = char_size; i > 0; i--)
  {
    set_column_address(col - (char_size - i));
    st7525_data(CalLite24[char_loc + char_size + (i - 1)]);
  }
  set_page_address(page + 2);
  for (uint8_t i = char_size; i > 0; i--)
  {
    set_column_address(col - (char_size - i));
    st7525_data(CalLite24[char_loc + (char_size * 2) + (i - 1)]);
  }
  return char_size;
}

void ST7525::drawstring_callite24(uint8_t page, uint8_t col, const char *c)
{
  uint8_t next_pos = 0;
  while (c[0] != 0)
  {
    next_pos += drawchar_callite24(page, col + next_pos, c[0]);
    c++;
    next_pos++;
  }
}

void ST7525::drawstring_right_callite24(uint8_t page, uint8_t col, const char *c)
{
  uint8_t next_pos = col;
  for (int i = strlen(c); i > 0; i--)
  {
    next_pos -= drawchar_right_callite24(page, next_pos, c[i - 1]);
    next_pos--;
  }
}

void ST7525::drawstring_center_arial14(uint8_t page, uint8_t x1, uint8_t x2, const char *c)
{
  int str_size = 0;
  int box_size = x2 - x1;
  int fit_str_index = 0;
  for (int i = 0; i < strlen(c); i++)
  {
    str_size += Arial_14[6 + (c[i] - 32)];
    str_size += 2;
    if (str_size > box_size)
    {
      str_size -= 2;
      str_size += Arial_14[6 + (c[i] - 32)];
      fit_str_index = i;
      break;
    }
    fit_str_index = i;
  }
  int pos = box_size - str_size;
  pos /= 2;
  uint8_t next_pos = 0;
  for (int j = 0; j <= fit_str_index; j++)
  {
    next_pos += drawchar_arial14(page, pos + next_pos, c[j]);
    next_pos = next_pos + 2;
  }
}

void ST7525::drawstring_center_callite24(uint8_t page, uint8_t x1, uint8_t x2, const char *c)
{
  int str_size = 0;
  int box_size = x2 - x1;
  int fit_str_index = 0;
  for (int i = 0; i < strlen(c); i++)
  {
    str_size += CalLite24[6 + (c[i] - 32)];
    str_size += 2;
    if (str_size > box_size)
    {
      str_size -= 2;
      str_size -= CalLite24[6 + (c[i] - 32)];
      fit_str_index = i;
      break;
    }
    fit_str_index = i;
  }
  int pos = box_size - str_size;
  pos /= 2;
  uint8_t next_pos = 0;
  for (int j = 0; j <= fit_str_index; j++)
  {
    next_pos += drawchar_callite24(page, pos + next_pos, c[j]);
    next_pos++;
  }
}

void ST7525::drawline(uint8_t page, uint8_t y, uint8_t x0, uint8_t x1)
{
  set_page_address(page);
  set_column_address(x0);
  for (int i = 0; i < (x1 - x0); i++)
    st7525_data(y);
}

void ST7525::drwa_battery_low(uint8_t page, uint8_t x1, uint8_t x2)
{
  for (int i = 0; i < 2; i++)
  {
    set_page_address(page + i);
    uint8_t col = ((x2 - x1) / 2) - 13;
    set_column_address(col);
    for (int j = 0; j < 26; j++)
    {
      st7525_data(low_bat[i][j]);
    }
  }
}

void ST7525::begin(uint8_t contrast)
{
  esp_err_t ret;
  spi_bus_config_t buscfg = {};
  buscfg.miso_io_num = -1;
  buscfg.mosi_io_num = sid;
  buscfg.sclk_io_num = sclk;
  buscfg.quadwp_io_num = -1;
  buscfg.quadhd_io_num = -1;
  buscfg.max_transfer_sz = 1024;

  spi_device_interface_config_t devcfg = {};
  devcfg.clock_speed_hz = 26 * 1000 * 1000; // Clock out at 26 MHz
  devcfg.mode = 0;                          // SPI mode 0
  devcfg.spics_io_num = -1;                 // CS pin
  devcfg.queue_size = 7;                    // We want to be able to queue 7 transactions at a time
  // devcfg.pre_cb = lcd_spi_pre_transfer_callback; // Specify pre-transfer
  // callback to handle D/C line

  // Initialize the SPI bus
  ret = spi_bus_initialize(spi_host, &buscfg, SPI_DMA_CH_AUTO);
  ESP_ERROR_CHECK(ret);
  // Attach the LCD to the SPI bus
  ret = spi_bus_add_device(spi_host, &devcfg, &spi);
  ESP_ERROR_CHECK(ret);
  st7525_init();
  // st7525_command(CMD_DISPLAY_ON);
  // st7525_command(CMD_SET_ALLPTS_NORMAL);
  // st7525_set_brightness(contrast);
}

void ST7525::st7525_init(void)
{

  // Initialize non-SPI GPIOs
  gpio_set_direction(a0, GPIO_MODE_OUTPUT);
  gpio_set_direction(cs, GPIO_MODE_OUTPUT);
  gpio_set_direction(rst, GPIO_MODE_OUTPUT);
  gpio_pad_select_gpio((gpio_num_t)bck_light);
  gpio_set_direction(bck_light, GPIO_MODE_OUTPUT);

  // Reset the display
  gpio_set_level(rst, 1);
  vTaskDelay(200 / portTICK_RATE_MS);
  gpio_set_level(rst, 0);
  vTaskDelay(200 / portTICK_RATE_MS);
  gpio_set_level(rst, 1);
  vTaskDelay(200 / portTICK_RATE_MS);
  gpio_set_level(cs, 0);
  vTaskDelay(200 / portTICK_RATE_MS);

  st7525_command(0xe2); // system reset

  st7525_command(0xa0); // set Frame Rate[A0: 76fps, A1b: 95fps, A2b:
                        // 132fps, A3b: 168fps(fps: frame-per-second)]

  st7525_command(0xeb); // set BR
  st7525_command(0x2f); // set Power Control

  st7525_command(0xc4); // set LCD Mapping Control
  st7525_command(0x81); // set PM
  st7525_command(0x60); // set PM 110
  st7525_command(0xaf); // display Enable
  /// Enable backlight
  gpio_set_level(bck_light, 0);
}

inline void ST7525::spiwrite(uint8_t c)
{
  gpio_set_level(cs, 0);
  gpio_set_level(a0, 1);
  esp_err_t ret;
  spi_transaction_t t;
  memset(&t, 0, sizeof(t));                   // Zero out the transaction
  t.length = 8;                               // Len is in bytes, transaction length is in bits.
  t.tx_buffer = &c;                           // Data
  t.user = (void *)1;                         // D/C needs to be set to 1
  ret = spi_device_polling_transmit(spi, &t); // Transmit!
  assert(ret == ESP_OK);                      // Should have had no issues.
  gpio_set_level(cs, 1);
}

void ST7525::st7525_command(uint8_t c)
{
  gpio_set_level(cs, 0);
  gpio_set_level(a0, 0);
  esp_err_t ret;
  spi_transaction_t t;
  memset(&t, 0, sizeof(t));                   // Zero out the transaction
  t.length = 8;                               // Command is 8 bits
  t.tx_buffer = &c;                           // The data is the cmd itself
  t.user = (void *)0;                         // D/C needs to be set to 0
  ret = spi_device_polling_transmit(spi, &t); // Transmit!
  assert(ret == ESP_OK);                      // Should have had no issues.
  gpio_set_level(cs, 1);
}

void ST7525::st7525_data(uint8_t c)
{
  gpio_set_level(cs, 0);
  gpio_set_level(a0, 1);
  esp_err_t ret;
  spi_transaction_t t;
  memset(&t, 0, sizeof(t));                   // Zero out the transaction
  t.length = 8;                               // Len is in bytes, transaction length is in bits.
  t.tx_buffer = &c;                           // Data
  t.user = (void *)1;                         // D/C needs to be set to 1
  ret = spi_device_polling_transmit(spi, &t); // Transmit!
  assert(ret == ESP_OK);                      // Should have had no issues.
  gpio_set_level(cs, 1);
  st7525_buffer[_page][_col] = c;
  _col++;
}

void ST7525::st7525_set_brightness(uint8_t val)
{
  st7525_command(CMD_SET_CONTRAST);
  st7525_data(val);
}

// clear everything
void ST7525::clear(void) {}

// this doesnt touch the buffer, just clears the display RAM - might be handy
void ST7525::clear_display(void)
{
  for (int i = 0; i < 0x08; i++)
  {
    set_page_address(i);
    set_column_address(0x00);
    for (int j = 0; j < 192; j++)
    {
      st7525_data(0x00);
    }
  }
}

void ST7525::clear_page(uint8_t add)
{
  set_page_address(add);
  set_column_address(0x00);
  for (int i = 0; i < 192; i++)
    st7525_data(0x00);
}

void ST7525::clear_single_page_box(uint8_t add, uint8_t x1, uint8_t x2)
{
  set_page_address(add);
  set_column_address(x1);
  for (int i = x1; i < x2; i++)
    st7525_data(0x00);
}

void ST7525::back_light(bool light)
{
  gpio_set_level(bck_light, light);
}

void ST7525::drawmark(uint8_t page, uint8_t col)
{
  for (int i = 0; i < 3; i++)
  {
    set_page_address(page + i);
    set_column_address(col);
    for (int j = 0; j < 20; j++)
    {
      st7525_data(mark_buffer[i][j]);
    }
  }
}

void ST7525::drawcrossmark(uint8_t page, uint8_t col)
{
  for (int i = 0; i < 2; i++)
  {
    set_page_address(page + i);
    set_column_address(col);
    for (int j = 0; j < 16; j++)
    {
      st7525_data(cross_mark_buffer[i][j]);
    }
  }
}

void ST7525::draw_app_qr(uint8_t page, uint8_t col)
{
  for (int i = 0; i < 7; i++)
  {
    set_page_address(page + i);
    set_column_address(col);
    for (int j = 0; j < 49; j++)
    {
      st7525_data(app_qr[i][j]);
    }
  }
}

void ST7525::draw_help_qr(uint8_t page, uint8_t col)
{
  for (int i = 0; i < 6; i++)
  {
    set_page_address(page + i);
    set_column_address(col);
    for (int j = 0; j < 43; j++)
    {
      st7525_data(help_qr[i][j]);
    }
  }
}

void ST7525::copy_current_screen()
{
  for (int i = 0; i < 8; i++)
  {
    for (int j = 0; j < 192; j++)
    {
      st7525_saved_buffer[i][j] = st7525_buffer[i][j];
    }
  }
}

void ST7525::print_saved_screen()
{
  for (int i = 0; i < 8; i++)
  {
    set_page_address(i);
    set_column_address(0);
    for (int j = 0; j < 192; j++)
    {
      st7525_data(st7525_saved_buffer[i][j]);
    }
  }
}