#include "ADP5589.h"

static const uint8_t gpio_dir_reg[] = {ADP5589_ADR_GPIO_DIRECTION_A,
                                       ADP5589_ADR_GPIO_DIRECTION_B,
                                       ADP5589_ADR_GPIO_DIRECTION_C};

static const uint8_t gpo_data_reg[] = {ADP5589_ADR_GPO_DATA_OUT_A,
                                       ADP5589_ADR_GPO_DATA_OUT_B,
                                       ADP5589_ADR_GPO_DATA_OUT_C};

ADP5589::ADP5589(void) {}

void ADP5589::begin() {
  i2c_config_t conf = {};
  conf.mode = I2C_MODE_MASTER;
  conf.sda_io_num = SDA_GPIO;
  conf.scl_io_num = SCL_GPIO;
  conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
  conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
  conf.master.clk_speed = 400000;

  i2c_param_config(I2C_MASTER_NUM, &conf);

  esp_err_t err = i2c_driver_install(I2C_MASTER_NUM, conf.mode, 0, 0, 0);

  // enable the internal oscillator, set to 500khz

  writeRegister(ADP5589_ADR_GENERAL_CFG_B,
                ADP5589_GENERAL_CFG_B_OSC_EN |
                    ADP5589_GENERAL_CFG_B_CORE_FREQ(3));
}

// void ADP5589::createEventMatrix(void) {
//   // read the active rows and columns
//   uint8_t activeRows = readRegister(ADP5589_ADR_PIN_CONFIG_A);
//   uint8_t activeColumns = readRegister(ADP5589_ADR_PIN_CONFIG_B);

//   // free all active events
//   for (int i = 0; i < numEvents; i++) {
//     free(eventMatrix[i]);
//   }
//   numEvents = 0;

//   for (int i = 0; i < 7; i++) {
//     // if this row is active, set the event active on all active columns
//     if ((activeRows >> i) & 1) {
//       for (int j = 0; j < 7; j++) {
//         if ((activeColumns >> j) & 1) {
//           eventMatrix[numEvents] = evt;
//           numEvents++;
//         }
//       }
//     }
//   }
// }

// void ADP5589::update(void) {
//   int newEvents = readRegister(ADP5589_ADR_STATUS) & 0x1F;

//   // update all events from the last read
//   for (int i = 0; i < numEvents; i++) {
//     event *evt = eventMatrix[i];
//     if (evt->status == ADP5589_RISING)
//       evt->status = ADP5589_HIGH;
//     else if (evt->status == ADP5589_FALLING)
//       evt->status = ADP5589_LOW;
//   }

//   // read all events in the fifo
//   for (int i = 0; i < newEvents; i++) {
//     uint8_t evt = readRegister(ADP5589_ADR_FIFO1);
//     uint8_t evtNumber = evt & 0x7F;
//     bool evtStatus = evt >> 7;

//     // update the event matrix
//     for (int j = 0; j < numEvents; j++) {
//       event *e = eventMatrix[j];
//       if (e->number == evtNumber) {
//         if (evtStatus)
//           e->status = ADP5589_RISING;
//         else
//           e->status = ADP5589_FALLING;
//       }
//     }
//   }

// // call any callbacks
// for (int i = 0; i < numCallbacks; i++) {
//   callback *cb = callbacks[i];
//   event *evt = eventMatrix[cb->evtIndex];
//   if (cb->callbackType == evt->status)
//     cb->callback(evt->number);
// }
// }

void ADP5589::activateRow(uint8_t row) {
  if (row <= 7) {
    writeRegister(ADP5589_ADR_PIN_CONFIG_A,
                  (1 << row) | readRegister(ADP5589_ADR_PIN_CONFIG_A));
    // createEventMatrix();
  }
}

void ADP5589::activateColumn(uint8_t col) {
  if (col <= 7) {
    writeRegister(ADP5589_ADR_PIN_CONFIG_B,
                  (1 << col) | readRegister(ADP5589_ADR_PIN_CONFIG_B));
    // createEventMatrix();
  }
}

void ADP5589::gpioSetDirection(uint8_t gpio, uint8_t dir) {
  if (gpio > 0 && gpio <= 19 && dir >= 0 && dir <= 1) {
    uint8_t bit = ((gpio - 1) % 8);
    uint8_t reg = gpio_dir_reg[(gpio - 1) / 8];

    uint8_t toWrite = readRegister(reg);
    toWrite ^= (-dir ^ toWrite) & (1 << bit);

    writeRegister(reg, toWrite);
  }
}

void ADP5589::writeRegister(uint8_t reg, uint8_t val) {
  // Write the specified value to the specified register
  uint8_t buf[2];
  buf[0] = reg;
  buf[1] = val & 0xFF;
  esp_err_t err = i2c_master_write_to_device(I2C_MASTER_NUM,
                                             ADP5589_DEFAULT_ADDR, buf, 2, 10);
}

void ADP5589::gpioWrite(uint8_t gpio, uint8_t val) {
  // Write the specified value to the specified GPIO pin
  if (gpio > 0 && gpio <= 19 && val >= 0 && val <= 1) {
    uint8_t bit = ((gpio - 1) % 8);
    uint8_t reg = gpo_data_reg[(gpio - 1) / 8];

    uint8_t toWrite = readRegister(reg);
    toWrite ^= (-val ^ toWrite) & (1 << bit);

    writeRegister(reg, toWrite);
  }
}

uint8_t ADP5589::readRegister(uint8_t reg) {
  uint8_t resp;
  esp_err_t err = i2c_master_write_to_device(
      I2C_MASTER_NUM, ADP5589_DEFAULT_ADDR, &reg, 1, 100 / portTICK_RATE_MS);
  err = i2c_master_read_from_device(I2C_MASTER_NUM, ADP5589_DEFAULT_ADDR, &resp,
                                    1, 100 / portTICK_RATE_MS);
  return resp;
}

uint8_t ADP5589::getEvent(uint8_t row, uint8_t col) {
  return 1 + (row * ADP5589_NUM_COLUMNS) + col;
}